var DORSLIDERCAROUSEL = {
	init:function(){
		DORSLIDERCAROUSEL.SliderCarousel();
	},
	SliderCarousel:function(){
		$('.dor-slider-carousel').owlCarousel({
	        items: 1,
	        loop: true,
	        navigation: false,
	        nav: false,
	        autoplay: false,
	        margin:0,
	        responsive: {
	            0: {items: 1},
	            1300: {items: 1},
	            1200: {items: 1},
	            880: {items: 1},
	            650: {items: 1},
	            420: {items: 1},
	            300: {items: 1}
	        },
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	    });
	    jQuery(".owl-dot").each(function(index){
	    	jQuery(this).find("span").text(index + 1);
	    });
	}
};
jQuery(document).ready(function(){
	DORSLIDERCAROUSEL.init();
});