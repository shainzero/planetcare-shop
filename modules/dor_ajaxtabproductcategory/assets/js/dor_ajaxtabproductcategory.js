var PROCATEAJAXTAB = {
	init:function(){
		PROCATEAJAXTAB.ActTab();
		PROCATEAJAXTAB.LoadMoreData();
		PROCATEAJAXTAB.LoadDefault();
	},
	ReloadProcess:function(){
		var html = "";
		html += '<div class="graph__preloader">';
	        html += '<div class="js-preloader preloader">';
	          html += '<b class="preloader__bar -bar1"></b>';
	          html += '<b class="preloader__bar -bar2"></b>';
	          html += '<b class="preloader__bar -bar3"></b>';
	          html += '<b class="preloader__bar -bar4"></b>';
	          html += '<b class="preloader__bar -bar5"></b>';
	        html += '</div>';
	    html += '</div>';
	    return html;
	},
	LoadDefault:function(){
		var checkTab = jQuery("#dorTabAjax").attr("data-defaulttab");
		var processHtml = PROCATEAJAXTAB.ReloadProcess();
		if(typeof checkTab == "undefined") return false;
		type = 1;
		if(isNaN(parseInt(checkTab))){
			type = 0;
		}
		var limit = parseInt(jQuery("#cate-tab-data-"+checkTab+" .load-more-tab").attr("data-limit"));
		jQuery("#dorTabProductCategoryContent.tab-content").css("min-height",765);
		jQuery("#dorTabProductCategoryContent .loaddingAjaxTab").remove();
		jQuery("#dorTabProductCategoryContent .graph__preloader").remove();
		//jQuery("#dorTabProductCategoryContent").append('<span class="loaddingAjaxTab">Loadding...</span>');
		jQuery("#dorTabProductCategoryContent").append(processHtml);
		var urlAjax = jQuery("#dorTabAjax").attr("data-ajaxurl");
		var params = {}
		params.cateID = checkTab;
		params.type = type;
		jQuery.ajax({
            url: urlAjax,
            data:params,
            type:"POST",
            success:function(data){
            	setTimeout(function(){
            		var results = JSON.parse(data);
	            	//jQuery(".tab_container.dorlistproducts .sliderLoadingTab").remove();
	            	jQuery("#dorTabProductCategoryContent .graph__preloader").remove();
	            	jQuery("#cate-tab-data-"+checkTab+" .dor-content-items > div").html(results);
	            	var lengthVal = jQuery(results).find(".product-miniature").length;
	            	if(lengthVal < limit){
	            		jQuery("#cate-tab-data-"+checkTab+" .load-more-tab").remove();
	            	}
	            	jQuery(".loaddingAjaxTab").remove();
	            	jQuery("#dorTabProductCategoryContent.tab-content").css("min-height","auto");
	            	jQuery("#dorTabAjax li a").removeClass("active");
					if(typeof OREGON != "undefined" && typeof OREGON.ToolTipBootstrap == "function"){
						OREGON.ToolTipBootstrap();
					}
					if(typeof OREGON != "undefined" && typeof OREGON.NewProductHome == "function"){
						OREGON.NewProductHome();
					}
					if(typeof OREGON.DorQuantity != "undefined" && typeof OREGON.DorQuantity == "function"){
	            		OREGON.DorQuantity();
	            	}
					if(typeof DORCORE != "undefined" && typeof DORCORE.LazyLoad != "undefined" && typeof DORCORE.LazyLoad == "function"){
	            		DORCORE.LazyLoad();
	            	}
            	},600);
                return false;
            }
        });
	},
	ActTab:function(){
		jQuery("#dorTabAjax > li a").unbind("click");
		jQuery("#dorTabAjax > li a").click(function(){
			var _this = this;
			var processHtml = PROCATEAJAXTAB.ReloadProcess();
			var idTxt = jQuery(this).attr("href");
			var idObjTxt = idTxt;
			idTxt = idTxt.replace("#cate-tab-data-","");
			type = 1;
			var idObj = parseInt(idTxt);
			if(isNaN(idObj)){
				idObj = idTxt;
				type = 0;
			}
			var limit = parseInt(jQuery("#cate-tab-data-"+idObj+" .load-more-tab").attr("data-limit"));
			var checkExist = jQuery("#cate-tab-data-"+idObj+" .dor-content-items > div").html();
			if(jQuery.trim(checkExist).length == 0 || checkExist == undefined){
				jQuery("#dorTabProductCategoryContent.tab-content").css("min-height",765);
				jQuery("#dorTabProductCategoryContent .loaddingAjaxTab").remove();
				jQuery("#dorTabProductCategoryContent .graph__preloader").remove();
				//jQuery("#dorTabProductCategoryContent").append('<span class="loaddingAjaxTab">Loadding...</span>');
				jQuery("#dorTabProductCategoryContent").append(processHtml);
				var urlAjax = jQuery(this).closest("ul").attr("data-ajaxurl");
				var params = {}
				params.cateID = idObj;
				params.type = type;
				jQuery.ajax({
		            url: urlAjax,
		            data:params,
		            type:"POST",
		            success:function(data){
		            	setTimeout(function(){
		            		var results = JSON.parse(data);
			            	//jQuery(".tab_container.dorlistproducts .sliderLoadingTab").remove();
			            	jQuery("#dorTabProductCategoryContent .graph__preloader").remove();
			            	jQuery("#cate-tab-data-"+idObj+" .dor-content-items > div").html(results);
			            	var lengthVal = jQuery(results).find(".product-miniature").length;
			            	if(lengthVal < limit){
			            		jQuery("#cate-tab-data-"+idObj+" .load-more-tab").remove();
			            	}


			            	jQuery(".loaddingAjaxTab").remove();
			            	jQuery("#dorTabProductCategoryContent.tab-content").css("min-height","auto");
			            	jQuery("#dorTabAjax li a").removeClass("active");
							jQuery(_this).addClass("active");
							jQuery(_this).closest("li").addClass("in active");
							if(typeof OREGON != "undefined" && typeof OREGON.ToolTipBootstrap == "function"){
								OREGON.ToolTipBootstrap();
							}
							if(typeof OREGON != "undefined" && typeof OREGON.NewProductHome == "function"){
								OREGON.NewProductHome();
							}
							if(typeof OREGON.DorQuantity != "undefined" && typeof OREGON.DorQuantity == "function"){
			            		OREGON.DorQuantity();
			            	}
			            	if(typeof DORCORE != "undefined" && typeof DORCORE.LazyLoad != "undefined" && typeof DORCORE.LazyLoad == "function"){
			            		DORCORE.LazyLoad();
			            	}
		            	},600);
		                return false;
		            }
		        });
			}else{
				jQuery("#dorTabAjax li a").removeClass("active");
				jQuery("#dorTabAjax li").removeClass("active");
				jQuery(_this).addClass("active");
				jQuery(_this).closest("li").addClass("active");
				jQuery("#dorTabProductCategoryContent .tab-pane").removeClass('active');
				jQuery(idObjTxt+".tab-pane").addClass('in active');
			}
		});
	},
	LoadMoreData:function(){
		jQuery(".load-more-tab").click(function(){
			var _this=this;
			var limit = parseInt(jQuery(_this).attr("data-limit"))
			var urlAjax = jQuery(_this).attr("data-ajax");
			var page = jQuery(_this).attr("data-page");
			var idTxt = jQuery(_this).closest(".tab-pane").attr("id");
			jQuery("#"+idTxt).append("<span class='load-more-small'></span>");
			idTxt = idTxt.replace("cate-tab-data-","");
			type = 1;
			var idObj = parseInt(idTxt);
			if(isNaN(idObj)){
				idObj = idTxt;
				type = 0;
			}
			var params = {}
			params.cateID = idObj;
			params.type = type;
			params.page = page;
			setTimeout(function(){
				jQuery.ajax({
		            url: urlAjax,
		            data:params,
		            type:"POST",
		            success:function(data){
		            	var results = JSON.parse(data);
		            	jQuery("#cate-tab-data-"+idObj+" .product_list").append(results);
		            	var lengthVal = jQuery(results).find(".product-miniature").length;
		            	if(lengthVal >= limit){
		            		jQuery(_this).attr("data-page",parseInt(page)+1);
		            	}else{
		            		jQuery(_this).remove();
		            	}
		            	jQuery(".load-more-small").remove();
		            	if(typeof OREGON.DorQuantity != "undefined" && typeof OREGON.DorQuantity == "function"){
		            		OREGON.DorQuantity();
		            	}
		            	if(typeof DORCORE != "undefined" && typeof DORCORE.LazyLoad != "undefined" && typeof DORCORE.LazyLoad == "function"){
		            		DORCORE.LazyLoad();
		            	}
		                return false;
		            }
		        });
	        },1000);
		});
	}
};

jQuery(document).ready(function(){
	PROCATEAJAXTAB.init();
});