<div id="dorFilterPriceRange">
	<div class="dorFilterInner">
		<div class="dor-filter-price">
			<h3 class="title_block">{l s='Filter by price' d='dor_filter'}</h3>
			
			<div id="slider-range"></div>
			<div class="amount-price-show">
				<span class="txt-price">{l s='Price' d='dor_filter'}:</span>
				<span class="pull-right" id="amount"></span>
			</div>
			<div class="value-range">
				<input id="amount1" type="hidden" /> 
				<input id="amount2" type="hidden" /> 
				<input name="submit_range" value="{l s='Filter' d='dor_filter'}" type="submit" />
			</div>
		</div>
	</div>
</div>