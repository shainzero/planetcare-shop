<?php
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
class dorpagesDorcategoryModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
	}
	
	public function initContent()
	{
		parent::initContent();
		$this->context->smarty->assign(array());
		$this->context->smarty->assign(array('page_name'=>"dor-page-category"));
		$categories = $this->getTemplateVarSubCategories(3,1);
		$this->context->smarty->assign( array(
                                            'page_name'=>"dor-page-category",
                                            'categories'=>$categories
                                            ));
		$this->setTemplate('about/dorcategory.tpl'); 
	}
	public function getTemplateVarSubCategories()
	    {
		return array_map(function (array $category) {
		    $object = new Category(
			$category['id_category'],
			1
		    );

		    $category['image'] = $this->getImage(
			$object,
			$object->id_image
		    );

		    $category['url'] = $this->context->link->getCategoryLink(
			$category['id_category'],
			$category['link_rewrite']
		    );

		    return $category;
		}, $this->getSubCategories(3,1));
	    }
	public function getSubCategories($parentID, $idLang, $active = true)
	    {
		$sqlGroupsWhere = '';
		$sqlGroupsJoin = '';
		if (Group::isFeatureActive()) {
		    $sqlGroupsJoin = 'LEFT JOIN `'._DB_PREFIX_.'category_group` cg ON (cg.`id_category` = c.`id_category`)';
		    $groups = FrontController::getCurrentCustomerGroups();
		    $sqlGroupsWhere = 'AND cg.`id_group` '.(count($groups) ? 'IN ('.implode(',', $groups).')' : '='.(int) Group::getCurrent()->id);
		}

		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT c.*, cl.`id_lang`, cl.`name`, cl.`description`, cl.`link_rewrite`, cl.`meta_title`, cl.`meta_keywords`, cl.`meta_description`
			FROM `'._DB_PREFIX_.'category` c
			'.Shop::addSqlAssociation('category', 'c').'
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category` AND `id_lang` = '.(int) $idLang.' '.Shop::addSqlRestrictionOnLang('cl').')
			'.$sqlGroupsJoin.'
			WHERE `id_parent` = '.(int) $parentID.'
			'.($active ? 'AND `active` = 1' : '').'
			'.$sqlGroupsWhere.'
			GROUP BY c.`id_category`
			ORDER BY `level_depth` ASC, category_shop.`position` ASC');

		foreach ($result as &$row) {
		    $row['id_image'] = Tools::file_exists_cache(_PS_CAT_IMG_DIR_.$row['id_category'].'.jpg') ? (int) $row['id_category'] : Language::getIsoById($idLang).'-default';
		    $row['legend'] = 'no picture';
		}

		return $result;
	    }
	protected function getImage($object, $id_image)
	    {
		$retriever = new ImageRetriever(
		    $this->context->link
		);

		return $retriever->getImage($object, $id_image);
	    }

	public function setMedia()
    {
        parent::setMedia();
        $this->addjQuery();
    }
}
