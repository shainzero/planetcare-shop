<?php
class dorpagesDorpage5ModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
	}
	
	public function initContent()
	{
		parent::initContent();
		$this->context->smarty->assign(array());
		$this->context->smarty->assign(array('page_name'=>"dor-page5"));
		$this->setTemplate('dorpages/dorpage5.tpl');
	}
	public function setMedia()
    {
        parent::setMedia();
        $this->addjQuery();
    }
}
