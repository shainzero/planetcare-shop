<div class="dor-wishlist">
	<a class="addToDorWishlist" href="#" onclick="WishlistCart('wishlist_block_list', 'add', jQuery(this).closest('.js-product-miniature').attr('data-id-product'), jQuery(this).closest('.js-product-miniature').attr('data-id-product-attribute'), 1, 0); return false;" data-toggle="tooltip" data-placement="right" data-original-title="{l s='Add to Wishlist' mod='dorblockwishlist'}">
		<i class="pe-7s-like"></i>
		<span class="wishlist-txt">{l s="Add to Wishlist" mod='dorblockwishlist'}</span>
	</a>
</div>