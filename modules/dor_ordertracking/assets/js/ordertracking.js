var ORDERTRACK = {
	init:function(){
		ORDERTRACK.SubmitTracking();
	},
	SubmitTracking:function(){
		$("#ordertrack").click(function(){
			$("#track_order_form").submit();
		});
	}
}

jQuery(document).ready(function(){
	ORDERTRACK.init();
});