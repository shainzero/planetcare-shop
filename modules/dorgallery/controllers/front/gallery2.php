<?php

if (!class_exists( 'DorImageBase' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/DorImageBase.php');
}
class dorgalleryGallery2ModuleFrontController extends ModuleFrontController
{
    public $phpself = 'gallery';
    public $ssl = true;
    public $dorgalleryCategory;
    public function init(){
            parent::init();
    }
    public function initContent(){
        parent::initContent();
        $quanlity_image = 90;
        $thumbWidth1 = 270;$thumbHeight1 = 310;
        $thumbWidth2 = 570;$thumbHeight2 = 310;
        $thumbWidth3 = 570;$thumbHeight3 = 650;
        $this->context->smarty->assign(array('page_name'=>"dor-gallery"));
        $params = $_GET;
        $limit = Configuration::get('drogallery_perpage');
        $limit = 11;
        $start = 0;
        $page = isset($params['page'])?$params['page']:1;
        $current = $page;
        $page = $page - 1;
        $start = $page*$limit;
        $cateId = isset($params['cateid'])?$params['cateid']:0;
        $type = isset($params['type'])?$params['type']:"";
        $template_name  = 'gallery/gallery2.tpl';
        $listCategory = array();
        if($type == ""){
            $listCategory = $this->getListCategory();
        }
        $listgallerys = $this->getItemGallery($cateId,$start,$limit);
        $totalGalleryPerPage = count($listgallerys);
        $galleries1 = array();
        $galleries2 = array();
        $galleries3 = array();
        if($totalGalleryPerPage > 0){
            foreach ($listgallerys as $key => $item) {
                $pathImg = "dorgallery/images/".$item['id_dorgallery'].".jpg";
                $linkRewrite = $item['id_dorgallery']."_".$item['link_rewrite'];
                if($key == 3 || $key == 7){
                    $thumbWidth = $thumbWidth3;
                    $thumbHeight = $thumbHeight3;
                }elseif($key == 2 || $key == 5 || $key == 8){
                    $thumbWidth = $thumbWidth2;
                    $thumbHeight = $thumbHeight2;
                }else{
                    $thumbWidth = $thumbWidth1;
                    $thumbHeight = $thumbHeight1;
                }
                $images = DorImageBase::renderThumbDorGallery($pathImg,$linkRewrite,$thumbWidth,$thumbHeight,$quanlity_image);
                $imageSource = _PS_MODULE_DIR_.$pathImg;
                list($width, $height) = getimagesize($imageSource); 
                $arr = array('h' => $height, 'w' => $width );
                $item['imagesize'] = $width.'x'.$height;
                $item['image'] = __PS_BASE_URI__.'modules/'.$pathImg;
                $item['thumb_image'] = $images;
                if($key <= 3){
                    $galleries1[] = $item;
                }
                elseif($key <= 6 && $key >= 4){
                    $galleries2[] = $item;
                }
                else{
                    $galleries3[] = $item;
                }
                
            }
        }
        $pathRootView = _PS_MODULE_DIR_.'dorgallery/views/templates';
        $pathRootThemeView = _PS_THEME_DIR_.'templates/gallery';
        $this->context->smarty->assign(
            array(
                'page_name'=>"dor-gallery",
                'tabdefaultID'=>0,
                'current'=>$current,
                'limit'=>$limit,
                'totalGalleryPerPage'=>$totalGalleryPerPage,
                'pathRootView'=>$pathRootView,
                'categories'=>$listCategory,
                'galleries1'=>$galleries1,
                'galleries2'=>$galleries2,
                'galleries3'=>$galleries3
            )
        );
        if($type != "" && $type == "ajax"){
            $varsData = "";
            $varsData = require_once $pathRootThemeView.'/_item/v2/listsajax.tpl.php';
            die;
        }else{
            $this->setTemplate($template_name); 
        }
        
               
    }
    public function getListCategory($id_lang=null,$id_shop=null){
        if($id_lang == null){
            $id_lang = (int)Context::getContext()->language->id;
        }
        if($id_shop == null){
            $id_shop = (int)Context::getContext()->shop->id;
        }
        $sql = '
                SELECT * FROM `'._DB_PREFIX_.'dorgallery_category` dgc 
                    INNER JOIN `'._DB_PREFIX_.'dorgallery_category_lang` dgcl ON(dgcl.`id_dorgallery_category` = dgc.`id_dorgallery_category` AND dgcl.`id_lang` = '.(int)($id_lang).')
                    INNER JOIN `'._DB_PREFIX_.'dorgallery_category_shop` dgcs ON dgcs.id_dorgallery_category = dgc.id_dorgallery_category and dgcs.id_shop = '.(int) $id_shop.' WHERE dgc.`active`= 1';
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        
        return $result;
    }
    public function getItemGallery($cateID = 0,$limit_start,$limit,$id_lang=null,$id_shop=null){
        if($id_lang == null){
            $id_lang = (int)Context::getContext()->language->id;
        }
        if($id_shop == null){
            $id_shop = (int)Context::getContext()->shop->id;
        }
        if($limit_start == '')
            $limit_start = 0;
        if($limit == '')
            $limit = 11;

        $where = " WHERE dg.active = 1";
        $sql = 'SELECT dg.*, dgl.*, dgcl.`name` cate_name FROM '._DB_PREFIX_.'dorgallery dg';
        if($cateID != 0){
            $where .= ' AND dg.id_dorgallery_category = '.(int) $cateID;
        }
        $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_lang dgl ON dg.id_dorgallery = dgl.id_dorgallery AND dgl.id_lang = '.$id_lang;
        $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_shop dgs ON dgs.id_dorgallery = dg.id_dorgallery AND dgs.id_shop = '.(int)$id_shop;
        $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_category dgc ON dg.id_dorgallery_category = dgc.id_dorgallery_category';
        $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_category_lang dgcl ON dgc.id_dorgallery_category = dgcl.id_dorgallery_category AND dgcl.id_lang = '.$id_lang;
        $sql .= $where;
        $sql .= ' ORDER BY dg.id_dorgallery DESC LIMIT '.$limit_start.','.$limit;
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        return $result;
    }
 }