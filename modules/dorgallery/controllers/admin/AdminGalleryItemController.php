<?php
class AdminGalleryItemController extends AdminController {

    public $asso_type = 'shop';

    public function __construct() {
        $this->table = 'dorgallery';
        $this->className = 'DorGalleryItem';
        $this->lang = true;
        $this->image_dir = '../modules/dorgallery/images';
        $this->context = Context::getContext();
        $this->_defaultOrderBy = 'created';
        $this->_defaultorderWay = 'DESC';
        $this->bootstrap = true;
            if (Shop::isFeatureActive())
                 Shop::addTableAssociation($this->table, array('type' => 'shop'));
		parent::__construct();
        $this->fields_list = array(
                            'id_dorgallery' => array(
                                    'title' => Context::getContext()->getTranslator()->trans('Id', array(), 'Modules.Dorgallery'),
                                    'width' => 50,
                                    'type' => 'text',
                                    'orderby' => true,
                                    'filter' => true,
                                    'search' => true
                            ),
                'viewed' => array(
                                    'title' => Context::getContext()->getTranslator()->trans('View', array(), 'Modules.Dorgallery'),
                                    'width' => 50,
                                    'type' => 'text',
                                    'lang' => true,
                                    'orderby' => true,
                                    'filter' => false,
                                    'search' => false
                            ),
                             'image' => array(
                                    'title' => Context::getContext()->getTranslator()->trans('Image', array(), 'Modules.Dorgallery'),
                                    'image' => $this->image_dir,
                                    'orderby' => false,
                                    'search' => false,
                                    'width' => 200,
                                    'align' => 'center',
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                               ),
                            'name' => array(
                                    'title' => Context::getContext()->getTranslator()->trans('Title', array(), 'Modules.Dorgallery'),
                                    'width' => 440,
                                    'type' => 'text',
                                    'lang' => true,
                                    'orderby' => true,
                                    'filter' => true,
                                    'search' => true
                            ),
                            'created' => array(
                                    'title' => Context::getContext()->getTranslator()->trans('Posted Date', array(), 'Modules.Dorgallery'),
                                    'width' => 100,
                                    'type' => 'date',
                                    'lang' => true,
                                    'orderby' => true,
                                    'filter' => true,
                                    'search' => true
                            ),
                            'active' => array(
                                'title' => Context::getContext()->getTranslator()->trans('Status', array(), 'Modules.Dorgallery'),
                                'width' => '70',
                                'align' => 'center',
                                'active' => 'status',
                                'type' => 'bool',
                                'orderby' => true,
                                'filter' => true,
                                'search' => true
                            )
                    );
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'dorgallery_shop sbs ON a.id_dorgallery=sbs.id_dorgallery && sbs.id_shop IN('.implode(',',Shop::getContextListShopID()).')';
        $this->_select = 'sbs.id_shop';
        $this->_defaultOrderBy = 'a.id_dorgallery';
        $this->_defaultOrderWay = 'DESC';
        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
        {
           $this->_group = 'GROUP BY a.dorgallery';
        }
        parent::__construct();
    }
    public function renderList() {
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        return parent::renderList();
    }
    public function postProcess()
    {
        $DorGalleryItem = new DorGalleryItem();
        $DorGalleryCategory = new DorGalleryCategory();
        
        if (Tools::isSubmit('deletedorgallery') && Tools::getValue('id_dorgallery') != '')
        {
            $DorGalleryItem = new DorGalleryItem((int) Tools::getValue('id_dorgallery'));
            
            if (!$DorGalleryItem->delete()){
                $this->errors[] = Tools::displayError('An error occurred while deleting the object.')
                        . ' <b>' . $this->table . ' (' . Db::getInstance()->getMsgError() . ')</b>';
            }else{
                Hook::exec('actionsbdeletepost', array('DorGalleryItem' => $DorGalleryItem));
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminGalleryItem'));
            }
        }elseif (Tools::isSubmit('submitAdddorgallery'))
        {
            parent::validateRules();
            if (count($this->errors))
                return false;
             if (!$id_dorgallery = (int) Tools::getValue('id_dorgallery')) {
                $DorGalleryItem = new DorGalleryItem();
                $id_lang_default = Configuration::get('PS_LANG_DEFAULT');
                        $languages = Language::getLanguages(false);
			foreach ($languages as $language){
				$title = str_replace('"','',htmlspecialchars_decode(html_entity_decode(Tools::getValue('name_'.$language['id_lang']))));
				$DorGalleryItem->name[$language['id_lang']] = $title;
				$DorGalleryItem->meta_keyword[$language['id_lang']] = (string)Tools::getValue('meta_keyword_'.$language['id_lang']);
				$DorGalleryItem->meta_description[$language['id_lang']] = Tools::getValue('meta_description_'.$language['id_lang']);
				$DorGalleryItem->short_description[$language['id_lang']] = (string)Tools::getValue('short_description_'.$language['id_lang']);
				$DorGalleryItem->content[$language['id_lang']] = Tools::getValue('content_'.$language['id_lang']);
                                if(Tools::getValue('link_rewrite_'.$language['id_lang'])=='' && Tools::getValue('link_rewrite_'.$language['id_lang']) == null){
                                    $DorGalleryItem->link_rewrite[$language['id_lang']] = str_replace(array(' ',':', '\\', '/', '#', '!','*','.','?'),'-',Tools::getValue('name_'.$id_lang_default));
                                }else{
                                    $DorGalleryItem->link_rewrite[$language['id_lang']] = str_replace(array(' ',':', '\\', '/', '#', '!','*','.','?'),'-',Tools::getValue('link_rewrite_'.$language['id_lang']));
                                }
            }
            $DorGalleryItem->id_parent = Tools::getValue('id_parent');   
            $DorGalleryItem->position = 0;
            $DorGalleryItem->active = Tools::getValue('active');
            
            $DorGalleryItem->id_dorgallery_category = Tools::getValue('id_dorgallery_category');
            $DorGalleryItem->comment_status = Tools::getValue('comment_status');
            $DorGalleryItem->id_author = $this->context->employee->id;
            $DorGalleryItem->created = Date('y-m-d H:i:s');
            $DorGalleryItem->modified = Date('y-m-d H:i:s');
            $DorGalleryItem->available = 1;
            $DorGalleryItem->viewed = 1;
   
    
            $DorGalleryItem->post_type = Tools::getValue('post_type');


                          
			if (!$DorGalleryItem->save())
				$this->errors[] = Tools::displayError('An error has occurred: Can\'t save the current object');
			else{
                Hook::exec('actionsbnewpost', array('DorGalleryItem' => $DorGalleryItem));
                            $this->processImage($_FILES,$DorGalleryItem->id);
			  Tools::redirectAdmin($this->context->link->getAdminLink('AdminGalleryItem'));
                         }
            }elseif($id_dorgallery = Tools::getValue('id_dorgallery')) {

                $DorGalleryItem = new $DorGalleryItem($id_dorgallery);
                $languages = Language::getLanguages(false);
			foreach ($languages as $language){
                $title = str_replace('"','',htmlspecialchars_decode(html_entity_decode(Tools::getValue('name_'.$language['id_lang']))));
				$DorGalleryItem->name[$language['id_lang']] = $title;
				$DorGalleryItem->meta_keyword[$language['id_lang']] = Tools::getValue('meta_keyword_'.$language['id_lang']);
				$DorGalleryItem->meta_description[$language['id_lang']] = Tools::getValue('meta_description_'.$language['id_lang']);
				$DorGalleryItem->short_description[$language['id_lang']] = Tools::getValue('short_description_'.$language['id_lang']);
				$DorGalleryItem->content[$language['id_lang']] = Tools::getValue('content_'.$language['id_lang']);
				$DorGalleryItem->link_rewrite[$language['id_lang']] = str_replace(array(' ',':', '\\', '/', '#', '!','*','.','?'),'-',Tools::getValue('link_rewrite_'.$language['id_lang']));
                        }
                        $DorGalleryItem->id_parent = Tools::getValue('id_parent');
                        $DorGalleryItem->active = Tools::getValue('active');
                        $DorGalleryItem->id_dorgallery_category = Tools::getValue('id_dorgallery_category');
                        $DorGalleryItem->id_author = $this->context->employee->id;
                        $DorGalleryItem->modified = Date('y-m-d H:i:s');
                if (!$DorGalleryItem->update())
                    $this->errors[] = Tools::displayError('An error occurred while updating an object.')
                            . ' <b>' . $this->table . ' (' . Db::getInstance()->getMsgError() . ')</b>';
                else
                  Hook::exec('actionsbupdatepost', array('DorGalleryItem' => $DorGalleryItem));
                    $this->processImage($_FILES,$DorGalleryItem->id_dorgallery);
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminGalleryItem'));
            }
        }elseif (Tools::isSubmit('statusdorgallery') && Tools::getValue($this->identifier)) {

            if ($this->tabAccess['edit'] === '1') {
                if (Validate::isLoadedObject($object = $this->loadObject())) {
                    if ($object->toggleStatus()) {
                        Hook::exec('actionsbtogglepost', array('DorGallery' => $this->object));
                        $identifier = ((int) $object->id_parent ? '&id_dorgallery=' . (int) $object->id_parent : '');
                        Tools::redirectAdmin($this->context->link->getAdminLink('AdminGalleryItem'));
                    } else
                        $this->errors[] = Tools::displayError('An error occurred while updating the status.');
                } else
                    $this->errors[] = Tools::displayError('An error occurred while updating the status for an object.')
                            . ' <b>' . $this->table . '</b> ' . Tools::displayError('(cannot load object)');
            }else
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
        }elseif(Tools::isSubmit('dorgalleryOrderby')&& Tools::isSubmit('dorgalleryOrderway'))
        {
            $this->_defaultOrderBy = Tools::getValue('dorgalleryOrderby');
            $this->_defaultOrderWay = Tools::getValue('dorgalleryOrderway');
        }
    }

    public function processImage($FILES,$id) {
            if (isset($FILES['image']) && isset($FILES['image']['tmp_name']) && !empty($FILES['image']['tmp_name'])) {
                if ($error = ImageManager::validateUpload($FILES['image'], 4000000))
                    return $this->displayError($this->l('Invalid image'));
                else {
                    $ext = substr($FILES['image']['name'], strrpos($FILES['image']['name'], '.') + 1);
                    $file_name = $id . '.' . $ext;

                    $path = _PS_MODULE_DIR_ .'dorgallery/images/' . $file_name;
                    if (!move_uploaded_file($FILES['image']['tmp_name'], $path))
                        return $this->displayError($this->l('An error occurred while attempting to upload the file.'));
                    else{

                        list($width, $height) = getimagesize($path); 
                        $arr = array('h' => $height, 'w' => $width );
                        DorGalleryItem::UpdateWithHeighImage($width,$height,$id);
                        $miniImage = '../img/tmp/'.$this->table.'_mini_'.Tools::getvalue('id_dorgallery').'_1.'.$ext;
                        unlink($miniImage);
                    }
                }
            }
    }
    
    public function renderForm() 
     {
      $img_desc = '';
        $img_desc .= Context::getContext()->getTranslator()->trans('Upload a Feature Image from your computer.<br/>N.B : Only jpg image is allowed', array(), 'Modules.Dorgallery');
        if(Tools::getvalue('id_dorgallery') != '' && Tools::getvalue('id_dorgallery') != NULL){
             $img_desc .= '<br/><img style="height:auto;width:300px;clear:both;border:1px solid black;" alt="" src="'.__PS_BASE_URI__.'modules/dorgallery/images/'.Tools::getvalue('id_dorgallery').'.jpg" /><br />';
        }
        $this->fields_form = array(
          'legend' => array(
          'title' => Context::getContext()->getTranslator()->trans('Dor Gallery Item', array(), 'Modules.Dorgallery'),
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'post_type',
                    'default_value' => 0,
                ),
                array(
                    'type' => 'file',
                    'label' => Context::getContext()->getTranslator()->trans('Gallery Image:', array(), 'Modules.Dorgallery'),
                    'name' => 'image',
                    'display_image' => false,
                    'desc' => $img_desc
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Gallery Title', array(), 'Modules.Dorgallery'),
                    'name' => 'name',
                    'class' => 'galleryCategoryName',
                    'size' => 60,
                    'required' => true,
                    'desc' => Context::getContext()->getTranslator()->trans('Enter Your Gallery Title', array(), 'Modules.Dorgallery'),
                    'lang' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => Context::getContext()->getTranslator()->trans('Description', array(), 'Modules.Dorgallery'),
                    'name' => 'content',
                    'lang' => true,
                    'rows' => 10,
                    'cols' => 62,
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'required' => true,
                    'desc' => Context::getContext()->getTranslator()->trans('Enter Your Gallery Description', array(), 'Modules.Dorgallery')
                ),
                array(
					'type' => 'select',
					'label' => Context::getContext()->getTranslator()->trans('Gallery Category', array(), 'Modules.Dorgallery'),
					'name' => 'id_dorgallery_category',
					'options' => array(
						'query' =>DorGalleryCategory::getCategory(),
						'id' => 'id_dorgallery_category',
						'name' => 'name'
					),
					'desc' => Context::getContext()->getTranslator()->trans('Select Your Parent Category', array(), 'Modules.Dorgallery')
                      ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Meta Keyword', array(), 'Modules.Dorgallery'),
                    'name' => 'meta_keyword',
                    'lang' => true,
                    'size' => 60,
                    'required' => false,
                    'desc' => Context::getContext()->getTranslator()->trans('Enter Your Gallery Meta Keyword. Separated by comma(,)', array(), 'Modules.Dorgallery')
                ),
                array(
                    'type' => 'textarea',
                    'label' => Context::getContext()->getTranslator()->trans('Meta Description', array(), 'Modules.Dorgallery'),
                    'name' => 'meta_description',
                    'rows' => 10,
                    'cols' => 62,
                    'lang' => true,
                    'required' => false,
                    'desc' => Context::getContext()->getTranslator()->trans('Enter Your Gallery Meta Description', array(), 'Modules.Dorgallery')
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Link Rewrite', array(), 'Modules.Dorgallery'),
                    'name' => 'link_rewrite',
                    'size' => 60,
                    'lang' => true,
                    'required' => false,
                    'desc' => Context::getContext()->getTranslator()->trans('Enetr Your Gallery Slug. Use In SEO Friendly URL', array(), 'Modules.Dorgallery')
                ),
                array(
                                        'type' => 'radio',
                                        'label' => Context::getContext()->getTranslator()->trans('Status', array(), 'Modules.Dorgallery'),
                                        'name' => 'active',
                                        'required' => false,
                                        'class' => 't',
                                        'is_bool' => true,
                                        'values' => array(
                                            array(
                                            'id' => 'active',
                                            'value' => 1,
                                            'label' => Context::getContext()->getTranslator()->trans('Enabled', array(), 'Modules.Dorgallery')
                                            ),
                                            array(
                                            'id' => 'active',
                                            'value' => 0,
                                            'label' => Context::getContext()->getTranslator()->trans('Disabled', array(), 'Modules.Dorgallery')
                                            )
                                            )
                                     )
            ),
            'submit' => array(
                'title' => Context::getContext()->getTranslator()->trans('Save', array(), 'Modules.Dorgallery'),
                'class' => 'button'
            )
        );
        
         if (Shop::isFeatureActive())
		{
			$this->fields_form['input'][] = array(
				'type' => 'shop',
				'label' => Context::getContext()->getTranslator()->trans('Shop association:', array(), 'Modules.Dorgallery'),
				'name' => 'checkBoxShopAsso',
			);
		}
                
        if (!($DorGalleryItem = $this->loadObject(true)))
            return;
        
        $this->fields_form['submit'] = array(
            'title' => Context::getContext()->getTranslator()->trans('Save', array(), 'Modules.Dorgallery'),
            'class' => 'button'
        );
      
      $image = ImageManager::thumbnail(_MODULE_DORGALLERY_DIR_ . $DorGalleryItem->id_dorgallery . '.jpg', $this->table . '_' . (int) $DorGalleryItem->id_dorgallery . '.' . $this->imageType, 350, $this->imageType, true);

        $this->fields_value = array(
            'image' => $image ? $image : false,
            'size' => $image ? filesize(_MODULE_DORGALLERY_DIR_ . $DorGalleryItem->id_dorgallery . '.jpg') / 1000 : false
        );
        return parent::renderForm();
    }
    
    public function initToolbar() {
        
        parent::initToolbar();
    }
}
