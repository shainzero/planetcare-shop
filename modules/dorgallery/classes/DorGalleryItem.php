<?php


class DorGalleryItem extends ObjectModel
{
      public $id_dorgallery;
      public $id_dorgallery_category;
      public $position = 0;
      public $active = 1;
      public $created;
      public $viewed;
      public $name;
      public $meta_keyword;
      public $meta_description;
      public $image;
      public $content;
      public $link_rewrite;
      public $image_width;
      public $image_height;

      public static $definition = array(
		'table' => 'dorgallery',
		'primary' => 'id_dorgallery',
                'multilang'=>true,
		'fields' => array(
                     'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                     'position' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                     'id_dorgallery_category' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                     'created' => array('type' => self::TYPE_DATE, 'validate' => 'isString'),
                     'viewed' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                     'image' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                      'name' => array('type' => self::TYPE_STRING, 'validate' => 'isString','lang'=>true,'required'=>true),
                      'meta_keyword' => array('type' => self::TYPE_STRING, 'lang'=>true, 'validate' => 'isString'),
                      'meta_description' => array('type' => self::TYPE_STRING, 'lang'=>true, 'validate' => 'isString'),
                      'content' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString','required'=>true),
                      'link_rewrite' => array('type' => self::TYPE_STRING, 'lang'=>true, 'validate' => 'isString','required'=>false),
                      'image_width' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt','required'=>false),
                      'image_height' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt','required'=>false)
                     ),
	);
      

    
    public function __construct($id = null, $id_lang = null, $id_shop = null){
            Shop::addTableAssociation('dorgallery', array('type' => 'shop'));
                    parent::__construct($id, $id_lang, $id_shop);
            }
            
    public static function getPost($id_post,$id_lang = null){
        $result = array();  
        if($id_lang == null){
                    $id_lang = (int)Context::getContext()->language->id;
                }
        $sql = 'SELECT * FROM '._DB_PREFIX_.'dorgallery p INNER JOIN 
                '._DB_PREFIX_.'dorgallery_lang pl ON p.id_dorgallery=pl.id_dorgallery INNER JOIN 
                '._DB_PREFIX_.'dorgallery_shop ps ON pl.id_dorgallery = ps.id_dorgallery 
                WHERE pl.id_lang='.$id_lang.'
                AND p.active= 1 AND p.id_dorgallery = '.$id_post;
        
        if (!$post = Db::getInstance()->executeS($sql))
			return false;
                $result['id_post'] = $post[0]['id_dorgallery'];
                $result['meta_title'] = $post[0]['meta_title'];
                $result['meta_description'] = $post[0]['meta_description'];
                $result['short_description'] = $post[0]['short_description'];
                $result['meta_keyword'] = $post[0]['meta_keyword'];
				if((Module::isEnabled('smartshortcode') == 1) && (Module::isInstalled('smartshortcode') == 1)){
				 require_once(_PS_MODULE_DIR_ . 'smartshortcode/smartshortcode.php');
				$smartshortcode = new SmartShortCode();
				$result['content'] = $smartshortcode->parse($post[0]['content']);
				}else{
				
				 $result['content'] = $post[0]['content'];
				 }
                $result['active'] = $post[0]['active'];
                $result['created'] = $post[0]['created'];
                $result['comment_status'] = $post[0]['comment_status'];
                $result['viewed'] = $post[0]['viewed'];
                $result['image_width'] = $post[0]['image_width'];
                $result['image_height'] = $post[0]['image_height'];
                $result['id_dorgallery_category'] = $post[0]['id_dorgallery_category'];
                if (file_exists(_PS_MODULE_DIR_.'dorgallery/images/' . $post[0]['id_dorgallery'] . '.jpg') )
                {
                   $image =   $post[0]['id_dorgallery'] . '.jpg';
                   $result['post_img'] = $image;
		}
                else
                {
                   $result['post_img'] =NULL;
                }
        return $result;
    }
    
    public static function getAllPost($id_lang = null,$limit_start,$limit){
        if($id_lang == null){
                    $id_lang = (int)Context::getContext()->language->id;
                }
        if($limit_start == '')
            $limit_start = 0;
        if($limit == '')
            $limit = 5;
        $result = array();
        $BlogCategory = '';
      
        $sql = 'SELECT * FROM '._DB_PREFIX_.'dorgallery p INNER JOIN 
                '._DB_PREFIX_.'dorgallery_lang pl ON p.id_dorgallery=pl.id_dorgallery INNER JOIN 
                '._DB_PREFIX_.'dorgallery_shop ps ON pl.id_dorgallery = ps.id_dorgallery AND ps.id_shop = '.(int) Context::getContext()->shop->id.'
                WHERE pl.id_lang='.$id_lang.'
                AND p.active= 1 ORDER BY p.id_dorgallery DESC LIMIT '.$limit_start.','.$limit;
        
        if (!$posts = Db::getInstance()->executeS($sql))
			return false;
                $DorGalleryCategory = new DorGalleryCategory();
        $i = 0;
            foreach($posts as $post){
                $result[$i]['id_dorgallery'] = $post['id_dorgallery'];
                $result[$i]['viewed'] = $post['viewed'];
                $result[$i]['name'] = $post['name'];
                $result[$i]['meta_description'] = $post['meta_description'];
                $result[$i]['short_description'] = $post['short_description'];
                $result[$i]['content'] = $post['content'];
                $result[$i]['meta_keyword'] = $post['meta_keyword'];
                $result[$i]['id_dorgallery_category'] = $post['id_dorgallery_category']; 
                $result[$i]['link_rewrite'] = $post['link_rewrite'];
                $result[$i]['cat_name'] = $DorGalleryCategory->getCatName($post['id_dorgallery_category']);
                $result[$i]['cat_link_rewrite'] = $DorGalleryCategory->getCatLinkRewrite($post['id_dorgallery_category']);
                 if (file_exists(_PS_MODULE_DIR_.'dorgallery/images/' . $post['id_dorgallery'] . '.jpg'))
                    {
                       $image =   $post['id_dorgallery'];
                       $result[$i]['post_img'] = $image;
                    }
                    else
                    {
                       $result[$i]['post_img'] = 'no';
                    }
                $result[$i]['created'] = $post['created'];
                $i++;
            }
        return $result;
    }
    
    public static function getToltal($id_lang = null){
        if($id_lang == null){
                    $id_lang = (int)Context::getContext()->language->id;
                }
        $sql = 'SELECT * FROM '._DB_PREFIX_.'dorgallery p INNER JOIN
                '._DB_PREFIX_.'dorgallery_lang pl ON p.id_dorgallery=pl.id_dorgallery INNER JOIN 
                '._DB_PREFIX_.'dorgallery_shop ps ON pl.id_dorgallery = ps.id_dorgallery AND ps.id_shop = '.(int) Context::getContext()->shop->id.'
                WHERE pl.id_lang='.$id_lang.'
                AND p.active= 1';
        if (!$posts = Db::getInstance()->executeS($sql))
			return false;           
        return count($posts);
    }
        
     public static function getToltalByCategory($id_lang = null,$id_dorgallery_category = null){
        if($id_lang == null){
                    $id_lang = (int)Context::getContext()->language->id;
                }
        if($id_dorgallery_category == null){
                    $id_dorgallery_category = 1;
                }
        $sql = 'SELECT * FROM '._DB_PREFIX_.'dorgallery p INNER JOIN
                '._DB_PREFIX_.'dorgallery_lang pl ON p.id_dorgallery=pl.id_dorgallery INNER JOIN 
                '._DB_PREFIX_.'dorgallery_shop ps ON pl.id_dorgallery = ps.id_dorgallery AND ps.id_shop = '.(int) Context::getContext()->shop->id.'
                WHERE pl.id_lang='.$id_lang.'
                AND p.active= 1 AND p.id_dorgallery_category = '.$id_dorgallery_category;
        if (!$posts = Db::getInstance()->executeS($sql))
      return false;
        return count($posts);
    }
        
    public function add($autodate = true, $null_values = false){
		if (!parent::add($autodate, $null_values))
			return false;
		else
		  return true;
	   }
        
    public static function postViewed($id_post){
	    
	    $sql = 'UPDATE '._DB_PREFIX_.'dorgallery as p SET p.viewed = (p.viewed+1) where p.id_dorgallery = '.$id_post;
	 	
		return Db::getInstance()->execute($sql);
	
	return true;
	}
        
    public static function getArchiveResult($month = null,$year = null,$limit_start = 0, $limit = 5){
         $BlogCategory = '';
         $result = array();
         $id_lang = (int)Context::getContext()->language->id;
         if($month != '' and $month != NULL and $year != '' and $year != NULL)
         {
             $sql = 'SELECT * FROM '._DB_PREFIX_.'smart_blog_post s INNER JOIN '._DB_PREFIX_.'smart_blog_post_lang sl ON s.id_smart_blog_post = sl.id_smart_blog_post
                 and sl.id_lang = '.$id_lang.' INNER JOIN '._DB_PREFIX_.'smart_blog_post_shop ps ON ps.id_smart_blog_post = s.id_smart_blog_post AND ps.id_shop = '.(int) Context::getContext()->shop->id. '
            where s.active = 1 and MONTH(s.created) = '.$month.' AND YEAR(s.created) = '.$year.' ORDER BY s.id_smart_blog_post DESC';
         }elseif($month == '' and $month == NULL and $year != '' and $year != NULL)
         {
              $sql = 'SELECT * FROM '._DB_PREFIX_.'smart_blog_post s INNER JOIN '._DB_PREFIX_.'smart_blog_post_lang sl ON s.id_smart_blog_post = sl.id_smart_blog_post
                 and sl.id_lang = '.$id_lang.' INNER JOIN '._DB_PREFIX_.'smart_blog_post_shop ps ON ps.id_smart_blog_post = s.id_smart_blog_post AND ps.id_shop = '.(int) Context::getContext()->shop->id. '
           where s.active = 1 AND YEAR(s.created) = '.$year.' ORDER BY s.id_smart_blog_post DESC';
              
         }elseif($month != '' and $month != NULL and $year == '' and $year == NULL)
         {
               $sql = 'SELECT * FROM '._DB_PREFIX_.'smart_blog_post s INNER JOIN '._DB_PREFIX_.'smart_blog_post_lang sl ON s.id_smart_blog_post = sl.id_smart_blog_post
                 and sl.id_lang = '.$id_lang.' INNER JOIN '._DB_PREFIX_.'smart_blog_post_shop ps ON ps.id_smart_blog_post = s.id_smart_blog_post AND ps.id_shop = '.(int) Context::getContext()->shop->id. '
           where s.active = 1 AND   MONTH(s.created) = '.$month.'  ORDER BY s.id_smart_blog_post DESC';
               
         }else{
             $sql = 'SELECT * FROM '._DB_PREFIX_.'smart_blog_post s INNER JOIN '._DB_PREFIX_.'smart_blog_post_lang sl ON s.id_smart_blog_post = sl.id_smart_blog_post
                 and sl.id_lang = '.$id_lang.' INNER JOIN '._DB_PREFIX_.'smart_blog_post_shop ps ON ps.id_smart_blog_post = s.id_smart_blog_post AND ps.id_shop = '.(int) Context::getContext()->shop->id. '
            where s.active = 1 ORDER BY s.id_smart_blog_post DESC';
         }
        if (!$posts = Db::getInstance()->executeS($sql))
			return false;
        
                $DorGalleryCategory = new DorGalleryCategory();
            $i = 0;
            foreach($posts as $post){
                $result[$i]['id_post'] = $post['id_smart_blog_post'];
                $result[$i]['viewed'] = $post['viewed'];
                $result[$i]['name'] = $post['name'];
                $result[$i]['short_description'] = $post['short_description'];
                $result[$i]['meta_description'] = $post['meta_description'];
                $result[$i]['content'] = $post['content'];
                $result[$i]['meta_keyword'] = $post['meta_keyword'];
                $result[$i]['id_dorgallery_category'] = $post['id_dorgallery_category']; 
                $result[$i]['link_rewrite'] = $post['link_rewrite'];
                $result[$i]['cat_name'] = $DorGalleryCategory->getCatName($post['id_dorgallery_category']);
                $result[$i]['cat_link_rewrite'] = $DorGalleryCategory->getCatLinkRewrite($post['id_dorgallery_category']);
                 if (file_exists(_PS_MODULE_DIR_.'dorgallery/images/' . $post['id_dorgallery'] . '.jpg') )
                {
                   $image =   $post['id_smart_blog_post'];
                   $result[$i]['post_img'] = $image;
		}
                else
                {
                   $result[$i]['post_img'] ='no';
                }
                $result[$i]['created'] = $post['created'];
                $i++;
            }
        return $result;
    }
    
    public static function getArchiveD($month,$year){

        $sql = 'SELECT DAY(p.created) as day FROM '._DB_PREFIX_.'smart_blog_post p INNER JOIN '._DB_PREFIX_.'smart_blog_post_shop ps ON p.id_smart_blog_post = ps.id_smart_blog_post AND ps.id_shop = '.(int) Context::getContext()->shop->id.' 
                 where MONTH(p.created) = '.$month.' AND YEAR(p.created) = '.$year.' GROUP BY DAY(p.created)';

        if (!$posts = Db::getInstance()->executeS($sql))
			return false;
                   
        return $posts;
        
    }
    
    public static function getArchiveM($year){
         
        $sql = 'SELECT MONTH(p.created) as month FROM '._DB_PREFIX_.'smart_blog_post p  INNER JOIN '._DB_PREFIX_.'smart_blog_post_shop ps ON p.id_smart_blog_post = ps.id_smart_blog_post AND ps.id_shop = '.(int) Context::getContext()->shop->id.' 
                 where YEAR(p.created) = '.$year.' GROUP BY MONTH(p.created)';
        
        if (!$posts = Db::getInstance()->executeS($sql))
			return false;
        return $posts;
        
    }
    
    public static function getArchive(){
         $result = array();
        $sql = 'SELECT YEAR(p.created) as year FROM '._DB_PREFIX_.'smart_blog_post p INNER JOIN '._DB_PREFIX_.'smart_blog_post_shop ps ON p.id_smart_blog_post = ps.id_smart_blog_post AND ps.id_shop = '.(int) Context::getContext()->shop->id.' 
                GROUP BY YEAR(p.created)';
        
        if (!$posts = Db::getInstance()->executeS($sql))
			return false;
                        $i = 0;
                    foreach ($posts as $value) {
                       $result[$i]['year'] = $value['year'];
                       $result[$i]['month'] = DorGallery::getArchiveM($value['year']);
                       $months = DorGallery::getArchiveM($value['year']);
                       $j = 0;
                       foreach ($months as $month) {
                               $result[$i]['month'][$j]['day'] = DorGallery::getArchiveD($month['month'],$value['year']);
                               $j++;
                        }
                       $i++;
                    }
        return $result;
    }
    
    
    public static function getBlogImage(){
         
         $id_lang = (int)Context::getContext()->language->id;
            
                $sql = 'SELECT id_smart_blog_post FROM '._DB_PREFIX_.'smart_blog_post';
                
               if (!$result = Db::getInstance()->executeS($sql))
                               return false;
               return $result;
           }
           
    public static function GetPostSlugById($id_post,$id_lang = null){
          if($id_lang == null)
              $id_lang = (int)Context::getContext()->language->id;
          
        $sql = 'SELECT * FROM '._DB_PREFIX_.'smart_blog_post p INNER JOIN 
                '._DB_PREFIX_.'smart_blog_post_lang pl ON p.id_smart_blog_post=pl.id_smart_blog_post INNER JOIN 
                '._DB_PREFIX_.'smart_blog_post_shop ps ON pl.id_smart_blog_post = ps.id_smart_blog_post 
                WHERE pl.id_lang='.$id_lang.'
                AND p.active= 1 AND p.id_smart_blog_post = '.$id_post;

        if (!$post = Db::getInstance()->executeS($sql))
			return false;
        
        return $post[0]['link_rewrite'];
    }
    
    public static function GetPostMetaByPost($id_post,$id_lang = null){
          if($id_lang == null)
              $id_lang = (int)Context::getContext()->language->id;
          
        $sql = 'SELECT * FROM '._DB_PREFIX_.'smart_blog_post p INNER JOIN 
                '._DB_PREFIX_.'smart_blog_post_lang pl ON p.id_smart_blog_post=pl.id_smart_blog_post INNER JOIN 
                '._DB_PREFIX_.'smart_blog_post_shop ps ON pl.id_smart_blog_post = ps.id_smart_blog_post 
                WHERE pl.id_lang='.$id_lang.'
                AND p.active= 1 AND p.id_smart_blog_post = '.$id_post;

        if (!$post = Db::getInstance()->executeS($sql))
			return false;
        
                            if($post[0]['meta_title'] == '' && $post[0]['meta_title'] == NULL){
                                $meta['meta_title'] = Configuration::get('drogallery_metatitle');
                            }else{
                                $meta['meta_title'] = $post[0]['meta_title'];
                            }
                            
                            if($post[0]['meta_description'] == '' && $post[0]['meta_description'] == NULL){
                               $meta['meta_description'] = Configuration::get('drogallery_metadesc');
                            }else{
                                $meta['meta_description'] = $post[0]['meta_description'];
                            }
                            
                            if($post[0]['meta_keyword'] == '' && $post[0]['meta_keyword'] == NULL){
                               $meta['meta_keywords'] = Configuration::get('drogallery_metakeyword');
                            }else{
                                $meta['meta_keywords'] = $post[0]['meta_keyword'];
                            }
        return $meta;
    }

    public static function UpdateWithHeighImage($width,$height,$id){
      return (Db::getInstance()->execute('
        UPDATE `'._DB_PREFIX_.'dorgallery` SET
        `image_width` = '.(int)$width.', `image_height` = '.(int)$height.'
        WHERE `id_dorgallery` = '.(int)($id)));
    }
}