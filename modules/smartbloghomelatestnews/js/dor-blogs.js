var DORBLOG = {
	init:function(){
        DORBLOG.BlogHome();
        
        window.addEventListener('load', function (){
            DORBLOG.AjaxLoadBlog();
        });
	},
	BlogHome:function(){
		jQuery('#dor-blog-home-style1 .row-latest-news .gst-post-list').owlCarousel({
		    items: 2,
            loop: true,
            navigation: false,
            nav: true,
            autoplay: false,
            lazyLoad: true,
            margin:30,
            responsive: {
                0: {items: 2},
                1200: {items: 2},
                992: {items: 2},
                768: {items: 2},
                600: {items: 1},
                480: {items: 1},
                320: {items: 1}
            },
            onInitialize: function (event) {
                if ($('#dor-blog-home-style1 .row-latest-news .gst-post-list').find('.item-blog-data').length <= 1) {
                   this.settings.loop = false;
                }
            },
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
		});

        jQuery('#dor-blog-home-style2 .row-latest-news .gst-post-list').owlCarousel({
            items: 2,
            loop: true,
            navigation: false,
            nav: true,
            autoplay: false,
            lazyLoad: true,
            margin:30,
            responsive: {
                0: {items: 2},
                1200: {items: 2},
                992: {items: 2},
                768: {items: 2},
                600: {items: 1},
                480: {items: 1},
                320: {items: 1}
            },
            onInitialize: function (event) {
                if ($('#dor-blog-home-style2 .row-latest-news .gst-post-list').find('.item-blog-data').length <= 1) {
                   this.settings.loop = false;
                }
            },
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
        });

        jQuery('#dor-blog-home-style3 .row-latest-news .gst-post-list').owlCarousel({
            items: 3,
            loop: true,
            navigation: false,
            nav: true,
            autoplay: false,
            lazyLoad: true,
            margin:20,
            responsive: {
                0: {items: 1},
                1200: {items: 3},
                992: {items: 3},
                768: {items: 2},
                650: {items: 2, margin:20},
                530: {items: 2, margin:20},
                300: {items: 1, margin:0}
            },
            onInitialize: function (event) {
                if ($('#dor-blog-home-style3 .row-latest-news .gst-post-list').find('.item-blog-data').length <= 1) {
                   this.settings.loop = false;
                }
            },
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
        });
        jQuery('#dor-blog-home-style4 .row-latest-news .gst-post-list').owlCarousel({
            items: 3,
            loop: true,
            navigation: false,
            nav: false,
            autoplay: false,
            lazyLoad: true,
            dots:true,
            margin:20,
            responsive: {
                0: {items: 1},
                1200: {items: 3},
                992: {items: 3},
                768: {items: 2},
                650: {items: 2, margin:20},
                530: {items: 2, margin:20},
                300: {items: 1, margin:0}
            },
            onInitialize: function (event) {
                if ($('#dor-blog-home-style4 .row-latest-news .gst-post-list').find('.item-blog-data').length <= 1) {
                   this.settings.loop = false;
                }
            },
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
        });
	},
    AjaxLoadBlog:function(){
        var urlAjax = jQuery(".dorblogHomeAjax").attr("data-url");
        if(typeof urlAjax != "undefined"){
            var params = {}
            jQuery.ajax({
                url: urlAjax,
                data:params,
                type:"POST",
                success:function(data){
                    setTimeout(function(){
                        var results = JSON.parse(data);
                        jQuery(".dorblogHomeAjax > div").remove();
                        jQuery(".dorblogHomeAjax").html(results);
                        DORBLOG.BlogHome();
                        if(typeof DORCORE != "undefined" && typeof DORCORE.LazyLoad != "undefined" && typeof DORCORE.LazyLoad == "function"){
                            DORCORE.LazyLoad();
                        }
                    },500);
                    return false;
                }
            });
        }
    }
}
jQuery(document).ready(function(){
	DORBLOG.init();
});