var DORTHEMETOOL = {
	idShop: (typeof DOR != undefined && typeof DOR.id_shop != undefined)?DOR.id_shop:1,
	PathFont:"",
	PathColor:"",
	init:function(){
		DORTHEMETOOL.ControlOption();
		DORTHEMETOOL.ChooseColorOption();
		//DORTHEMETOOL.ChooseFontOption();
		DORTHEMETOOL.ChooseBackgroundOption();
		DORTHEMETOOL.ChooseModeOption();
		DORTHEMETOOL.ChooseHeaderFloatOption();
		DORTHEMETOOL.ResetOptions();
		DORTHEMETOOL.RebuiltColor();
		//DORTHEMETOOL.RebuiltFont();
		DORTHEMETOOL.RebuiltBackground();
		DORTHEMETOOL.RebuiltMode();
		DORTHEMETOOL.RebuiltHeaderFloat();
		DORTHEMETOOL.TolAction();
		DORTHEMETOOL.TopbarAct();
		DORTHEMETOOL.AdvanceAct();
		
		var cssSource = DORTHEMETOOL.BuildStoreTool();
		if(cssSource != null){
			$("body").append("<style title='customStyle'>"+cssSource+"</style>");
		}
	},
	TolAction:function(){
		$('.color').colorPicker();
		$("#dor-option-tool .cl-tr.cl-tr-style-label i").click(function(){
			var checkOpen = $(this).closest(".tool-class-opt").hasClass("open");
			if(!checkOpen){
				$(this).closest(".tool-class-opt").addClass("open");
			}else{
				$(this).closest(".tool-class-opt").removeClass("open");
			}
		});
	},
	TopbarAct:function(){
		var topbarToolStyle = localStorage.getItem("topbar_shop"+DORTHEMETOOL.idShop);
		if(topbarToolStyle != null){

			var tolbg = localStorage.getItem("topbar_bg_shop"+DORTHEMETOOL.idShop);
			if(tolbg != null){
				$("#dorTopbarBgOutside").val(tolbg).css({
					"background-color":tolbg,
					"color":"#dddddd",
				});
			}

			var tolcl = localStorage.getItem("topbar_cl_shop"+DORTHEMETOOL.idShop);
			if(tolcl != null){
				$("#dorTopbarTxtColor").val(tolcl).css({
					"background-color":tolcl,
					"color":"#dddddd",
				});
			}

			var tollink = localStorage.getItem("topbar_link_shop"+DORTHEMETOOL.idShop);
			if(tollink != null){
				$("#dorTopbarLinkColor").val(tollink).css({
					"background-color":tollink,
					"color":"#dddddd",
				});
			}
		}

		$("#save-topbar-color").click(function(){
			var tolbg = $.trim($("#dorTopbarBgOutside").val());
			var tolcl = $.trim($("#dorTopbarTxtColor").val());
			var tollink = $.trim($("#dorTopbarLinkColor").val());
			var cssSource = DORTHEMETOOL.BuildStoreTool();
			var cssData = "";
			if(tolbg != ""){
				cssData += "#dor-topbar, .dor-topbar-wrapper, .dor-topbar-inner{background:"+tolbg+" !important}";
				localStorage.setItem("topbar_bg_shop"+DORTHEMETOOL.idShop, tolbg);
			}else{
				localStorage.removeItem("topbar_bg_shop"+DORTHEMETOOL.idShop);
			}
			if(tolcl != ""){
				cssData += "#dor-topbar01 .topbar-infomation span, #dor-topbar01 .line-selected, #dor-topbar01 .topbar-infomation-right ul li span, #dor-topbar01 .topbar-infomation i, #dor-topbar01 .line-selected i, #dor-topbar01 .topbar-infomation-right i, #dor-topbar01 .topbar-infomation li span::after, #dor-topbar01 .topbar-infomation-right ul li span::after{color:"+tolcl+" !important;}";
				cssData += "#dor-topbar01 .topbar-infomation li span::after, #dor-topbar01 .topbar-infomation-right ul li span::after{background-color:"+tolcl+" !important;}";
				localStorage.setItem("topbar_cl_shop"+DORTHEMETOOL.idShop, tolcl);
			}else{
				localStorage.removeItem("topbar_cl_shop"+DORTHEMETOOL.idShop);
			}
			if(tollink != ""){
				cssData += "#dor-topbar a, .dor-topbar-wrapper a, .dor-topbar-inner a{color:"+tollink+" !important}";
				localStorage.setItem("topbar_link_shop"+DORTHEMETOOL.idShop, tollink);
			}else{
				localStorage.removeItem("topbar_link_shop"+DORTHEMETOOL.idShop);
			}
			if(cssData != ""){
				$("style[title='customStyle']").remove();
				if($.trim(cssSource) != ""){
					var advToolStyle = localStorage.getItem("adv_style_shop"+DORTHEMETOOL.idShop);
					if(advToolStyle != null){
						cssData += advToolStyle;
					}
				}
				$("body").append("<style title='customStyle'>"+cssData+"</style>");
				localStorage.setItem("topbar_shop"+DORTHEMETOOL.idShop, cssData);
			}else{
				localStorage.removeItem("topbar_shop"+DORTHEMETOOL.idShop);
			}
		});
	},
	AdvanceAct:function(){
		var advToolStyle = localStorage.getItem("adv_style_shop"+DORTHEMETOOL.idShop);
		if(advToolStyle != null){
			var pColor = localStorage.getItem("adv_pcolor_shop"+DORTHEMETOOL.idShop);
			if(pColor != null){
				$("#dorPriceColor").val(pColor).css({
					"background-color":pColor,
					"color":"#dddddd",
				});
			}

			var ppColor = localStorage.getItem("adv_ppcolor_shop"+DORTHEMETOOL.idShop);
			if(ppColor != null){
				$("#dorPricePrimaryColor").val(ppColor).css({
					"background-color":ppColor,
					"color":"#dddddd",
				});
			}

			var poColor = localStorage.getItem("adv_pocolor_shop"+DORTHEMETOOL.idShop);
			if(poColor != null){
				$("#dorOldPriceColor").val(poColor).css({
					"background-color":poColor,
					"color":"#dddddd",
				});
			}
			var flSaleBg = localStorage.getItem("adv_flsaleBg_shop"+DORTHEMETOOL.idShop);
			if(flSaleBg != null){
				$("#dorFlagSaleBg").val(flSaleBg).css({
					"background-color":flSaleBg,
					"color":"#dddddd",
				});
			}
			var flSaleColor = localStorage.getItem("adv_flsaleColor_shop"+DORTHEMETOOL.idShop);
			if(flSaleColor != null){
				$("#dorFlagSaleColor").val(flSaleColor).css({
					"background-color":flSaleColor,
					"color":"#dddddd",
				});
			}
			var flNewBg = localStorage.getItem("adv_flnewBg_shop"+DORTHEMETOOL.idShop);
			if(flNewBg != null){
				$("#dorFlagNewBg").val(flNewBg).css({
					"background-color":flNewBg,
					"color":"#dddddd",
				});
			}
			var flNewColor = localStorage.getItem("adv_flnewColor_shop"+DORTHEMETOOL.idShop);
			if(flNewColor != null){
				$("#dorFlagNewColor").val(flNewColor).css({
					"background-color":flNewColor,
					"color":"#dddddd",
				});
			}
		}
		$("#save-dv-option").click(function(){
			var pColor = $.trim($("#dorPriceColor").val());
			var ppColor = $.trim($("#dorPricePrimaryColor").val());
			var poColor = $.trim($("#dorOldPriceColor").val());
			var flSaleBg = $.trim($("#dorFlagSaleBg").val());
			var flSaleColor = $.trim($("#dorFlagSaleColor").val());
			var flNewBg = $.trim($("#dorFlagNewBg").val());
			var flNewColor = $.trim($("#dorFlagNewColor").val());
			var cssSource = DORTHEMETOOL.BuildStoreTool();
			var cssData = "";
			if(pColor != ""){
				cssData += "#products-viewed .product-price-and-shipping span.price, .products .product-price-and-shipping span.price, .product_list .product-price-and-shipping span.price, #dor-tab-product-category2 .product-price-and-shipping > span.price{color:"+pColor+" !important}";
				localStorage.setItem("adv_pcolor_shop"+DORTHEMETOOL.idShop, pColor);
			}else{
				localStorage.removeItem("adv_pcolor_shop"+DORTHEMETOOL.idShop);
			}
			if(ppColor != ""){
				cssData += "#products-viewed .product-price-and-shipping > span:nth-child(3).price, .product_list .product-price-and-shipping > span:nth-child(3).price, .products .product-price-and-shipping > span:nth-child(3).price, #dor-tab-product-category2 .product-price-and-shipping > span:nth-child(3).price{color:"+ppColor+" !important}";
				localStorage.setItem("adv_ppcolor_shop"+DORTHEMETOOL.idShop, ppColor);
			}else{
				localStorage.removeItem("adv_ppcolor_shop"+DORTHEMETOOL.idShop);
			}
			if(poColor != ""){
				cssData += ".regular-price{color:"+poColor+" !important}";
				localStorage.setItem("adv_pocolor_shop"+DORTHEMETOOL.idShop, poColor);
			}else{
				localStorage.removeItem("adv_pocolor_shop"+DORTHEMETOOL.idShop);
			}
			if(flSaleBg != ""){
				cssData += ".quickview .product-flags > li.product-flag.on-sale::before, .sale-box.box-status::before, #content .product-flags > li.product-flag.on-sale::before{border-color: transparent "+flSaleBg+" transparent transparent}";
				cssData += ".quickview .product-flags > li.product-flag.on-sale, .sale-box.box-status, #content .product-flags > li.product-flag.on-sale{background-color:"+flSaleBg+"}";
				localStorage.setItem("adv_flsaleBg_shop"+DORTHEMETOOL.idShop, flSaleBg);
			}else{
				localStorage.removeItem("adv_flsaleBg_shop"+DORTHEMETOOL.idShop);
			}
			if(flSaleColor != ""){
				cssData += ".quickview .product-flags > li.product-flag.on-sale, .sale-box.box-status, #content .product-flags > li.product-flag.on-sale{color:"+flSaleColor+" !important}";
				localStorage.setItem("adv_flsaleColor_shop"+DORTHEMETOOL.idShop, flSaleColor);
			}else{
				localStorage.removeItem("adv_flsaleColor_shop"+DORTHEMETOOL.idShop);
			}
			if(flNewBg != ""){
				cssData += ".box-status::before, .product-tabs-content a::before, #content .product-flags > li.product-flag::before, .quickview .product-flags > li.product-flag::before{border-color: transparent "+flNewBg+" transparent transparent}";
				cssData += "#product #content-wrapper #content .product-flags > li.product-flag.new::before{border-color: transparent "+flNewBg+" transparent transparentt !important}";
				cssData += ".quickview .product-flags > li.product-flag, .box-status, .product-tabs-content a, #content .product-flags > li.product-flag{background-color: "+flNewBg+"}";
				cssData += "#product #content .product-flags > li.product-flag.new{background-color: "+flNewBg+" !important}";
				localStorage.setItem("adv_flnewBg_shop"+DORTHEMETOOL.idShop, flNewBg);
			}else{
				localStorage.removeItem("adv_flnewBg_shop"+DORTHEMETOOL.idShop);
			}
			if(flNewColor != ""){
				cssData += ".quickview .product-flags > li.product-flag, .box-status, .product-tabs-content a, #content .product-flags > li.product-flag{color:"+flNewColor+" !important}";
				localStorage.setItem("adv_flnewColor_shop"+DORTHEMETOOL.idShop, flNewColor);
			}else{
				localStorage.removeItem("adv_flnewColor_shop"+DORTHEMETOOL.idShop);
			}
			if(cssData != ""){
				$("style[title='customStyle']").remove();
				if($.trim(cssSource) != ""){
					var topbarToolStyle = localStorage.getItem("topbar_shop"+DORTHEMETOOL.idShop);
					if(topbarToolStyle != null){
						cssData += topbarToolStyle;
					}
				}
				$("body").append("<style title='customStyle'>"+cssData+"</style>");
				localStorage.setItem("adv_style_shop"+DORTHEMETOOL.idShop, cssData);
			}else{
				localStorage.removeItem("adv_style_shop"+DORTHEMETOOL.idShop);
			}
		});
	},

	BuildStoreTool:function(){
		var cssStyle = "";
		var topbarToolStyle = localStorage.getItem("topbar_shop"+DORTHEMETOOL.idShop);
		if(topbarToolStyle != null){
			cssStyle += topbarToolStyle;
		}
		var advToolStyle = localStorage.getItem("adv_style_shop"+DORTHEMETOOL.idShop);
		if(advToolStyle != null){
			cssStyle += advToolStyle;
		}
		return cssStyle;
	},

	ControlOption:function(){
		jQuery(".dor-wrap > .control").unbind("click");
		jQuery(".dor-wrap > .control").click(function(){
			var pos = jQuery(".dor-wrap").position();
			if(pos.left == 0){
				$( ".dor-wrap" ).animate({
					left: "-228"
				}, 600, function() {});
			}else{
				$( ".dor-wrap" ).animate({
					left: "0"
				}, 600, function() {});
			}
		});
	},
	ChooseFontOption:function(){
		var DOR_FONT = "";
		jQuery("#dor_font_options").change(function(){
			var font = jQuery(this).val();
			if(font == "") font = "font";
			localStorage.setItem("optionFont-Shop"+DORTHEMETOOL.idShop, font);
			var linkFont = DORTHEMETOOL.PathFont+"/"+font+".css";
			$('head link[href="'+DOR_FONT+'"]').attr('href',linkFont);
			DOR_FONT = linkFont;
		});
	},
	ChooseColorOption:function(){
		jQuery(".cl-td-layoutcolor a").click(function(){
			var color = jQuery(this).closest(".cl-td-layoutcolor").attr("id");
			localStorage.setItem("optionColor-Shop"+DORTHEMETOOL.idShop, color);
			DORTHEMETOOL.OptionFindCss(color);
			jQuery(".cl-td-layout").removeClass("selected");
			jQuery("#"+color).addClass("selected");
		});
	},
	ChooseModeOption:function(){
		jQuery(".mode_theme").change(function(){
			var mode = jQuery(this).val();
			jQuery("main").removeClass("full").removeClass("boxed");
			jQuery("main").addClass(mode);
			localStorage.setItem("optionMode-Shop"+DORTHEMETOOL.idShop, mode);
			window.location.reload();
		});
	},
	ChooseHeaderFloatOption:function(){
		jQuery(".headerfloat_theme").change(function(){
			var headerfloat = jQuery(this).val();
			localStorage.setItem("optionHeaderFloat-Shop"+DORTHEMETOOL.idShop, headerfloat);
			window.location.reload();
		});
	},
	ChooseBackgroundOption:function(){
		jQuery(".cl-pattern .cl-image").click(function(){
			for(var i=1;i<=30;i++){
				jQuery("body").removeClass("pattern"+i);
			}
			var bg = jQuery(this).attr("id");
			localStorage.setItem("optionBg-Shop"+DORTHEMETOOL.idShop, bg);
			$('body').addClass(bg);
			jQuery(".cl-image").removeClass("selected");
			jQuery("#"+bg).addClass("selected");
		});
	},
	ResetOptions:function(){
		jQuery(".cl-reset").click(function(){
			localStorage.removeItem("optionColor-Shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("optionFont-Shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("optionBg-Shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("optionMode-Shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("optionHeaderFloat-Shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("topbar_bg_shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("topbar_cl_shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("topbar_link_shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("topbar_shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("adv_pcolor_shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("adv_ppcolor_shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("adv_pocolor_shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("adv_flsaleBg_shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("adv_flsaleColor_shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("adv_flnewBg_shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("adv_flnewColor_shop"+DORTHEMETOOL.idShop);
			localStorage.removeItem("adv_style_shop"+DORTHEMETOOL.idShop);
			window.location.reload();
		});
	},
	OptionFindCss:function(color){
		$("head").children("link[rel='stylesheet']").each(function(index, ele) {
			var eleTxt = jQuery(ele).attr("href");
			var eleArr = eleTxt.split('/');
			var newLink = eleArr.slice(0, -1).join("/")+"/"+color+".css";
			 var res = eleTxt.match(/assets\/css\/dorado\/color\//g); 
			 if(res == null){
			 	res = eleTxt.match(/css\/color\//g); 
			 }
			if(res != null){
				if(color != null){
					jQuery(ele).attr("href",newLink);
				}
			}
		});
	},
	RebuiltColor:function(){
		DORTHEMETOOL.OptionFindCss(color);
		var color = localStorage.getItem("optionColor-Shop"+DORTHEMETOOL.idShop);
		if(color != null){
			DORTHEMETOOL.OptionFindCss(color);
		}
	},
	RebuiltFont:function(){
		var font = localStorage.getItem("optionFont-Shop"+DORTHEMETOOL.idShop);
		var linkUrl = DOR_FONT;
		if(typeof linkUrl != "undefined"){
			var linkUrls = linkUrl.split("/");
			var newArr = linkUrls.slice(0, -1);
			var newLink = newArr.join("/");
			DORTHEMETOOL.PathFont = newLink;
			if(font != null){
				var linkFont = newLink+"/"+font+".css";
				$('head link[href="'+DOR_FONT+'"]').attr('href',linkFont);
				jQuery("#dor_font_options option[value='"+font+"']").attr("selected","selected");
				DOR_FONT = linkFont;
			}
		}
	},
	RebuiltBackground:function(){
		var bgs = localStorage.getItem("optionBg-Shop"+DORTHEMETOOL.idShop);
		if(bgs != null){
			for(var i=1;i<=30;i++){
				jQuery("body").removeClass("pattern"+i);
			}
			jQuery("body").addClass(bgs);
			jQuery("#"+bgs).addClass("selected");
		}
	},
	RebuiltMode:function(){
		var mode = localStorage.getItem("optionMode-Shop"+DORTHEMETOOL.idShop);
		if(mode != null){
			jQuery("main").removeClass("full").removeClass("boxed");
			jQuery("main").addClass(mode);
			jQuery(".mode_theme").removeAttr("checked");
			jQuery("input[value='"+mode+"']").prop('checked', true);
		}
	},
	RebuiltHeaderFloat:function(){
		var headerfloat = localStorage.getItem("optionHeaderFloat-Shop"+DORTHEMETOOL.idShop);
		if(headerfloat != null){
			jQuery(".headerfloat_theme").removeAttr("checked");
			jQuery("input[value='"+headerfloat+"']").prop('checked', true);
		}
	}
}

$(document).ready(function(){
	DORTHEMETOOL.init();
});