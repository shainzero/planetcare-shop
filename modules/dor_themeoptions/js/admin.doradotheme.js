var ADM_DORTHEME = {
	init:function(){
		ADM_DORTHEME.AccTabTheme();
		ADM_DORTHEME.ClearColor();
		ADM_DORTHEME.ChoseThemeColor();
		ADM_DORTHEME.ChoseBackgroundImage();
		ADM_DORTHEME.EnableCategoryThumb();
		ADM_DORTHEME.OptionThemeSkin();
		ADM_DORTHEME.RenderFlip();
		ADM_DORTHEME.FlipImage();
		ADM_DORTHEME.ChooseCategoryList();

		jQuery("#tab-AdminDorMenu .categorytab").mouseover(function(){
			var pos = jQuery("#tab-AdminDorMenu").position();
			var hd = jQuery("#tab-AdminDorMenu .dor-submenu-lists-adm").height();
			var hdoc = jQuery("body").height();
			var bottom = hdoc - pos.top;
			if(hd > bottom){
				bottom = 20;
			}else{
				bottom = (bottom-70) - (hd/2);
			}
			jQuery("#tab-AdminDorMenu .dor-submenu-lists-adm").css("bottom",bottom);
		});

		jQuery("#tab-AdminSmartBlog .categorytab").mouseover(function(){
			var pos = jQuery("#tab-AdminSmartBlog").position();
			var hd = jQuery("#tab-AdminSmartBlog .dor-submenu-lists-adm").height();
			var hdoc = jQuery("body").height();
			var bottom = hdoc - pos.top;
			if(hd > bottom){
				bottom = 20;
			}else{
				bottom = (bottom-70) - (hd/2);
			}
			jQuery("#tab-AdminSmartBlog .dor-submenu-lists-adm").css("bottom",bottom);
		});

		jQuery("#dorDetailCols").change(function(){
			var choosed = $(this).val();
			if(choosed == "proDetailCol1"){
				$(".box_dor_position_image").removeClass("hidden");
			}else{
				$(".box_dor_position_image").addClass("hidden");
			}
		});

	},
	ChooseCategoryList:function(){
		var checked = $("#dorCategoryCols option:selected").val();
		if(checked != "")
			ADM_DORTHEME.ChooseCategoryListAction(checked);
		$("#dorCategoryCols").change(function(){
			var key = $(this).val();
			ADM_DORTHEME.ChooseCategoryListAction(key);
		});
	},
	ChooseCategoryListAction:function(key){
		if(key == "proCateCol1"){
			$("#proCateRowNumber option").css("display","block");
		}else{
			$("#proCateRowNumber option[value='5']").css("display","none");
			$("#proCateRowNumber option[value='6']").css("display","none");
			var checkedCurrent = $("#proCateRowNumber option:selected").val();
			if(parseInt(checkedCurrent) > 4){
				$("#proCateRowNumber option").removeAttr("selected");
				$("#proCateRowNumber option[value='3']").attr("selected","selected");
			}
		}
	},
	RenderFlip:function(){
		var checkPage = jQuery("body").attr('class');
		var checkFormId = jQuery("#form_id_product").val();
		if(checkPage == "adminproducts" && typeof checkFormId != "undefined" && parseInt(checkFormId) > 0){
			var urlAjax = baseDir+"modules/dor_themeoptions/ajax.php";
			var params = {}
				params.action = 1;
				params.productID = checkFormId;
				jQuery.ajax({
		            url: urlAjax,
		            data:params,
		            type:"POST",
		            success:function(data){
		            	data = JSON.parse(data);
		            	var imageID = data.id_image;
		            	setTimeout(function(){
		            		$(".dz-image-preview[data-id='"+imageID+"'] .dorFlip").prop("checked", true);
		            	},500);
		                return false;
		            }
		        });
		}
	},
	FlipImage:function(){
		$('body').unbind('on');
		$('.fieldFlipImage > .dorFlip').unbind('click');
		jQuery("body").on("click",".fieldFlipImage > .dorFlip",function(){
			var check = jQuery(this).is(":checked");
			jQuery(".fieldFlipImage > .dorFlip").prop("checked", false);
			var status = 0;
			if(check){
				$(this).prop("checked", true);
				status = 1;
			}
			var imageID = jQuery(this).closest(".dz-image-preview").attr("data-id");
			var productID = jQuery("#form_id_product").val();
			var urlAjax = baseDir+"modules/dor_themeoptions/ajax.php";
			var params = {}
				params.imageID = imageID;
				params.productID = productID;
				params.action = 0;
				params.status = status;
				jQuery.ajax({
		            url: urlAjax,
		            data:params,
		            type:"POST",
		            success:function(data){
		            	
		                return false;
		            }
		        });
		});
	},
	AccTabTheme:function(){
		jQuery(".square-button").click(function(){
			var _this=this;
			var objID = jQuery(this).closest("div.tool-class-admin").attr("id");
			  jQuery( "#"+objID+" .box_lab" ).slideToggle( "fast", function() {
			    if(jQuery(this).is(":visible"))
				  {
					var itext = '<i class="fa fa-minus-square"></i>';
					jQuery(_this).find('i').remove();
				  }     
				else
				  {
					var itext = '<i class="fa fa-plus-square"></i>';
					jQuery(_this).find('i').remove();
				  }
				jQuery(_this).html(itext);
			  });
		});

		
	},
	OptionThemeSkin:function(){
		var checkStatusColor = jQuery("input[name='dorEnableThemeColor'][checked='checked']").val();
		if(parseInt(checkStatusColor) == 0){
			jQuery('#themeColorOption').addClass("opt-hidden");
		}else{
			jQuery('#themeColorOption').removeClass("opt-hidden");
		}

		var checkStatusBgImage = jQuery("input[name='dorEnableBgImage'][checked='checked']").val();
		if(parseInt(checkStatusBgImage) == 0){
			jQuery('#themeBgImageOption').addClass("opt-hidden");
		}else{
			jQuery('#themeBgImageOption').removeClass("opt-hidden");
		}

		jQuery("input[name='dorEnableThemeColor']").change(function(){
			var checkStatusColor = jQuery(this).val();
			if(parseInt(checkStatusColor) == 0){
				jQuery('#themeColorOption').addClass("opt-hidden");
			}else{
				jQuery('#themeColorOption').removeClass("opt-hidden");
			}
		});
		jQuery("input[name='dorEnableBgImage']").change(function(){
			var checkStatusBgImage = jQuery(this).val();
			if(parseInt(checkStatusBgImage) == 0){
				jQuery('#themeBgImageOption').addClass("opt-hidden");
			}else{
				jQuery('#themeBgImageOption').removeClass("opt-hidden");
			}
		});
	},
	ClearColor:function(){
		jQuery(".clear-bg").click(function(){
			jQuery(this).closest('.input-group').find(".mColorPickerInput").val("").css("background-color","transparent");
		})
	},
	ChoseThemeColor:function(){
		$('.cl-td-layout').click(function(){
	        var val = $(this).attr('id');
	        if($(this).hasClass("active")) val = "";
	        $("#dorthemecolor").remove();
	        $(".cl-pattern").append('<input type=hidden id="dorthemecolor" name="dorthemecolor" value="'+val+'">');
	        if(!$(this).hasClass("active")){
            	$(".cl-td-layout").removeClass('active');
            	$(this).addClass('active');
            }else{
            	$(this).removeClass('active');
            }
	    });
	},
	ChoseBackgroundImage:function(){
		jQuery(".cl-pattern > div.cl-image").click(function(){
			var val = $(this).attr('id');
			if($(this).hasClass("active")) val = "";
			$("#bgdorthemebg").remove();
            $(".cl-pattern").append('<input type=hidden id="bgdorthemebg" name="dorthemebg" value="'+val+'">');
            if(!$(this).hasClass("active")){
            	$("div.cl-image").removeClass('active');
            	$(this).addClass('active');
            }else{
            	$(this).removeClass('active');
            }
		});
	},
	EnableCategoryThumb:function(){
		jQuery("input[name='dorCategoryThumb']").change(function(){
			var val = jQuery(this).val();
			if(parseInt(val) == 1){
				jQuery(".group-cate-thumb").removeClass("hidden");
			}else{
				jQuery(".group-cate-thumb").addClass("hidden");
			}
		});
	},
	ControlArrow:function(){
		jQuery(".arow-control").click(function(){
			jQuery(this).closest(".advance-class-admin").find(".data-dor-admin").slideToggle(100,function(){

			});
		});
	},
	MergeMenu:function(){
		jQuery("#tab-AdminDorMenu").append('<ul class="dor-submenu-lists-adm"><span class="point-start"></span></ul>');
		jQuery("#tab-AdminSmartBlog").append('<ul class="dor-submenu-lists-adm"><span class="point-start"></span></ul>');
		jQuery("#nav-sidebar > ul > li").each(function( index ) {
			var idObj = jQuery(this).attr("id");
			var checkDorTabSub2 = idObj.indexOf('subtab-Admindor');
			var checkDorTabSub = idObj.indexOf('subtab-AdminDor');
			var checkDorTabSub3 = idObj.indexOf('subtab-AdminTestimonials');
			if(checkDorTabSub2 !== -1 || checkDorTabSub3 !== -1 || checkDorTabSub !== -1){
				jQuery(this).detach().insertAfter('#tab-AdminDorMenu .dor-submenu-lists-adm .point-start');
			}

			var checkBlogTabSub2 = idObj.indexOf('subtab-AdminAboutUs');
			var checkBlogTabSub = idObj.indexOf('subtab-AdminBlog');
			var checkBlogTabSub3 = idObj.indexOf('subtab-AdminImageType');
			if(checkBlogTabSub2 !== -1 || checkBlogTabSub3 !== -1 || checkBlogTabSub !== -1){
				jQuery(this).detach().insertAfter('#tab-AdminSmartBlog .dor-submenu-lists-adm .point-start');
			}
		});
	},
	KeyUpTitle:function(objTitle,objAlias){
        jQuery(objTitle).keyup(function(){
            var value = jQuery(this).val();
            var text = ADM_DORTHEME.RewriteAlias(value);
            jQuery(objAlias).val(text);
        });
    },
	RewriteAlias: function(str) {
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
        str = str.replace(/đ/g,"d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,"-");
        /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
        str= str.replace(/-+-/g,"-"); //thay thế 2- thành 1-
        str= str.replace(/^\-+|\-+$/g,"");
        //cắt bỏ ký tự - ở đầu và cuối chuỗi
        return str;
    },
}

$(document).ready(function(){
	ADM_DORTHEME.init();
	ADM_DORTHEME.ControlArrow();
	ADM_DORTHEME.MergeMenu();
});