var DORCORE = {
	checkPage:jQuery("body").attr("id"),
	init:function(){
		DORCORE.LazyLoad();
		DORCORE.CoverMaskCustom();
		
		setTimeout(function(){
			DORCORE.CoverThumbAct();
		},400);
		
		var thumbSelected = $(".dorMaskCustom img.thumb.selected").attr("data-thumb-id");
		if(typeof thumbSelected != "undefined" && DORCORE.checkPage == "product"){
			DORCORE.ZoomImage("#"+thumbSelected+" img");
		}
	},

	
	LazyLoad:function(){
		if(typeof DOR != 'undefined' && typeof DOR.dorLazyLoad != 'undefined' && parseInt(DOR.dorLazyLoad) == 1){
			jQuery("img.dorlazy").lazyload({
			    effect : "fadeIn"
			});
		}
	},
	ZoomImage:function(objZoom){
		if(typeof DOR != 'undefined' && typeof DOR.dorZoomImage != 'undefined' && parseInt(DOR.dorZoomImage) == 1){
			$(objZoom).elevateZoom({
			    zoomType: "inner",
				cursor: "crosshair",
				zoomWindowFadeIn: 500,
				zoomWindowFadeOut: 750
			});
		}
	},
	CoverMaskCustom:function(){
		$("body").on("click",".dorMaskCustom .thumb",function(){
			var thumbId = $(this).attr("data-thumb-id");
			$(".dor-cover-image a").addClass("hidden");
			$("#"+thumbId).removeClass("hidden");
			if(DORCORE.checkPage == "product"){
				DORCORE.ZoomImage("#"+thumbId+" img");
			}
		});
	},
	CoverThumbAct:function(){
		var checkStyleLeft = $(".images-container").hasClass("dorDetailMainImage_left");
		var checkStyleRight = $(".images-container").hasClass("dorDetailMainImage_right");
		if(checkStyleLeft || checkStyleRight){
			var withSize = parseInt($( window ).width());
			DORCORE.ResizeThumbnailImage(withSize,checkStyleLeft,checkStyleRight);
			$( window ).resize(function() {
				var withSize = parseInt($( window ).width());
				DORCORE.ResizeThumbnailImage(withSize,checkStyleLeft,checkStyleRight);
			});
		}


	},
	ResizeThumbnailImage:function($size,$left,$right){
		if(parseInt($size) >= 992){
			jQuery(".js-qv-mask.mask.scroll").detach().insertAfter('.product-cover');
			var marginTopPadd = 20;
			var thumbItem = 4;
			var thumbLists = parseInt($(".product-images.js-qv-product-images > .thumb-container").length);
			var thumbPadding = 15;
			var hCover = $(".product-cover").height();

			var hThumb = $(".thumb-container img").height();
			var totalHthumb = (hThumb * thumbItem) + ((thumbItem+1)*thumbPadding);
			if(hCover <= 0){
				hCover = totalHthumb - 1;
			}
			var totalListsHthumb = (hThumb * thumbLists) + ((thumbLists+1)*thumbPadding);
			var checkThumb = totalHthumb;
			$(".js-qv-mask.mask.scroll").height(hCover - 19);

			jQuery(".dor-scroll-box-arrows > i").click(function(event){
				event.preventDefault();
				var checkPos = $(this).hasClass("bottom");
				if(checkPos){
					if(checkThumb < totalListsHthumb){
						$('.product-images.js-qv-product-images').animate({
							marginTop: '-='+(hThumb+marginTopPadd)
						}, 500);
						checkThumb = checkThumb + (hThumb+marginTopPadd);
					}
				}else{
					if(checkThumb > totalHthumb){
						$('.product-images.js-qv-product-images').animate({
							marginTop: '+='+(hThumb+marginTopPadd)
						}, 500);
						checkThumb = checkThumb - (hThumb+marginTopPadd);
					}
				}
			});
			if($left)
				$(".images-container").addClass('dorDetailMainImage_left');
			if($right)
				$(".images-container").addClass('dorDetailMainImage_right');
			jQuery(".dor-scroll-box-arrows").removeClass("hidden");
		}else{
			jQuery(".images-container .js-qv-mask.mask.scroll").detach().insertAfter('.images-container');
			var thumbHeight = parseInt($(".product-images > li.thumb-container .thumb.js-thumb.selected").height());
			jQuery(".js-qv-mask.mask").css("height","auto");
			$(".images-container").removeClass('dorDetailMainImage_left');
			$(".images-container").removeClass('dorDetailMainImage_right');
			jQuery(".dor-scroll-box-arrows").addClass("hidden");
		}
	}
};

jQuery(document).ready(function(){
	DORCORE.init();
});