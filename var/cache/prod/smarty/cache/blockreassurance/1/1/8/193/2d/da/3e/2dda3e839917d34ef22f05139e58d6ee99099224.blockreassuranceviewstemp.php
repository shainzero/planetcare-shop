<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:52:13
  from 'module:blockreassuranceviewstemp' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf287cdd2f139_70467525',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9ffc009d1b66ea89054a8e253403b7d3a67d8150' => 
    array (
      0 => 'module:blockreassuranceviewstemp',
      1 => 1541767127,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5bf287cdd2f139_70467525 (Smarty_Internal_Template $_smarty_tpl) {
?>  <div id="block-reassurance">
    <ul>
              <li>
          <div class="block-reassurance-item">
            <img src="http://localhost/planetcare/modules/blockreassurance/img/ic_verified_user_black_36dp_1x.png" alt="Política de seguridad (editar con el módulo Información de seguridad y confianza para el cliente)">
            <span class="h6">Política de seguridad (editar con el módulo Información de seguridad y confianza para el cliente)</span>
          </div>
        </li>
              <li>
          <div class="block-reassurance-item">
            <img src="http://localhost/planetcare/modules/blockreassurance/img/ic_local_shipping_black_36dp_1x.png" alt="Política de envío (editar con el módulo Información de seguridad y confianza para el cliente)">
            <span class="h6">Política de envío (editar con el módulo Información de seguridad y confianza para el cliente)</span>
          </div>
        </li>
              <li>
          <div class="block-reassurance-item">
            <img src="http://localhost/planetcare/modules/blockreassurance/img/ic_swap_horiz_black_36dp_1x.png" alt="Política de devolución (editar con el módulo Información de seguridad y confianza para el cliente)">
            <span class="h6">Política de devolución (editar con el módulo Información de seguridad y confianza para el cliente)</span>
          </div>
        </li>
          </ul>
  </div>
<?php }
}
