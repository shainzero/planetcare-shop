<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:42:43
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_megamenu\views\templates\hook\megamenu.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf285937f4b98_06103582',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a9cea7385c46eb819779c6f006ce671d799e1deb' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_megamenu\\views\\templates\\hook\\megamenu.tpl',
      1 => 1497132526,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5bf285937f4b98_06103582 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="dor-megamenu col-lg-12 col-sx-12 col-sm-12">
    <div class="navbar navbar-default " role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle open_menu">
                <i class="material-icons">&#xE8FE;</i>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="dor-top-menu" class="collapse navbar-collapse navbar-ex1-collapse">
            <div class="close_menu" style="display:none;">
                <span class="btn-close"><i class="material-icons">&#xE14C;</i></span>
            </div>
            <div class="mobile-logo-menu hidden-lg hidden-md">
                <a href="http://localhost/planetcare/" title="PlanetCare">
                    <img class="logo img-responsive" src="/planetcare/themes/dor_organick1/assets/dorado/img/logo-menu.png" alt="PlanetCare"/>
                </a>
            </div>
            <ul class="nav navbar-nav megamenu"><li class=" "><a target="_self" data-rel="2" href="/planetcare/en/"><span class="menu-title">Home</span></a></li><li class=" parent dropdown aligned-fullwidth"><a class="dropdown-toggle" data-toggle="dropdown"  target="_self" data-rel="5" href="http://localhost/planetcare/3-"><span class="menu-title">Shop</span><b class="caret"></b></a><span class="caretmobile hidden"></span><ul class="dropdown-menu level1 megamenu-content" role="menu" style="width:300px;"><li><div class="row"></div></li></ul></li><li class=" "><a target="_self" data-rel="6" href="/planetcare/en/blogs.html"><span class="menu-title">Blog</span></a></li><li class=" "><a target="_self" data-rel="7" href="/planetcare/en/contact-us"><span class="menu-title">Contact</span></a></li></ul>        </div>
    </div>  
</nav><?php }
}
