<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:38:28
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_megamenu\views\templates\hook\megamenu.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf284941f6767_13653404',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a9cea7385c46eb819779c6f006ce671d799e1deb' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_megamenu\\views\\templates\\hook\\megamenu.tpl',
      1 => 1497132526,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5bf284941f6767_13653404 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="dor-megamenu col-lg-12 col-sx-12 col-sm-12">
    <div class="navbar navbar-default " role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle open_menu">
                <i class="material-icons">&#xE8FE;</i>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="dor-top-menu" class="collapse navbar-collapse navbar-ex1-collapse">
            <div class="close_menu" style="display:none;">
                <span class="btn-close"><i class="material-icons">&#xE14C;</i></span>
            </div>
            <div class="mobile-logo-menu hidden-lg hidden-md">
                <a href="http://localhost/planetcare/" title="PlanetCare">
                    <img class="logo img-responsive" src="/planetcare/themes/dor_organick1/assets/dorado/img/logo-menu.png" alt="PlanetCare"/>
                </a>
            </div>
            <ul class="nav navbar-nav megamenu"><li class=" "><a target="_self" data-rel="2" href="/planetcare/en/"><span class="menu-title">Home</span></a></li><li class=" parent dropdown aligned-fullwidth"><a class="dropdown-toggle" data-toggle="dropdown"  target="_self" data-rel="5" href="http://localhost/planetcare/en/3-"><span class="menu-title">Shop</span><b class="caret"></b></a><span class="caretmobile hidden"></span><ul class="dropdown-menu level1 megamenu-content" role="menu" style="width:300px;"><li><div class="row"><div class="col-sm-3"><div class="widget-content"><div class="widget-links block ">
		<div class="widget-inner block_content">	
		<div id="tabs1628764867" class="panel-group">
			<ul class="nav-links" data-id="myTab">
			  			</ul>
	</div></div>
</div>


</div></div><div class="col-sm-3"><div class="widget-content"><div class="widget-links block ">
		<div class="widget-inner block_content">	
		<div id="tabs591907616" class="panel-group">
			<ul class="nav-links" data-id="myTab">
			  			</ul>
	</div></div>
</div>


</div></div><div class="col-sm-3"><div class="widget-content"></div></div><div class="col-sm-3"><div class="widget-content"><div class="widget-html block ">
		<div class="widget-inner block_content">
			</div>
</div>
</div></div></div></li></ul></li><li class=" parent dropdown aligned-left"><a class="dropdown-toggle" data-toggle="dropdown"  target="_self" data-rel="4" href="/planetcare/en/gallery"><span class="menu-title">Galleries</span><b class="caret"></b></a><span class="caretmobile hidden"></span><ul class="dropdown-menu level1" role="menu" style="width:240px;"><li><a target="_self" href="/planetcare/en/gallery?type=1"><span class="menu-title">Gallery V1</span></a></li><li><a target="_self" href="/planetcare/en/gallery?type=2"><span class="menu-title">Gallery V2</span></a></li></ul></li><li class=" parent dropdown aligned-left"><a class="dropdown-toggle" data-toggle="dropdown"  target="_self" data-rel="6" href="/planetcare/en/blogs.html"><span class="menu-title">Blog</span><b class="caret"></b></a><span class="caretmobile hidden"></span><ul class="dropdown-menu level1 megamenu-content" role="menu" style="width:240px;"><li><div class="row"><div class="col-sm-12"><div class="widget-content"><div class="widget-links block ">
		<div class="widget-inner block_content">	
		<div id="tabs220268860" class="panel-group">
			<ul class="nav-links" data-id="myTab">
			  			</ul>
	</div></div>
</div>


</div></div></div></li></ul></li><li class=" parent dropdown aligned-left"><a class="dropdown-toggle" data-toggle="dropdown"  target="_self" data-rel="7" href="/planetcare/en/contact-us"><span class="menu-title">Contact</span><b class="caret"></b></a><span class="caretmobile hidden"></span><ul class="dropdown-menu level1 megamenu-content" role="menu" style="width:240px;"><li><div class="row"><div class="col-sm-12"><div class="widget-content"><div class="widget-links block ">
		<div class="widget-inner block_content">	
		<div id="tabs1197235404" class="panel-group">
			<ul class="nav-links" data-id="myTab">
			  			</ul>
	</div></div>
</div>


</div></div></div></li></ul></li></ul>        </div>
    </div>  
</nav><?php }
}
