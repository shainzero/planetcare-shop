<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:46
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_imageslider\views\templates\hook\slider.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280aab0e604_16348234',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c6d854e323ccfee8e43e92dccf174f58009487fb' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_imageslider\\views\\templates\\hook\\slider.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5bf280aab0e604_16348234 (Smarty_Internal_Template $_smarty_tpl) {
?>
  <div id="dorSlideShow" class="homeslider-container animatedParent animateOnce" data-interval="1000" data-wrap="true" data-pause="hover" data-arrow=true data-nav=1>
    <div id="Dor_Full_Slider" style="width: 1920px; height: 870px;">
        <!-- Loading Screen -->
        <div class="slider-loading" data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div class="slider-loading-img"></div>
        </div>
        <div class="slider-content-wrapper" data-u="slides" style="width: 1920px; height: 870px;">
                               <div class="slider-content effectSlider0 slide-item-1" data-p="225.00" style="display:none;">
                  <img data-u="image" src="http://localhost/planetcare/modules/dor_imageslider/images/1db1e6cd5c776a18635aed14a81eb4ea038d6e44_slider-2.jpg" alt="" />
                                    <div class="dor-info-perslider">
                    <div class="dor-info-perslider-inner">
                      <div class="dor-info-perslider-wrapper">
                        <div class="container">
                                                                                                                                  <div class="dor-slider-desc" data-u="caption"><div class="dor-slider-desc-inner animated dor-fadeInUp"><p style="color:#ffffff;font-size:62px;font-weight:bolder;">GET INVOLVED, WE CAN</p>
<p style="color:#ffffff;font-size:62px;font-weight:bolder;">ONLY DO THIS</p>
<p style="color:#ffffff;font-size:62px;font-weight:bolder;">TOGETHER : LOVE OUR PLANET</p></div></div>
                                                                            </div>
                      </div>
                    </div>
                  </div>
              </div>
                                           <div class="slider-content effectSlider0 slide-item-2" data-p="225.00" style="display:none;">
                  <img data-u="image" src="http://localhost/planetcare/modules/dor_imageslider/images/8e0b184fa6e8af334066430e8aae716b93b25c80_slider-1.jpg" alt="" />
                                    <div class="dor-info-perslider">
                    <div class="dor-info-perslider-inner">
                      <div class="dor-info-perslider-wrapper">
                        <div class="container">
                                                                                                                                  <div class="dor-slider-desc" data-u="caption"><div class="dor-slider-desc-inner animated dor-fadeInUp"><p style="color:#ffffff;font-size:75px;font-weight:bolder;">FILTERS</p>
<p style="color:#ffffff;font-size:25px;text-transform:uppercase;">A range of filters is <strong>ready to stop</strong> our washing machines from emitting microfibres. Every washing machine can now be equiped with a microfibres filter that comes with ana efficiency guarantee.</p></div></div>
                                                                            </div>
                      </div>
                    </div>
                  </div>
              </div>
                            </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="dorNavSlider" style="bottom:70px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype"><div data-u="numbertemplate"></div></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="dorArrowLeft" style="" data-autocenter="2"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></span>
        <span data-u="arrowright" class="dorArrowRight" style="" data-autocenter="2"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
    </div>
  </div>
<?php }
}
