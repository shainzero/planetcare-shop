<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:36:21
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_searchcategories\dorsearch-top.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf28415e202b7_90887442',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1568ba5c74c35c175698fec3575356fa6409ff64' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_searchcategories\\dorsearch-top.tpl',
      1 => 1512112344,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5bf28415e202b7_90887442 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- pos search module TOP -->
<div id="dor_search_top" class="center_column col-lg-4 col-md-4 col-xs-12 col-sm-12 clearfix" >
    <form method="get" action="http://localhost/planetcare/en/search" id="searchbox" class="form-inline">
        <div class="pos_search form-group no-uniform col-lg-4 col-md-4 col-xs-4 col-sm-4">
            <i class="fa fa-th"></i>
            <button type="button" class="dropdown-toggle form-control" data-toggle="dropdown">
               <span data-bind="label">All Category</span>&nbsp;<span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#" data-value="0">All Category</a></li>
               <!-- <li><a href="#" data-value="2">&ndash;Home </a></li> -->
            </ul>
        </div>
        <div class="dor_search form-group col-lg-8 col-md-8 col-xs-8 col-sm-8">
			<input class="search_query form-control" type="text" id="dor_query_top" name="search_query" value="" placeholder="Type your search here..." autocomplete="off" />
			<button type="submit" name="submit_search" class="btn btn-default"><i class="pe-7s-search"></i></button>
        </div>
        <label for="dor_query_top"></label>
        <input type="hidden" name="controller" value="search" />
        <input type="hidden" name="orderby" value="position" />
        <input type="hidden" name="orderby" value="categories" />
        <input type="hidden" name="orderway" value="desc" />
        <input type="hidden" name="valSelected" value="0" />
    </form>
</div>
<!-- /pos search module TOP -->
<?php }
}
