<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:47
  from 'C:\xampp\htdocs\planetcare\modules\smartbloghomelatestnews\views\templates\front\dorblog_latest_news_home_style4.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280ab036139_44331118',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '988afab4ff91dc15db1bf4b717cf8e7e67dccda8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\modules\\smartbloghomelatestnews\\views\\templates\\front\\dorblog_latest_news_home_style4.tpl',
      1 => 1541774554,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5bf280ab036139_44331118 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- Latest News -->
    <div id="dor-blog-home-style4">
    <div class="blog-home-inner">
        <section class="gst-row row-latest-news ovh">
            <div class="theme-container">
                <div class="gst-column no-padding">
                    <div class="fancy-heading text-left title-header-tab">
                        <h3 class="head-tab-lists title-mod-news"><span>From Our Blog</span></h3>
                        <p><span>Unique and Efficient</span></p>
                    </div>
                    <div class="row gst-post-list animatedParent animateOnce">
                                                                                                                                                                                                <div class="item-blog-data animated bounceInUp">
                            <div class="col-item-blog">
                            	<div class="blog-home-items">
	                            	<div class="blog-home-items-inner">
		                                <div class="item-blog-media">
		                                    <a href="http://localhost/planetcare/blogs/3_answer-to-your-question-about-prestashop-1-6.html">
		                                        <img src="/planetcare/img/dorthumbs/470x280/smartblog/images/3.jpg" alt="" />
		                                    </a>
		                                </div>
		                                <div class="item-content-blog">
		                                    <div class="media-body">
		                                    	<div class="entry-meta">
			                                        <div class="entry-time meta-date blog-line">
			                                            <time datetime="2015-12-09T21:10:20+00:00">
			                                                <i class="material-icons">&#xE192;</i>
			                                                <span class="entry-time-date dblock"> 9</span>
			                                                Nov
			                                            </time>
			                                        </div>
			                                        <div class="vcard author entry-author blog-line">
			                                            <a class="url fn n" rel="author" href="#">
			                                                <i class="material-icons">&#xE853;</i>&nbsp;Tina<span class="hidden"> Kirn</span>
			                                            </a>
			                                        </div>
			                                        <div class="entry-reply blog-line">
			                                            <a href="http://localhost/planetcare/blogs/3_answer-to-your-question-about-prestashop-1-6.html#comments" class="comments-link">
			                                                <i class="fa fa-comment dblock"></i>
			                                                0 Comment
			                                            </a>
			                                        </div>
			                                    </div>
		                                        <div class="entry-header">
		                                            <span class="entry-categories hidden">
		                                                <a href="http://localhost/planetcare/blogs/category/1_uncategories.html">Uncategories</a>
		                                            </span>
		                                            <h3 class="entry-title">
		                                                <a href="http://localhost/planetcare/blogs/3_answer-to-your-question-about-prestashop-1-6.html">Answers to your Questions about...</a>
		                                            </h3>
		                                            <p class="news-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy...</p>
		                                            <a href="http://localhost/planetcare/blogs/3_answer-to-your-question-about-prestashop-1-6.html" class="read-more-link thm-clr hidden"><span class="dor-effect-hzt">Read More</span> <i class="fa fa-long-arrow-right hidden"></i> </a>
		                                        </div>
		                                    </div>
		                                    
		                                </div>
	                                </div>
	                            </div>
                            </div>
                        </div>
                                                                                                                                                                                                                    <div class="item-blog-data animated bounceInUp">
                            <div class="col-item-blog">
                            	<div class="blog-home-items">
	                            	<div class="blog-home-items-inner">
		                                <div class="item-blog-media">
		                                    <a href="http://localhost/planetcare/blogs/2_what-is-bootstrap.html">
		                                        <img src="/planetcare/img/dorthumbs/470x280/smartblog/images/2.jpg" alt="" />
		                                    </a>
		                                </div>
		                                <div class="item-content-blog">
		                                    <div class="media-body">
		                                    	<div class="entry-meta">
			                                        <div class="entry-time meta-date blog-line">
			                                            <time datetime="2015-12-09T21:10:20+00:00">
			                                                <i class="material-icons">&#xE192;</i>
			                                                <span class="entry-time-date dblock"> 9</span>
			                                                Nov
			                                            </time>
			                                        </div>
			                                        <div class="vcard author entry-author blog-line">
			                                            <a class="url fn n" rel="author" href="#">
			                                                <i class="material-icons">&#xE853;</i>&nbsp;Tina<span class="hidden"> Kirn</span>
			                                            </a>
			                                        </div>
			                                        <div class="entry-reply blog-line">
			                                            <a href="http://localhost/planetcare/blogs/2_what-is-bootstrap.html#comments" class="comments-link">
			                                                <i class="fa fa-comment dblock"></i>
			                                                0 Comment
			                                            </a>
			                                        </div>
			                                    </div>
		                                        <div class="entry-header">
		                                            <span class="entry-categories hidden">
		                                                <a href="http://localhost/planetcare/blogs/category/1_uncategories.html">Uncategories</a>
		                                            </span>
		                                            <h3 class="entry-title">
		                                                <a href="http://localhost/planetcare/blogs/2_what-is-bootstrap.html">What is Bootstrap? – The History and...</a>
		                                            </h3>
		                                            <p class="news-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy...</p>
		                                            <a href="http://localhost/planetcare/blogs/2_what-is-bootstrap.html" class="read-more-link thm-clr hidden"><span class="dor-effect-hzt">Read More</span> <i class="fa fa-long-arrow-right hidden"></i> </a>
		                                        </div>
		                                    </div>
		                                    
		                                </div>
	                                </div>
	                            </div>
                            </div>
                        </div>
                                                                                                                                                                                                                    <div class="item-blog-data animated bounceInUp">
                            <div class="col-item-blog">
                            	<div class="blog-home-items">
	                            	<div class="blog-home-items-inner">
		                                <div class="item-blog-media">
		                                    <a href="http://localhost/planetcare/blogs/1_from-now-we-are-certified-web-agency.html">
		                                        <img src="/planetcare/img/dorthumbs/470x280/smartblog/images/1.jpg" alt="" />
		                                    </a>
		                                </div>
		                                <div class="item-content-blog">
		                                    <div class="media-body">
		                                    	<div class="entry-meta">
			                                        <div class="entry-time meta-date blog-line">
			                                            <time datetime="2015-12-09T21:10:20+00:00">
			                                                <i class="material-icons">&#xE192;</i>
			                                                <span class="entry-time-date dblock"> 9</span>
			                                                Nov
			                                            </time>
			                                        </div>
			                                        <div class="vcard author entry-author blog-line">
			                                            <a class="url fn n" rel="author" href="#">
			                                                <i class="material-icons">&#xE853;</i>&nbsp;Tina<span class="hidden"> Kirn</span>
			                                            </a>
			                                        </div>
			                                        <div class="entry-reply blog-line">
			                                            <a href="http://localhost/planetcare/blogs/1_from-now-we-are-certified-web-agency.html#comments" class="comments-link">
			                                                <i class="fa fa-comment dblock"></i>
			                                                0 Comment
			                                            </a>
			                                        </div>
			                                    </div>
		                                        <div class="entry-header">
		                                            <span class="entry-categories hidden">
		                                                <a href="http://localhost/planetcare/blogs/category/1_uncategories.html">Uncategories</a>
		                                            </span>
		                                            <h3 class="entry-title">
		                                                <a href="http://localhost/planetcare/blogs/1_from-now-we-are-certified-web-agency.html">From Now we are certified web agency</a>
		                                            </h3>
		                                            <p class="news-desc">Smartdatasoft is an offshore web development company located in Bangladesh. We are serving this sector since 2010. Our team is...</p>
		                                            <a href="http://localhost/planetcare/blogs/1_from-now-we-are-certified-web-agency.html" class="read-more-link thm-clr hidden"><span class="dor-effect-hzt">Read More</span> <i class="fa fa-long-arrow-right hidden"></i> </a>
		                                        </div>
		                                    </div>
		                                    
		                                </div>
	                                </div>
	                            </div>
                            </div>
                        </div>
                                                                </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- Latest News --><?php }
}
