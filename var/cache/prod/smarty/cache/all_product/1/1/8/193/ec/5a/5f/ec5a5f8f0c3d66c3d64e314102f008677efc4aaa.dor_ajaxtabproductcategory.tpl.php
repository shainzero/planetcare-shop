<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:51:42
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_ajaxtabproductcategory\views\templates\hook\dor_ajaxtabproductcategory.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf287ae6e8ac0_11463933',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f8a263b91afa540de1d5dbc61a13f5ab2fe54abf' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_ajaxtabproductcategory\\views\\templates\\hook\\dor_ajaxtabproductcategory.tpl',
      1 => 1541774553,
      2 => 'file',
    ),
    'd416a269ec804154de0e722966e124a3b23eea72' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\modules\\dor_ajaxtabproductcategory\\views\\templates\\hook\\product-item.tpl',
      1 => 1513000444,
      2 => 'file',
    ),
    '0cb3e44b1192f6d09fa40bb103d66f059894427a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\templates\\catalog\\_partials\\miniatures\\product.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5bf287ae6e8ac0_11463933 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="dor-tab-product-category" class="show-hover2">
	<div class="title-header-tab">
		<h3><span>Our Products</span></h3>
		<p><span>Unique and Efficient</span></p>
	</div>
	<div class="dor-tab-product-category-wrapper">
		<ul role="tablist" class="nav nav-tabs" id="dorTabAjax"  data-ajaxurl="http://localhost/planetcare/modules/dor_ajaxtabproductcategory/productcategory-ajax.php">
									<li class="first_item  active " data-rel="tab_all_product"  >
			<a aria-expanded="false" data-toggle="tab" id="cate-tab-data-all_product-tab" href="#cate-tab-data-all_product">All</a>
			</li>
							
							
		</ul>
		<div class="tab-content" id="dorTabProductCategoryContent">
											<div aria-labelledby="cate-tab-data-all_product-tab" id="cate-tab-data-all_product" class="tab-pane fade  active  in">
				<div class="productTabContent_all_product dor-content-items">
					<div class="row">
					   <div class="product_list grid row-item">
      
  <article class="ajax_block_product product-miniature js-product-miniature" data-id-product="23" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="product-container">
      <div class="left-block">
        <div class="product-image-container">
          
            
<a href="http://localhost/planetcare/en/inicio/23-external-filter.html" class="thumbnail product-thumbnail product_img_link">
  <img
    class = "img-responsive thumbnail-image-1"
    src="http://localhost/planetcare/3-home_default/external-filter.jpg"
    alt = ""
    data-full-size-image-url = "http://localhost/planetcare/3-large_default/external-filter.jpg"
  >
  </a>


          
                    <div class="box-items">
                          <a class="new-box box-status" href="http://localhost/planetcare/en/inicio/23-external-filter.html">
                <span class="new-label">New</span>
              </a>
                                  </div>
                    <div class="product-add-wishlist">
                                    <div class="dor-wishlist">
	<a class="addToDorWishlist" href="#" onclick="WishlistCart('wishlist_block_list', 'add', jQuery(this).closest('.js-product-miniature').attr('data-id-product'), jQuery(this).closest('.js-product-miniature').attr('data-id-product-attribute'), 1, 0); return false;" data-toggle="tooltip" data-placement="right" data-original-title="Add to Wishlist">
		<i class="pe-7s-like"></i>
		<span class="wishlist-txt">Add to Wishlist</span>
	</a>
</div>
                      </div>
        </div>
        
        <div class="show-btn-products">        
                    
          <div class="control-action-buttons">
            <div class="action-button">
                <ul class="dor-product-act-button">
                    <li class="product-add-compare">
                    <div class="dor-compare-button-fel">
	<div class="compare">
		<a class="add_to_compare" href="http://localhost/planetcare/en/inicio/23-external-filter.html" data-productid="23" data-toggle="tooltip" title="" data-original-title="Add compare"><i class="pe-7s-shuffle"></i><span class="compare-button-txt hidden">Add to compare</span></a>
	</div>
</div>
                    </li>
                    <li class="product-addtocart">
                      <form action="http://localhost/planetcare/en/cart" method="post" class="dor-addcart-button">
                                                <div class="add">
                          <input type="hidden" name="token" value="46fde8b9a31f16af6a5f5e2b8da76bd3">
                          <input name="id_product" value="23" type="hidden">
                          <input type="hidden" name="id_customization" value="0">
                          <a href="http://localhost/planetcare/en/cart" class="cart-button button ajax_add_to_cart_button add-to-cart" data-button-action="add-to-cart" data-toggle="tooltip" data-original-title="Add to cart" disabled>
                            <i class="pe-7s-cart"></i>
                            <span class="hidden">Add to cart</span>
                          </a>
                        </div>
                      </form>
                    </li>
                    <li class="product-quickview">
                      <a href="#" class="quick-view countdown-view-detail" data-link-action="quickview" data-toggle="tooltip" data-original-title="View detail">
                         <i class="pe-7s-search"></i>
                      </a>
                    </li>
                </ul>
            </div>
          </div>
        </div>
      </div>
      
      <div class="right-block">
          <div class="product-cate"><span>Home</span></div>
          
            <h5 class="product-title-item" itemprop="name"><a href="http://localhost/planetcare/en/inicio/23-external-filter.html" class="product-name">EXTERNAL FILTER</a></h5>
          
          <div class="review-price-product">
                                <div class="hook-reviews">
             <div class="comments_note">	
	<div class="star_content clearfix">
						<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
				</div>
	<div class="dor-num-reviews hidden">
		<span>0 Review</span>
		<i>/</i><a href="#" class="add-your-review">Add Your Review</a>
	</div>
</div>
            </div>
                    <div class="dor-show-value-product clearfix">
            
                              <div class="content_price">
                  <div class="product-price-and-shipping">
                    
                    
                    <span itemprop="price" class="price ">€0.00</span>
                    
                  
                  </div>
                </div>
                          
            <div class="highlighted-informations no-variants hidden-sm-down">
              
                              
            </div>
          </div>
        </div>
        
          <div class="product-description-short hidden" itemprop="description"><p>External filter is the most effective and captures<b> up to 90%</b> of all microfibres.</p></div>
        
    </div>
    
      <ul class="product-flags hidden">
                  <li class="new">New</li>
              </ul>
    
    
  </div>
  </article>

      
  <article class="ajax_block_product product-miniature js-product-miniature" data-id-product="22" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="product-container">
      <div class="left-block">
        <div class="product-image-container">
          
            
<a href="http://localhost/planetcare/en/inicio/22-in-drum-filter.html" class="thumbnail product-thumbnail product_img_link">
  <img
    class = "img-responsive thumbnail-image-1"
    src="http://localhost/planetcare/2-home_default/in-drum-filter.jpg"
    alt = ""
    data-full-size-image-url = "http://localhost/planetcare/2-large_default/in-drum-filter.jpg"
  >
  </a>


          
                    <div class="box-items">
                          <a class="new-box box-status" href="http://localhost/planetcare/en/inicio/22-in-drum-filter.html">
                <span class="new-label">New</span>
              </a>
                                  </div>
                    <div class="product-add-wishlist">
                                    <div class="dor-wishlist">
	<a class="addToDorWishlist" href="#" onclick="WishlistCart('wishlist_block_list', 'add', jQuery(this).closest('.js-product-miniature').attr('data-id-product'), jQuery(this).closest('.js-product-miniature').attr('data-id-product-attribute'), 1, 0); return false;" data-toggle="tooltip" data-placement="right" data-original-title="Add to Wishlist">
		<i class="pe-7s-like"></i>
		<span class="wishlist-txt">Add to Wishlist</span>
	</a>
</div>
                      </div>
        </div>
        
        <div class="show-btn-products">        
                    
          <div class="control-action-buttons">
            <div class="action-button">
                <ul class="dor-product-act-button">
                    <li class="product-add-compare">
                    <div class="dor-compare-button-fel">
	<div class="compare">
		<a class="add_to_compare" href="http://localhost/planetcare/en/inicio/22-in-drum-filter.html" data-productid="22" data-toggle="tooltip" title="" data-original-title="Add compare"><i class="pe-7s-shuffle"></i><span class="compare-button-txt hidden">Add to compare</span></a>
	</div>
</div>
                    </li>
                    <li class="product-addtocart">
                      <form action="http://localhost/planetcare/en/cart" method="post" class="dor-addcart-button">
                                                <div class="add">
                          <input type="hidden" name="token" value="46fde8b9a31f16af6a5f5e2b8da76bd3">
                          <input name="id_product" value="22" type="hidden">
                          <input type="hidden" name="id_customization" value="0">
                          <a href="http://localhost/planetcare/en/cart" class="cart-button button ajax_add_to_cart_button add-to-cart" data-button-action="add-to-cart" data-toggle="tooltip" data-original-title="Add to cart" disabled>
                            <i class="pe-7s-cart"></i>
                            <span class="hidden">Add to cart</span>
                          </a>
                        </div>
                      </form>
                    </li>
                    <li class="product-quickview">
                      <a href="#" class="quick-view countdown-view-detail" data-link-action="quickview" data-toggle="tooltip" data-original-title="View detail">
                         <i class="pe-7s-search"></i>
                      </a>
                    </li>
                </ul>
            </div>
          </div>
        </div>
      </div>
      
      <div class="right-block">
          <div class="product-cate"><span>Home</span></div>
          
            <h5 class="product-title-item" itemprop="name"><a href="http://localhost/planetcare/en/inicio/22-in-drum-filter.html" class="product-name">IN-DRUM FILTER</a></h5>
          
          <div class="review-price-product">
                                <div class="hook-reviews">
             <div class="comments_note">	
	<div class="star_content clearfix">
						<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
				</div>
	<div class="dor-num-reviews hidden">
		<span>0 Review</span>
		<i>/</i><a href="#" class="add-your-review">Add Your Review</a>
	</div>
</div>
            </div>
                    <div class="dor-show-value-product clearfix">
            
                              <div class="content_price">
                  <div class="product-price-and-shipping">
                    
                    
                    <span itemprop="price" class="price ">€0.00</span>
                    
                  
                  </div>
                </div>
                          
            <div class="highlighted-informations no-variants hidden-sm-down">
              
                              
            </div>
          </div>
        </div>
        
          <div class="product-description-short hidden" itemprop="description"><p class="MsoListParagraphCxSpFirst" style="text-align:justify;"><span lang="en-gb" xml:lang="en-gb">In-drum filter comprises of housing, magnet for external fixation, external fixing point, flaps and internal filter material. The usage of the filter results in purification of the washing machine wastewater of fibres following the process of laundry washing. The filter is fixed to the washing machine drum rotating according to the washing machine wash setup/washing program. The fixing point prevents the in-drum filter from moving over the flat surface of the washing machine drum.</span></p>
<p></p>
<p></p>
<p class="MsoListParagraphCxSpMiddle" style="text-align:justify;"><span lang="en-gb" xml:lang="en-gb"> </span></p></div>
        
    </div>
    
      <ul class="product-flags hidden">
                  <li class="new">New</li>
              </ul>
    
    
  </div>
  </article>

      
  <article class="ajax_block_product product-miniature js-product-miniature" data-id-product="21" data-id-product-attribute="0" itemscope itemtype="http://schema.org/Product">
    <div class="product-container">
      <div class="left-block">
        <div class="product-image-container">
          
            
<a href="http://localhost/planetcare/en/inicio/21-add-on-filter.html" class="thumbnail product-thumbnail product_img_link">
  <img
    class = "img-responsive thumbnail-image-1"
    src="http://localhost/planetcare/1-home_default/add-on-filter.jpg"
    alt = ""
    data-full-size-image-url = "http://localhost/planetcare/1-large_default/add-on-filter.jpg"
  >
  </a>


          
                    <div class="box-items">
                          <a class="new-box box-status" href="http://localhost/planetcare/en/inicio/21-add-on-filter.html">
                <span class="new-label">New</span>
              </a>
                                  </div>
                    <div class="product-add-wishlist">
                                    <div class="dor-wishlist">
	<a class="addToDorWishlist" href="#" onclick="WishlistCart('wishlist_block_list', 'add', jQuery(this).closest('.js-product-miniature').attr('data-id-product'), jQuery(this).closest('.js-product-miniature').attr('data-id-product-attribute'), 1, 0); return false;" data-toggle="tooltip" data-placement="right" data-original-title="Add to Wishlist">
		<i class="pe-7s-like"></i>
		<span class="wishlist-txt">Add to Wishlist</span>
	</a>
</div>
                      </div>
        </div>
        
        <div class="show-btn-products">        
                    
          <div class="control-action-buttons">
            <div class="action-button">
                <ul class="dor-product-act-button">
                    <li class="product-add-compare">
                    <div class="dor-compare-button-fel">
	<div class="compare">
		<a class="add_to_compare" href="http://localhost/planetcare/en/inicio/21-add-on-filter.html" data-productid="21" data-toggle="tooltip" title="" data-original-title="Add compare"><i class="pe-7s-shuffle"></i><span class="compare-button-txt hidden">Add to compare</span></a>
	</div>
</div>
                    </li>
                    <li class="product-addtocart">
                      <form action="http://localhost/planetcare/en/cart" method="post" class="dor-addcart-button">
                                                <div class="add">
                          <input type="hidden" name="token" value="46fde8b9a31f16af6a5f5e2b8da76bd3">
                          <input name="id_product" value="21" type="hidden">
                          <input type="hidden" name="id_customization" value="0">
                          <a href="http://localhost/planetcare/en/cart" class="cart-button button ajax_add_to_cart_button add-to-cart" data-button-action="add-to-cart" data-toggle="tooltip" data-original-title="Add to cart" disabled>
                            <i class="pe-7s-cart"></i>
                            <span class="hidden">Add to cart</span>
                          </a>
                        </div>
                      </form>
                    </li>
                    <li class="product-quickview">
                      <a href="#" class="quick-view countdown-view-detail" data-link-action="quickview" data-toggle="tooltip" data-original-title="View detail">
                         <i class="pe-7s-search"></i>
                      </a>
                    </li>
                </ul>
            </div>
          </div>
        </div>
      </div>
      
      <div class="right-block">
          <div class="product-cate"><span>Home</span></div>
          
            <h5 class="product-title-item" itemprop="name"><a href="http://localhost/planetcare/en/inicio/21-add-on-filter.html" class="product-name">ADD-ON FILTER</a></h5>
          
          <div class="review-price-product">
                                <div class="hook-reviews">
             <div class="comments_note">	
	<div class="star_content clearfix">
						<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
				</div>
	<div class="dor-num-reviews hidden">
		<span>0 Review</span>
		<i>/</i><a href="#" class="add-your-review">Add Your Review</a>
	</div>
</div>
            </div>
                    <div class="dor-show-value-product clearfix">
            
                              <div class="content_price">
                  <div class="product-price-and-shipping">
                    
                    
                    <span itemprop="price" class="price ">€0.00</span>
                    
                  
                  </div>
                </div>
                          
            <div class="highlighted-informations no-variants hidden-sm-down">
              
                              
            </div>
          </div>
        </div>
        
          <div class="product-description-short hidden" itemprop="description"><p>Add-on filter assembly is produced for specific washing machines and will not fit other machines. When ordering you have to specify the exact type and make of the washing machine.</p></div>
        
    </div>
    
      <ul class="product-flags hidden">
                  <li class="new">New</li>
              </ul>
    
    
  </div>
  </article>

    </div>
 					</div>
				</div>
				<div class="loadmore-showdata text-center clearfix">
					<a href="#" class="dor-load-more-tab" data-page="2" data-limit="8" data-ajax="http://localhost/planetcare/modules/dor_ajaxtabproductcategory/productcategory-ajax.php" onclick="return false">
						<span>Load more</span>
					</a>
				</div>
			</div>
															
		</div>
	</div>
</div><?php }
}
