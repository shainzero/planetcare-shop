<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:54:18
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dorcompare\views\templates\hook\compareleft.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf2884a2730e6_20143791',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dbcbb4266ea0413ccccf3e553a9e243d6f8f3564' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dorcompare\\views\\templates\\hook\\compareleft.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf2884a2730e6_20143791 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="dorCompareLeftSidebar">
	<div class="dorCompareLeftInner">
		<div class="section-title">
			<h2 class="title_block">Compare Products <span class="counter qty hidden">0 items</span></h2>
		</div>
		
		<div class="list-compare-left">
			<ul>
			<?php if ($_smarty_tpl->tpl_vars['hasProduct']->value) {?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['compares']->value, 'product', false, NULL, 'for_products', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
?>
				<li><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['url'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a><span class="compare_remove" href="#" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Remove'),$_smarty_tpl ) );?>
" data-productid="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><i class="material-icons">&#xE872;</i></span></li>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			<?php } else { ?>
				<li class="empty">You have no items to compare.</li>
			<?php }?>
			</ul>
		</div>
		<div class="actions-footer-sidebar <?php if (!$_smarty_tpl->tpl_vars['hasProduct']->value) {?>compare-hide<?php }?>">
			<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'module','name'=>'dorcompare','controller'=>'compare','params'=>array()),$_smarty_tpl ) );?>
" class="dor-sidebar-compare"><span>Compare</span></a>
			<a href="#" onclick="return false" class="dor-sidebar-compare-clear"><span>Clear all</span></a>
		</div>
	</div>
</div><?php }
}
