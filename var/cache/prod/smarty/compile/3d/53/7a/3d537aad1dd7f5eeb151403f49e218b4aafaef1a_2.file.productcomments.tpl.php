<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:52:13
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dorproductreviews\productcomments.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf287cdd99884_91214576',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3d537aad1dd7f5eeb151403f49e218b4aafaef1a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dorproductreviews\\productcomments.tpl',
      1 => 1508416804,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf287cdd99884_91214576 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
<?php echo '</script'; ?>
>
	<div id="product_comments_block_tab">
	<input type="hidden" id="productCommentUrl" value="<?php echo htmlspecialchars(addslashes($_smarty_tpl->tpl_vars['link']->value->getModuleLink('dorproductreviews','default',array(),true)), ENT_QUOTES, 'UTF-8');?>
">
	<input type="hidden" id="secure_key" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['secure_key']->value, ENT_QUOTES, 'UTF-8');?>
">
	<?php if ($_smarty_tpl->tpl_vars['comments']->value) {?>
		<div class="product_comment_list_reviews">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['comments']->value, 'comment');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->value) {
?>
			<?php if ($_smarty_tpl->tpl_vars['comment']->value['content']) {?>
			<div class="comment row" itemprop="review" itemscope itemtype="https://schema.org/Review">
				<div class="comment_author col-sm-12">
					<span class="avatar-review">
						<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['img_url'], ENT_QUOTES, 'UTF-8');?>
dorado/avatar_user.jpg" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value['customer_name'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value['customer_name'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
					</span>
					<div class="comment-text">
						<div class="meta-rating-area">
							<div class="meta-area">
                            <?php if (isset($_smarty_tpl->tpl_vars['comment']->value['user_name']) && $_smarty_tpl->tpl_vars['comment']->value['user_name'] != '') {?>
                                <strong itemprop="author"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value['user_name'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</strong>
                            <?php } else { ?>
                                <strong itemprop="author"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value['customer_name'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</strong>
                            <?php }?>
                                <span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0], array( array('date'=>call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value['date_add'],'html','UTF-8' )),'full'=>0),$_smarty_tpl ) );?>
</span>
                            </div>
                            <div class="star_content user-rating"  itemprop="reviewRating" itemscope itemtype="https://schema.org/Rating">
								<?php
$_smarty_tpl->tpl_vars['__smarty_section_i'] = new Smarty_Variable(array());
if (true) {
for ($__section_i_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_i']->value['index'] = 0; $__section_i_0_iteration <= 5; $__section_i_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_i']->value['index']++){
?>
									<?php if ($_smarty_tpl->tpl_vars['comment']->value['grade'] <= (isset($_smarty_tpl->tpl_vars['__smarty_section_i']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_i']->value['index'] : null)) {?>
										<div class="star"></div>
									<?php } else { ?>
										<div class="star star_on"></div>
									<?php }?>
								<?php
}
}
?>
	            				<meta itemprop="worstRating" content = "0" />
								<meta itemprop="ratingValue" content = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value['grade'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
	            				<meta itemprop="bestRating" content = "5" />
							</div>
						</div>
						<div class="description">
                            <p itemprop="reviewBody"><?php echo htmlspecialchars(nl2br(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value['content'],'html','UTF-8' ))), ENT_QUOTES, 'UTF-8');?>
</p>
                        </div>
					</div>
				</div>
			</div>
			<?php }?>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		</div>
        <?php if ((!$_smarty_tpl->tpl_vars['too_early']->value && ($_smarty_tpl->tpl_vars['logged']->value || $_smarty_tpl->tpl_vars['allow_guests']->value))) {?>
		<p class="align_center">
			<a id="new_comment_tab_btn" class="open-comment-form" href="#new_comment_form"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Write your review','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
 !</a>
		</p>
        <?php }?>
	<?php } else { ?>
		<?php if ((!$_smarty_tpl->tpl_vars['too_early']->value && ($_smarty_tpl->tpl_vars['logged']->value || $_smarty_tpl->tpl_vars['allow_guests']->value))) {?>
		<p class="align_center">
			<a id="new_comment_tab_btn" class="open-comment-form" href="#new_comment_form"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Be the first to write your review','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
 !</a>
		</p>
		<?php } else { ?>
		<p class="align_center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No customer reviews for the moment.','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
</p>
		<?php }?>
	<?php }?>	
	</div>

<?php if (isset($_smarty_tpl->tpl_vars['product']->value) && $_smarty_tpl->tpl_vars['product']->value) {?>
<!-- Fancybox -->
<div id="dor-review-form">
	<div id="new_comment_form">
		<form id="id_new_comment_form" action="#">
			<h2 class="title hidden"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Write your review','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
</h2>
			<div class="new_comment_form_content">
				<div id="new_comment_form_error" class="error" style="display:none;padding:15px 25px">
					<ul></ul>
				</div>
				<h2><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add a review','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
</h2>
				<p class="comment-notes"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your email address will not be published.Required fields are marked','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
<span class="required">*</span></p>

				<?php if (count($_smarty_tpl->tpl_vars['criterions']->value) > 0) {?>
					<ul id="criterions_list">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['criterions']->value, 'criterion');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['criterion']->value) {
?>
						<li>
							<label><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['name'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</label>
							<div class="star_content">
								<input class="star" type="radio" name="criterion[<?php echo htmlspecialchars(round($_smarty_tpl->tpl_vars['criterion']->value['id_dorproduct_comment_criterion']), ENT_QUOTES, 'UTF-8');?>
]" value="1" />
								<input class="star" type="radio" name="criterion[<?php echo htmlspecialchars(round($_smarty_tpl->tpl_vars['criterion']->value['id_dorproduct_comment_criterion']), ENT_QUOTES, 'UTF-8');?>
]" value="2" />
								<input class="star" type="radio" name="criterion[<?php echo htmlspecialchars(round($_smarty_tpl->tpl_vars['criterion']->value['id_dorproduct_comment_criterion']), ENT_QUOTES, 'UTF-8');?>
]" value="3" />
								<input class="star" type="radio" name="criterion[<?php echo htmlspecialchars(round($_smarty_tpl->tpl_vars['criterion']->value['id_dorproduct_comment_criterion']), ENT_QUOTES, 'UTF-8');?>
]" value="4" />
								<input class="star" type="radio" name="criterion[<?php echo htmlspecialchars(round($_smarty_tpl->tpl_vars['criterion']->value['id_dorproduct_comment_criterion']), ENT_QUOTES, 'UTF-8');?>
]" value="5" checked="checked" />
							</div>
							<div class="clearfix"></div>
						</li>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</ul>
				<?php }?>
				<div class="review-form-group">
					<div class="cmt-review-group row clearfix">
						<?php if ($_smarty_tpl->tpl_vars['allow_guests']->value == true && !$_smarty_tpl->tpl_vars['logged']->value) {?>
						<div class="field-review-cmt col-lg-4 col-sm-4 col-xs-12">
							<label class="hidden"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your name','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
<sup class="required">*</sup></label>
							<input id="commentCustomerName" name="customer_name" type="text" value="" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your name','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
"/>
						</div>
						<?php }?>
						<div class="field-review-cmt col-lg-4 col-sm-4 col-xs-12">
							<label for="comment_email" class="hidden"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your email','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
<sup class="required">*</sup></label>
							<input id="comment_email" name="email" type="text" value="" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'E-mail','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
"/>
						</div>
						<div class="field-review-cmt col-lg-4 col-sm-4 col-xs-12">
							<label for="comment_website" class="hidden"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your website','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
<sup class="required">*</sup></label>
							<input id="comment_website" name="website" type="text" value="" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Website','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
"/>
						</div>
					</div>
					<div class="cmt-review-group row clearfix">
						<div class="field-review-cmt col-lg-12 col-sm-12 col-xs-12">
							<label for="comment_title" class="hidden"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Title for your review','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
<sup class="required">*</sup></label>
							<input id="comment_title" name="title" type="text" value="" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Title for your review','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
" />
						</div>
						<div class="field-review-cmt field-comment-review col-lg-12 col-sm-12 col-xs-12">
							<label for="content-review-comment" class="hidden"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your review','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
<sup class="required">*</sup></label>
							<textarea id="content-review-comment" name="content" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Comment:','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
"></textarea>
						</div>
						<div id="new_comment_form_footer">
							<input id="id_product_comment_send" name="id_product" type="hidden" value='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_product_comment_form']->value, ENT_QUOTES, 'UTF-8');?>
' />
							<p class="fr">
								<button id="submitNewMessage" name="submitMessage" type="submit"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Submit','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
</button>&nbsp;
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'or','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
&nbsp;<a href="#" onclick="return false" class="close-comment-form"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Cancel','mod'=>'dorproductreviews'),$_smarty_tpl ) );?>
</a>
							</p>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</form><!-- /end new_comment_form_content -->
	</div>
</div>
<!-- End fancybox -->
<?php }
}
}
