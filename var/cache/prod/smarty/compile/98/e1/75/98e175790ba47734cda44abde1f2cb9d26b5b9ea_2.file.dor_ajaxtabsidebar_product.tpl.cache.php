<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:54:18
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_ajaxtabsidebar_product\views\templates\hook\dor_ajaxtabsidebar_product.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf2884a301545_66526852',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '98e175790ba47734cda44abde1f2cb9d26b5b9ea' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_ajaxtabsidebar_product\\views\\templates\\hook\\dor_ajaxtabsidebar_product.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf2884a301545_66526852 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '880589065bf2884a2ecf21_88567600';
if (isset($_smarty_tpl->tpl_vars['productLists']->value) && $_smarty_tpl->tpl_vars['productLists']->value) {?>
<div id="dor-tabsidebar-product-category">
	<div class="dor-tabsidebar-product-category-wrapper">
		<ul role="tablist" class="nav nav-tabs dorTabAjaxSidebar" id="dorTabAjaxSidebar" data-ajaxurl="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
modules/dor_ajaxtabsidebar_product/productcategory-ajax.php">
			<?php $_smarty_tpl->_assignInScope('j', 0);?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listTabsProduct']->value, 'productTab', false, NULL, 'tabCatePro', array (
  'first' => true,
  'last' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['productTab']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['total'];
?>
			<li class="<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['first'] : null)) {?>first_item<?php } elseif ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['last'] : null)) {?>last_item<?php } else {
}?> <?php if ($_smarty_tpl->tpl_vars['productTab']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> active <?php }?>" data-rel="tab_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
"  >
			<div class="section-title"><h2 class="title_block"><a aria-expanded="false" data-toggle="tab" id="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
-tab" href="#cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a></h2></div>
			</li>
				<?php $_smarty_tpl->_assignInScope('j', $_smarty_tpl->tpl_vars['j']->value+1);?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

			<?php $_smarty_tpl->_assignInScope('i', 0);?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listTabs']->value, 'cate', false, NULL, 'tabCate', array (
  'first' => true,
  'last' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cate']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['total'];
?>
			<li class="<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['first'] : null)) {?>first_item<?php } elseif ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['last'] : null)) {?>last_item<?php } else {
}?> <?php if ($_smarty_tpl->tpl_vars['cate']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> active <?php }?>"><div class="section-title"><h2 class="title_block"><a aria-expanded="false" data-toggle="tab" id="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['id'], ENT_QUOTES, 'UTF-8');?>
-tab" href="#cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a></h2></div></li>
			<?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>	
		</ul>
		<div class="tab-content" id="dorTabSidebarProductCategoryContent">
		<?php if ($_smarty_tpl->tpl_vars['listTabsProduct']->value) {?>
			<?php $_smarty_tpl->_assignInScope('k', 0);?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listTabsProduct']->value, 'productTab', false, NULL, 'tabCatePro', array (
  'first' => true,
  'last' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['productTab']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['total'];
?>
			<div aria-labelledby="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
-tab" id="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['productTab']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> active <?php }?> in">
				<div class="productTabContent_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
 dor-content-items">
					<div class="row-sidebar nav-button-style-one">
					<?php if ($_smarty_tpl->tpl_vars['productTab']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['selfTmp']->value)."/product-item.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, true);
?> <?php }?>
					</div>
				</div>
				<div class="view-all">
					<a class="btn btn-scale btn-go-cate" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['url'], ENT_QUOTES, 'UTF-8');?>
"><span><span class="fa fa-long-arrow-right">&nbsp;</span></span>View All</a>
				</div>
			</div>
			<?php $_smarty_tpl->_assignInScope('k', $_smarty_tpl->tpl_vars['k']->value+1);?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		<?php }?>
			<?php $_smarty_tpl->_assignInScope('key', 0);?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listTabs']->value, 'cate', false, NULL, 'tabCate', array (
  'first' => true,
  'last' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cate']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['total'];
?>
			<div aria-labelledby="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['id'], ENT_QUOTES, 'UTF-8');?>
-tab" id="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['cate']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> active <?php }?> in">
				<div class="productTabContent_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['id'], ENT_QUOTES, 'UTF-8');?>
 dor-content-items">
					<div class="row">
					<?php if ($_smarty_tpl->tpl_vars['cate']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['productItemPath']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, true);
?> <?php }?>
					</div>
				</div>
				<div class="view-all">
					<a class="btn btn-scale btn-go-cate" href="#"><span><span class="fa fa-long-arrow-right">&nbsp;</span></span>View All</a>
				</div>
			</div>
			<?php $_smarty_tpl->_assignInScope('key', $_smarty_tpl->tpl_vars['key']->value+1);?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>	
		</div>
	</div>
</div>
<?php }
}
}
