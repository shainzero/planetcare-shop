<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:46
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_ajaxtabproductcategory\views\templates\hook\dor_ajaxtabproductcategory.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280aab72b55_33628168',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f8a263b91afa540de1d5dbc61a13f5ab2fe54abf' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_ajaxtabproductcategory\\views\\templates\\hook\\dor_ajaxtabproductcategory.tpl',
      1 => 1541774553,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf280aab72b55_33628168 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '14502602165bf280aab583f8_10809832';
?>
<div id="dor-tab-product-category" class="show-hover2">
	<div class="title-header-tab">
		<h3><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Our Products",'mod'=>"dor_ajaxtabproductcategory"),$_smarty_tpl ) );?>
</span></h3>
		<p><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Unique and Efficient",'mod'=>"dor_ajaxtabproductcategory"),$_smarty_tpl ) );?>
</span></p>
	</div>
	<div class="dor-tab-product-category-wrapper">
		<ul role="tablist" class="nav nav-tabs" id="dorTabAjax" <?php if ($_smarty_tpl->tpl_vars['firstAjax']->value == 1) {?>data-defaulttab = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tabID']->value['id'], ENT_QUOTES, 'UTF-8');?>
"<?php }?> data-ajaxurl="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value['force_ssl']) && $_smarty_tpl->tpl_vars['urls']->value['force_ssl']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url_ssl'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');
}?>modules/dor_ajaxtabproductcategory/productcategory-ajax.php">
			<?php $_smarty_tpl->_assignInScope('j', 0);?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listTabsProduct']->value, 'productTab', false, NULL, 'tabCatePro', array (
  'first' => true,
  'last' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['productTab']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['total'];
?>
			<li class="<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['first'] : null)) {?>first_item<?php } elseif ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['last'] : null)) {?>last_item<?php } else {
}?> <?php if ($_smarty_tpl->tpl_vars['productTab']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> active <?php }?>" data-rel="tab_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
"  >
			<a aria-expanded="false" data-toggle="tab" id="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
-tab" href="#cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a>
			</li>
				<?php $_smarty_tpl->_assignInScope('j', $_smarty_tpl->tpl_vars['j']->value+1);?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

			<?php $_smarty_tpl->_assignInScope('i', 0);?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listTabs']->value, 'cate', false, NULL, 'tabCate', array (
  'first' => true,
  'last' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cate']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['total'];
?>
			<li class="<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['first'] : null)) {?>first_item<?php } elseif ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['last'] : null)) {?>last_item<?php } else {
}?> <?php if ($_smarty_tpl->tpl_vars['cate']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> active <?php }?>"><a aria-expanded="false" data-toggle="tab" id="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['id'], ENT_QUOTES, 'UTF-8');?>
-tab" href="#cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a></li>
			<?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>	
		</ul>
		<div class="tab-content" id="dorTabProductCategoryContent">
		<?php if ($_smarty_tpl->tpl_vars['listTabsProduct']->value) {?>
			<?php $_smarty_tpl->_assignInScope('k', 0);?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listTabsProduct']->value, 'productTab', false, NULL, 'tabCatePro', array (
  'first' => true,
  'last' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['productTab']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_tabCatePro']->value['total'];
?>
			<div aria-labelledby="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
-tab" id="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['productTab']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> active <?php }?> in">
				<div class="productTabContent_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
 dor-content-items">
					<div class="row">
					<?php if ($_smarty_tpl->tpl_vars['productTab']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['self']->value)."/product-item.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, true);
?> <?php }?>
					</div>
				</div>
				<div class="loadmore-showdata text-center clearfix">
					<a href="#" class="dor-load-more-tab" data-page="2" data-limit="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['optionsConfig']->value['number_per_page'], ENT_QUOTES, 'UTF-8');?>
" data-ajax="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value['force_ssl']) && $_smarty_tpl->tpl_vars['urls']->value['force_ssl']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url_ssl'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');
}?>modules/dor_ajaxtabproductcategory/productcategory-ajax.php" onclick="return false">
						<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Load more",'mod'=>"dor_ajaxtabproductcategory"),$_smarty_tpl ) );?>
</span>
					</a>
				</div>
			</div>
			<?php $_smarty_tpl->_assignInScope('k', $_smarty_tpl->tpl_vars['k']->value+1);?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		<?php }?>
			<?php $_smarty_tpl->_assignInScope('key', 0);?>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listTabs']->value, 'cate', false, NULL, 'tabCate', array (
  'first' => true,
  'last' => true,
  'index' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cate']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_tabCate']->value['total'];
?>
			<div aria-labelledby="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['id'], ENT_QUOTES, 'UTF-8');?>
-tab" id="cate-tab-data-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['cate']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> active <?php }?> in">
				<div class="productTabContent_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cate']->value['id'], ENT_QUOTES, 'UTF-8');?>
 dor-content-items">
					<div class="row">
					<?php if ($_smarty_tpl->tpl_vars['cate']->value['id'] == $_smarty_tpl->tpl_vars['tabID']->value['id']) {?> <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['productItemPath']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array(), 0, true);
?> <?php }?>
					</div>
				</div>
				<div class="loadmore-showdata text-center clearfix">
					<a href="#" class="dor-load-more-tab" data-page="2" data-ajax="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value['force_ssl']) && $_smarty_tpl->tpl_vars['urls']->value['force_ssl']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url_ssl'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');
}?>modules/dor_ajaxtabproductcategory/productcategory-ajax.php" data-limit="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['optionsConfig']->value['number_per_page'], ENT_QUOTES, 'UTF-8');?>
" onclick="return false">
						<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Load more",'mod'=>"dor_ajaxtabproductcategory"),$_smarty_tpl ) );?>
</span>
					</a>
				</div>
			</div>
			<?php $_smarty_tpl->_assignInScope('key', $_smarty_tpl->tpl_vars['key']->value+1);?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>	
		</div>
	</div>
</div><?php }
}
