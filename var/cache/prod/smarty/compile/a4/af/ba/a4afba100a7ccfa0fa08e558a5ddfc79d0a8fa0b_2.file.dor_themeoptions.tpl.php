<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:36:21
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_themeoptions\views\templates\front\dor_themeoptions.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf284159df0f3_34901213',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a4afba100a7ccfa0fa08e558a5ddfc79d0a8fa0b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_themeoptions\\views\\templates\\front\\dor_themeoptions.tpl',
      1 => 1512030765,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf284159df0f3_34901213 (Smarty_Internal_Template $_smarty_tpl) {
?><style type="text/css">
	/****Top Nav***/
	<?php if ($_smarty_tpl->tpl_vars['dorTopbarColorText']->value != '') {?>
        #dor-topbar01 .topbar-infomation span, #dor-topbar01 .line-selected,
        #dor-topbar01 .topbar-infomation-right ul li span,
        #dor-topbar01 .topbar-infomation i, #dor-topbar01 .line-selected i, 
        #dor-topbar01 .topbar-infomation-right i,
        #dor-topbar01 .topbar-infomation li span::after, 
        #dor-topbar01 .topbar-infomation-right ul li span::after {
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorTopbarColorText']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
        #dor-topbar01 .topbar-infomation li span::after, 
        #dor-topbar01 .topbar-infomation-right ul li span::after{
            background-color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorTopbarColorText']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorTopbarBgOutside']->value != '') {?>
    #dor-topbar, .dor-topbar-wrapper, .dor-topbar-inner{
    	background:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorTopbarBgOutside']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
    }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorTopbarBgColor']->value != '') {?>
    .dor-topbar-inner .container{
    	background:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorTopbarBgColor']->value, ENT_QUOTES, 'UTF-8');?>
;
    }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorTopbarColorLink']->value != '') {?>
    #dor-topbar a{
    	color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorTopbarColorLink']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
    }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorTopbarColorLinkHover']->value != '') {?>
    #dor-topbar a:hover{
    	color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorTopbarColorLinkHover']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
    }
    <?php }?>
    /***Header****/
	<?php if ($_smarty_tpl->tpl_vars['dorHeaderBgOutside']->value != '') {?>
        body header#header,
        header#header.fixed.fixed-tran{
            background-color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorHeaderBgOutside']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
	<?php if ($_smarty_tpl->tpl_vars['dorHeaderBgColor']->value != '') {?>
        body header#header .container,
        header#header.fixed.fixed-tran .container{
            background-color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorHeaderBgColor']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['dorHeaderColorText']->value != '') {?>
        body header#header .menu-group-show div, 
        body header#header .menu-group-show div span, 
        body header#header .menu-group-show div p{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorHeaderColorText']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
	<?php if ($_smarty_tpl->tpl_vars['dorHeaderColorLink']->value != '') {?>
        body header#header a{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorHeaderColorLink']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorHeaderColorLinkHover']->value != '') {?>
        body header#header a:hover,
        body header#header a:hover span,
        body header#header a:hover p{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorHeaderColorLinkHover']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorHeaderColorIcon']->value != '') {?>
        header#header i{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorHeaderColorIcon']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
	<?php if ($_smarty_tpl->tpl_vars['dorHeaderColorIconHover']->value != '') {?>
        body header#header.header-absolute button[type="button"]:hover i,
        body header#header.header-absolute i:hover{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorHeaderColorIconHover']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
	
	/****Footer****/
	<?php if ($_smarty_tpl->tpl_vars['dorFooterBgOutside']->value != '') {?>
    body #footer,
    body #footer .doradoFooterAdv,
	body #footer .footer-container,
    body #footer .footer-copyright-payment{
		background:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFooterBgOutside']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
	}
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['dorFooterBgColor']->value != '') {?>
	body #footer .footer-container .container{
		background:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFooterBgColor']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
	}
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['dorFooterColorText']->value != '') {?>
	body #footer .footer-container .container,
	body #footer .footer-container .container div,
	body #footer .footer-container .container span,
    body #footer .footer-container .container section,
    body #footer .footer-container .container h4,
    body #footer .footer-container .container h3,
    body #footer .footer-container .container h3.h3,
    body #footer .footer-container .container h5,
    body #footer .footer-container .container strong,
    body #footer .footer-container .container h3 > a.text-uppercase,
	body #footer .footer-container .container i{
		color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFooterColorText']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
	}
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['dorFooterColorLink']->value != '') {?>
	body #footer .footer-container .container a,
    body #footer .footer-container .container a span,
    body #footer .footer-container .container a > i,
    body #footer .footer-container .container a > em,
	body #footer .footer-container .container div a{
		color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFooterColorLink']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
	}
	<?php }?>
	<?php if ($_smarty_tpl->tpl_vars['dorFooterColorLinkHover']->value != '') {?>
	body #footer .footer-container .container a:hover,
	body #footer .footer-container .container div a:hover,
    body #footer .footer-container .container a:hover span,
    body #footer .footer-container .container div a:hover span{
		color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFooterColorLinkHover']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
	}
	<?php }?>


    /*****Mega Menu*****/
    <?php if ($_smarty_tpl->tpl_vars['dorMegamenuBgOutside']->value != '') {?>
        body #header .header-top .dor-header-menu,
        body #header .menu-group-show > .header-megamenu{
            background:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorMegamenuBgOutside']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorMegamenuBgColor']->value != '') {?>
        body #header .header-top .dor-megamenu .navbar-nav,
        body #header .dor-megamenu .navbar-nav,
        body header#header.fixed.fixed-tran .menu-group-show .container,
        body header#header.fixed.fixed-tran .menu-group-show{
            background:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorMegamenuBgColor']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorMegamenuColorLink']->value != '') {?>
        body #header .dor-megamenu ul.navbar-nav > li > a span.menu-title{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorMegamenuColorLink']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorMegamenuColorLinkHover']->value != '') {?>
        body #header .dor-megamenu ul.navbar-nav > li.active > a span.menu-title,
        body #header .dor-megamenu ul.navbar-nav > li > a:hover span.menu-title{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorMegamenuColorLinkHover']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorMegamenuColorSubText']->value != '') {?>
        body .dor-megamenu #dor-top-menu .dropdown-menu,
        body .dor-megamenu #dor-top-menu .dropdown-menu div,
        body .dor-megamenu #dor-top-menu .dropdown-menu ul li{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorMegamenuColorSubText']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorMegamenuColorSubLink']->value != '') {?>
        body .dor-megamenu #dor-top-menu .dropdown-menu a,
        body .dor-megamenu #dor-top-menu .dropdown-menu a span{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorMegamenuColorSubLink']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorMegamenuColorSubLinkHover']->value != '') {?>
        body .dor-megamenu #dor-top-menu .dropdown-menu a:hover,
        body .dor-megamenu #dor-top-menu .dropdown-menu a:hover span{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorMegamenuColorSubLinkHover']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
        body .dor-megamenu #dor-top-menu .dropdown-menu li > a:hover{
            border-bottom-color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorMegamenuColorSubLinkHover']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['dorVermenuBgOutside']->value != '') {?>
        body #dor-verticalmenu .dor-vertical-title{
            background-color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorVermenuBgOutside']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['dorVermenuBgColor']->value != '') {?>
        body #dor-verticalmenu .dor-verticalmenu{
            background-color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorVermenuBgColor']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['dorVermenuColorText']->value != '') {?>
        body #dor-verticalmenu .dor-vertical-title h4,
        body #dor-verticalmenu .dor-vertical-title h4 span,
        body header#header #dor-verticalmenu .dor-vertical-title h4,
        body header#header #dor-verticalmenu .dor-vertical-title h4 span,
        body .fa-icon-menu > i{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorVermenuColorText']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['dorVermenuColorLink']->value != '') {?>
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu > li > a > span.menu-title,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu > li > a > span.menu-icon i{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorVermenuColorLink']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorVermenuColorLinkHover']->value != '') {?>
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu > li > a:hover > span.menu-title,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu > li > a:hover > span.menu-icon i{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorVermenuColorLinkHover']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorVermenuColorSubText']->value != '') {?>
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu div,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu .widget-content,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu .widget-content div{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorVermenuColorSubText']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorVermenuColorSubLink']->value != '') {?>
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu > li > a,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu ul li a,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu a > span,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu .widget-content .widget-heading a{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorVermenuColorSubLink']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['dorVermenuColorSubLinkHover']->value != '') {?>
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu > li > a:hover,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu ul li a:hover,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu a:hover > span,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu .widget-content .widget-heading a:hover,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu .widget-content .widget-heading a:hover span{
            color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorVermenuColorSubLinkHover']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    /*****End Mega Menu*****/


    <?php if (isset($_smarty_tpl->tpl_vars['dorPriceColor']->value) && $_smarty_tpl->tpl_vars['dorPriceColor']->value != '') {?>
        #products-viewed .product-price-and-shipping span.price, .products .product-price-and-shipping span.price, .product_list .product-price-and-shipping span.price,
        #dor-tab-product-category2 .product-price-and-shipping > span.price {
            color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorPriceColor']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>

    <?php if (isset($_smarty_tpl->tpl_vars['dorPricePrimaryColor']->value) && $_smarty_tpl->tpl_vars['dorPricePrimaryColor']->value != '') {?>
        #products-viewed .product-price-and-shipping > span:nth-child(3).price, .product_list .product-price-and-shipping > span:nth-child(3).price, .products .product-price-and-shipping > span:nth-child(3).price, #dor-tab-product-category2 .product-price-and-shipping > span:nth-child(3).price {
            color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorPricePrimaryColor']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>

    <?php if (isset($_smarty_tpl->tpl_vars['dorOldPriceColor']->value) && $_smarty_tpl->tpl_vars['dorOldPriceColor']->value != '') {?>
        .regular-price {
            color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorOldPriceColor']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['dorFlagSaleBg']->value) && $_smarty_tpl->tpl_vars['dorFlagSaleBg']->value != '') {?>
        .quickview .product-flags > li.product-flag.on-sale::before, .sale-box.box-status::before, #content .product-flags > li.product-flag.on-sale::before {
            border-color: transparent <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFlagSaleBg']->value, ENT_QUOTES, 'UTF-8');?>
 transparent transparent;
        }
        .quickview .product-flags > li.product-flag.on-sale, .sale-box.box-status, #content .product-flags > li.product-flag.on-sale {
            background-color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFlagSaleBg']->value, ENT_QUOTES, 'UTF-8');?>
;
        }
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['dorFlagSaleColor']->value) && $_smarty_tpl->tpl_vars['dorFlagSaleColor']->value != '') {?>
        
        .quickview .product-flags > li.product-flag.on-sale, .sale-box.box-status, #content .product-flags > li.product-flag.on-sale {
            color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFlagSaleColor']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['dorFlagNewBg']->value) && $_smarty_tpl->tpl_vars['dorFlagNewBg']->value != '') {?>
        .box-status::before, .product-tabs-content a::before, #content .product-flags > li.product-flag::before, .quickview .product-flags > li.product-flag::before{
            border-color: transparent <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFlagNewBg']->value, ENT_QUOTES, 'UTF-8');?>
 transparent transparent;
        }
        .quickview .product-flags > li.product-flag, .box-status, .product-tabs-content a, #content .product-flags > li.product-flag{
            background-color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFlagNewBg']->value, ENT_QUOTES, 'UTF-8');?>
;
        }
        #product #content-wrapper #content .product-flags > li.product-flag.new::before {
            border-color: transparent <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFlagNewBg']->value, ENT_QUOTES, 'UTF-8');?>
 transparent transparent !important;
        }
        #product #content .product-flags > li.product-flag.new{
            background-color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFlagNewBg']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['dorFlagNewColor']->value) && $_smarty_tpl->tpl_vars['dorFlagNewColor']->value != '') {?>
    .quickview .product-flags > li.product-flag, .box-status, .product-tabs-content a, #content .product-flags > li.product-flag{
            color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorFlagNewColor']->value, ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php }?>
</style><?php }
}
