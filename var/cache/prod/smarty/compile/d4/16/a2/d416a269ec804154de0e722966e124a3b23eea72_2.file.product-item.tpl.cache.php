<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:51:42
  from 'C:\xampp\htdocs\planetcare\modules\dor_ajaxtabproductcategory\views\templates\hook\product-item.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf287ae0399a1_70080141',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd416a269ec804154de0e722966e124a3b23eea72' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\modules\\dor_ajaxtabproductcategory\\views\\templates\\hook\\product-item.tpl',
      1 => 1513000444,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/miniatures/product.tpl' => 1,
  ),
),false)) {
function content_5bf287ae0399a1_70080141 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '7143221325bf287ae003526_36540324';
if (isset($_smarty_tpl->tpl_vars['products']->value) && $_smarty_tpl->tpl_vars['products']->value) {?>
  <div class="product_list grid row-item">
  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products']->value, 'product', false, NULL, 'products', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
?>
    <?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
  </div>
<?php }
}
}
