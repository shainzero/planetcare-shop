<?php
/* Smarty version 3.1.32, created on 2018-11-20 12:35:03
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\smartblogpopularposts\views\templates\front\smartblogpopularposts.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf3f16799b549_95286471',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '161ddb2728578facd1d23e4e836c08e468fd828c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\smartblogpopularposts\\views\\templates\\front\\smartblogpopularposts.tpl',
      1 => 1501167272,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf3f16799b549_95286471 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\xampp\\htdocs\\planetcare\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
$_smarty_tpl->compiled->nocache_hash = '1228104275bf3f16797fbb8_63091437';
if (isset($_smarty_tpl->tpl_vars['posts']->value) && !empty($_smarty_tpl->tpl_vars['posts']->value)) {?>
<div id="recent_article_smart_blog_block_left"  class="block blogModule boxPlain">
   <div class="section-title"><h2 class="title_block"><a href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog'), ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Popular Posts','mod'=>'smartblogpopularposts'),$_smarty_tpl ) );?>
</a></h2></div>
   <div class="popular-post">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['posts']->value, 'post');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
         <?php $_smarty_tpl->_assignInScope('options', null);?>
         <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['id_post'] = $_smarty_tpl->tpl_vars['post']->value['id_smart_blog_post'];
$_smarty_tpl->_assignInScope('options', $_tmp_array);?>
         <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['slug'] = $_smarty_tpl->tpl_vars['post']->value['link_rewrite'];
$_smarty_tpl->_assignInScope('options', $_tmp_array);?>
        <div class="single-popular-post clearfix">
          <div class="post-image">
              <a title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['meta_title'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_post',$_smarty_tpl->tpl_vars['options']->value), ENT_QUOTES, 'UTF-8');?>
">
                 <img alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['meta_title'], ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['thumb_image'], ENT_QUOTES, 'UTF-8');?>
">
             </a>
          </div>
          <div class="popular-post-details">
              <div class="popular-post-date">
                  <span><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['created'],"%B %d, %Y"), ENT_QUOTES, 'UTF-8');?>
</span>
              </div>
              <h3>
                  <a title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['meta_title'], ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_post',$_smarty_tpl->tpl_vars['options']->value), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['meta_title'], ENT_QUOTES, 'UTF-8');?>
</a>
              </h3>
          </div>
        </div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
   </div>
</div>
<?php }
}
}
