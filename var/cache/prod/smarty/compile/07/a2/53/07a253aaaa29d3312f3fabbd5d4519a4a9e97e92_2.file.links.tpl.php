<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:38:27
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_megamenu\views\templates\hook\widgets\links.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf28493e66dd3_40634351',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '07a253aaaa29d3312f3fabbd5d4519a4a9e97e92' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_megamenu\\views\\templates\\hook\\widgets\\links.tpl',
      1 => 1498962399,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf28493e66dd3_40634351 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['links']->value)) {?>
<div class="widget-links block <?php if (isset($_smarty_tpl->tpl_vars['additionclss']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['additionclss']->value, ENT_QUOTES, 'UTF-8');
}?>">
	<?php if (isset($_smarty_tpl->tpl_vars['widget_heading']->value) && !empty($_smarty_tpl->tpl_vars['widget_heading']->value)) {?>
	<div class="widget-heading title_block">
		<a href="#" onclick="return false" class="img link-cate-custom"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['widget_heading']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</a>
		<span class="caretmobile hidden"></span>
	</div>
	<?php }?>
	<div class="widget-inner block_content">	
		<div id="tabs<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['id']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="panel-group">
			<ul class="nav-links" data-id="myTab">
			  <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['links']->value, 'ac', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['ac']->value) {
?>  
			  <li><a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['ac']->value['link'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" ><?php echo $_smarty_tpl->tpl_vars['ac']->value['icon_class'];
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['ac']->value['text'],'htmlall','UTF-8' ));?>
</a></li>
			  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</ul>
	</div></div>
</div>
<?php }?>


<?php }
}
