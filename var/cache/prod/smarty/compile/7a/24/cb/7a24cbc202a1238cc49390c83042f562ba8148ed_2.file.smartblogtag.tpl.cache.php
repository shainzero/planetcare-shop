<?php
/* Smarty version 3.1.32, created on 2018-11-20 12:35:03
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\smartblogtag\views\templates\front\smartblogtag.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf3f167af5a02_16054339',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7a24cbc202a1238cc49390c83042f562ba8148ed' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\smartblogtag\\views\\templates\\front\\smartblogtag.tpl',
      1 => 1501167309,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf3f167af5a02_16054339 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '11595802315bf3f167ade8d9_17101044';
if (isset($_smarty_tpl->tpl_vars['tags']->value) && !empty($_smarty_tpl->tpl_vars['tags']->value)) {?>
<div  id="tags_blog_block_left"  class="block blogModule boxPlain">
    <div class="section-title">
    <h2 class='title_block'><a href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog'), ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Tags Post','mod'=>'smartblogtag'),$_smarty_tpl ) );?>
</a></h2>
    </div>
    <div class="block_content">
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tags']->value, 'tag');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['tag']->value) {
?>
            <?php $_smarty_tpl->_assignInScope('options', null);?>
                <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['tag'] = $_smarty_tpl->tpl_vars['tag']->value['alias'];
$_smarty_tpl->_assignInScope('options', $_tmp_array);?>
                <?php if ($_smarty_tpl->tpl_vars['tag']->value != '') {?>
                    <a href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_tag',$_smarty_tpl->tpl_vars['options']->value), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tag']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a>
                <?php }?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
   </div>
</div>
<?php }
}
}
