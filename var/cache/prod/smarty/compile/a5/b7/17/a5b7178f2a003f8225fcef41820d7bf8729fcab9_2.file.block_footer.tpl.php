<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:47
  from 'C:\xampp\htdocs\planetcare\modules\dor_managerblockfooter\block_footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280ab160238_63865165',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a5b7178f2a003f8225fcef41820d7bf8729fcab9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\modules\\dor_managerblockfooter\\block_footer.tpl',
      1 => 1480304166,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf280ab160238_63865165 (Smarty_Internal_Template $_smarty_tpl) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['staticblocks']->value, 'block', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['block']->value) {
?>
	<?php if ($_smarty_tpl->tpl_vars['block']->value['active'] == 1) {?>
		<p class ="title_block"> <?php ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['title'], ENT_QUOTES, 'UTF-8');
$_prefixVariable3 = ob_get_clean();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>$_prefixVariable3),$_smarty_tpl ) );?>
 </p>
	<?php }?>
	<?php echo $_smarty_tpl->tpl_vars['block']->value['description'];?>

	<?php if ($_smarty_tpl->tpl_vars['block']->value['insert_module'] == 1) {?>
	      <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_module'], ENT_QUOTES, 'UTF-8');?>

	 <?php }
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
