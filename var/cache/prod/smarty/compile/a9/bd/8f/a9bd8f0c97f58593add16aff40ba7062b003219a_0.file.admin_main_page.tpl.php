<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:24:27
  from 'C:\xampp\htdocs\planetcare\modules\dor_megamenu\views\templates\hook\admin_main_page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf2814b6b0c14_26055990',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a9bd8f0c97f58593add16aff40ba7062b003219a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\modules\\dor_megamenu\\views\\templates\\hook\\admin_main_page.tpl',
      1 => 1497151647,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf2814b6b0c14_26055990 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="page-content">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#megamenu-manager"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'MegaMenu Manager','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</a></li>
        <li><a data-toggle="tab" href="#widgets-manager"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Widgets Manager','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</a></li>
    </ul>
    <br>
    <div class="tab-content">
        <div id="megamenu-manager" class="tab-pane fade in active">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <form id="choice-menu-form" method="post" action="" enctype="multipart/form-data">
                        <?php echo $_smarty_tpl->tpl_vars['html_choices_select']->value;?>

                    </form>
                </div>
                <div class="col-md-8 col-sm-12">

                    <form id="menu-form" method="post" action="" enctype="multipart/form-data">
                        <h4><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Menu Structure','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</h4>
                        <p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Drag each item into the order you prefer. Click the action buttion on the right of the item to edit or delete menu. Click submenu settings to setting sub megamenu.','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</p>
                        <div id="menu-form-list">
                            <div class="menu-form-list-wrapper">
                                <?php echo $_smarty_tpl->tpl_vars['list_menu']->value;?>

                            </div>
                            <div class="clearfix"></div>
                            <button class="btn btn-primary save-menu-position" type="button"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Update Position','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div id="widgets-manager" class="tab-pane fade in">
            <form id="main-widget-form" method="post" action="" enctype="multipart/form-data">
                <button class="btn btn-primary add-widget" type="button"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add Widget','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</button>
                <div class="widget-list-items">
                    <?php echo $_smarty_tpl->tpl_vars['list_widgets']->value;?>

                </div>
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade bs-example-modal-lg" id="menuModal" tabindex="-1" role="dialog" data-aria-labelledby="menuModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="menuModalLabel"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Setting Sub Megamenu','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</h4>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Close','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</button>
                    <button type="button" class="btn btn-primary" id="save-button"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Save','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
    var id_shop = <?php echo $_smarty_tpl->tpl_vars['id_shop']->value;?>
;
    var ajaxurl = '<?php echo $_smarty_tpl->tpl_vars['ajaxurl']->value;?>
';
    var submenu_title = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Setting Sub Megamenu','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
";
    var editmenu_title = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Edit Menu','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
";
    var adminajaxurl = "<?php echo $_smarty_tpl->tpl_vars['adminajaxurl']->value;?>
";
    var secure_key = "<?php echo $_smarty_tpl->tpl_vars['secure_key']->value;?>
";
    var delete_text = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Do you want to delete this menu and all Sub menus?','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
";
    var addwidget_title = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'List Widgets','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
";
    var formwidget_title = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Widget Form','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
";
    var deletewidget_text = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Do you want to delete this widget?','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
";
    var selectrow_text = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please select a row','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
";
    var selectcolumn_text = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please select a column','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
";
    var addingwidget_text = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Loading ...','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
";
<?php echo '</script'; ?>
><?php }
}
