<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:38:28
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_megamenu\views\templates\hook\megamenu.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf284941920c8_99347517',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a9cea7385c46eb819779c6f006ce671d799e1deb' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_megamenu\\views\\templates\\hook\\megamenu.tpl',
      1 => 1497132526,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf284941920c8_99347517 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '13365810035bf2849418e694_85085772';
?>
<nav class="dor-megamenu col-lg-12 col-sx-12 col-sm-12">
    <div class="navbar navbar-default " role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle open_menu">
                <i class="material-icons">&#xE8FE;</i>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="dor-top-menu" class="collapse navbar-collapse navbar-ex1-collapse">
            <div class="close_menu" style="display:none;">
                <span class="btn-close"><i class="material-icons">&#xE14C;</i></span>
            </div>
            <div class="mobile-logo-menu hidden-lg hidden-md">
                <a href="<?php if (isset($_smarty_tpl->tpl_vars['force_ssl']->value) && $_smarty_tpl->tpl_vars['force_ssl']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url_ssl'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');
}?>" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
">
                    <img class="logo img-responsive" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['urls']->value['theme_assets'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
dorado/img/logo-menu.png" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
"/>
                </a>
            </div>
            <?php echo $_smarty_tpl->tpl_vars['output']->value;?>
        </div>
    </div>  
</nav><?php }
}
