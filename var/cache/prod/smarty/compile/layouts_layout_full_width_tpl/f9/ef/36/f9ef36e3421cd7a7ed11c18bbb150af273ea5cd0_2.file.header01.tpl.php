<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:46
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\templates\_partials\dorado\header\header01.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280aa26df08_91587140',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f9ef36e3421cd7a7ed11c18bbb150af273ea5cd0' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\templates\\_partials\\dorado\\header\\header01.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf280aa26df08_91587140 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5979508255bf280aa267321_28694467', 'header');
}
/* {block 'header_banner'} */
class Block_1814741325bf280aa267801_66117499 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

		  <div class="header-banner">
		    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayBanner'),$_smarty_tpl ) );?>

		  </div>
		<?php
}
}
/* {/block 'header_banner'} */
/* {block 'header_nav'} */
class Block_9253012075bf280aa26b311_83392137 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

		              <nav class="header-nav">
		                <div class="hidden-sm-down-">
		                  <div class="right-nav">
		                      <div class="nav-search-button">
		                      	<button type="button"><i class="pe-7s-search"></i></button>
		                      	<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'dorHeaderSearch', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'dorHeaderSearch'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
								<?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'dorHeaderSearch')) {?>
								  <div class="dorHeaderSearch-Wapper">
								    <div class="dor-headersearch-show">
								      <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'dorHeaderSearch');?>

								    </div>
								  </div>
								<?php }?>
		                      </div>
		                      
		                      <div class="dor-block-selection">
		                      	<div class="nav-setting-button"><button type="button"><i class="pe-7s-config"></i></button></div>
		                      	<div class="dor-setting-lists">
                          		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayNav1'),$_smarty_tpl ) );?>

		                      	</div>
		                      </div>
		                      <div class="nav-cart">
		                      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayNav2'),$_smarty_tpl ) );?>

		                      <div class="pull-xs-right" id="_mobile_cart"></div>
		                      </div>
		                  </div>
		                </div>
		                <div class="hidden-md-up text-xs-center mobile hidden">
		                  <div class="pull-xs-left" id="menu-icon">
		                    <i class="material-icons d-inline">&#xE5D2;</i>
		                  </div>
		                  <div class="pull-xs-right" id="_mobile_user_info"></div>
		                  
		                  <div class="clearfix"></div>
		                </div>
		              </nav>
		            <?php
}
}
/* {/block 'header_nav'} */
/* {block 'header_top'} */
class Block_13870982645bf280aa268252_70663286 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

		  <div class="header-top no-padding">
		    <div class="container">
		       <div class="row">
		       	<div class="col-md-2 dor-main-logo">
		       		<div class="main-logo-inner">
			       		<div class="main-logo-wrapper">
					        <div class="item-logo" id="_desktop_logo">
					         <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'index') {?>
					          <h1 class="h1-logo no-margin">
					            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
">
					              <img class="logo img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
">
					            </a>
					          </h1>
					          <?php } else { ?>
					          <div class="h1-logo no-margin">
					            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
">
					              <img class="logo img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
">
					            </a>
					          </div>
					          <?php }?>
					        </div>
					        <div class="item-logo top-logo" id="_mobile_logo"></div>
				        </div>
			        </div>
		        </div>
		        <div class="dor-mainmenu-inner col-md-8">
			        <div class="head-dormenu">
			            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayTop'),$_smarty_tpl ) );?>

			        </div>
		        </div>
		        <div class="dor-header-setting-inner col-md-2">
		        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'headerDorado1'),$_smarty_tpl ) );?>

		          <div class="head-dorsetting pull-right">
		            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9253012075bf280aa26b311_83392137', 'header_nav', $this->tplIndex);
?>

		          </div>
		        </div>
		      </div>
		      <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
		        <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
		        <div class="js-top-menu-bottom">
		          <div id="_mobile_contact_link"></div>
		        </div>
		      </div>
		    </div>
		  </div>
		  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayNavFullWidth'),$_smarty_tpl ) );?>

		<?php
}
}
/* {/block 'header_top'} */
/* {block 'header'} */
class Block_5979508255bf280aa267321_28694467 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_5979508255bf280aa267321_28694467',
  ),
  'header_banner' => 
  array (
    0 => 'Block_1814741325bf280aa267801_66117499',
  ),
  'header_top' => 
  array (
    0 => 'Block_13870982645bf280aa268252_70663286',
  ),
  'header_nav' => 
  array (
    0 => 'Block_9253012075bf280aa26b311_83392137',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<div id="dor-header01" class="header-content-wrapper dorheader01">
		<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1814741325bf280aa267801_66117499', 'header_banner', $this->tplIndex);
?>

		
		<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13870982645bf280aa268252_70663286', 'header_top', $this->tplIndex);
?>

	</div>
<?php
}
}
/* {/block 'header'} */
}
