<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:46
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\templates\_partials\stylesheets.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280aa07c039_50865988',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '624c95109e1c9ed760511ec0a3a3d437eeacee2b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\templates\\_partials\\stylesheets.tpl',
      1 => 1541767128,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf280aa07c039_50865988 (Smarty_Internal_Template $_smarty_tpl) {
?><link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['stylesheets']->value['external'], 'stylesheet');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['stylesheet']->value) {
?>
  <link rel="stylesheet" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['uri'], ENT_QUOTES, 'UTF-8');?>
" type="text/css" media="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['media'], ENT_QUOTES, 'UTF-8');?>
">
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">

<?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "product" || $_smarty_tpl->tpl_vars['page']->value['page_name'] == 'module-dorgallery-gallery' || $_smarty_tpl->tpl_vars['page']->value['page_name'] == 'module-dorgallery-gallery2') {?>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/libs/photoswipe.css?v=4.1.1-1.0.4" rel="stylesheet" />
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/libs/default-skin/default-skin.css?v=4.1.1-1.0.4" rel="stylesheet" />
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != '') {?>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/css/topbar.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/css/header.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/css/organick.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/css/aboutus.css" rel="stylesheet" type="text/css"/>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'contact') {?>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/css/contact-form.css" rel="stylesheet" type="text/css"/>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'module-smartblog-category' || $_smarty_tpl->tpl_vars['page']->value['page_name'] == 'module-smartblog-details' || $_smarty_tpl->tpl_vars['page']->value['page_name'] == 'module-smartblog-search' || $_smarty_tpl->tpl_vars['page']->value['page_name'] == 'module-smartblog-tagpost') {?>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/css/blog.css" rel="stylesheet" type="text/css"/>
<?php }
if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != '') {?>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/css/theme.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/css/responsive.css" rel="stylesheet" type="text/css"/>
<?php }
if (isset($_smarty_tpl->tpl_vars['dorthemecolor']->value) && $_smarty_tpl->tpl_vars['dorthemecolor']->value != '' && isset($_smarty_tpl->tpl_vars['dorEnableThemeColor']->value) && $_smarty_tpl->tpl_vars['dorEnableThemeColor']->value == 1) {?>
	<?php if ($_smarty_tpl->tpl_vars['pathTmpColor']->value == 1) {?>
	<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/css/color/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorthemecolor']->value, ENT_QUOTES, 'UTF-8');?>
.css" rel="stylesheet" type="text/css"/>
	<?php } else { ?>
	<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
modules/dor_themeoptions/css/color/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorthemecolor']->value, ENT_QUOTES, 'UTF-8');?>
.css" rel="stylesheet" type="text/css"/>
	<?php }
}
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['stylesheets']->value['inline'], 'stylesheet');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['stylesheet']->value) {
?>
  <style>
    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['stylesheet']->value['content'], ENT_QUOTES, 'UTF-8');?>

  </style>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
}
}
