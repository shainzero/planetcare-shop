<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:42:43
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\templates\errors\not-found.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf285939be5a3_26981174',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'aa1648f93596e9092cd7a027b66e99037f83b44a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\templates\\errors\\not-found.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf285939be5a3_26981174 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="pagenotfound">
	<div class="dorpagenotfound">	
		<div class="dorpagenotfound_inner">	
			<div class="dorpagenotfound_wrapper">	
				<div class="dorpagenotfound_data">	
				  <div class="logo-notfound align-center"><a href="#"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
img/cms/dorado/fix-logo.png"></a></div>
				  <div class="pagenotfound-info">
				    <h1>404</h1>
				    <h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"This's Not The Web Page You're Looking For"),$_smarty_tpl ) );?>
</h3>
				    <div class="comeback-home"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please try one of the following pages'),$_smarty_tpl ) );?>
<a href="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value['force_ssl']) && $_smarty_tpl->tpl_vars['urls']->value['force_ssl']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url_ssl'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');
}?>" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Home'),$_smarty_tpl ) );?>
"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Homepage'),$_smarty_tpl ) );?>
</span></a></div>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php }
}
