<?php
/* Smarty version 3.1.32, created on 2018-11-20 12:35:03
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\templates\smartblog\posts.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf3f16720f6d6_23327648',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c7c3283f8eae077d113a2c38512505ee5f9a3d2b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\templates\\smartblog\\posts.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./comment_loop.tpl' => 1,
  ),
),false)) {
function content_5bf3f16720f6d6_23327648 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17740760405bf3f166e35092_05701427', 'page_header_container');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18881225655bf3f166e36a53_85375156', 'page_content');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_header_container'} */
class Block_17740760405bf3f166e35092_05701427 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_header_container' => 
  array (
    0 => 'Block_17740760405bf3f166e35092_05701427',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content'} */
class Block_18881225655bf3f166e36a53_85375156 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content' => 
  array (
    0 => 'Block_18881225655bf3f166e36a53_85375156',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\xampp\\htdocs\\planetcare\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>

<div id="dor-blog-detail" class="center_column dor-two-cols col-xs-12 col-sm-9">
<h1 class="h1 hidden"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'News & Blog','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</h1>
<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'path', null, null);?><a href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog'), ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Blog list','mod'=>'smartblog'),$_smarty_tpl ) );?>
</a><span class="navigation-pipe"></span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8');
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
<div id="content" class="block">
   <div itemtype="#" itemscope="" id="sdsblogArticle" class="blog-post">
		<?php $_smarty_tpl->_assignInScope('catOptions', null);?>
		<?php $_tmp_array = isset($_smarty_tpl->tpl_vars['catOptions']) ? $_smarty_tpl->tpl_vars['catOptions']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['id_category'] = $_smarty_tpl->tpl_vars['id_category']->value;
$_smarty_tpl->_assignInScope('catOptions', $_tmp_array);?>
		<?php $_tmp_array = isset($_smarty_tpl->tpl_vars['catOptions']) ? $_smarty_tpl->tpl_vars['catOptions']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['slug'] = $_smarty_tpl->tpl_vars['cat_link_rewrite']->value;
$_smarty_tpl->_assignInScope('catOptions', $_tmp_array);?>
      	<div data-itemprop="articleBody">

      		<div class="blog-post-content-area blog-details">
                <!-- single-blog-start -->
                <div class="single-blog">
                    <div class="blog-image">
                        <?php $_smarty_tpl->_assignInScope('activeimgincat', '0');?>
	                    <?php $_smarty_tpl->_assignInScope('activeimgincat', $_smarty_tpl->tpl_vars['smartshownoimg']->value);?> 
	                    <?php if (($_smarty_tpl->tpl_vars['post_img']->value != "no" && $_smarty_tpl->tpl_vars['activeimgincat']->value == 0) || $_smarty_tpl->tpl_vars['activeimgincat']->value == 1) {?>
	                        <a id="post_images" href="#"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['thumb_image'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8');?>
"></a>
	                    <?php }?>
                    </div>
                    <div class="blog-content">
                        <div class="blog-info">
                            <span class="author-name">
                                <i class="fa fa-user"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"By"),$_smarty_tpl ) );?>
 
                                <a href="#"><?php if ($_smarty_tpl->tpl_vars['smartshowauthor']->value == 1) {?><span data-itemprop="author"><?php if ($_smarty_tpl->tpl_vars['smartshowauthorstyle']->value != 0) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['firstname']->value, ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lastname']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['lastname']->value, ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['firstname']->value, ENT_QUOTES, 'UTF-8');
}?></span><?php }?></a>
                            </span>
                            <span class="blog-date">
                               <a href="#">
                                    <span class="month-date"><small><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['created'],"%B %d, %Y"), ENT_QUOTES, 'UTF-8');?>
</small></span>
                               </a>
                           </span>
                           <span class="comments-number">
                                <i class="fa fa-comment"></i><?php if ($_smarty_tpl->tpl_vars['countcomment']->value != '') {
echo htmlspecialchars($_smarty_tpl->tpl_vars['countcomment']->value, ENT_QUOTES, 'UTF-8');
} else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'0','mod'=>'smartblog'),$_smarty_tpl ) );
}?> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Comment"),$_smarty_tpl ) );?>

                            </span>
                        </div>
                        <div class="title-desc">
                            <h4><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['meta_title']->value, ENT_QUOTES, 'UTF-8');?>
</h4>
                            <div class="content-blog-detail"><?php echo $_smarty_tpl->tpl_vars['content']->value;?>
</div>
                        </div>
                    </div>
                    <div class="blog-detail-footer clearfix">
                    	<?php if ($_smarty_tpl->tpl_vars['tags']->value) {?>
                       <span class="single-post-tags pull-left">
                            <i class="fa fa-tags"></i>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['tags']->value, 'tag');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['tag']->value) {
?>
	                            <?php $_smarty_tpl->_assignInScope('options', null);?>
	                            <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['tag'] = $_smarty_tpl->tpl_vars['tag']->value['name'];
$_smarty_tpl->_assignInScope('options', $_tmp_array);?>
	                            <a title="tag" href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_tag',$_smarty_tpl->tpl_vars['options']->value), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tag']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a>
	                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </span>
                        <?php }?>
                        <div class="share-blog-widget pull-right">
				            <span class="blog-icon-share"><i class="fa fa-share-alt" aria-hidden="true"></i></span><a href="#"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Share this post"),$_smarty_tpl ) );?>
</a>
				        </div>
                    </div>
                </div>
                <!-- single-blog-end -->
                <?php if ($_smarty_tpl->tpl_vars['smartshowauthor']->value == 1) {?>
                <div class="blog-detail-author clearfix">
                	<div class="author-media-info pull-left">
                		<div><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
img/cms/dorado/blog/avatar_user.jpg" alt=""></div>
                	</div>
                	<div class="author-content-info pull-left">
                		<h3><?php if ($_smarty_tpl->tpl_vars['smartshowauthorstyle']->value != 0) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['firstname']->value, ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lastname']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['lastname']->value, ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['firstname']->value, ENT_QUOTES, 'UTF-8');
}?></h3>
                		<div>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Indemon strunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus.</div>
                		<div class="shere-post">
					        <ul>
					            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
					            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
					            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
					            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
					        </ul>
					    </div>
                	</div>
                </div>
                <?php }?>
            </div>
      </div>
     
      <div class="sdsarticleBottom">
        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['HOOK_SMART_BLOG_POST_FOOTER']->value, ENT_QUOTES, 'UTF-8');?>

      </div>
   </div>
<div class="shere-button-area clearfix">
    <div class="nav-button pull-left">
        <a href="#">
            <i class="fa fa-caret-left"></i><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Previous"),$_smarty_tpl ) );?>
</span>
        </a>
    </div>
    
    <div class="nav-button pull-right">
        <a href="#">
            <span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Next"),$_smarty_tpl ) );?>
</span><i class="fa fa-caret-right"></i>
        </a>
    </div>
</div>
<?php if ($_smarty_tpl->tpl_vars['countcomment']->value != '') {?>
<div id="articleComments" class="clearfix">
            <h3 class="total-comment">
            	<?php if ($_smarty_tpl->tpl_vars['countcomment']->value > 1) {?>
            		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Comments','mod'=>'smartblog'),$_smarty_tpl ) );?>

            	<?php } else { ?>
            		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Comment','mod'=>'smartblog'),$_smarty_tpl ) );?>

            	<?php }?>
            	<span>
            	<?php if ($_smarty_tpl->tpl_vars['countcomment']->value != '') {?>
            		<?php if ($_smarty_tpl->tpl_vars['countcomment']->value < 10) {?>
            			(0<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countcomment']->value, ENT_QUOTES, 'UTF-8');?>
)
            		<?php } else { ?>
            			(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countcomment']->value, ENT_QUOTES, 'UTF-8');?>
)
            		<?php }?>
            	<?php } else { ?>
            		(<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'0','mod'=>'smartblog'),$_smarty_tpl ) );?>
)
            	<?php }?>
            	
            	</span>
            </h3>
        <div id="comments">      
            <ul class="commentList">
                  <?php $_smarty_tpl->_assignInScope('i', 1);?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['comments']->value, 'comment');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->value) {
?>
                    
                       <?php $_smarty_tpl->_subTemplateRender("file:./comment_loop.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('childcommnets'=>$_smarty_tpl->tpl_vars['comment']->value), 0, true);
?>
                   
                  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </ul>
        </div>
</div>
 <?php }?>

</div>
<?php if (Configuration::get('smartenablecomment') == 1) {
if ($_smarty_tpl->tpl_vars['comment_status']->value == 1) {?>
<div class="smartblogcomments clearfix" id="respond">
    <!-- <h4 id="commentTitle"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Leave a Comment",'mod'=>"smartblog"),$_smarty_tpl ) );?>
</h4> -->
    	<h4 class="comment-reply-title" id="reply-title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Leave a comment",'mod'=>"smartblog"),$_smarty_tpl ) );?>
 <small style="float:right;">
                <a style="display: none;" href="/wp/sellya/sellya/this-is-a-post-with-preview-image/#respond" 
                   id="cancel-comment-reply-link" rel="nofollow"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Cancel Reply",'mod'=>"smartblog"),$_smarty_tpl ) );?>
</a>
            </small>
        </h4>
		<div id="commentInput">
			<form action="" method="post" id="commentform">
			<div>
				
				<div class="field-cmt">
					<div class="row">
						<div class="field-mini-row col-lg-4 col-sm-4 col-sx-12">
							<input type="text" tabindex="1" class="inputName form-control cmt-field" value="" name="name" placeholder='<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Name",'mod'=>"smartblog"),$_smarty_tpl ) );?>
'>
						</div>
						<div class="field-mini-row col-lg-4 col-sm-4 col-sx-12">
							<input type="text" tabindex="2" class="inputMail form-control cmt-field" value="" name="mail" placeholder='<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Email",'mod'=>"smartblog"),$_smarty_tpl ) );?>
'>
						</div>
						<div class="field-mini-row col-lg-4 col-sm-4 col-sx-12">
							<input type="text" tabindex="3" value="" name="website" class="form-control grey" placeholder='<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Website",'mod'=>"smartblog"),$_smarty_tpl ) );?>
'>
						</div>
					</div>
				</div>
				<div class="field-cmt clearfix">
					<textarea tabindex="4" class="inputContent form-control cmt-field" rows="8" cols="50" name="comment" placeholder='<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Comment",'mod'=>"smartblog"),$_smarty_tpl ) );?>
'></textarea>
				</div>
			</div>
			<?php if (Configuration::get('smartcaptchaoption') == '1') {?>
			<div class="captcha-blog clearfix">
				<div class="captcha-blog-image"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
modules/smartblog/classes/CaptchaSecurityImages.php?width=100&height=40&characters=5"></div>
				<div class="col-lg-6 col-sm-6 col-xs-12 row">
					<input placeholder='<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Enter Captcha",'mod'=>"smartblog"),$_smarty_tpl ) );?>
' type="text" tabindex="" value="" name="smartblogcaptcha" class="smartblogcaptcha form-control cmt-field">
				</div>
			</div>
			<?php }?>
                 <input type='hidden' name='comment_post_ID' value='1478' id='comment_post_ID' />
                  <input type='hidden' name='id_post' value='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_post']->value, ENT_QUOTES, 'UTF-8');?>
' id='id_post' />

                <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
			<div class="button-submit-comment clearfix">
		        <div class="submit">
		            <input type="submit" name="addComment" id="submitComment" class="bbutton btn btn-default button-medium" value="Post Comment">
				</div>
			</div>
        </form>
	</div>
</div>
</div>
<div id="dor-smart-blog-right-sidebar" class="col-xs-12 col-sm-3 column">
	<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'displaySmartBlogRight', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displaySmartBlogRight'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
	  <?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'displaySmartBlogRight')) {?>
	    <div class="displaySmartBlogRight">
	      <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'displaySmartBlogRight');?>

	    </div>
	<?php }?>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
setTimeout(function(){
jQuery(document).ready(function(){


$('#submitComment').bind('click',function(event) {
event.preventDefault();
 
 
var data = { 'action':'postcomment', 
'id_post':$('input[name=\'id_post\']').val(),
'comment_parent':$('input[name=\'comment_parent\']').val(),
'name':$('input[name=\'name\']').val(),
'website':$('input[name=\'website\']').val(),
'smartblogcaptcha':$('input[name=\'smartblogcaptcha\']').val(),
'comment':$('textarea[name=\'comment\']').val(),
'mail':$('input[name=\'mail\']').val() };
	$.ajax( {
	  url: prestashop.urls.base_url + 'modules/smartblog/ajax.php',
	  data: data,
	  
	  dataType: 'json',
	  
	  beforeSend: function() {
				$('.success, .warning, .error').remove();
				$('#submitComment').attr('disabled', true);
				$('#commentInput').before('<div class="attention"><img src="http://321cart.com/sellya/catalog/view/theme/default/image/loading.gif" alt="" />Please wait!</div>');

				},
				complete: function() {
				$('#submitComment').attr('disabled', false);
				$('.attention').remove();
				},
		success: function(json) {
			if (json['error']) {
					 
						$('#commentInput').before('<div class="warning">' + '<i class="icon-warning-sign icon-lg"></i>' + json['error']['common'] + '</div>');
						
						if (json['error']['name']) {
							$('.inputName').after('<span class="error">' + json['error']['name'] + '</span>');
						}
						if (json['error']['mail']) {
							$('.inputMail').after('<span class="error">' + json['error']['mail'] + '</span>');
						}
						if (json['error']['comment']) {
							$('.inputContent').after('<span class="error">' + json['error']['comment'] + '</span>');
						}
						if (json['error']['captcha']) {
							$('.smartblogcaptcha').after('<span class="error">' + json['error']['captcha'] + '</span>');
						}
					}
					
					if (json['success']) {
						$('input[name=\'name\']').val('');
						$('input[name=\'mail\']').val('');
						$('input[name=\'website\']').val('');
						$('textarea[name=\'comment\']').val('');
				 		$('input[name=\'smartblogcaptcha\']').val('');
					
						$('#commentInput').before('<div class="success">' + json['success'] + '</div>');
						setTimeout(function(){
							$('.success').fadeOut(300).delay(450).remove();
													},2500);
					
					}
				}
			} );
		} );
		
 




    var addComment = {
	moveForm : function(commId, parentId, respondId, postId) {

		var t = this, div, comm = t.I(commId), respond = t.I(respondId), cancel = t.I('cancel-comment-reply-link'), parent = t.I('comment_parent'), post = t.I('comment_post_ID');

		if ( ! comm || ! respond || ! cancel || ! parent )
			return;
 
		t.respondId = respondId;
		postId = postId || false;

		if ( ! t.I('wp-temp-form-div') ) {
			div = document.createElement('div');
			div.id = 'wp-temp-form-div';
			div.style.display = 'none';
			respond.parentNode.insertBefore(div, respond);
		}


		comm.parentNode.insertBefore(respond, comm.nextSibling);
		if ( post && postId )
			post.value = postId;
		parent.value = parentId;
		cancel.style.display = '';

		cancel.onclick = function() {
			var t = addComment, temp = t.I('wp-temp-form-div'), respond = t.I(t.respondId);

			if ( ! temp || ! respond )
				return;

			t.I('comment_parent').value = '0';
			temp.parentNode.insertBefore(respond, temp);
			temp.parentNode.removeChild(temp);
			this.style.display = 'none';
			this.onclick = null;
			return false;
		};

		try { t.I('comment').focus(); }
		catch(e) {}

		return false;
	},

	I : function(e) {
		return document.getElementById(e);
	}
};
});
   },2000)   
      
<?php echo '</script'; ?>
>
<?php }
}
if (isset($_smarty_tpl->tpl_vars['smartcustomcss']->value)) {?>
    <style>
        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['smartcustomcss']->value, ENT_QUOTES, 'UTF-8');?>

    </style>
<?php }
}
}
/* {/block 'page_content'} */
}
