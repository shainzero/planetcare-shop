<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:47
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\templates\_partials\dor-subscribe-popup.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280ab4c34f5_88352011',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '98a4ce031d144e21b38db8a727022761663a0b75' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\templates\\_partials\\dor-subscribe-popup.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf280ab4c34f5_88352011 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- Subscribe Popup 1 -->
<section class="subscribe-me">
    <a href="#close" onclick="return false" class="sb-close-btn close popup-cls b-close"><i class="fa-times fa"></i></a>      
    <div class="modal-content <?php if ($_smarty_tpl->tpl_vars['dorSubsPop']->value == 1) {?>subscribe-1 wht-clr<?php } else { ?>subscribe-2 blk-clr<?php }?>">   
        <div class="login-wrap text-center">
            <h2><i class="fa fa-send-o" aria-hidden="true"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Join Our Newsletter','mod'=>'blocknewsletter'),$_smarty_tpl ) );?>
</h2>
            <p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'sign up for our newsletter and get','mod'=>'blocknewsletter'),$_smarty_tpl ) );?>
 <span>25%</span> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'off your next order. Pretty sweet, we know','mod'=>'blocknewsletter'),$_smarty_tpl ) );?>
</p>
            <div class="login-form spctop-30"> 
                <form class="subscribe" action="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getPageLink('index',null,null,null,false,null,true),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" method="post">
                    <div class="form-group<?php if (isset($_smarty_tpl->tpl_vars['msg']->value) && $_smarty_tpl->tpl_vars['msg']->value) {?> <?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>form-error<?php } else { ?>form-ok<?php }
}?>" >
                        <input class="inputNew form-control grey newsletter-input" id="dorNewsletter-input" type="text" name="email" size="18" value=""  placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your email address','mod'=>'blocknewsletter'),$_smarty_tpl ) );?>
"/>
                    </div>
                    <div class="form-group">
                        <button class="alt fancy-button" type="submit" name="submitNewsletter"> <span class="fa fa-envelope"></span> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Subscribe','mod'=>'blocknewsletter'),$_smarty_tpl ) );?>
 </button>
                        <input type="hidden" name="action" value="0" />
                    </div>
                    <div class="form-group checkAgainSubs"><input type="checkbox" name="notShowSubs"> <span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Don't show this popup again",'mod'=>'blocknewsletter'),$_smarty_tpl ) );?>
</span></div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- / Subscribe Popup 1 --><?php }
}
