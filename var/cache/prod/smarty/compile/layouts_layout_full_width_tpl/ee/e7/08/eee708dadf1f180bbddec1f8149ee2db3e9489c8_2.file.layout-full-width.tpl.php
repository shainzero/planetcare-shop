<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:45
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\templates\layouts\layout-full-width.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280a9aea7d6_87651643',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'eee708dadf1f180bbddec1f8149ee2db3e9489c8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\templates\\layouts\\layout-full-width.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf280a9aea7d6_87651643 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4171868495bf280a9ae3f95_44121601', 'left_column');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7257690285bf280a9ae4651_50548942', 'right_column');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12475721825bf280a9ae4be2_24639392', 'content_wrapper');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'layouts/layout-both-columns.tpl');
}
/* {block 'left_column'} */
class Block_4171868495bf280a9ae3f95_44121601 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_4171868495bf280a9ae3f95_44121601',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'left_column'} */
/* {block 'right_column'} */
class Block_7257690285bf280a9ae4651_50548942 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'right_column' => 
  array (
    0 => 'Block_7257690285bf280a9ae4651_50548942',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'right_column'} */
/* {block 'content'} */
class Block_18791521725bf280a9ae4f72_27648739 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <p>Hello world! This is HTML5 Boilerplate.</p>
    <?php
}
}
/* {/block 'content'} */
/* {block 'product_footer'} */
class Block_3461841725bf280a9ae5d00_72169663 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <div class="dor-before-product clearfix">
      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterProduct','product'=>$_smarty_tpl->tpl_vars['product']->value,'category'=>$_smarty_tpl->tpl_vars['category']->value),$_smarty_tpl ) );?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'dorRelatedProductByBrand','product'=>$_smarty_tpl->tpl_vars['product']->value,'category'=>$_smarty_tpl->tpl_vars['category']->value),$_smarty_tpl ) );?>

    </div>
  <?php
}
}
/* {/block 'product_footer'} */
/* {block 'content_wrapper'} */
class Block_12475721825bf280a9ae4be2_24639392 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_12475721825bf280a9ae4be2_24639392',
  ),
  'content' => 
  array (
    0 => 'Block_18791521725bf280a9ae4f72_27648739',
  ),
  'product_footer' => 
  array (
    0 => 'Block_3461841725bf280a9ae5d00_72169663',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div id="content-wrapper">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18791521725bf280a9ae4f72_27648739', 'content', $this->tplIndex);
?>

  </div>
<?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product') {?>
  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3461841725bf280a9ae5d00_72169663', 'product_footer', $this->tplIndex);
?>

<?php }
}
}
/* {/block 'content_wrapper'} */
}
