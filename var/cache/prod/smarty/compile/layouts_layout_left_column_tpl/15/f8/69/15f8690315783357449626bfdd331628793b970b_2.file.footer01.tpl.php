<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:54:18
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\templates\_partials\dorado\footer\footer01.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf2884a9025d1_41557526',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '15f8690315783357449626bfdd331628793b970b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\templates\\_partials\\dorado\\footer\\footer01.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf2884a9025d1_41557526 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<div class="dorFooterMain">
  <div class="container">
    <div class="row">
      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'doradoFooterTop'),$_smarty_tpl ) );?>

    </div>
  </div>
  <div class="footer-container dorFooterInner">
    <div class="container">
    	<div class="row">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9755155885bf2884a900020_00892208', 'hook_doradoFooter1');
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9150214965bf2884a900aa7_75058886', 'hook_footer_before');
?>

      </div>
      <div class="row">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10512290445bf2884a901427_30142786', 'hook_footer_after');
?>

      </div>
    </div>
  </div>
  <div class="doradoFooterAdv animatedParent animateOnce clearfix">
    <div class="container">
      <div class="row animated growIn">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7938020955bf2884a901d86_88612511', 'hook_doradoFooterAdv');
?>

      </div>
    </div>
  </div>
</div><?php }
/* {block 'hook_doradoFooter1'} */
class Block_9755155885bf2884a900020_00892208 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_doradoFooter1' => 
  array (
    0 => 'Block_9755155885bf2884a900020_00892208',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'doradoFooter1'),$_smarty_tpl ) );?>

        <?php
}
}
/* {/block 'hook_doradoFooter1'} */
/* {block 'hook_footer_before'} */
class Block_9150214965bf2884a900aa7_75058886 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_before' => 
  array (
    0 => 'Block_9150214965bf2884a900aa7_75058886',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterBefore'),$_smarty_tpl ) );?>

        <?php
}
}
/* {/block 'hook_footer_before'} */
/* {block 'hook_footer_after'} */
class Block_10512290445bf2884a901427_30142786 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_footer_after' => 
  array (
    0 => 'Block_10512290445bf2884a901427_30142786',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayFooterAfter'),$_smarty_tpl ) );?>

        <?php
}
}
/* {/block 'hook_footer_after'} */
/* {block 'hook_doradoFooterAdv'} */
class Block_7938020955bf2884a901d86_88612511 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_doradoFooterAdv' => 
  array (
    0 => 'Block_7938020955bf2884a901d86_88612511',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'doradoFooterAdv'),$_smarty_tpl ) );?>

      <?php
}
}
/* {/block 'hook_doradoFooterAdv'} */
}
