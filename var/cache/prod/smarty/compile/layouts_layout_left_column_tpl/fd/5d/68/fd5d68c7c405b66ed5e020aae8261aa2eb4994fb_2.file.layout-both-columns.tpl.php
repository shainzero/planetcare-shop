<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:54:17
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\templates\layouts\layout-both-columns.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf288497c5595_19167558',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fd5d68c7c405b66ed5e020aae8261aa2eb4994fb' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\templates\\layouts\\layout-both-columns.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/head.tpl' => 1,
    'file:catalog/_partials/product-activation.tpl' => 1,
    'file:_partials/header.tpl' => 1,
    'file:_partials/notifications.tpl' => 1,
    'file:_partials/breadcrumb.tpl' => 1,
    'file:_partials/footer.tpl' => 1,
    'file:_partials/javascript.tpl' => 1,
    'file:_partials/dor-subscribe-popup.tpl' => 1,
  ),
),false)) {
function content_5bf288497c5595_19167558 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!doctype html>
<html lang="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
">

  <head>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1511313985bf2884978c618_12571544', 'head');
?>

  </head>
  <?php if (isset($_GET['detail_col']) && $_GET['detail_col'] != '') {?>
    <?php $_smarty_tpl->_assignInScope('dorDetailCols', "proDetailCol".((string)$_GET['detail_col']));?>
  <?php }?>
  <?php if (isset($_GET['cate_type']) && $_GET['cate_type'] != '') {?>
    <?php $_smarty_tpl->_assignInScope('dorCategoryCols', "proCateCol".((string)$_GET['cate_type']));?>
  <?php }?>
  <?php if (isset($_GET['cate_type']) && $_GET['cate_type'] != '' && $_GET['cate_type'] == 1) {?>
    <?php $_smarty_tpl->_assignInScope('proCateRowNumber', "4");?>
    <?php if (isset($_GET['cate_row']) && $_GET['cate_row'] != '') {?>
      <?php $_smarty_tpl->_assignInScope('proCateRowNumber', ((string)$_GET['cate_row']));?>
    <?php }?>
    <?php if (isset($_GET['cate_row']) && $_GET['cate_row'] != '' && $_GET['cate_row'] > 6) {?>
      <?php $_smarty_tpl->_assignInScope('proCateRowNumber', "4");?>
    <?php }?>
  <?php }?>

  <?php if (isset($_GET['topbar']) && $_GET['topbar'] != '') {?>
    <?php $_smarty_tpl->_assignInScope('dorTopbarSkin', "topbar".((string)$_GET['topbar']));?>
  <?php }?>
  <?php if (isset($_GET['header']) && $_GET['header'] != '') {?>
    <?php $_smarty_tpl->_assignInScope('dorHeaderSkin', "header".((string)$_GET['header']));?>
  <?php }?>
  <body id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page_name'], ENT_QUOTES, 'UTF-8');?>
" class="<?php if (isset($_smarty_tpl->tpl_vars['dorthemebg']->value) && $_smarty_tpl->tpl_vars['dorthemebg']->value != '') {
echo htmlspecialchars($_smarty_tpl->tpl_vars['dorthemebg']->value, ENT_QUOTES, 'UTF-8');?>
 <?php }
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'classnames' ][ 0 ], array( $_smarty_tpl->tpl_vars['page']->value['body_classes'] )), ENT_QUOTES, 'UTF-8');
if (isset($_smarty_tpl->tpl_vars['dorCategoryCols']->value) && $_smarty_tpl->tpl_vars['dorCategoryCols']->value) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorCategoryCols']->value, ENT_QUOTES, 'UTF-8');
}
if (isset($_smarty_tpl->tpl_vars['dorDetailCols']->value) && $_smarty_tpl->tpl_vars['dorDetailCols']->value) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorDetailCols']->value, ENT_QUOTES, 'UTF-8');
}
if (isset($_smarty_tpl->tpl_vars['dorthemebg']->value) && $_smarty_tpl->tpl_vars['dorthemebg']->value) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorthemebg']->value, ENT_QUOTES, 'UTF-8');
}?> <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category' && isset($_smarty_tpl->tpl_vars['proCateRowNumber']->value) && $_smarty_tpl->tpl_vars['proCateRowNumber']->value != '') {?>proCateRowNumber<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['proCateRowNumber']->value, ENT_QUOTES, 'UTF-8');
}?> <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'category' && isset($_smarty_tpl->tpl_vars['proCateTypePage']->value) && $_smarty_tpl->tpl_vars['proCateTypePage']->value == 2) {?>showLoadmore<?php }?>">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7296806355bf288497a1584_72747984', 'hook_after_body_opening_tag');
?>

    <main<?php if (isset($_smarty_tpl->tpl_vars['dorlayoutmode']->value) && $_smarty_tpl->tpl_vars['dorlayoutmode']->value != '') {?> class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorlayoutmode']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9952481915bf288497a3988_08249603', 'product_activation');
?>

      <?php if (isset($_smarty_tpl->tpl_vars['dorTopbarSkin']->value) && $_smarty_tpl->tpl_vars['dorTopbarSkin']->value != '' && $_smarty_tpl->tpl_vars['page']->value['page_name'] != 'pagenotfound') {?>
      <section id="topbar" class="dor-topbar-main">
        <?php $_smarty_tpl->_assignInScope('urlTopbar', (("_partials/dorado/topbar/").($_smarty_tpl->tpl_vars['dorTopbarSkin']->value)).(".tpl"));?> 
        <?php $_smarty_tpl->_subTemplateRender($_smarty_tpl->tpl_vars['urlTopbar']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
      </section>
      <?php }?>
      <header id="header" class="header-absolute">
          <?php $_smarty_tpl->_subTemplateRender('file:_partials/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      </header>
      
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9439500505bf288497a7e62_66287622', 'notifications');
?>

      <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'index') {?>
      <section id="dor-homeslider">
        <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'dorHomeSlider', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'dorHomeSlider'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
        <?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'dorHomeSlider')) {?>
          <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'dorHomeSlider');?>

        <?php }?>
      </section>

      <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'blockDorado1', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'blockDorado1'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
      <?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'blockDorado1')) {?>
        <div class="blockDorado1 blockPosition dor-bg-white">
          <div class="container">
            <div class="row">
            <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'blockDorado1');?>

            </div>
          </div>
        </div>
      <?php }?>
      <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'blockDorado2', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'blockDorado2'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
      <?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'blockDorado2')) {?>
        <div class="blockDorado2 blockPosition dor-bg-white">
            <div class="container">
              <div class="row">
              <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'blockDorado2');?>

              </div>
            </div>
        </div>
      <?php }?>
      <div id="group-show-home" class="clearfix">
        <div class="container">
          <div class="row">
          <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'dorDailyDeal', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'dorDailyDeal'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
          <?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'dorDailyDeal')) {?>
            <div class="dorDailyDeal">
                <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'dorDailyDeal');?>

            </div>
          <?php }?>
          </div>
        </div>
      </div>
      
      <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'DorTabProductCate01', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'DorTabProductCate01'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
      <?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'DorTabProductCate01')) {?>
        <div class="DorTabProductCate01 blockPosition dor-bg-white">
          <div class="container">
            <div class="row">
            <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'DorTabProductCate01');?>

            </div>
          </div>
        </div>
      <?php }?>
      
      <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'blockDorado3', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'blockDorado3'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
      <?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'blockDorado3')) {?>
        <div class="blockDorado3 blockPosition dor-bg-white">
          <div class="container">
            <div class="row">
            <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'blockDorado3');?>

            </div>
          </div>
        </div>
      <?php }?>
      <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'dorBizproduct', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'dorBizproduct'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
      <?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'dorBizproduct')) {?>
        <div class="dorBizproduct blockPosition dor-bg-white">
          <div class="container">
            <div class="row">
            <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'dorBizproduct');?>

            </div>
          </div>
        </div>
      <?php }?>
      <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'DorTestimonial', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'DorTestimonial'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
      <?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'DorTestimonial')) {?>
        <div class="DorTestimonial blockPosition dor-bg-white">
          <div class="container">
            <div class="row">
            <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'DorTestimonial');?>

            </div>
          </div>
        </div>
      <?php }?>
      <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'DorHomeLatestNews', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'DorHomeLatestNews'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
      <?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'DorHomeLatestNews')) {?>
        <div class="DorHomeLatestNews blockPosition dor-bg-white">
          <div class="container">
            <div class="row">
            <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'DorHomeLatestNews');?>

            </div>
          </div>
        </div>
      <?php }?>

      <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'blockDorado10', null, null);
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'blockDorado10'),$_smarty_tpl ) );
$_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>
      <?php if ($_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'blockDorado10')) {?>
        <div class="blockDorado10 blockPosition dor-bg-white">
          <div class="container">
            <div class="row">
            <?php echo $_smarty_tpl->smarty->ext->_capture->getBuffer($_smarty_tpl, 'blockDorado10');?>

            </div>
          </div>
        </div>
      <?php }?>

      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'index') {?>
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15098191595bf288497b7ed0_16827889', 'breadcrumb');
?>

      <?php }?>
      <section id="wrapper">
        <div class="container">
          <div class="row">
          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17428791255bf288497b8c08_41903728', "left_column");
?>


          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3345796565bf288497ba4e4_48944687', "content_wrapper");
?>


          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20926562525bf288497bb316_48938394', "right_column");
?>

          </div>
        </div>
      </section>

      <footer id="footer">
        <?php $_smarty_tpl->_subTemplateRender("file:_partials/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      </footer>

    </main>
    <?php if (isset($_smarty_tpl->tpl_vars['dorOptReload']->value) && $_smarty_tpl->tpl_vars['dorOptReload']->value == 1) {?>
      <div class="dor-page-loading">
          <div id="loader"></div>
          <div class="loader-section section-left"></div>
          <div class="loader-section section-right"></div>
      </div>
    <?php }?>
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'dorSmartuser'),$_smarty_tpl ) );?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3766306455bf288497bdc89_79447914', 'javascript_bottom');
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9607356535bf288497beac1_84856370', 'hook_before_body_closing_tag');
?>

    <?php if (isset($_smarty_tpl->tpl_vars['dorSubscribe']->value) && $_smarty_tpl->tpl_vars['dorSubscribe']->value == 1) {?>
      <?php $_smarty_tpl->_subTemplateRender("file:_partials/dor-subscribe-popup.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php }?>
    <div id="to-top" class="to-top"> <i class="fa fa-angle-up"></i> </div>
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'dorthemeoptions'),$_smarty_tpl ) );?>

    <?php if (call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['page']->value['page_name'],'html','UTF-8' )) == 'contact') {?>
    <?php echo '<script'; ?>
 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMH_Sh8EdCWkG1OFhAih3FFhbkRYuo-0U"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/js/jquery.googlemap.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript">
      $(document).ready(function(){
        $("#mapContact").googleMap();
        $("#mapContact").addMarker({
            coords: [<?php if (isset($_smarty_tpl->tpl_vars['DorLatitude']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['DorLatitude']->value, ENT_QUOTES, 'UTF-8');
}?>, <?php if (isset($_smarty_tpl->tpl_vars['DorLongitude']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['DorLongitude']->value, ENT_QUOTES, 'UTF-8');
}?>],
            icon: prestashop.urls.base_url+'img/cms/dorado/icon/market-map.png',
            url: '<?php if (isset($_smarty_tpl->tpl_vars['DorMapUrl']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['DorMapUrl']->value, ENT_QUOTES, 'UTF-8');
}?>'
          });
      });
    <?php echo '</script'; ?>
>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "product" || $_smarty_tpl->tpl_vars['page']->value['page_name'] == 'module-dorgallery-gallery' || $_smarty_tpl->tpl_vars['page']->value['page_name'] == 'module-dorgallery-gallery2') {?>
    <?php echo '<script'; ?>
 src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/libs/photoswipe.js?v=4.1.1-1.0.4"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['theme_assets'], ENT_QUOTES, 'UTF-8');?>
dorado/libs/photoswipe-ui-default.min.js?v=4.1.1-1.0.4"><?php echo '</script'; ?>
>
    <?php }?>
  </body>

</html>
<?php }
/* {block 'head'} */
class Block_1511313985bf2884978c618_12571544 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_1511313985bf2884978c618_12571544',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php $_smarty_tpl->_subTemplateRender('file:_partials/head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php
}
}
/* {/block 'head'} */
/* {block 'hook_after_body_opening_tag'} */
class Block_7296806355bf288497a1584_72747984 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_after_body_opening_tag' => 
  array (
    0 => 'Block_7296806355bf288497a1584_72747984',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayAfterBodyOpeningTag'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_after_body_opening_tag'} */
/* {block 'product_activation'} */
class Block_9952481915bf288497a3988_08249603 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_activation' => 
  array (
    0 => 'Block_9952481915bf288497a3988_08249603',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/product-activation.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'product_activation'} */
/* {block 'notifications'} */
class Block_9439500505bf288497a7e62_66287622 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'notifications' => 
  array (
    0 => 'Block_9439500505bf288497a7e62_66287622',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:_partials/notifications.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'notifications'} */
/* {block 'breadcrumb'} */
class Block_15098191595bf288497b7ed0_16827889 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'breadcrumb' => 
  array (
    0 => 'Block_15098191595bf288497b7ed0_16827889',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php $_smarty_tpl->_subTemplateRender('file:_partials/breadcrumb.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
      <?php
}
}
/* {/block 'breadcrumb'} */
/* {block "left_column"} */
class Block_17428791255bf288497b8c08_41903728 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'left_column' => 
  array (
    0 => 'Block_17428791255bf288497b8c08_41903728',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
              <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product') {?>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayLeftColumnProduct'),$_smarty_tpl ) );?>

              <?php } else { ?>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayLeftColumn"),$_smarty_tpl ) );?>

              <?php }?>
            </div>
          <?php
}
}
/* {/block "left_column"} */
/* {block "content"} */
class Block_11836465115bf288497ba9b1_76456154 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <p>Hello world! This is HTML5 Boilerplate.</p>
              <?php
}
}
/* {/block "content"} */
/* {block "content_wrapper"} */
class Block_3345796565bf288497ba4e4_48944687 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_3345796565bf288497ba4e4_48944687',
  ),
  'content' => 
  array (
    0 => 'Block_11836465115bf288497ba9b1_76456154',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div id="content-wrapper" class="left-column right-column col-sm-4 col-md-6">
              <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11836465115bf288497ba9b1_76456154', "content", $this->tplIndex);
?>

            </div>
          <?php
}
}
/* {/block "content_wrapper"} */
/* {block "right_column"} */
class Block_20926562525bf288497bb316_48938394 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'right_column' => 
  array (
    0 => 'Block_20926562525bf288497bb316_48938394',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
              <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == 'product') {?>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayRightColumnProduct'),$_smarty_tpl ) );?>

              <?php } else { ?>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayRightColumn"),$_smarty_tpl ) );?>

              <?php }?>
            </div>
          <?php
}
}
/* {/block "right_column"} */
/* {block 'javascript_bottom'} */
class Block_3766306455bf288497bdc89_79447914 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'javascript_bottom' => 
  array (
    0 => 'Block_3766306455bf288497bdc89_79447914',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php $_smarty_tpl->_subTemplateRender("file:_partials/javascript.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('javascript'=>$_smarty_tpl->tpl_vars['javascript']->value['bottom']), 0, false);
?>
    <?php
}
}
/* {/block 'javascript_bottom'} */
/* {block 'hook_before_body_closing_tag'} */
class Block_9607356535bf288497beac1_84856370 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'hook_before_body_closing_tag' => 
  array (
    0 => 'Block_9607356535bf288497beac1_84856370',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayBeforeBodyClosingTag'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_before_body_closing_tag'} */
}
