<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:54:18
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\templates\_partials\breadcrumb.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf2884a091f46_48380150',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e4b63c345cfa4f26d2d3acd832e2c3e135f75cca' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\templates\\_partials\\breadcrumb.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf2884a091f46_48380150 (Smarty_Internal_Template $_smarty_tpl) {
?><nav data-depth="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['breadcrumb']->value['count'], ENT_QUOTES, 'UTF-8');?>
" class="breadcrumb hidden-sm-down dor-breadcrumb">
  <div class="container" id="title-page-show">
    <h1 class="category-name">
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "category" && isset($_smarty_tpl->tpl_vars['category']->value['name']) && $_smarty_tpl->tpl_vars['category']->value['name'] != '') {
echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8');
}?>
        <?php if ($_smarty_tpl->tpl_vars['page']->value['page_name'] == "product" && isset($_smarty_tpl->tpl_vars['category']->value->name) && $_smarty_tpl->tpl_vars['category']->value->name != '') {
echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value->name, ENT_QUOTES, 'UTF-8');
}?>
    </h1>
  </div>
  <div class="breadcrumbs-items">
    <div class="container">
      <div class="row">
        <ul itemscope itemtype="http://schema.org/BreadcrumbList">
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['breadcrumb']->value['links'], 'path', false, NULL, 'breadcrumb', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['path']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_breadcrumb']->value['iteration']++;
?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <a itemprop="item" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['path']->value['url'], ENT_QUOTES, 'UTF-8');?>
">
                <span itemprop="name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['path']->value['title'], ENT_QUOTES, 'UTF-8');?>
</span>
              </a>
              <meta itemprop="position" content="<?php echo htmlspecialchars((isset($_smarty_tpl->tpl_vars['__smarty_foreach_breadcrumb']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_breadcrumb']->value['iteration'] : null), ENT_QUOTES, 'UTF-8');?>
">
            </li>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </ul>
      </div>
    </div>
  </div>
</nav><?php }
}
