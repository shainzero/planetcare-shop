<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:46
  from 'C:\xampp\htdocs\planetcare\modules\dor_bizproduct\bizproduct.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280aad1f8f0_20232069',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '67b6ab72456d9a97f136f24bd2c1e768d3491e37' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\modules\\dor_bizproduct\\bizproduct.tpl',
      1 => 1517630603,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf280aad1f8f0_20232069 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="product-biz list-products arrowStyleDot1 animatedParent animateOnce">
	<div class="row-biz">
		<div class="dor-biz-product">
			<div class="biz-header">
				<h2>
					<span class="biz-title"><span class="dor-title-fix"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Popular Products','mod'=>'dor_bizproduct'),$_smarty_tpl ) );?>
</span></span>
					<span class="sug-line-title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Share your single post here. You can choose the latest posts or best articles to show on your homepage','mod'=>'dor_bizproduct'),$_smarty_tpl ) );?>
</span>
				</h2>
			</div>
			<div class="biz-contents animated bounceInUp" data-ajaxurl="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
modules/dor_bizproduct/bizproduct-ajax.php">
					<ul class="tab-biz-control hidden-lg hidden-sm hidden-md col-sm-12 col-sx-12">
						<?php $_smarty_tpl->_assignInScope('i', 1);?>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['productTabslider']->value, 'bizTab', false, NULL, 'posTabProduct', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['bizTab']->value) {
?>
						<li><a href="#bizData-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['bizTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="biz-tabtitle dor-underline-from-center <?php if ($_smarty_tpl->tpl_vars['i']->value == 1) {?>active<?php }?>"><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['bizTab']->value['name'], ENT_QUOTES, 'UTF-8');?>
</span></a></li>
						<?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);?>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</ul>
					<?php $_smarty_tpl->_assignInScope('count', 1);?>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['productTabslider']->value, 'productTab', false, NULL, 'posTabProduct', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['productTab']->value) {
?>
					<div id="bizData-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="biz-group col-lg-4 col-sm-4 col-sx-4 col-md-4">
						<div>
							<h3 class="biz-tabtitle"><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['name'], ENT_QUOTES, 'UTF-8');?>
</span></h3>
							<div id="bizTab-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="biz-group-content">
								
							</div>
							<div class="view-more-cat-link clearfix"><a href="<?php ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['productTab']->value['link'], ENT_QUOTES, 'UTF-8');
$_prefixVariable2 = ob_get_clean();
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getPageLink($_prefixVariable2),'html' )), ENT_QUOTES, 'UTF-8');?>
">View more<i class="fa fa-long-arrow-right"></i></a></div>
						</div>
					</div>
					<?php $_smarty_tpl->_assignInScope('count', $_smarty_tpl->tpl_vars['count']->value+1);?>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>	
			</div>
		</div>	
	</div>
</div><?php }
}
