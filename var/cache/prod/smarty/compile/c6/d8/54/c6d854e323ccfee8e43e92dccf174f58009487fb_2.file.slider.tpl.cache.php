<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:46
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_imageslider\views\templates\hook\slider.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280aa9f9597_70397912',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c6d854e323ccfee8e43e92dccf174f58009487fb' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_imageslider\\views\\templates\\hook\\slider.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf280aa9f9597_70397912 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '4311025235bf280aa9e5783_48886779';
?>

<?php if ($_smarty_tpl->tpl_vars['dorslider']->value['slides']) {?>
  <div id="dorSlideShow" class="homeslider-container animatedParent animateOnce" data-interval="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorslider']->value['speed'], ENT_QUOTES, 'UTF-8');?>
" data-wrap="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorslider']->value['wrap'], ENT_QUOTES, 'UTF-8');?>
" data-pause="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorslider']->value['pause'], ENT_QUOTES, 'UTF-8');?>
" data-arrow=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorslider']->value['arrow'], ENT_QUOTES, 'UTF-8');?>
 data-nav=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorslider']->value['nav'], ENT_QUOTES, 'UTF-8');?>
>
    <div id="Dor_Full_Slider" style="width: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorslider']->value['thumbWidth'], ENT_QUOTES, 'UTF-8');?>
px; height: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorslider']->value['thumbHeight'], ENT_QUOTES, 'UTF-8');?>
px;">
        <!-- Loading Screen -->
        <div class="slider-loading" data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div class="slider-loading-img"></div>
        </div>
        <div class="slider-content-wrapper" data-u="slides" style="width: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorslider']->value['thumbWidth'], ENT_QUOTES, 'UTF-8');?>
px; height: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dorslider']->value['thumbHeight'], ENT_QUOTES, 'UTF-8');?>
px;">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['dorslider']->value['slides'], 'slide', false, 'i');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['slide']->value) {
?>
          <?php if ($_smarty_tpl->tpl_vars['slide']->value['active']) {?>
             <div class="slider-content effectSlider<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['effect'], ENT_QUOTES, 'UTF-8');?>
 slide-item-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['i']->value+1, ENT_QUOTES, 'UTF-8');?>
" data-p="225.00" style="display:none;">
                  <img data-u="image" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['image_url'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['slide']->value['legend'] )), ENT_QUOTES, 'UTF-8');?>
" />
                  <?php if (isset($_smarty_tpl->tpl_vars['slide']->value['imageproduct']) && $_smarty_tpl->tpl_vars['slide']->value['imageproduct'] != '') {?>
                  <div class="product-item-image animated dor-growIn">
                      <?php if (isset($_smarty_tpl->tpl_vars['slide']->value['imageproduct']) && $_smarty_tpl->tpl_vars['slide']->value['imageproduct'] != '') {?>
                      <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['imageproduct'], ENT_QUOTES, 'UTF-8');?>
" alt="" />
                      <?php }?>
                      <?php if (isset($_smarty_tpl->tpl_vars['slide']->value['price']) && $_smarty_tpl->tpl_vars['slide']->value['price'] != '') {?>
                      <div data-u="caption" data-t="16" class="dor-slider-price">
                        <div class="price-slider button--sacnite button--round-l">
                          <span>-<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Only",'mod'=>"dor_homeslider"),$_smarty_tpl ) );?>
-</span>
                          <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['price'], ENT_QUOTES, 'UTF-8');?>
</span>
                        </div>
                      </div>
                      <?php }?>
                  </div>
                  <?php }?>
                  <div class="dor-info-perslider">
                    <div class="dor-info-perslider-inner">
                      <div class="dor-info-perslider-wrapper">
                        <div class="container">
                          <?php if (isset($_smarty_tpl->tpl_vars['slide']->value['title_image']) && $_smarty_tpl->tpl_vars['slide']->value['title_image'] != '') {?>
                          <div class="dor-slider-title-image animated dor-growIn" data-u="caption">
                            <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['slide']->value['title_image'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt=""/>
                          </div>
                          <?php }?>
                          <?php if (isset($_smarty_tpl->tpl_vars['slide']->value['title']) && $_smarty_tpl->tpl_vars['slide']->value['title'] != '') {?>
                          <div class="dor-slider-title" data-u="caption"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['slide']->value['title'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div>
                          <?php }?>
                          <?php if (isset($_smarty_tpl->tpl_vars['slide']->value['legend']) && $_smarty_tpl->tpl_vars['slide']->value['legend'] != '') {?>
                          <div class="dor-slider-caption animated dor-fadeInRight" data-u="caption"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['slide']->value['legend'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div>
                          <?php }?>
                          <?php if ($_smarty_tpl->tpl_vars['slide']->value['description']) {?>
                          <div class="dor-slider-desc" data-u="caption"><div class="dor-slider-desc-inner animated dor-fadeInUp"><?php echo $_smarty_tpl->tpl_vars['slide']->value['description'];?>
</div></div>
                          <?php }?>
                          <?php if ($_smarty_tpl->tpl_vars['slide']->value['txtReadmore1'] || $_smarty_tpl->tpl_vars['slide']->value['txtReadmore2']) {?>
                          <div class="slider-read-more" data-u="caption">
                            <?php if ($_smarty_tpl->tpl_vars['slide']->value['txtReadmore1']) {?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['UrlReadmore1'], ENT_QUOTES, 'UTF-8');?>
" class="dor-effect-hzt button--winona" data-text="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['txtReadmore1'], ENT_QUOTES, 'UTF-8');?>
"><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['txtReadmore1'], ENT_QUOTES, 'UTF-8');?>
</span></a><?php }?>
                            <?php if ($_smarty_tpl->tpl_vars['slide']->value['txtReadmore2']) {?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['UrlReadmore2'], ENT_QUOTES, 'UTF-8');?>
" class="dor-effect-hzt button--winona" data-text="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['txtReadmore2'], ENT_QUOTES, 'UTF-8');?>
"><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['txtReadmore2'], ENT_QUOTES, 'UTF-8');?>
</span></a><?php }?>
                          </div>
                          <?php }?>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="dorNavSlider" style="bottom:70px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype"><div data-u="numbertemplate"></div></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="dorArrowLeft" style="" data-autocenter="2"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></span>
        <span data-u="arrowright" class="dorArrowRight" style="" data-autocenter="2"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
    </div>
  </div>
<?php }
}
}
