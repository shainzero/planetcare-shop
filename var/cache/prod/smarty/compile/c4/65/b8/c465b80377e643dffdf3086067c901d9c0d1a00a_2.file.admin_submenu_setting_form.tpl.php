<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:40:30
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_megamenu\views\templates\hook\admin_submenu_setting_form.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf2850e91bee1_47304503',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c465b80377e643dffdf3086067c901d9c0d1a00a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_megamenu\\views\\templates\\hook\\admin_submenu_setting_form.tpl',
      1 => 1482377475,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf2850e91bee1_47304503 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="submenu_setting_form">
	<form id="submenu-form" class="form-horizontal" method="post" action="" enctype="multipart/form-data">
	<input type="hidden" name="id_dormegamenu" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_dormegamenu']->value, ENT_QUOTES, 'UTF-8');?>
">
		<div class="row">
			<div class="col-lg-3">
				<div class="tab-left">
					<ul class="nav nav-pills nav-stacked" role="tablist">
						<?php if ($_smarty_tpl->tpl_vars['show_menumenu']->value) {?>
					    <li role="presentation" class="active"><a href="#sub-tab-megamenu" data-toggle="tab"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Mega Menu','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</a></li>
					    <?php }?>
					    <li role="presentation"<?php if (!$_smarty_tpl->tpl_vars['show_menumenu']->value) {?> class="active"<?php }?>><a href="#sub-tab-general" data-toggle="tab"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'General Setting','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</a></li>
				  	</ul>
			  	</div>
			</div>
			<div class="col-lg-9">
				<div class="tab-right">
					<div class="tab-content">
						<!-- Megamenu Tab -->
						<?php if ($_smarty_tpl->tpl_vars['show_menumenu']->value) {?>
						<div id="sub-tab-megamenu" class="tab-pane in active">
							<div class="megamenu-setting-header">
								<div class="megamenu-action">
									<button class="btn btn-primary add-row" type="button"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add Row','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</button>
									<button class="btn btn-danger delete-row" type="button"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Delete Row','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</button>
									<button class="btn btn-primary add-column" type="button"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add Column','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</button>
									<button class="btn btn-danger delete-column" type="button"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Delete Column','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</button>
								</div>
								<div class="columncls" style="display:none;">
									<div class="row">
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label col-sm-6"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Width:','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</label>
												<div class="col-sm-6">
													<select class="column_width" name="column_width">
														<option value="12"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'1/1 (100%)','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
														<option value="6"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'1/2 (50%)','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
														<option value="4"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'1/3 (33.3%)','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
														<option value="3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'1/4 (25%)','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
														<option value="2"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'1/6 (16.6%)','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label class="control-label col-sm-6"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Widget:','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</label>
												<div class="col-sm-6">
													<select name="widget_list" class="widget_list">
														<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['listWidgets']->value, 'widget');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['widget']->value) {
?>
															<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['widget']->value['wkey'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['widget']->value['name'], ENT_QUOTES, 'UTF-8');?>
</option>
														<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
													</select>
												</div>
											</div>
										</div>
										<div class="col-sm-4">
											<button class="btn btn-primary add-widget-column" type="button"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add Widget','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</button>
										</div>
									</div>
								</div>
							</div>
							<div class="megamenu-setting-content">
								<!-- Content Here -->
							</div>
							<div class="after-megamenu-setting-content">
								<div class="alert alert-info" role="alert"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'If you build sub megamenu, all sub menu items of this menu will not show.','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</div>
							</div>
							<?php if (isset($_smarty_tpl->tpl_vars['data']->value['params']) && $_smarty_tpl->tpl_vars['data']->value['params']) {?>
							<?php echo '<script'; ?>
 type="text/javascript">
								jQuery(document).ready(function(){
									dorMegamenuForm.submenuInit(<?php echo $_smarty_tpl->tpl_vars['data']->value['params'];?>
);
								});
							<?php echo '</script'; ?>
>
							<?php }?>
					  	</div>
					  	<?php }?>
					  	<!-- General Tab -->
					  	<div id="sub-tab-general" class="tab-pane <?php if (!$_smarty_tpl->tpl_vars['show_menumenu']->value) {?> in active<?php }?>">
						  	<div class="tab-general-wrapper">
								<h4><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Menu Item Settings','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</h4>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Target','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</label>
									<div class="col-sm-9">
										<select name="target">
											<option value="_self" <?php if ($_smarty_tpl->tpl_vars['data']->value['target'] == '_self') {?>selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Self window','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
											<option value="_blank" <?php if ($_smarty_tpl->tpl_vars['data']->value['target'] == '_blank') {?>selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'New window','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sticky Label','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</label>
									<div class="col-sm-9">
										<select name="sticky_lable">
											<option value="" <?php if ($_smarty_tpl->tpl_vars['data']->value['sticky_lable'] == '') {?>selected="selected"<?php }?>></option>
											<option value="hot" <?php if ($_smarty_tpl->tpl_vars['data']->value['sticky_lable'] == 'hot') {?>selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Hot','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
											<option value="new" <?php if ($_smarty_tpl->tpl_vars['data']->value['sticky_lable'] == 'new') {?>selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'New','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
											<option value="featured" <?php if ($_smarty_tpl->tpl_vars['data']->value['sticky_lable'] == 'featured') {?>selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Featured','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Font Awesome Icon','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</label>
									<div class="col-sm-9">
										<input type="text" class="icon_class" name="icon_class" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['icon_class'], ENT_QUOTES, 'UTF-8');?>
">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Addition Class','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</label>
									<div class="col-sm-9">
										<input type="text" class="addition_class" name="addition_class" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['addition_class'], ENT_QUOTES, 'UTF-8');?>
">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Menu Background URL','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</label>
									<div class="col-sm-9">
										<input type="text" class="menu_background" name="menu_background" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['menu_background'], ENT_QUOTES, 'UTF-8');?>
">
									</div>
								</div>
								<h4><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'SubMenu Settings','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</h4>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sub Menu Align','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</label>
									<div class="col-sm-9">
										<select name="submenu_align">
											<option value="left" <?php if ($_smarty_tpl->tpl_vars['data']->value['submenu_align'] == 'left') {?>selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Left','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
											<option value="right" <?php if ($_smarty_tpl->tpl_vars['data']->value['submenu_align'] == 'right') {?>selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Right','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
											<option value="center" <?php if ($_smarty_tpl->tpl_vars['data']->value['submenu_align'] == 'center') {?>selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Center','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
											<option value="fullwidth" <?php if ($_smarty_tpl->tpl_vars['data']->value['submenu_align'] == 'fullwidth') {?>selected="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Fullwidth','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sub Menu Width','mod'=>'dormegamenu'),$_smarty_tpl ) );?>
</label>
									<div class="col-sm-9">
										<input type="text" class="submenu_width" name="submenu_width" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data']->value['submenu_width'], ENT_QUOTES, 'UTF-8');?>
">
									</div>
								</div>
								<div class="dorclearfix"></div>
							</div>
					  	</div>
					</div>
			  	</div>
			</div>
		</div>
	</form>
</div><?php }
}
