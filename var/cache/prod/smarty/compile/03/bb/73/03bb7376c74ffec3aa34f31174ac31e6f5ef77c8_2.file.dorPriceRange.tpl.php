<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:54:18
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_filter\views\templates\hook\dorPriceRange.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf2884a1dcff6_19740646',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '03bb7376c74ffec3aa34f31174ac31e6f5ef77c8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_filter\\views\\templates\\hook\\dorPriceRange.tpl',
      1 => 1519391060,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf2884a1dcff6_19740646 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="dorFilterPriceRange">
	<div class="dorFilterInner">
		<div class="dor-filter-price">
			<h3 class="title_block"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Filter by price','d'=>'dor_filter'),$_smarty_tpl ) );?>
</h3>
			
			<div id="slider-range"></div>
			<div class="amount-price-show">
				<span class="txt-price"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Price','d'=>'dor_filter'),$_smarty_tpl ) );?>
:</span>
				<span class="pull-right" id="amount"></span>
			</div>
			<div class="value-range">
				<input id="amount1" type="hidden" /> 
				<input id="amount2" type="hidden" /> 
				<input name="submit_range" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Filter','d'=>'dor_filter'),$_smarty_tpl ) );?>
" type="submit" />
			</div>
		</div>
	</div>
</div><?php }
}
