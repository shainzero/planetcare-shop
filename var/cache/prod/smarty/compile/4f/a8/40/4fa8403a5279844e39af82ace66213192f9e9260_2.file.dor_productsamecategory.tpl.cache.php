<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:52:16
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_productsamecategory\views\templates\hook\dor_productsamecategory.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf287d08c4fd5_25801054',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4fa8403a5279844e39af82ace66213192f9e9260' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_productsamecategory\\views\\templates\\hook\\dor_productsamecategory.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/miniatures/product.tpl' => 1,
  ),
),false)) {
function content_5bf287d08c4fd5_25801054 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '15784554495bf287d08bb452_37035301';
?>

<?php if ((isset($_smarty_tpl->tpl_vars['products']->value) && count($_smarty_tpl->tpl_vars['products']->value) > 0 && $_smarty_tpl->tpl_vars['products']->value !== false) || $_smarty_tpl->tpl_vars['ajaxLoad']->value == 1) {?>
<div class="clearfix blockproductscategory show-hover2">
	<div class="same-list-title">
		<h3 class="productscategory_h2">
			<?php if ((isset($_smarty_tpl->tpl_vars['products']->value) && count($_smarty_tpl->tpl_vars['products']->value) == 1) && $_smarty_tpl->tpl_vars['ajaxLoad']->value == 0) {?>
				<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Related Product','d'=>'dor_productsamecategory'),$_smarty_tpl ) );?>
</span>
			<?php } else { ?>
				<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Related Products','d'=>'dor_productsamecategory'),$_smarty_tpl ) );?>
</span>
			<?php }?>
		</h3>
	</div>
	<div id="productscategory_same">
	<div id="productscategory_list_data" class="productscategory_list arrowStyleDot1" <?php if ($_smarty_tpl->tpl_vars['ajaxLoad']->value == 1) {?> data-ajaxurl="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value['force_ssl']) && $_smarty_tpl->tpl_vars['urls']->value['force_ssl']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url_ssl'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');
}?>modules/dor_productsamecategory/ajax.php" data-product-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['options']->value['id_product'], ENT_QUOTES, 'UTF-8');?>
" data-category-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['options']->value['id_category'], ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
	    <div class="productSameCategory-inner">
		    <div class="productSameCategory-wrapper">
		    <?php if (isset($_smarty_tpl->tpl_vars['ajaxLoad']->value) && $_smarty_tpl->tpl_vars['ajaxLoad']->value == 0 && isset($_smarty_tpl->tpl_vars['products']->value) && count($_smarty_tpl->tpl_vars['products']->value) > 0) {?>
				<div class="product_list_related product_list grid">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products']->value, 'product');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
?>
			      <?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
			    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				</div>
			<?php }?>
			</div>
		</div>
	</div>
	</div>
</div>
<?php }
}
}
