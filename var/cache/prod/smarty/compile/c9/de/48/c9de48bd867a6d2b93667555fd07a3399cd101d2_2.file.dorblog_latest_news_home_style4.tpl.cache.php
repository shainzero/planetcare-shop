<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:36:22
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\smartbloghomelatestnews\views\templates\front\dorblog_latest_news_home_style4.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf2841611c464_38498609',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c9de48bd867a6d2b93667555fd07a3399cd101d2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\smartbloghomelatestnews\\views\\templates\\front\\dorblog_latest_news_home_style4.tpl',
      1 => 1512730241,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf2841611c464_38498609 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'C:\\xampp\\htdocs\\planetcare\\vendor\\smarty\\smarty\\libs\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
$_smarty_tpl->compiled->nocache_hash = '7590910825bf28416108c33_17422290';
?>
<!-- Latest News -->
<?php if (isset($_smarty_tpl->tpl_vars['view_data']->value) && !empty($_smarty_tpl->tpl_vars['view_data']->value)) {?>
    <?php $_smarty_tpl->_assignInScope('i', 1);?>
<div id="dor-blog-home-style4">
    <div class="blog-home-inner">
        <section class="gst-row row-latest-news ovh">
            <div class="theme-container">
                <div class="gst-column no-padding">
                    <div class="fancy-heading text-left title-header-tab">
                        <h3 class="head-tab-lists title-mod-news"><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'From Our Blog','d'=>'smartbloghomelatestnews'),$_smarty_tpl ) );?>
</span></h3>
                        <p><span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Organic and freshly','d'=>'smartbloghomelatestnews'),$_smarty_tpl ) );?>
</span></p>
                    </div>
                    <div class="row gst-post-list animatedParent animateOnce">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['view_data']->value, 'post', false, 'i');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value => $_smarty_tpl->tpl_vars['post']->value) {
?>
                        <?php $_smarty_tpl->_assignInScope('catOptions', null);?>
                        <?php $_smarty_tpl->_assignInScope('options', null);?>
                        <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['id_post'] = $_smarty_tpl->tpl_vars['post']->value['id'];
$_smarty_tpl->_assignInScope('options', $_tmp_array);?>
                        <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['slug'] = $_smarty_tpl->tpl_vars['post']->value['link_rewrite'];
$_smarty_tpl->_assignInScope('options', $_tmp_array);?>
                        <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['catOptions']) ? $_smarty_tpl->tpl_vars['catOptions']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['id_category'] = $_smarty_tpl->tpl_vars['post']->value['category'];
$_smarty_tpl->_assignInScope('catOptions', $_tmp_array);?>
                        <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['catOptions']) ? $_smarty_tpl->tpl_vars['catOptions']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['slug'] = $_smarty_tpl->tpl_vars['post']->value['category_link_rewrite'];
$_smarty_tpl->_assignInScope('catOptions', $_tmp_array);?>
                        <div class="item-blog-data animated bounceInUp">
                            <div class="col-item-blog">
                            	<div class="blog-home-items">
	                            	<div class="blog-home-items-inner">
		                                <div class="item-blog-media">
		                                    <a href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_post',$_smarty_tpl->tpl_vars['options']->value), ENT_QUOTES, 'UTF-8');?>
">
		                                        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['thumb_image'], ENT_QUOTES, 'UTF-8');?>
" alt="" />
		                                    </a>
		                                </div>
		                                <div class="item-content-blog">
		                                    <div class="media-body">
		                                    	<div class="entry-meta">
			                                        <div class="entry-time meta-date blog-line">
			                                            <time datetime="2015-12-09T21:10:20+00:00">
			                                                <i class="material-icons">&#xE192;</i>
			                                                <span class="entry-time-date dblock"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['date_added'],"%e"), ENT_QUOTES, 'UTF-8');?>
</span>
			                                                <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['date_added'],"%b"), ENT_QUOTES, 'UTF-8');?>

			                                            </time>
			                                        </div>
			                                        <div class="vcard author entry-author blog-line">
			                                            <a class="url fn n" rel="author" href="#">
			                                                <i class="material-icons">&#xE853;</i>&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
<span class="hidden"> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
</span>
			                                            </a>
			                                        </div>
			                                        <div class="entry-reply blog-line">
			                                            <a href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_post',$_smarty_tpl->tpl_vars['options']->value), ENT_QUOTES, 'UTF-8');?>
#comments" class="comments-link">
			                                                <i class="fa fa-comment dblock"></i>
			                                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['totalcomment'], ENT_QUOTES, 'UTF-8');?>
 Comment
			                                            </a>
			                                        </div>
			                                    </div>
		                                        <div class="entry-header">
		                                            <span class="entry-categories hidden">
		                                                <a href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_category',$_smarty_tpl->tpl_vars['catOptions']->value), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post']->value['category_name'], ENT_QUOTES, 'UTF-8');?>
</a>
		                                            </span>
		                                            <h3 class="entry-title">
		                                                <a href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_post',$_smarty_tpl->tpl_vars['options']->value), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['title'],40,'...' )),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</a>
		                                            </h3>
		                                            <p class="news-desc"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['short_description'],130,'...' )),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</p>
		                                            <a href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_post',$_smarty_tpl->tpl_vars['options']->value), ENT_QUOTES, 'UTF-8');?>
" class="read-more-link thm-clr hidden"><span class="dor-effect-hzt">Read More</span> <i class="fa fa-long-arrow-right hidden"></i> </a>
		                                        </div>
		                                    </div>
		                                    
		                                </div>
	                                </div>
	                            </div>
                            </div>
                        </div>
                        <?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php }?>
<!-- Latest News --><?php }
}
