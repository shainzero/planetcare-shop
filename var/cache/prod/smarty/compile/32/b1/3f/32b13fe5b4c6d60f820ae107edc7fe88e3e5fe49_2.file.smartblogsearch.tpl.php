<?php
/* Smarty version 3.1.32, created on 2018-11-20 12:35:03
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\smartblogsearch\views\templates\front\smartblogsearch.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf3f16774e087_95314944',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '32b13fe5b4c6d60f820ae107edc7fe88e3e5fe49' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\smartblogsearch\\views\\templates\\front\\smartblogsearch.tpl',
      1 => 1501167291,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf3f16774e087_95314944 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="block blogModule boxPlain clearfix" id="smartblogsearch">
      <h2 class='sdstitle_block title_block'><a href='<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_list'), ENT_QUOTES, 'UTF-8');?>
'><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"Blog Search",'mod'=>"smartblogsearch"),$_smarty_tpl ) );?>
</a></h2>
	<div id="sdssearch_block_top" class="block_content">
		<form action="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_search'), ENT_QUOTES, 'UTF-8');?>
" method="post" id="searchbox">
		    <input type="hidden" value="0" name="smartblogaction">
			<input type="text" value="" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'TYPE KEYWORD','mod'=>'smartblogsearch'),$_smarty_tpl ) );?>
" name="smartsearch" id="search_query_top" class="search_query form-control ac_input" autocomplete="off">
			<button class="btn btn-default button-search" name="smartblogsubmit" type="submit">
			<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search','mod'=>'smartblogsearch'),$_smarty_tpl ) );?>
</span>
			</button>
		</form>
	</div>
</div>




<?php }
}
