<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:38:28
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_megamenu\views\templates\hook\widgets\html.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf28494101749_51790806',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e963d057b41907a82ea717f6bf6fbb9c610ce457' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_megamenu\\views\\templates\\hook\\widgets\\html.tpl',
      1 => 1487298162,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf28494101749_51790806 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['html']->value)) {?>
<div class="widget-html block <?php if (isset($_smarty_tpl->tpl_vars['additionclss']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['additionclss']->value, ENT_QUOTES, 'UTF-8');
}?>">
	<?php if (isset($_smarty_tpl->tpl_vars['widget_heading']->value) && !empty($_smarty_tpl->tpl_vars['widget_heading']->value)) {?>
	<div class="widget-heading title_block">
		<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['widget_heading']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

	</div>
	<?php }?>
	<div class="widget-inner block_content">
		<?php echo $_smarty_tpl->tpl_vars['html']->value;?>
	</div>
</div>
<?php }
}
}
