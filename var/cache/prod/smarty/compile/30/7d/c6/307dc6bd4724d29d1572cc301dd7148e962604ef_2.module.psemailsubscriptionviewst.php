<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:47
  from 'module:psemailsubscriptionviewst' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280ab1c6ac9_99683001',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '307dc6bd4724d29d1572cc301dd7148e962604ef' => 
    array (
      0 => 'module:psemailsubscriptionviewst',
      1 => 1541767127,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf280ab1c6ac9_99683001 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="block_newsletter footer-block-wap">
      <div class="block_newsletter_data">
        <div class="title-block text-center">
        <h4 class="hidden-sm-down"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Join our newsletter','d'=>'Shop.Theme'),$_smarty_tpl ) );?>
</h4>
        </div>
        <div class="title clearfix hidden-md-up" data-target="#footer_newsletter_apse" data-toggle="collapse"><span class="h3"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Join our newsletter','d'=>'Shop.Theme'),$_smarty_tpl ) );?>
</span> <span class="pull-xs-right"> <span class="navbar-toggler collapse-icons"> <i class="material-icons add"></i> <i class="material-icons remove"></i> </span> </span></div>
        <div id="footer_newsletter_apse" class="toggle-footer collapse">
          <form id="main-newsletter-footer" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
#footer" method="post">
            <div class="row-newsletter">
              <div class="col-newsletter-data">
                <button
                  class="btn btn-primary pull-xs-right hidden-xs-down btnSubmitNewsletter"
                  name="submitNewsletter"
                  type="submit"
                  value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Subscribe','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
">
                  <i class="fa fa-send-o" aria-hidden="true"></i>
                </button>
                <button
                  class="btn btn-primary pull-xs-right hidden hidden-sm-up btnSubmitNewsletter"
                  name="submitNewsletter"
                  type="submit"
                  value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'OK','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
"
                >
                </button>

                <div class="input-wrapper">
                  <input
                    name="email"
                    type="text"
                    value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
"
                    placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Type your email here','d'=>'Shop.Forms.Labels'),$_smarty_tpl ) );?>
"
                  >
                </div>
                <input type="hidden" name="action" value="0">
                <div class="clearfix"></div>
              </div>
              <div class="col-xs-12">
                  <?php if ($_smarty_tpl->tpl_vars['conditions']->value) {?>
                    <p class="hidden"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['conditions']->value, ENT_QUOTES, 'UTF-8');?>
</p>
                  <?php }?>
                  <?php if ($_smarty_tpl->tpl_vars['msg']->value) {?>
                    <p class="alert <?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>alert-danger<?php } else { ?>alert-success<?php }?>">
                      <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['msg']->value, ENT_QUOTES, 'UTF-8');?>

                    </p>
                  <?php }?>
              </div>
            </div>
          </form>
          <div class="newsletter-info"><p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign up for our newsletter and get','d'=>'Shop.Theme'),$_smarty_tpl ) );?>
&nbsp;<span>50% <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'off','d'=>'Shop.Theme'),$_smarty_tpl ) );?>
</span>&nbsp;<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'your next order. Pretty sweet, we know.','d'=>'Shop.Theme'),$_smarty_tpl ) );?>
</p></div>
          <ul class="footer-newsletter-social no-padding">
            <li><a href="#" data-toggle="tooltip" data-original-title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Facebook','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#" data-toggle="tooltip" data-original-title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Twitter','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#" data-toggle="tooltip" data-original-title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Pinterest','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
            <li><a href="#" data-toggle="tooltip" data-original-title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Instagram','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
          </ul>
        </div>
      </div>
</div>
<?php }
}
