<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:51:42
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_themeoptions\views\templates\front\dor_flipimage.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf287ae4fe4d2_80252857',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0194f2bd9f6325dbf25e02521bf9d493039e3608' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_themeoptions\\views\\templates\\front\\dor_flipimage.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf287ae4fe4d2_80252857 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['dorLazyLoad']->value) && $_smarty_tpl->tpl_vars['dorLazyLoad']->value == 1) {?>
<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['url'], ENT_QUOTES, 'UTF-8');?>
" class="thumbnail product-thumbnail product_img_link">
  <img
    class = "img-responsive thumbnail-image-1 dorlazy owl-lazy"
    data-src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['bySize']['home_default']['url'], ENT_QUOTES, 'UTF-8');?>
"
    data-original="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['bySize']['home_default']['url'], ENT_QUOTES, 'UTF-8');?>
"
    alt = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['legend'], ENT_QUOTES, 'UTF-8');?>
"
    data-full-size-image-url = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['url'], ENT_QUOTES, 'UTF-8');?>
"
  >
  <?php if (isset($_smarty_tpl->tpl_vars['product']->value['flip']) && $_smarty_tpl->tpl_vars['product']->value['flip']) {?>
  <img
    class = "img-responsive thumbnail-image-2 dorlazy owl-lazy"
    data-src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['flip']['bySize']['home_default']['url'], ENT_QUOTES, 'UTF-8');?>
"
    data-original="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['flip']['bySize']['home_default']['url'], ENT_QUOTES, 'UTF-8');?>
"
    alt = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['flip']['legend'], ENT_QUOTES, 'UTF-8');?>
"
    data-full-size-image-url = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['flip']['large']['url'], ENT_QUOTES, 'UTF-8');?>
"
  >
  <?php }?>
</a>


<?php } else { ?>

<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['url'], ENT_QUOTES, 'UTF-8');?>
" class="thumbnail product-thumbnail product_img_link">
  <img
    class = "img-responsive thumbnail-image-1"
    src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['bySize']['home_default']['url'], ENT_QUOTES, 'UTF-8');?>
"
    alt = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['legend'], ENT_QUOTES, 'UTF-8');?>
"
    data-full-size-image-url = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['cover']['large']['url'], ENT_QUOTES, 'UTF-8');?>
"
  >
  <?php if (isset($_smarty_tpl->tpl_vars['product']->value['flip']) && $_smarty_tpl->tpl_vars['product']->value['flip']) {?>
  <img
    class = "img-responsive thumbnail-image-2"
    src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['flip']['bySize']['home_default']['url'], ENT_QUOTES, 'UTF-8');?>
"
    alt = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['flip']['legend'], ENT_QUOTES, 'UTF-8');?>
"
    data-full-size-image-url = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['flip']['large']['url'], ENT_QUOTES, 'UTF-8');?>
"
  >
  <?php }?>
</a>

<?php }
}
}
