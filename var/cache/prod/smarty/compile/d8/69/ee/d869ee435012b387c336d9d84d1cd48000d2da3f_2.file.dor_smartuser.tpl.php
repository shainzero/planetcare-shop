<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:36:22
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_smartuser\dor_smartuser.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf28416322951_59612526',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd869ee435012b387c336d9d84d1cd48000d2da3f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_smartuser\\dor_smartuser.tpl',
      1 => 1516761993,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf28416322951_59612526 (Smarty_Internal_Template $_smarty_tpl) {
if (!$_smarty_tpl->tpl_vars['logged']->value) {?>

<div id="loginFormSmart" class="dor-formsmart">
	<span class="button b-close"><span>X</span></span>
	<form id="login-form" action="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value['force_ssl']) && $_smarty_tpl->tpl_vars['urls']->value['force_ssl']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url_ssl'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');
}
if ($_smarty_tpl->tpl_vars['iso_code_lang']->value == 'es') {?>iniciar-sesion<?php } else { ?>login<?php }?>" method="post">
		<h2 class="title-heading"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
modules/dor_smartuser/img/logo.png" alt=""></h2>
		<h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'WELCOME TO OUR WONDERFUL WORLD','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</h3>
		<p class="smart-sign-txt"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Did you know that we ship to over','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
 <span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'24 different countries','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</span></p>

		<section>
			<input name="back" value="my-account" type="hidden">
			<a href="#" class="smart-fb-btn btn"> <i class="fa fa-facebook btn-icon"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Login with Facebook','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</a>
			<p class="line-smart signup"> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'OR SIGN IN','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
 </p>
			<div class="form-group row ">
				<label class="col-md-3 form-control-label required hidden">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>

				</label>
				<div class="col-md-12">
					<input class="form-control" name="email" value="" required="" type="email" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
">
				</div>
				<div class="col-md-3 form-control-comment"></div>
			</div>
			<div class="form-group row ">
				<label class="col-md-3 form-control-label required hidden"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Password','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</label>
				<div class="col-md-12">
					<div class="input-group js-parent-focus">
						<input class="form-control js-child-focus js-visible-password" name="password" value="" required="" type="password" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Password','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
">
					</div>
				</div>
				<div class="col-md-3 form-control-comment"></div>
			</div>
			<div class="smartdor-footer form-footer text-xs-center clearfix">
				<input name="submitLogin" value="1" type="hidden">
				<button class="btn btn-primary" data-link-action="sign-in" type="submit">
				<span class="fa fa-lightbulb-o"></span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign in','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>

				</button>
			</div>
			<div class="auth-dor-moreinfo clearfix hidden">
				<p>* <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Denotes mandatory field.','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</p>
				<p>** <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'At least one telephone number is required.','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</p>
			</div>
			<div class="dor-button-connect">
				<a href="#" onclick="return false" class="smartRegister reActLogReg"><i aria-hidden="true" class="fa fa-user-plus"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Register','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</a>
				<a rel="nofollow" title="Recover your forgotten password" class="lost_password_smart" onclick="return false" href="#"><i aria-hidden="true" class="fa fa-key"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Forgot your password?','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</a>
			</div>
		</section>
		
	</form>
</div>

<div id="registerFormSmart" class="dor-formsmart">
	<span class="button b-close"><span>X</span></span>
	<form action="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value['force_ssl']) && $_smarty_tpl->tpl_vars['urls']->value['force_ssl']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url_ssl'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');
}
if ($_smarty_tpl->tpl_vars['iso_code_lang']->value == 'es') {?>iniciar-sesion<?php } else { ?>login<?php }?>?create_account=1" id="customer-form" class="js-customer-form" method="post">
		<h2 class="title-heading"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
modules/dor_smartuser/img/logo.png" alt=""></h2>
		<h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'WELCOME TO OUR WONDERFUL WORLD','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</h3>
		<p class="smart-sign-txt"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Did you know that we ship to over','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
 <span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'24 different countries','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</span></p>
		<section>
			<input name="id_customer" value="" type="hidden">
			<a href="#" class="smart-fb-btn btn"> <i class="fa fa-facebook btn-icon"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Register with Facebook','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</a>
			<p class="line-smart signup"> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'OR SIGN UP','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
 </p>
			<div class="form-group row hidden">
				<label class="col-md-3 form-control-label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Social title','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</label>
				<div class="col-md-12 form-control-valign">
					<label class="radio-inline">
						<span class="custom-radio">
							<input name="id_gender" value="1" type="radio">
							<span></span>
						</span>
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Mr.','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>

					</label>
					<label class="radio-inline">
						<span class="custom-radio">
							<input name="id_gender" value="2" type="radio">
							<span></span>
						</span>
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Mrs.','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>

					</label>
				</div>
				<div class="col-md-3 form-control-comment"></div>
			</div>
			<div class="form-group form-group-smart row ">
				<div class="col-md-6">
					<div class="field-group-smart">
						<input class="form-control" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'First name','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
" name="firstname" value="" required="" type="text">
					</div>
					<div class="col-md-3 form-control-comment"></div>
				</div>
				<div class="col-md-6">
					<div class="field-group-smart">
						<input class="form-control" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Last name','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
" name="lastname" value="" required="" type="text">
					</div>
					<div class="col-md-3 form-control-comment"></div>
				</div>
			</div>
			<div class="form-group row ">
				<label class="col-md-3 form-control-label required hidden">Email</label>
				<div class="col-md-12">
					<input class="form-control" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
" name="email" value="" required="" type="email">
				</div>
				<div class="col-md-3 form-control-comment"></div>
			</div>
			<div class="form-group row ">
				<label class="col-md-3 form-control-label required hidden">Password</label>
				<div class="col-md-12">
					<div class="input-group js-parent-focus">
						<input class="form-control js-child-focus js-visible-password" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Password','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
" name="password" value="" required="" type="password">
					</div>
				</div>
				<div class="col-md-3 form-control-comment"></div>
			</div>
			<div class="form-group row hidden">
				<label class="col-md-3 form-control-label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Birthdate','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</label>
				<div class="col-md-12">
					<input class="form-control" name="birthday" value="" placeholder="MM/DD/YYYY" type="text">
					<span class="form-control-comment">
					(E.g.: 05/31/1970)
					</span>
				</div>
				<div class="col-md-3 form-control-comment">Optional</div>
			</div>
			<div class="form-group row hidden">
				<label class="col-md-3 form-control-label"></label>
				<div class="col-md-12 hidden">
					<span class="custom-checkbox">
						<input name="optin" value="1" type="checkbox">
						<span><i class="material-icons checkbox-checked"></i></span>
						<label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Receive offers from our partners','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</label>
					</span>
				</div>
				<div class="col-md-3 form-control-comment"></div>
			</div>
			<div class="form-group row hidden">
				<label class="col-md-3 form-control-label">
				</label>
				<div class="col-md-12">
					<span class="custom-checkbox">
						<input name="newsletter" value="1" type="checkbox">
						<span><i class="material-icons checkbox-checked"></i></span>
						<label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign up for our newsletter','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
<br><em><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You may unsubscribe at any moment. For that purpose, please find our contact info in the legal notice.','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</em></label>
					</span>
				</div>
				<div class="col-md-3 form-control-comment"></div>
			</div>
			<div class="smartdor-footer form-footer clearfix">
				<input name="submitCreate" value="1" type="hidden">
				<label class="col-md-3 form-control-label"></label>
				<button class="btn btn-primary form-control-submit pull-xs-left" data-link-action="save-customer" type="submit">
				<i class="fa fa-user-plus"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign up','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>

				</button>
			</div>
			<div class="dor-button-connect clearfix">
				<a href="#" onclick="return false" class="smartLogin reActLogReg"> <i class="fa fa-lightbulb-o"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Login','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</a>
				<a rel="nofollow" title="Recover your forgotten password" class="lost_password_smart" onclick="return false" href="#"><i aria-hidden="true" class="fa fa-key"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
Forgot your password?' d='</a>
			</div>
		</section>
	</form>
</div>
<div id="smartForgotPass" class="dor-formsmart">
	<span class="button b-close"><span>X</span></span>
	<div class="center_column" id="center_column_smart">
		<div class="box">
			<form action="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value['force_ssl']) && $_smarty_tpl->tpl_vars['urls']->value['force_ssl']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url_ssl'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');
}?>password-recovery" method="post">
			    <h2 class="title-heading"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
modules/dor_smartuser/img/logo.png" alt=""></h2>
				<h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'WELCOME TO OUR WONDERFUL WORLD','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</h3>
				<p class="smart-sign-txt"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Did you know that we ship to over','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
 <span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'24 different countries','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</span></p>
			    <div class="smartdor-header">
			      	<p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please enter the email address you used to register. You will receive a temporary link to reset your password.','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</p>
			    </div>
			    <section class="form-fields">
			      	<div class="form-group row">
				        <label class="col-md-3 form-control-label required hidden">Email address</label>
				        <div class="col-md-12">
				          	<input name="email" id="email" value="" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email address','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
" class="form-control" required="" type="email">
				        </div>
			      	</div>
					<div class="smartdor-footer form-footer text-xs-center">
						<button class="form-control-submit btn btn-primary" name="submit" type="submit">
							<i class="fa fa-key" aria-hidden="true"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Retrieve Password','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>

						</button>
					</div>
			      	<div class="dor-button-connect clearfix">
						<a href="#" onclick="return false" class="smartLogin reActLogReg"> <i class="fa fa-lightbulb-o"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Login','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</a>
						<a href="#" onclick="return false" class="smartRegister reActLogReg"><i aria-hidden="true" class="fa fa-user-plus"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Register','d'=>'Modules.Dor_Smartuser.Shop'),$_smarty_tpl ) );?>
</a>
					</div>
			    </section>
		    
		  	</form>
		</div>
	</div>
</div>
<?php }
}
}
