<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:52:16
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_productsamecategory\views\templates\hook\dor_productsamebrand.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf287d0a08968_52909923',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4d36ce8ce7c50e60761043a8362b4d610e3986f0' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_productsamecategory\\views\\templates\\hook\\dor_productsamebrand.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/miniatures/product.tpl' => 1,
  ),
),false)) {
function content_5bf287d0a08968_52909923 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '18642227165bf287d09ffe73_86738879';
?>

<?php if ((isset($_smarty_tpl->tpl_vars['products']->value) && count($_smarty_tpl->tpl_vars['products']->value) > 0 && $_smarty_tpl->tpl_vars['products']->value !== false) || $_smarty_tpl->tpl_vars['ajaxLoad']->value == 1) {?>
<div class="clearfix blockproductscategory related-product-brand show-hover2">
	<div class="same-list-title">
		<h3 class="productscategory_h2">
			<?php if ((isset($_smarty_tpl->tpl_vars['products']->value) && count($_smarty_tpl->tpl_vars['products']->value) == 1) && $_smarty_tpl->tpl_vars['ajaxLoad']->value == 0) {?>
				<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Related Product By Brand','d'=>'dor_productsamecategory'),$_smarty_tpl ) );?>
</span>
			<?php } else { ?>
				<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Related Products By Brand','d'=>'dor_productsamecategory'),$_smarty_tpl ) );?>
</span>
			<?php }?>
		</h3>
	</div>
	<div id="productssamebrand">
		<div id="productssamebrand_list_data" class="productscategory_list arrowStyleDot1" <?php if ($_smarty_tpl->tpl_vars['ajaxLoad']->value == 1) {?> data-ajaxurl="<?php if (isset($_smarty_tpl->tpl_vars['urls']->value['force_ssl']) && $_smarty_tpl->tpl_vars['urls']->value['force_ssl']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url_ssl'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');
}?>modules/dor_productsamecategory/ajax.php" data-product-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['options']->value['id_product'], ENT_QUOTES, 'UTF-8');?>
" data-brand-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['options']->value['id_brand'], ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
		    <div class="productSameCategory-inner">
			    <div class="productSameCategory-wrapper">
			   	<?php if (isset($_smarty_tpl->tpl_vars['ajaxLoad']->value) && $_smarty_tpl->tpl_vars['ajaxLoad']->value == 0 && isset($_smarty_tpl->tpl_vars['products']->value) && count($_smarty_tpl->tpl_vars['products']->value) > 0) {?>
					<div class="product_list_related product_list grid">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products']->value, 'product');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
?>
				      <?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 9999, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
				    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</div>
				<?php }?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php }
}
}
