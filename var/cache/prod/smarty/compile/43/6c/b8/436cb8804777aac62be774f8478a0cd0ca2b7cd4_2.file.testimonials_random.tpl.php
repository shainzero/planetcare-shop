<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:36:22
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dor_testimonials\views\templates\front\testimonials_random.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf28416041be5_46882827',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '436cb8804777aac62be774f8478a0cd0ca2b7cd4' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dor_testimonials\\views\\templates\\front\\testimonials_random.tpl',
      1 => 1517630668,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf28416041be5_46882827 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['testimonials']->value) {?>
<div id="testimonials-client" class="testimonials-client animatedParent animateOnce col-lg-12 col-sm-12 col-xs-12">
  <div id="testimonials-wrapper" class="animated bounceInUp">
        <h3>
          <span class="font-vbs fnt-size-50"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>"We love our clients",'mod'=>"dor_testimonials"),$_smarty_tpl ) );?>
</span>
        </h3>
        <ul class="testimonials-slide">
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['testimonials']->value, 'testimonial', false, 'test');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['test']->value => $_smarty_tpl->tpl_vars['testimonial']->value) {
?>
            <?php if ($_smarty_tpl->tpl_vars['testimonial']->value['active'] == 1) {?>
              <li >
                <div class="media-content">
                  <?php if ($_smarty_tpl->tpl_vars['testimonial']->value['media']) {?>
                    <?php if (in_array($_smarty_tpl->tpl_vars['testimonial']->value['media_type'],$_smarty_tpl->tpl_vars['arr_img_type']->value)) {?>
                      <a class="fancybox-media" data-rel="fancybox-button">
                        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['mediaUrl']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['testimonial']->value['media'], ENT_QUOTES, 'UTF-8');?>
" alt="Image Testimonial">
                      </a>
                    <?php }?>
                  <?php }?>
                </div>
                <h3 class="testimonials-title hidden"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['testimonial']->value['title_post'],250 )), ENT_QUOTES, 'UTF-8');?>
</h3>
                <div class="content_test">
                  <p class="des_testimonial">“<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['testimonial']->value['content'],1000 )), ENT_QUOTES, 'UTF-8');?>
"</p>
                </div>
                <div class="testimonial-rate star_content clearfix">
                <?php
$_smarty_tpl->tpl_vars['__smarty_section_i'] = new Smarty_Variable(array());
if (true) {
for ($__section_i_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_i']->value['index'] = 0; $__section_i_0_iteration <= 5; $__section_i_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_i']->value['index']++){
?>
                <?php if ($_smarty_tpl->tpl_vars['testimonial']->value['rating'] <= (isset($_smarty_tpl->tpl_vars['__smarty_section_i']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_i']->value['index'] : null)) {?>
                  <div class="star"></div>
                <?php } else { ?>
                  <div class="star star_on"></div>
                <?php }?>
                <?php
}
}
?>
                </div>
                <p class="des_namepost"><span class="test-namepost"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['testimonial']->value['name_post'], ENT_QUOTES, 'UTF-8');?>
</span>/<span class="test-companypost"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['testimonial']->value['company'], ENT_QUOTES, 'UTF-8');?>
</span></p>
              </li>
            <?php }?>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </ul>
  </div>
</div>
<?php }
}
}
