<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:51:42
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\dorcompare\views\templates\hook\comparebutton.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf287ae3dbf28_70307804',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ead52e93b4a0666a6d44bfebcb88beff4797d57e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\dorcompare\\views\\templates\\hook\\comparebutton.tpl',
      1 => 1541767127,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf287ae3dbf28_70307804 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '6622008005bf287ae3d9908_38093235';
?>
<div class="dor-compare-button-fel">
	<div class="compare">
		<a class="add_to_compare" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['link'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" data-productid="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['id_product'], ENT_QUOTES, 'UTF-8');?>
" data-toggle="tooltip" title="" data-original-title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add compare'),$_smarty_tpl ) );?>
"><i class="pe-7s-shuffle"></i><span class="compare-button-txt hidden"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to compare'),$_smarty_tpl ) );?>
</span></a>
	</div>
</div><?php }
}
