<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:46
  from 'C:\xampp\htdocs\planetcare\modules\dor_searchcategories\dorsearch-top.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280aa6e2383_93469553',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3ae4bfb1843877d8b4ae3cb4d0ac321b4e725aaf' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\modules\\dor_searchcategories\\dorsearch-top.tpl',
      1 => 1512112344,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf280aa6e2383_93469553 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '20344975655bf280aa6de304_08984735';
?>

<!-- pos search module TOP -->
<div id="dor_search_top" class="center_column col-lg-4 col-md-4 col-xs-12 col-sm-12 clearfix" >
    <form method="get" action="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getPageLink('search'),'html' )), ENT_QUOTES, 'UTF-8');?>
" id="searchbox" class="form-inline">
        <div class="pos_search form-group no-uniform col-lg-4 col-md-4 col-xs-4 col-sm-4">
            <i class="fa fa-th"></i>
            <button type="button" class="dropdown-toggle form-control" data-toggle="dropdown">
               <span data-bind="label"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'All Category','mod'=>'dor_searchcategories'),$_smarty_tpl ) );?>
</span>&nbsp;<span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#" data-value="0"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'All Category','mod'=>'dor_searchcategories'),$_smarty_tpl ) );?>
</a></li>
               <!-- <?php echo $_smarty_tpl->tpl_vars['categories_option']->value;?>
 -->
            </ul>
        </div>
        <div class="dor_search form-group col-lg-8 col-md-8 col-xs-8 col-sm-8">
			<input class="search_query form-control" type="text" id="dor_query_top" name="search_query" value="<?php echo htmlspecialchars(stripslashes(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['search_query']->value,'html','UTF-8' ))), ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Type your search here...','mod'=>'dor_searchcategories'),$_smarty_tpl ) );?>
" autocomplete="off" />
			<button type="submit" name="submit_search" class="btn btn-default"><i class="pe-7s-search"></i></button>
        </div>
        <label for="dor_query_top"></label>
        <input type="hidden" name="controller" value="search" />
        <input type="hidden" name="orderby" value="position" />
        <input type="hidden" name="orderby" value="categories" />
        <input type="hidden" name="orderway" value="desc" />
        <input type="hidden" name="valSelected" value="0" />
    </form>
</div>
<!-- /pos search module TOP -->
<?php }
}
