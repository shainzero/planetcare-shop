<?php
/* Smarty version 3.1.32, created on 2018-11-19 10:21:46
  from 'module:pscustomersigninpscustome' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf280aa76de03_93807801',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd5f8f570180f74d1dbdd1a1d2af0445e90a6650c' => 
    array (
      0 => 'module:pscustomersigninpscustome',
      1 => 1541767127,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf280aa76de03_93807801 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="user-info selection-options-wrapper">
  <label class="hidden"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Account','d'=>'Shop.Theme'),$_smarty_tpl ) );?>
</label>
  <span class="line-selected hidden"><i class="pe-7s-user"></i> <span class="select-hidden hidden"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Account','d'=>'Shop.Theme'),$_smarty_tpl ) );?>
</span></span>
  <ul class="toogle_content">
    <li><a class="link-myaccount" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['my_account_url']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View my customer account','d'=>'Shop.Theme.CustomerAccount'),$_smarty_tpl ) );?>
"><i class="pe-7s-users"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My account','d'=>'Shop.Theme.CustomerAccount'),$_smarty_tpl ) );?>
</a></li>
    <li><a class="link-wishlist wishlist_block" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('dorblockwishlist','dorwishlist',array(),true),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My wishlist','d'=>'Shop.Theme.CustomerAccount'),$_smarty_tpl ) );?>
"><i class="pe-7s-like"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My wishlist','d'=>'Shop.Theme.CustomerAccount'),$_smarty_tpl ) );?>
</a></li>
    <li><a class="link-mycart" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['cart'], ENT_QUOTES, 'UTF-8');?>
?action=show" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My cart','d'=>'Shop.Theme.CustomerAccount'),$_smarty_tpl ) );?>
">
    <i class="pe-7s-cart"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My cart','d'=>'Shop.Theme.CustomerAccount'),$_smarty_tpl ) );?>
</a></li>
    <?php if ($_smarty_tpl->tpl_vars['logged']->value) {?>
    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['logout_url']->value, ENT_QUOTES, 'UTF-8');?>
" class="btn btn-default signout-button" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Log out to your customer account','d'=>'Shop.Theme.CustomerAccount'),$_smarty_tpl ) );?>
"><span><i class="material-icons">&#xE0DA;</i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign out','d'=>'Shop.Theme.CustomerAccount'),$_smarty_tpl ) );?>
</span></a>
    <?php } else { ?>
    <li><a href="#" onclick="return false" class="smartLogin"><i class="pe-7s-key"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign in popup','d'=>'Shop.Theme.CustomerAccount'),$_smarty_tpl ) );?>
</a></li>
    <li><a href="#" onclick="return false" class="smartRegister"><i class="pe-7s-add-user"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sign up popup','d'=>'Shop.Theme.CustomerAccount'),$_smarty_tpl ) );?>
</a></li>
    <?php }?>
  </ul>
</div>
<?php }
}
