<?php
/* Smarty version 3.1.32, created on 2018-11-20 12:35:03
  from 'C:\xampp\htdocs\planetcare\themes\dor_organick1\modules\smartblogcategories\views\templates\front\smartblogcategories.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bf3f16786c0d7_52773913',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9ff08b61a654a85918fcf74204fefd760a160f19' => 
    array (
      0 => 'C:\\xampp\\htdocs\\planetcare\\themes\\dor_organick1\\modules\\smartblogcategories\\views\\templates\\front\\smartblogcategories.tpl',
      1 => 1501167222,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bf3f16786c0d7_52773913 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '20344627075bf3f16784b8a5_35096256';
if (isset($_smarty_tpl->tpl_vars['categories']->value) && !empty($_smarty_tpl->tpl_vars['categories']->value)) {?>
<div id="category_blog_block_left"  class="block blogModule boxPlain">
  <div class="section-title"><h2 class="title_block"><a href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_list'), ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Categories','mod'=>'smartblogcategories'),$_smarty_tpl ) );?>
</a></h2></div>
   <div class="sideber-menu">
      <ul>
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value, 'category');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
?>
      <?php $_smarty_tpl->_assignInScope('options', null);?>
      <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['id_category'] = $_smarty_tpl->tpl_vars['category']->value['id_smart_blog_category'];
$_smarty_tpl->_assignInScope('options', $_tmp_array);?>
      <?php $_tmp_array = isset($_smarty_tpl->tpl_vars['options']) ? $_smarty_tpl->tpl_vars['options']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['slug'] = $_smarty_tpl->tpl_vars['category']->value['link_rewrite'];
$_smarty_tpl->_assignInScope('options', $_tmp_array);?>
        <li>
          <a href="<?php echo htmlspecialchars(smartblog::GetSmartBlogLink('smartblog_category',$_smarty_tpl->tpl_vars['options']->value), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['meta_title'], ENT_QUOTES, 'UTF-8');?>
</a>
        </li>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      </ul>
    </div>
</div>
<?php }
}
}
