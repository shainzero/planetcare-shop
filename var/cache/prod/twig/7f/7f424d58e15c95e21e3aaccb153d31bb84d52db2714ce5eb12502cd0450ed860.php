<?php

/* __string_template__12918a1297602d66ed77e695ef6df172c6f27116ade83c622abcc8f5dd48785a */
class __TwigTemplate_e6071c6ae3de5b09135bd366d32613ddc0643e9510844a14686ff746f4661749 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'extra_stylesheets' => array($this, 'block_extra_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
            'content_footer' => array($this, 'block_content_footer'),
            'sidebar_right' => array($this, 'block_sidebar_right'),
            'javascripts' => array($this, 'block_javascripts'),
            'extra_javascripts' => array($this, 'block_extra_javascripts'),
            'translate_javascripts' => array($this, 'block_translate_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/planetcare/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/planetcare/img/app_icon.png\" />

<title>Manage installed modules • PlanetCare</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminModulesManage';
    var iso_user = 'en';
    var lang_is_rtl = '0';
    var full_language_code = 'en-us';
    var full_cldr_language_code = 'en-US';
    var country_iso_code = 'SI';
    var _PS_VERSION_ = '1.7.4.3';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'A new order has been placed on your shop.';
    var order_number_msg = 'Order number: ';
    var total_msg = 'Total: ';
    var from_msg = 'From: ';
    var see_order_msg = 'View this order';
    var new_customer_msg = 'A new customer registered on your shop.';
    var customer_name_msg = 'Customer name: ';
    var new_msg = 'A new message was posted on your shop.';
    var see_msg = 'Read this message';
    var token = '6093160fd11941c19a50bee4ad1b1f97';
    var token_admin_orders = '86c8f0222ca8cfa14e4f97d7ba873f82';
    var token_admin_customers = '93f90d0f8554af1be719670e518c0328';
    var token_admin_customer_threads = '66530d4a722ba99d3d62af5f23c9e9c1';
    var currentIndex = 'index.php?controller=AdminModulesManage';
    var employee_token = '4bc4b395f035cb393439626d21a72672';
    var choose_language_translate = 'Choose language';
    var default_language = '8';
    var admin_modules_link = '/planetcare/admin283xmyh6j/index.php/module/catalog/recommended?route=admin_module_catalog_post&_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM';
    var tab_modules_list = '';
    var update_success_msg = 'Update successful';
    var errorLogin = 'PrestaShop was unable to log in to Addons. Please check your credentials and your Internet connection.';
    var search_product_msg = 'Search for a product';
  </script>

      <link href=\"/planetcare/modules/gamification/views/css/gamification.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/planetcare/admin283xmyh6j/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/planetcare/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/planetcare/modules/dor_themeoptions/css/admin.doradotheme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/planetcare/modules/dor_themeoptions/font/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/planetcare/modules/dorgallery/css/dorgallery.admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/planetcare/modules/dor_managerblocks/css/managerblock.admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/planetcare/modules/dor_ajaxtabproductcategory/assets/css/dor_ajaxtabproductcategory_admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/planetcare/modules/dor_ajaxtabsidebar_product/assets/css/dor_ajaxtabsidebar_product_admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/planetcare/modules/dor_dailydeals/views/css/dor_dailydeals_admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/planetcare/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/planetcare/admin283xmyh6j/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/planetcare\\/admin283xmyh6j\\/\";
var baseDir = \"\\/planetcare\\/\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euro\",\"format\":\"\\u00a4#,##0.00\"};
var host_mode = false;
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/planetcare/js/jquery/jquery-1.11.0.min.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/js/jquery/jquery-migrate-1.2.1.min.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/modules/gamification/views/js/gamification_bt.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/modules/dor_themeoptions/js/admin.doradotheme.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/modules/smartblog/js/admin.dorblog.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/modules/dorgallery/js/admin.dorgallery.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/modules/dor_managerblockfooter/js/dorblockfooter.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/modules/dor_bizproduct/js/dorbizproduct.admin.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/modules/dor_ajaxtabproductcategory/assets/js/dor_ajaxtabproductcategory_admin.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/modules/dor_ajaxtabsidebar_product/assets/js/dor_ajaxtabsidebar_product_admin.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/modules/dor_dailydeals/views/js/dor_dailydeals_admin.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/admin283xmyh6j/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/js/admin.js?v=1.7.4.3\"></script>
<script type=\"text/javascript\" src=\"/planetcare/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/js/tools.js?v=1.7.4.3\"></script>
<script type=\"text/javascript\" src=\"/planetcare/admin283xmyh6j/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/planetcare/admin283xmyh6j/themes/default/js/vendor/nv.d3.min.js\"></script>

  <script>
\t\t\t\tvar ids_ps_advice = new Array();
\t\t\t\tvar admin_gamification_ajax_url = 'http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminGamification&token=c307cafa8eb8b49d7141da46e8fdb626';
\t\t\t\tvar current_id_tab = 45;
\t\t\t</script><style>
.icon-AdminLabMenu:before{
  content: \"\\f03b\";
}
</style><style>
.icon-AdminSmartBlog:before{
  content: \"\\f14b\";
   }
 
</style><style>
.icon-AdminGalleryShow:before{
  content: \"\\f14b\";
   }
 
</style>

";
        // line 111
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>
<body class=\"lang-en adminmodulesmanage\">


<header id=\"header\">
  <nav id=\"header_infos\" class=\"main-header\">

    <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

        
        <i class=\"material-icons js-mobile-menu\">menu</i>
    <a id=\"header_logo\" class=\"logo float-left\" href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDashboard&amp;token=9634e06b2abcff210b9463fa5d37e93f\"></a>
    <span id=\"shop_version\">1.7.4.3</span>

    <div class=\"component\" id=\"quick-access-container\">
      <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Quick Access
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=4555bbc55ec6128823c7ab4d9e04b1ab\"
                 data-item=\"Catalog evaluation\"
      >Catalog evaluation</a>
          <a class=\"dropdown-item active\"
         href=\"http://localhost/planetcare/admin283xmyh6j/index.php/module/manage?token=806470ac91733c4f86dd75e57fd86caf\"
                 data-item=\"Installed modules\"
      >Installed modules</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCategories&amp;addcategory&amp;token=6086dd108732fc536df8255a80c45535\"
                 data-item=\"New category\"
      >New category</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/planetcare/admin283xmyh6j/index.php/product/new?token=806470ac91733c4f86dd75e57fd86caf\"
                 data-item=\"New product\"
      >New product</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=9cf90164dcc749c229708cf38d40cb9d\"
                 data-item=\"New voucher\"
      >New voucher</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminOrders&amp;token=86c8f0222ca8cfa14e4f97d7ba873f82\"
                 data-item=\"Orders\"
      >Orders</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminModules&amp;&amp;configure=smartblog&amp;token=530186f0f4742f20ea93882e1f1a6d5b\"
                 data-item=\"Smart Blog Setting\"
      >Smart Blog Setting</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-method=\"remove\"
        data-quicklink-id=\"5\"
        data-rand=\"111\"
        data-icon=\"icon-AdminModulesSf\"
        data-url=\"index.php/module/manage\"
        data-post-link=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminQuickAccesses&token=60950fa011323246c6eb5784ba3bd0b9\"
        data-prompt-text=\"Please name this shortcut:\"
        data-link=\"Installed modules - List\"
      >
        <i class=\"material-icons\">remove_circle_outline</i>
        Remove from QuickAccess
      </a>
        <a class=\"dropdown-item\" href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminQuickAccesses&token=60950fa011323246c6eb5784ba3bd0b9\">
      <i class=\"material-icons\">settings</i>
      Manage quick accesses
    </a>
  </div>
</div>
    </div>
    <div class=\"component\" id=\"header-search-container\">
      <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/planetcare/admin283xmyh6j/index.php?controller=AdminSearch&amp;token=02550725c81e939e9d6949b27b8cf7ce\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Search (e.g.: product reference, customer name…)\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        Everywhere
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"Everywhere\" href=\"#\" data-value=\"0\" data-placeholder=\"What are you looking for?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> Everywhere</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catalog\" href=\"#\" data-value=\"1\" data-placeholder=\"Product name, SKU, reference...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catalog</a>
        <a class=\"dropdown-item\" data-item=\"Customers by name\" href=\"#\" data-value=\"2\" data-placeholder=\"Email, name...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Customers by name</a>
        <a class=\"dropdown-item\" data-item=\"Customers by ip address\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Customers by IP address</a>
        <a class=\"dropdown-item\" data-item=\"Orders\" href=\"#\" data-value=\"3\" data-placeholder=\"Order ID\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Orders</a>
        <a class=\"dropdown-item\" data-item=\"Invoices\" href=\"#\" data-value=\"4\" data-placeholder=\"Invoice Number\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i></i> Invoices</a>
        <a class=\"dropdown-item\" data-item=\"Carts\" href=\"#\" data-value=\"5\" data-placeholder=\"Cart ID\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Carts</a>
        <a class=\"dropdown-item\" data-item=\"Modules\" href=\"#\" data-value=\"7\" data-placeholder=\"Module name\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Modules</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">SEARCH</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
    </div>

            <div class=\"component\" id=\"header-shop-list-container\">
        <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://localhost/planetcare/\" target= \"_blank\">
      <i class=\"material-icons\">visibility</i>
      View my shop
    </a>
  </div>
    </div>
          <div class=\"component header-right-component\" id=\"header-notifications-container\">
        <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Orders<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Customers<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Messages<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No new order for now :(<br>
              Have you checked your <strong><a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCarts&token=01481754c270995fe170cded57c6b0ed&action=filterOnlyAbandonedCarts\">abandoned carts</a></strong>?<br>Your next order could be hiding there!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No new customer for now :(<br>
              Have you considered selling on marketplaces?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No new message for now.<br>
              Seems like all your customers are happy :)
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      from <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registered <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
      </div>
        <div class=\"component\" id=\"header-employee-container\">
      <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"text-center employee_avatar\">
      <img class=\"avatar rounded-circle\" src=\"http://profile.prestashop.com/tina.kirn%40planetcare.org.jpg\" />
      <span>Tina Kirn</span>
    </div>
    <a class=\"dropdown-item employee-link profile-link\" href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminEmployees&amp;token=4bc4b395f035cb393439626d21a72672&amp;id_employee=1&amp;updateemployee\">
      <i class=\"material-icons\">settings_applications</i>
      Your profile
    </a>
    <a class=\"dropdown-item employee-link\" id=\"header_logout\" href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminLogin&amp;token=3398b5b50e5801e4277032db798e0b3d&amp;logout\">
      <i class=\"material-icons\">power_settings_new</i>
      <span>Sign out</span>
    </a>
  </div>
</div>
    </div>

      </nav>
  </header>

<nav class=\"nav-bar d-none d-md-block\">
  <span class=\"menu-collapse\">
    <i class=\"material-icons\">chevron_left</i>
    <i class=\"material-icons\">chevron_left</i>
  </span>

  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDashboard&amp;token=9634e06b2abcff210b9463fa5d37e93f\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Dashboard</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Sell</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminOrders&amp;token=86c8f0222ca8cfa14e4f97d7ba873f82\" class=\"link\">
                    <i class=\"material-icons mi-shopping_basket\">shopping_basket</i>
                    <span>
                    Orders
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminOrders&amp;token=86c8f0222ca8cfa14e4f97d7ba873f82\" class=\"link\"> Orders
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminInvoices&amp;token=08227a4a867502707a0e012da3d4554a\" class=\"link\"> Invoices
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminSlip&amp;token=d86400e6df51f2550faf3b1e7ace0c8c\" class=\"link\"> Credit Slips
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDeliverySlip&amp;token=d39752bcf31cfb16bd34b9d9af42f96d\" class=\"link\"> Delivery Slips
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCarts&amp;token=01481754c270995fe170cded57c6b0ed\" class=\"link\"> Shopping Carts
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/planetcare/admin283xmyh6j/index.php/product/catalog?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\">
                    <i class=\"material-icons mi-store\">store</i>
                    <span>
                    Catalog
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/planetcare/admin283xmyh6j/index.php/product/catalog?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\"> Products
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCategories&amp;token=6086dd108732fc536df8255a80c45535\" class=\"link\"> Categories
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminTracking&amp;token=229f61fb4f698920b7ce728cd37e1d69\" class=\"link\"> Monitoring
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminAttributesGroups&amp;token=51153de098dec1a78db9513ec3e2ea10\" class=\"link\"> Attributes &amp; Features
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminManufacturers&amp;token=50d803d115e3bda32c594cfadf7891e9\" class=\"link\"> Brands &amp; Suppliers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminAttachments&amp;token=362ed67903c43fa884baca25c95b65a1\" class=\"link\"> Files
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCartRules&amp;token=9cf90164dcc749c229708cf38d40cb9d\" class=\"link\"> Discounts
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/planetcare/admin283xmyh6j/index.php/stock/?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCustomers&amp;token=93f90d0f8554af1be719670e518c0328\" class=\"link\">
                    <i class=\"material-icons mi-account_circle\">account_circle</i>
                    <span>
                    Customers
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCustomers&amp;token=93f90d0f8554af1be719670e518c0328\" class=\"link\"> Customers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminAddresses&amp;token=2cef5e2d29501426ec3a0b31d0324a45\" class=\"link\"> Addresses
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCustomerThreads&amp;token=66530d4a722ba99d3d62af5f23c9e9c1\" class=\"link\">
                    <i class=\"material-icons mi-chat\">chat</i>
                    <span>
                    Customer Service
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCustomerThreads&amp;token=66530d4a722ba99d3d62af5f23c9e9c1\" class=\"link\"> Customer Service
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminOrderMessage&amp;token=d1e2a81007648767ec36e28173491919\" class=\"link\"> Order Messages
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminReturn&amp;token=2c914b69b0edae804249360ced4361f0\" class=\"link\"> Merchandise Returns
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminStats&amp;token=4555bbc55ec6128823c7ab4d9e04b1ab\" class=\"link\">
                    <i class=\"material-icons mi-assessment\">assessment</i>
                    <span>
                    Stats
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Improve</span>
          </li>

                          
                
                                                
                                                    
                <li class=\"link-levelone has_submenu -active open ul-open\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"/planetcare/admin283xmyh6j/index.php/module/manage?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Modules
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_up
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/planetcare/admin283xmyh6j/index.php/module/manage?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\"> Modules &amp; Services
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\" id=\"subtab-AdminAddonsCatalog\">
                              <a href=\"/planetcare/admin283xmyh6j/index.php/module/addons-store?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\"> Modules Catalog
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"50\" id=\"subtab-AdminParentThemes\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminThemes&amp;token=02d3cfd2366be32f5f8fdc42c2a88f63\" class=\"link\">
                    <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                    <span>
                    Design
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-50\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"121\" id=\"subtab-AdminThemesParent\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminThemes&amp;token=02d3cfd2366be32f5f8fdc42c2a88f63\" class=\"link\"> Theme &amp; Logo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"52\" id=\"subtab-AdminThemesCatalog\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminThemesCatalog&amp;token=8902820765edc88d5c4d4f8c2f5a174d\" class=\"link\"> Theme Catalog
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"53\" id=\"subtab-AdminCmsContent\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCmsContent&amp;token=3231715bac0bf70d0aa7c007074d633b\" class=\"link\"> Pages
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminModulesPositions&amp;token=0ed564cabbba73b4a493638dec5e42b0\" class=\"link\"> Positions
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminImages\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminImages&amp;token=6d9fe3485d2274f6ee07b1d98c28cae4\" class=\"link\"> Image Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"120\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminLinkWidget&amp;token=1d2b4b09298c679d7c01ad475fe07275\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"56\" id=\"subtab-AdminParentShipping\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCarriers&amp;token=f8154526c6668b4fc39001fc895b8208\" class=\"link\">
                    <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                    <span>
                    Shipping
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-56\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminCarriers\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminCarriers&amp;token=f8154526c6668b4fc39001fc895b8208\" class=\"link\"> Carriers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"58\" id=\"subtab-AdminShipping\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminShipping&amp;token=253c2296fc1fdefc7112f2d75ea28c92\" class=\"link\"> Preferences
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"59\" id=\"subtab-AdminParentPayment\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminPayment&amp;token=1f44bf40d67d783db7ffa76b4a171264\" class=\"link\">
                    <i class=\"material-icons mi-payment\">payment</i>
                    <span>
                    Payment
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-59\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"60\" id=\"subtab-AdminPayment\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminPayment&amp;token=1f44bf40d67d783db7ffa76b4a171264\" class=\"link\"> Payment Methods
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"61\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminPaymentPreferences&amp;token=d315571d6469f4f53f0bb1dbe1736f28\" class=\"link\"> Preferences
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"62\" id=\"subtab-AdminInternational\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminLocalization&amp;token=4caad4768854e99aac0ce69c3caed72b\" class=\"link\">
                    <i class=\"material-icons mi-language\">language</i>
                    <span>
                    International
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-62\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"63\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminLocalization&amp;token=4caad4768854e99aac0ce69c3caed72b\" class=\"link\"> Localization
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"68\" id=\"subtab-AdminParentCountries\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminZones&amp;token=4a11da0d0f4be475ce8558090def759a\" class=\"link\"> Locations
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"72\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminTaxes&amp;token=6abd68f57e1b338cdd1afe9daa432d7c\" class=\"link\"> Taxes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"75\" id=\"subtab-AdminTranslations\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminTranslations&amp;token=5209e30ec4d901606f555d2eac640fdd\" class=\"link\"> Translations
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"76\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Configure</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"77\" id=\"subtab-ShopParameters\">
                  <a href=\"/planetcare/admin283xmyh6j/index.php/configure/shop/preferences?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\">
                    <i class=\"material-icons mi-settings\">settings</i>
                    <span>
                    Shop Parameters
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-77\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"78\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"/planetcare/admin283xmyh6j/index.php/configure/shop/preferences?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\"> Configuration
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminOrderPreferences&amp;token=7216be7c4db79074de7d940c9ed6634f\" class=\"link\"> Order Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"84\" id=\"subtab-AdminPPreferences\">
                              <a href=\"/planetcare/admin283xmyh6j/index.php/configure/shop/product_preferences?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\"> Products
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"/planetcare/admin283xmyh6j/index.php/configure/shop/customer_preferences?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\"> Customer Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"89\" id=\"subtab-AdminParentStores\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminContacts&amp;token=7c748cf6f31af1e8818ac8f55e427bf7\" class=\"link\"> Contact
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"92\" id=\"subtab-AdminParentMeta\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminMeta&amp;token=901f433c54ac66db4f5bedac08a86ab5\" class=\"link\"> Traffic &amp; SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminSearchConf&amp;token=9188cce48a7209b78104aa4de1c4ca94\" class=\"link\"> Search
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"125\" id=\"subtab-AdminGamification\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminGamification&amp;token=c307cafa8eb8b49d7141da46e8fdb626\" class=\"link\"> Merchant Expertise
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"99\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/planetcare/admin283xmyh6j/index.php/configure/advanced/system_information?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\">
                    <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                    <span>
                    Advanced Parameters
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-99\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\" id=\"subtab-AdminInformation\">
                              <a href=\"/planetcare/admin283xmyh6j/index.php/configure/advanced/system_information?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\"> Information
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\" id=\"subtab-AdminPerformance\">
                              <a href=\"/planetcare/admin283xmyh6j/index.php/configure/advanced/performance?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\"> Performance
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"102\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"/planetcare/admin283xmyh6j/index.php/configure/advanced/administration?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\"> Administration
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"103\" id=\"subtab-AdminEmails\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminEmails&amp;token=d244b7c27e89a6d3f75aea51d8dcecf2\" class=\"link\"> E-mail
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"104\" id=\"subtab-AdminImport\">
                              <a href=\"/planetcare/admin283xmyh6j/index.php/configure/advanced/import?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" class=\"link\"> Import
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"105\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminEmployees&amp;token=4bc4b395f035cb393439626d21a72672\" class=\"link\"> Employees
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminRequestSql&amp;token=81075fdc67c54474781a0d8db80b2f89\" class=\"link\"> Database
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"112\" id=\"subtab-AdminLogs\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminLogs&amp;token=7b5c7e5d5f0bcdf420204547376d9164\" class=\"link\"> Logs
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"113\" id=\"subtab-AdminWebservice\">
                              <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminWebservice&amp;token=c9f3b5328680b4cb111aea309f00f2f3\" class=\"link\"> Webservice
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"127\" id=\"tab-AdminDorMenu\">
              <span class=\"title\">Dor Extensions</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"128\" id=\"subtab-Admindorthemeoptions\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=Admindorthemeoptions&amp;token=0c3b6fdb5a320ca148130a72e1924366\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Theme Configuration
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"140\" id=\"subtab-AdminDorHomeSlider\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorHomeSlider&amp;token=6f89c2d71d1422b643fd11b27282ee62\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Slider Homepage
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"141\" id=\"subtab-AdminDorManagerBlocks\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorManagerBlocks&amp;token=06ee3c21b902a310731c9f1c41550bfa\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Manage Content Blocks Html
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"142\" id=\"subtab-AdminDorManagerFooter\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorManagerFooter&amp;token=ce40c83e81f5e72da23894031e4f5ee6\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Manage Footer Blocks
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"144\" id=\"subtab-AdminTestimonials\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminTestimonials&amp;token=a35113f2aaf31d14dfc1cc24acab11e7\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Manage Testimonials
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"145\" id=\"subtab-AdminDorBizproduct\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorBizproduct&amp;token=48102b430b27e02f5ddcc3cf3f7ea8de\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Biz Product
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"146\" id=\"subtab-AdminDorTabCategory\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorTabCategory&amp;token=efa2ab6011d0bf6c9ecb75680c552c70\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Ajax Tab Product and Category
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"147\" id=\"subtab-AdminDorPriceFilter\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorPriceFilter&amp;token=bdd329278c448e112a70948af76c53eb\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Price Filter
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"148\" id=\"subtab-AdminDorOrderTracking\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorOrderTracking&amp;token=61acbeb2fd744503af60fb2aa44468a4\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Order Tracking
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"150\" id=\"subtab-AdminDorProductComment\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorProductComment&amp;token=47bf5a610504f66a0cb5bb97c7e7cbb3\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Comment Review Management
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"151\" id=\"subtab-AdminDorBlockTags\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorBlockTags&amp;token=b50af3093c5d1c697e21f3707974d4a2\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Block Tags
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"152\" id=\"subtab-AdminDorRelated\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorRelated&amp;token=910f515ab77baae1b99553760c9becda\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Related Products
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"153\" id=\"subtab-AdminDorTabSidebar\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorTabSidebar&amp;token=8c710f9fc5b4956533ea84bc9409182c\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Ajax Tabs Sidebar
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"154\" id=\"subtab-AdminDorDailyDeal\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorDailyDeal&amp;token=dda94f865d72c600eb9506764de94220\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Daily Deals
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"155\" id=\"subtab-AdminDorVideoProducts\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorVideoProducts&amp;token=77c113a112dc405a93f6dde63d96145c\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Manage Video Products
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"156\" id=\"subtab-AdminDorCompare\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorCompare&amp;token=4b04551b1bb799ed2b43d032c7178c2f\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Comparison
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"157\" id=\"subtab-AdminDorHomeLatestNews\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDorHomeLatestNews&amp;token=2acc3132d79a7f33860f12c0860a0509\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Home Latest News
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"129\" id=\"tab-AdminSmartBlog\">
              <span class=\"title\">Blog</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"130\" id=\"subtab-AdminBlogSetup\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminBlogSetup&amp;token=49c57b5900f7901a22c9cb9d1aabf6f0\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Blog Configure
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"131\" id=\"subtab-AdminBlogCategory\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminBlogCategory&amp;token=142e9192e1f546895c79819065389442\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Blog Category
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"132\" id=\"subtab-AdminBlogcomment\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminBlogcomment&amp;token=0f950c20fdaf627167a37afa238d55d7\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Blog Comments
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"133\" id=\"subtab-AdminBlogPost\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminBlogPost&amp;token=586ae8187833dc5dcb5a3eb9a11f30ab\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Blog Post
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"134\" id=\"subtab-AdminImageType\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminImageType&amp;token=c674ffb179376f96be9d4840ea88b5cb\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Image Type
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"136\" id=\"tab-AdminGalleryShow\">
              <span class=\"title\">Dor Gallery</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"137\" id=\"subtab-AdminGallerySetup\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminGallerySetup&amp;token=f82a6fffc6ae5ddd6aade4c10a87aed2\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Gallery Configure
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"138\" id=\"subtab-AdminGalleryCategory\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminGalleryCategory&amp;token=a10874cf9f91449b716e7483983139bd\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Gallery Category
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"139\" id=\"subtab-AdminGalleryItem\">
                  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminGalleryItem&amp;token=7c94e09e9be2cc455c29b6aba2603966\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Dor Gallery Item
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
            </ul>
  
</nav>

<div id=\"main-div\">

  
    
<div class=\"header-toolbar\">
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Modules &amp; Services</li>
          
                      <li class=\"breadcrumb-item active\">
              <a href=\"/planetcare/admin283xmyh6j/index.php/module/manage?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" aria-current=\"page\">Installed modules</a>
            </li>
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
          <h1 class=\"title\">
            Manage installed modules          </h1>
      

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
            
                                                          <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-add_module\"
                  href=\"#\"                  title=\"Upload a module\"                  data-toggle=\"pstooltip\"
                  data-placement=\"bottom\"                >
                  <i class=\"material-icons\">cloud_upload</i>
                  Upload a module
                </a>
                                                                        <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-addons_connect\"
                  href=\"#\"                  title=\"Connect to Addons marketplace\"                  data-toggle=\"pstooltip\"
                  data-placement=\"bottom\"                >
                  <i class=\"material-icons\">vpn_key</i>
                  Connect to Addons marketplace
                </a>
                                                  
                              <a class=\"btn btn-outline-secondary btn-help btn-sidebar\" href=\"#\"
                   title=\"Help\"
                   data-toggle=\"sidebar\"
                   data-target=\"#right-sidebar\"
                   data-url=\"/planetcare/admin283xmyh6j/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop.com%252Fen%252Fdoc%252FAdminModules%253Fversion%253D1.7.4.3%2526country%253Den/Help?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\"
                   id=\"product_form_open_help\"
                >
                  Help
                </a>
                                    </div>
        </div>
      
    </div>
  </div>

  
      <div class=\"page-head-tabs\" id=\"head_tabs\">
      <ul class=\"nav nav-pills\">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <li class=\"nav-item\">
                    <a href=\"/planetcare/admin283xmyh6j/index.php/module/manage?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" id=\"subtab-AdminModulesManage\" class=\"nav-link tab active current\" data-submenu=\"45\">Installed modules</a>
                  </li>
                                                                <li class=\"nav-item\">
                    <a href=\"/planetcare/admin283xmyh6j/index.php/module/catalog?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" id=\"subtab-AdminModulesCatalog\" class=\"nav-link tab \" data-submenu=\"46\">Selection</a>
                  </li>
                                                                <li class=\"nav-item\">
                    <a href=\"/planetcare/admin283xmyh6j/index.php/module/notifications?_token=uKLcMNE0ijWb4tup5JytwCEgxrmrEr633a3wF57OQaM\" id=\"subtab-AdminModulesNotifications\" class=\"nav-link tab \" data-submenu=\"47\">Notifications</a>
                  </li>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </ul>
    </div>
    
</div>
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-EN&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/en/login?email=tina.kirn%40planetcare.org&amp;firstname=Tina&amp;lastname=Kirn&amp;website=http%3A%2F%2Flocalhost%2Fplanetcare%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-EN&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/planetcare/admin283xmyh6j/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Connect your shop to PrestaShop's marketplace in order to automatically import all your Addons purchases.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Don't have an account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Discover the Power of PrestaShop Addons! Explore the PrestaShop Official Marketplace and find over 3 500 innovative modules and themes that optimize conversion rates, increase traffic, build customer loyalty and maximize your productivity</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Connect to PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/en/forgot-your-password\">I forgot my password</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/en/login?email=tina.kirn%40planetcare.org&amp;firstname=Tina&amp;lastname=Kirn&amp;website=http%3A%2F%2Flocalhost%2Fplanetcare%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-EN&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tCreate an Account
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Sign in
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

    <div class=\"content-div  with-tabs\">

      

      
                        
      <div class=\"row \">
        <div class=\"col-sm-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>


  ";
        // line 1578
        $this->displayBlock('content_header', $context, $blocks);
        // line 1579
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 1580
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 1581
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 1582
        echo "
          
        </div>
      </div>

    </div>

  
</div>

<div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>Oh no!</h1>
  <p class=\"mt-3\">
    The mobile version of this page is not available yet.
  </p>
  <p class=\"mt-2\">
    Please use a desktop computer to access this page, until is adapted to mobile.
  </p>
  <p class=\"mt-2\">
    Thank you.
  </p>
  <a href=\"http://localhost/planetcare/admin283xmyh6j/index.php?controller=AdminDashboard&amp;token=9634e06b2abcff210b9463fa5d37e93f\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Back
  </a>
</div>
<div class=\"mobile-layer\"></div>

  <div id=\"footer\" class=\"bootstrap\">
    
</div>


  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-EN&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/en/login?email=tina.kirn%40planetcare.org&amp;firstname=Tina&amp;lastname=Kirn&amp;website=http%3A%2F%2Flocalhost%2Fplanetcare%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-EN&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/planetcare/admin283xmyh6j/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Connect your shop to PrestaShop's marketplace in order to automatically import all your Addons purchases.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Don't have an account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Discover the Power of PrestaShop Addons! Explore the PrestaShop Official Marketplace and find over 3 500 innovative modules and themes that optimize conversion rates, increase traffic, build customer loyalty and maximize your productivity</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Connect to PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/en/forgot-your-password\">I forgot my password</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/en/login?email=tina.kirn%40planetcare.org&amp;firstname=Tina&amp;lastname=Kirn&amp;website=http%3A%2F%2Flocalhost%2Fplanetcare%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-EN&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tCreate an Account
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Sign in
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

  </div>

";
        // line 1691
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
    }

    // line 111
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    public function block_extra_stylesheets($context, array $blocks = array())
    {
    }

    // line 1578
    public function block_content_header($context, array $blocks = array())
    {
    }

    // line 1579
    public function block_content($context, array $blocks = array())
    {
    }

    // line 1580
    public function block_content_footer($context, array $blocks = array())
    {
    }

    // line 1581
    public function block_sidebar_right($context, array $blocks = array())
    {
    }

    // line 1691
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function block_extra_javascripts($context, array $blocks = array())
    {
    }

    public function block_translate_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "__string_template__12918a1297602d66ed77e695ef6df172c6f27116ade83c622abcc8f5dd48785a";
    }

    public function getDebugInfo()
    {
        return array (  1770 => 1691,  1765 => 1581,  1760 => 1580,  1755 => 1579,  1750 => 1578,  1741 => 111,  1733 => 1691,  1622 => 1582,  1619 => 1581,  1616 => 1580,  1613 => 1579,  1611 => 1578,  140 => 111,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "__string_template__12918a1297602d66ed77e695ef6df172c6f27116ade83c622abcc8f5dd48785a", "");
    }
}
