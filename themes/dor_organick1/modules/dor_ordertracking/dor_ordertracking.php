<?php
if (!defined('_PS_VERSION_'))
	exit;
require_once(_PS_MODULE_DIR_ . 'dor_ordertracking/models/OrderTrackingProduct.php');
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Core\Product\ProductPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
use PrestaShop\PrestaShop\Core\Product\ProductExtraContentFinder;
class Dor_ordertracking extends Module
{
	public function __construct()
	{
		$this->name = 'dor_ordertracking';
		$this->tab = 'front_office_features';
		$this->version = '2.0.0';
		$this->author = 'Dorado Themes';
		$this->controllers = array('ordertracking','ordertrackingdetail');
		$this->bootstrap = true;
		parent::__construct();
		$this->_staticModel = new OrderTrackingProduct();
		$this->displayName = $this->l('Dor Order Tracking');
		$this->description = $this->l('Dor Order Tracking');
		$this->confirmUninstall = $this->l('Are you sure about removing these details?');
	}

	public function install()
	{
        $res = $this->installDb();
		if(!(int)Tab::getIdFromClassName('AdminDorMenu')) {
            $parent_tab = new Tab();
            // Need a foreach for the language
            $parent_tab->name[$this->context->language->id] = $this->l('Dor Extensions');
            $parent_tab->class_name = 'AdminDorMenu';
            $parent_tab->id_parent = 0; // Home tab
            $parent_tab->module = $this->name;
            $parent_tab->add();
        }
        $tab = new Tab();
        foreach (Language::getLanguages() as $language)
        $tab->name[$language['id_lang']] = $this->l('Dor Order Tracking');
        $tab->class_name = 'AdminDorOrderTracking';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminDorMenu'); 
        $tab->module = $this->name;
        $tab->add();
        $today = date("Y-m-d");
        Configuration::updateValue($this->name . '_maxitem',3);
        Configuration::updateValue($this->name . '_thumbwidth',450);
        Configuration::updateValue($this->name . '_thumbheight',450);
        Configuration::updateValue($this->name . '_quanlity',100);
        return parent::install() &&
                $this->registerHook('header')
                &&
                $this->registerHook('backOfficeHeader')
                &&
                $this->registerHook('dorOrderTrackingTop')
                &&
				$this->registerHook('dorOrderTracking');
	}
	public function uninstall()
	{
        $res = $this->uninstallDb();
		$tab = new Tab((int) Tab::getIdFromClassName('AdminDorOrderTracking'));
        $tab->delete();
        Configuration::deleteByName($this->name . '_maxitem');
        Configuration::deleteByName($this->name . '_thumbwidth');
        Configuration::deleteByName($this->name . '_thumbheight');
        Configuration::deleteByName($this->name . '_quanlity');
		if (!parent::uninstall() ||
            !$this->unregisterHook('header') ||
            !$this->unregisterHook('backOfficeHeader') ||
            !$this->unregisterHook('dorOrderTrackingTop') ||
            !$this->unregisterHook('dorOrderTracking')) {
            return false;
        }
        return true;
        return (bool)$res;
	}

    public function installDb(){
        return true;
    }

    private function uninstallDb() {
        return true;
    }

	public function hookHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).'assets/css/ordertracking.css', 'all');
		$this->context->controller->addJS(($this->_path).'assets/js/ordertracking.js');
		
        $linkModule = $this->context->link->getModuleLink('dor_ordertracking', 'ordertracking');
        Media::addJsDef(
            array(
                'DORTRACKING' => array(
                    "linkModule"=>$linkModule
                )
            )
        );


	}
	public function hookBackOfficeHeader()
	{
		if(!Tools::getIsset('configure') || Tools::getValue('configure') != $this->name)
			return;

		$this->context->controller->addCSS(array());
		$this->context->controller->addJquery();
		$this->context->controller->addJS(array());
	}
    public function hookDorOrderTrackingTop($params)
    {
        return $this->display(__FILE__, 'dor_ordertracking_top.tpl');
    }
    public function hookDorOrderTracking($params)
    {
        $product = $params['product'];
    }
	
	private function _postProcess() {
        Configuration::updateValue($this->name . '_maxitem', Tools::getValue('maxitem'));
        Configuration::updateValue($this->name . '_thumbwidth', Tools::getValue('thumbwidth'));
        Configuration::updateValue($this->name . '_thumbheight', Tools::getValue('thumbheight'));
        Configuration::updateValue($this->name . '_quanlity', Tools::getValue('quanlity'));
        $this->_html .= $this->displayConfirmation($this->l('Configuration updated'));
    }
	public function getContent()
	{
		$this->_postErrors = isset($this->_postErrors)?$this->_postErrors:array();
		$this->_html = isset($this->_html)?$this->_html:"";
		$this->context->smarty->assign(array());
		$output = '<h2>' . $this->displayName . '</h2>';
        if (Tools::isSubmit('submitProductCompare')) {
            if (!sizeof($this->_postErrors))
                $this->_postProcess();
            else {
                foreach ($this->_postErrors AS $err) {
                    $this->_html .= '<div class="alert error">' . $err . '</div>';
                }
            }
        }
        return $output . $this->_displayForm();
	}

	public  function _displayForm() {
		$id_lang = (int)Context::getContext()->language->id;
		$languages = $this->context->controller->getLanguages();
		$default_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $id_shop = (int)Context::getContext()->shop->id;
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => 'Limit Items:',
                        'name' => 'maxitem',
                        'class' => 'fixed-width-md',
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Thumb Width:',
                        'name' => 'thumbwidth',
                        'class' => 'fixed-width-md',
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Thumb Height:',
                        'name' => 'thumbheight',
                        'class' => 'fixed-width-md',
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Quanlity Image:',
                        'name' => 'quanlity',
                        'class' => 'fixed-width-md',
                    )

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitProductCompare';
        $helper->module = $this;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        $helper->override_folder = '/';
        return $helper->generateForm(array($fields_form));
    }
    public function getConfigFieldsValues()
    {
        return array(
            'maxitem' => Tools::getValue('maxitem', Configuration::get($this->name . '_maxitem')),
            'thumbwidth' => Tools::getValue('thumbwidth', Configuration::get($this->name . '_thumbwidth')),
            'thumbheight' => Tools::getValue('thumbheight', Configuration::get($this->name . '_thumbheight')),
            'quanlity' => Tools::getValue('quanlity', Configuration::get($this->name . '_quanlity')),
        );
    }
    
}
