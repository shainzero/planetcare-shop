<?php
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Core\Product\ProductPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
use PrestaShop\PrestaShop\Core\Product\ProductExtraContentFinder;
use PrestaShop\PrestaShop\Adapter\Order\OrderPresenter;
if (!class_exists( 'DorImageBase' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/DorImageBase.php');
}
require_once(_PS_MODULE_DIR_ . 'dor_ordertracking/models/OrderTrackingProduct.php');
class dor_ordertrackingOrdertrackingModuleFrontController extends ModuleFrontController
{
	public $ssl = true;
    public $order_presenter;
	public function __construct()
	{
		parent::__construct();
		$this->_staticModel = new OrderTrackingProduct();
		$this->context = Context::getContext();
		$this->nameModule = "dor_ordertracking";
	}

	public function initContent()
	{
		parent::initContent();
        $this->order_presenter = new OrderPresenter();
		$this->context->smarty->assign(array());
		$this->context->smarty->assign(array('page_name'=>"dor-page-ordertracking"));
        $reference = "";
        $email = "";
        $orders = array();
        $ordersDetail = array();
        if(isset($_GET) && isset($_GET['reference']) && isset($_GET['email']) && $_GET['reference'] != "" && $_GET['email'] != ""){
            $reference = $_GET['reference'];
            $email = $_GET['email'];
            $orders = $this->getTemplateVarOrders($reference, $email);
            if(count($orders) > 0){
                foreach ($orders as $key => $order) {
                    $products = $order['products'];
                    $ordersDetail[$key] = $order['details'];
                    $ordersHistory = $order['history'];
                    $status = $ordersHistory['current']['ostate_name'];
                    $ordersDetail[$key]['status'] = $status;
                    $assembler = new ProductAssembler($this->context);
                    $presenterFactory = new ProductPresenterFactory($this->context);
                    $presentationSettings = $presenterFactory->getPresentationSettings();
                    $presenter = new ProductPresenter(
                        new ImageRetriever(
                            $this->context->link
                        ),
                        $this->context->link,
                        new PriceFormatter(),
                        new ProductColorsRetriever(),
                        $this->context->getTranslator()
                    );

                    $products_for_template = [];

                    foreach ($products as $rawProduct) {
                        $products_for_template[] = $presenter->present(
                            $presentationSettings,
                            $assembler->assembleProduct($rawProduct),
                            $this->context->language
                        ); 
                    }
                    $orders[$key]['products'] = $products_for_template;
                }
                $hasOrder = 1;
            }else{
                $hasOrder = 2;
            }
        }else{
            $hasOrder = 0;
        }
        $cartUrl = $this->context->link->getPageLink('cart', true);
        $this->context->smarty->assign('static_token', Tools::getToken(false));
        $this->context->smarty->assign(array(
            'orders' => $orders,
            'ordersDetail' => $ordersDetail,
            'hasOrder' => $hasOrder,
            'cartUrl' => $cartUrl
        ));
		$this->setTemplate('ordertracking/ordertracking.tpl'); 
	}

    public function getTemplateVarOrders($reference, $email)
    {
        $orders = array();
        $id_shop = (int)Context::getContext()->shop->id;
        $customer_orders = OrderTrackingProduct::getCustomerOrders($this->context->customer->id,$reference, $email,$id_shop);

        foreach ($customer_orders as $customer_order) {
            $order = new Order((int) $customer_order['id_order']);
            $orders[$customer_order['id_order']] = $this->order_presenter->present($order);
        }
        
        return $orders;
    }

    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();
    
        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Tracking your order', [], 'Breadcrumb'),
            'url' => $this->context->link->getModuleLink('dor_ordertracking', 'ordertracking')
         ];
         return $breadcrumb;
    }
	
}
