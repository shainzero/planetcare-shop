<?php
if (!defined('_PS_VERSION_'))
    exit;
 
require_once (_PS_MODULE_DIR_.'smartblog/classes/SmartBlogPost.php');
require_once (_PS_MODULE_DIR_.'smartblog/smartblog.php');
if (!class_exists( 'DorImageBase' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/DorImageBase.php');
}
if (!class_exists( 'DorCaches' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/Caches/DorCaches.php');
}

class smartbloghomelatestnews extends Module {
    
     public function __construct(){
        $this->name = 'smartbloghomelatestnews';
        $this->tab = 'front_office_features';
        $this->version = '2.1.0';
        $this->bootstrap = true;
        $this->author = 'Dorado Themes';
        $this->secure_key = Tools::encrypt($this->name);

        parent::__construct();

        $this->displayName = $this->l('Dor Home Latest News');
        $this->description = $this->l('The Most Powerfull Presta shop Blog  Module\'s tag - by doradothemes');
        $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');
        $this->templateFile = 'module:smartbloghomelatestnews/views/templates/front/dorblog_home_style1';
        $this->templateFile1 = 'module:smartbloghomelatestnews/views/templates/front/smartblog_latest_news.tpl';
        $this->templateFile2 = 'module:smartbloghomelatestnews/views/templates/front/dorblog_home_style2.tpl';
        $this->templateFile3 = 'module:smartbloghomelatestnews/views/templates/front/dorblog_home_style3.tpl';
        $this->templateFile4 = 'module:smartbloghomelatestnews/views/templates/front/dorblog_latest_news_home_style4.tpl';



        }
        
     public function install(){
                $langs = Language::getLanguages();
                $id_lang = (int) Configuration::get('PS_LANG_DEFAULT');

                // Install Tabs
                if(!(int)Tab::getIdFromClassName('AdminDorMenu')) {
                    $parent_tab = new Tab();
                    // Need a foreach for the language
                    $parent_tab->name[$this->context->language->id] = Context::getContext()->getTranslator()->trans('Dor Extensions', array(), 'Modules.smartbloghomelatestnews');
                    $parent_tab->class_name = 'AdminDorMenu';
                    $parent_tab->id_parent = 0; // Home tab
                    $parent_tab->module = $this->name;
                    $parent_tab->add();
                }
                $tab = new Tab();
                foreach (Language::getLanguages() as $language)
                $tab->name[$language['id_lang']] = Context::getContext()->getTranslator()->trans('Dor Home Latest News', array(), 'Modules.smartbloghomelatestnews');
                $tab->class_name = 'AdminDorHomeLatestNews';
                $tab->id_parent = (int)Tab::getIdFromClassName('AdminDorMenu'); 
                $tab->module = $this->name;
                $tab->add();

                 if (!parent::install() 
                 || !$this->registerHook('header')
				 || !$this->registerHook('actionsbdeletepost')
				 || !$this->registerHook('actionsbnewpost')
				 || !$this->registerHook('actionsbupdatepost')
                 || !$this->registerHook('actionsbtogglepost')
                 || !$this->registerHook('rightColumn')
                 || !$this->registerHook('leftColumn')
				 || !$this->registerHook('DorHomeLatestNews')
				 )
            return false;
                 Configuration::updateGlobalValue('typeData',3);
                 Configuration::updateGlobalValue('smartshowhomepost',6);
                 Configuration::updateGlobalValue('blog_content_custom',"");
                 Configuration::updateGlobalValue('blog_quanlity_image',100);
                 Configuration::updateGlobalValue('blog_thumb_width',470);
                 Configuration::updateGlobalValue('blog_thumb_height',280);
                 Configuration::updateGlobalValue('blogAjaxLoad',0);
                 Configuration::updateGlobalValue('enableDorCache',0);
                 return true;
            }
            
     public function uninstall() {
        $tab = new Tab((int) Tab::getIdFromClassName('AdminDorHomeLatestNews'));
        $tab->delete();
	 $this->DeleteCache();
            if (!parent::uninstall() || !Configuration::deleteByName('typeData') || !Configuration::deleteByName('smartshowhomepost')|| !Configuration::deleteByName('blog_content_custom') || !Configuration::deleteByName('blog_quanlity_image')  || !Configuration::deleteByName('blog_thumb_width')  || !Configuration::deleteByName('blog_thumb_height') || !Configuration::deleteByName('smartshowhomepostcolumn') || !Configuration::deleteByName('blogAjaxLoad') || !Configuration::deleteByName('enableDorCache'))
                 return false;
            return true;
                }
     public function hookdorHomeLatestNews($params) {
                $id_shop = (int) Context::getContext()->shop->id;
                $id_lang = (int)Context::getContext()->language->id;
				$languages = Language::getLanguages(true, $this->context->shop->id);
                $dorTimeCache  = Configuration::get('dor_themeoptions_dorTimeCache',Configuration::get('dorTimeCache'));
                $dorTimeCache = $dorTimeCache?$dorTimeCache:86400;
                if(Module::isInstalled('smartblog') != 1){
                        $this->smarty->assign( array(
                                  'smartmodname' => $this->name
                         ));
                        return $this->display(__FILE__, 'views/templates/front/install_required.tpl');
                }
                else
                {
                    $blogAjaxLoad = Configuration::get('blogAjaxLoad');
                    $dorCaches  = Configuration::get('enableDorCache');
                    $styleTmp = "dorblog_home_style1.tpl";
                    $typeData = Configuration::get('typeData');
                            if (!$this->isCached($this->templateFile, $this->getCacheId()))
                                {
                                    $blogcomment = new Blogcomment();
                                    $thumbWidth = Configuration::get('blog_thumb_width');
                                    $thumbHeight = Configuration::get('blog_thumb_height');
                                    $quanlity = Configuration::get('blog_quanlity_image');
                                    
                                    $objCache = new DorCaches(array('name'=>'default','path'=>_PS_ROOT_DIR_.'/override/Dor/Caches/smartcaches/homeblogs/','extension'=>'.cache'));
                                    $fileCache = 'SmartBlogHomeCaches-Shop'.$id_shop.'-Lang'.$id_lang;
                                    $dataItems = array();
                                    if($blogAjaxLoad == 0){
                                        $objCache->setCache($fileCache);
                                        $cacheData = $objCache->renderData($fileCache);
                                        if($cacheData && $dorCaches){
                                            $dataItems = $cacheData['lists'];
                                        }else{
                                            //Configuration::get('smartshowhomepost')
                                            $view_data['posts'] = SmartBlogPost::GetPostLatestHome(Configuration::get('smartshowhomepost')); 
                                            $dataItems = array();
                                            if(!empty($view_data['posts'])){
                                                foreach ($view_data['posts'] as $key => $item) {
                                                    $totalCmt = $blogcomment->getToltalComment($item['id']);
                                                    $item['totalcomment'] = $totalCmt == ""?0:$totalCmt;
                                                    $pathImg = "smartblog/images/".$item['post_img'].".jpg";
                                                    $width=isset($thumbWidth) && $thumbWidth != ""?$thumbWidth:370;
                                                    $height=isset($thumbHeight) && $thumbHeight != ""?$thumbHeight:211;
                                                    $images = DorImageBase::renderThumb($pathImg,$width,$height,'','',true,$quanlity);
                                                    $item['thumb_image'] = $images;
                                                    $dataItems[$key] = $item;
                                                }
                                                if($dorCaches){
                                                    $data['lists'] = $dataItems;
                                                    $objCache->store($fileCache, $data, $expiration = $dorTimeCache);
                                                }
                                            }
                                        }
                                        if($typeData == 1){
                                            $styleTmp = "dorblog_home_style2.tpl";
                                            $this->templateFile = $this->templateFile2;
                                        }
                                        elseif ($typeData == 2){
                                            $styleTmp = "dorblog_home_style3.tpl";
                                            $this->templateFile = $this->templateFile3;
                                        }
                                        elseif ($typeData == 3){
                                            $styleTmp = "dorblog_latest_news_home_style4.tpl";
                                            $this->templateFile = $this->templateFile4;
                                        }
                                    }else{
                                        $styleTmp = "dorblog_home_ajax.tpl";
                                    }
                                    $this->smarty->assign( array(
											'view_data'     	 => $dataItems,
											'languages'     	 => $languages,
                                            'smartshowauthorstyle'=>Configuration::get('smartshowauthorstyle'),
                                            'smartshowauthor'=>Configuration::get('smartshowauthor'),
                                            'description' => (string)Configuration::get('blog_content_custom'),
											'column' => (int)Configuration::get('smartshowhomepostcolumn')
                                    ));
                                    
                                }
                        
                            return $this->display(__FILE__, 'views/templates/front/'.$styleTmp, $this->getCacheId());
                }  
            }
     public function getContent(){
                $html = '';
                if(Tools::isSubmit('save'.$this->name))
                {
                    Configuration::updateValue('typeData', Tools::getvalue('typeData'));
                    Configuration::updateValue('smartshowhomepost', Tools::getvalue('smartshowhomepost'));
                    Configuration::updateValue('blog_content_custom', Tools::getvalue('blog_content_custom'));
                    Configuration::updateValue('blog_quanlity_image', Tools::getvalue('blog_quanlity_image'));
                    Configuration::updateValue('blog_thumb_width', Tools::getvalue('blog_thumb_width'));
                    Configuration::updateValue('blog_thumb_height', Tools::getvalue('blog_thumb_height'));
                    Configuration::updateValue('blogAjaxLoad', Tools::getvalue('blogAjaxLoad'));
                    Configuration::updateValue('enableDorCache', Tools::getvalue('enableDorCache'));
                    $html = $this->displayConfirmation($this->l('The settings have been updated successfully.'));
                    $helper = $this->SettingForm();
                    $html .= $helper->generateForm($this->fields_form); 
                    return $html;
                }
                else
                {
                   $helper = $this->SettingForm();
                   $html .= $helper->generateForm($this->fields_form);
                   return $html;
                }
            }
            
     public function SettingForm() {
        $default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
        $typeData = array(
            array( 'id'=>'0','mode'=>'Style 1'),
            array('id'=>'1','mode'=>'Style 2'),
            array('id'=>'2','mode'=>'Style 3'),
            array('id'=>'3','mode'=>'Style 4'),
        );
        $this->fields_form[0]['form'] = array(
          'legend' => array(
          'title' => Context::getContext()->getTranslator()->trans('General Setting', array(), 'Modules.smartbloghomelatestnews'),
            ),
            'input' => array(
                array(
                        'type' => 'select',
                        'label' => 'Style: ',
                        'name' => 'typeData',
                        'options' => array(
                            'query' => $typeData,
                            'id' => 'id',
                            'name'=>'mode',
                        ),
                    ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Number of posts to dispay in Lastest News', array(), 'Modules.smartbloghomelatestnews'),
                    'name' => 'smartshowhomepost',
                    'size' => 15,
                    'required' => true
                ),
                array(
                    'type' => 'textarea',
                    'label' => Context::getContext()->getTranslator()->trans('Custom Text Content:', array(), 'Modules.smartbloghomelatestnews'),
                    'name' => 'blog_content_custom',
                    'class' => 'fixed-width-full',
                    'cols' => 60,
                    'rows' => 10,
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Quanlity Image:', array(), 'Modules.smartbloghomelatestnews'),
                    'name' => 'blog_quanlity_image',
                    'class' => 'fixed-width-md',
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Thumb width image:', array(), 'Modules.smartbloghomelatestnews'),
                    'name' => 'blog_thumb_width',
                    'class' => 'fixed-width-md',
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Thumb height image:', array(), 'Modules.smartbloghomelatestnews'),
                    'name' => 'blog_thumb_height',
                    'class' => 'fixed-width-md',
                ),
                array(
                        'type' => 'switch',
                        'label' => 'Ajax Load:',
                        'name' => 'blogAjaxLoad',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => Context::getContext()->getTranslator()->trans('Enabled', array(), 'Modules.smartbloghomelatestnews')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => Context::getContext()->getTranslator()->trans('Disabled', array(), 'Modules.smartbloghomelatestnews')
                            )
                        ),
                    ),
                array(
                        'type' => 'switch',
                        'label' => 'Use Dor Cache:',
                        'name' => 'enableDorCache',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => Context::getContext()->getTranslator()->trans('Enabled', array(), 'Modules.smartbloghomelatestnews')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => Context::getContext()->getTranslator()->trans('Disabled', array(), 'Modules.smartbloghomelatestnews')
                            )
                        ),
                    ),
            ),
            'submit' => array(
                'title' => Context::getContext()->getTranslator()->trans('Save', array(), 'Modules.smartbloghomelatestnews'),
                'class' => 'button'
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        foreach (Language::getLanguages(false) as $lang)
                            $helper->languages[] = array(
                                    'id_lang' => $lang['id_lang'],
                                    'iso_code' => $lang['iso_code'],
                                    'name' => $lang['name'],
                                    'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
                            );
        $helper->toolbar_btn = array(
            'save' =>
            array(
                'desc' => Context::getContext()->getTranslator()->trans('Save', array(), 'Modules.smartbloghomelatestnews'),
                'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save'.$this->name.'token=' . Tools::getAdminTokenLite('AdminModules'),
            )
        );
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;       
        $helper->toolbar_scroll = true;    
        $helper->submit_action = 'save'.$this->name;
        
        $helper->fields_value['typeData'] = Configuration::get('typeData');
        $helper->fields_value['smartshowhomepost'] = Configuration::get('smartshowhomepost');
        $helper->fields_value['blog_content_custom'] = Configuration::get('blog_content_custom');
        $helper->fields_value['smartshowhomepostcolumn'] = Configuration::get('smartshowhomepostcolumn');
        $helper->fields_value['blog_quanlity_image'] = Configuration::get('blog_quanlity_image');
        $helper->fields_value['blog_thumb_width'] = Configuration::get('blog_thumb_width');
        $helper->fields_value['blog_thumb_height'] = Configuration::get('blog_thumb_height');
        $helper->fields_value['blogAjaxLoad'] = Configuration::get('blogAjaxLoad');
        $helper->fields_value['enableDorCache'] = Configuration::get('enableDorCache');
        return $helper;
      }
	public function DeleteCache()
            {
				return $this->_clearCache($this->templateFile, $this->getCacheId());
			}
	public function hookactionsbdeletepost($params)
            {
                 return $this->DeleteCache();
            }
	public function hookactionsbnewpost($params)
            {
                 return $this->DeleteCache();
            }
	public function hookactionsbupdatepost($params)
            {
                return $this->DeleteCache();
            }
	public function hookactionsbtogglepost($params)
    {
        return $this->DeleteCache();
    }
    public function hookHeader($params)
    {
        if (!isset($this->context->controller->php_self) || $this->context->controller->php_self != 'index')
            return;
        $this->context->controller->addCSS(($this->_path).'css/dor_homeblog.css', 'all');
        $this->context->controller->addJS($this->_path.'js/dor-blogs.js');
    }
    public function hookAjaxCall($params)
    {
        global $smarty, $cookie;
        $varsData = "";
        $blogcomment = new Blogcomment();
        $id_shop = (int) Context::getContext()->shop->id;
        $thumbWidth = Configuration::get('blog_thumb_width');
        $thumbHeight = Configuration::get('blog_thumb_height');
        $quanlity = Configuration::get('blog_quanlity_image');
        $styleTmp = "smartblog_latest_news_home.tpl";
        $typeData = Configuration::get('typeData');

        $id_lang = (int)Context::getContext()->language->id;
        $dorTimeCache  = Configuration::get('dor_themeoptions_dorTimeCache',Configuration::get('dorTimeCache'));
        $dorTimeCache = $dorTimeCache?$dorTimeCache:86400;
        $dorCaches  = Configuration::get('enableDorCache');
        $objCache = new DorCaches(array('name'=>'default','path'=>_PS_ROOT_DIR_.'/override/Dor/Caches/smartcaches/homeblogs/','extension'=>'.cache'));
        $fileCache = 'SmartBlogHomeCaches-Shop'.$id_shop.'-Lang'.$id_lang;
        $dataItems = array();
        $objCache->setCache($fileCache);
        $cacheData = $objCache->renderData($fileCache);
        if($cacheData && $dorCaches){
            $dataItems = $cacheData['lists'];
        }else{
            $view_data['posts'] = SmartBlogPost::GetPostLatestHome(Configuration::get('smartshowhomepost')); 
            $dataItems = array();
            if(!empty($view_data['posts'])){
                foreach ($view_data['posts'] as $key => $item) {
                    $totalCmt = $blogcomment->getToltalComment($item['id']);
                    $item['totalcomment'] = $totalCmt == ""?0:$totalCmt;
                    $pathImg = "smartblog/images/".$item['post_img'].".jpg";
                    $width=isset($thumbWidth) && $thumbWidth != ""?$thumbWidth:370;
                    $height=isset($thumbHeight) && $thumbHeight != ""?$thumbHeight:211;
                    $images = DorImageBase::renderThumb($pathImg,$width,$height,'','',true,$quanlity);
                    $item['thumb_image'] = $images;
                    $dataItems[$key] = $item;
                }
                if($dorCaches){
                    $data['lists'] = $dataItems;
                    $objCache->store($fileCache, $data, $expiration = $dorTimeCache);
                }
            }
        }

        if($typeData == 1) $styleTmp = "dorblog_home_style2.tpl";
        elseif ($typeData == 2) $styleTmp = "dorblog_home_style3.tpl";
        elseif ($typeData == 3) $styleTmp = "dorblog_latest_news_home_style4.tpl";
        $this->smarty->assign( array(
                'view_data'          => $dataItems,
                'smartshowauthorstyle'=>Configuration::get('smartshowauthorstyle'),
                'smartshowauthor'=>Configuration::get('smartshowauthor'),
                'description' => (string)Configuration::get('blog_content_custom'),
                'column' => (int)Configuration::get('smartshowhomepostcolumn')
        ));
        $varsData = $this->display(__FILE__, 'views/templates/front/'.$styleTmp, $this->getCacheId());
        $dataResult = Tools::jsonEncode($varsData);
        return $dataResult;
    }
}