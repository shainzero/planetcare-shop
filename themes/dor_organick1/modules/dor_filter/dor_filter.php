<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
if (!defined('_PS_VERSION_'))
	exit;
class Dor_filter extends Module
{
	private $spacer_size = '1';
	public function __construct()
	{
		$this->name = 'dor_filter';
		$this->tab = 'front_office_features';
		$this->version = '2.0.0';
		$this->author = 'Dorado Themes';
		$this->need_instance = 0;
		$this->bootstrap =true ;
		parent::__construct();
		$this->displayName = $this->l('Dor Filter Search Custom');
		$this->description = $this->l('Block Filter Search Custom');
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
	}
	public function install()
	{
		// Install Tabs
        if(!(int)Tab::getIdFromClassName('AdminDorMenu')) {
            $parent_tab = new Tab();
            // Need a foreach for the language
            $parent_tab->name[$this->context->language->id] = $this->l('Dor Extensions');
            $parent_tab->class_name = 'AdminDorMenu';
            $parent_tab->id_parent = 0; // Home tab
            $parent_tab->module = $this->name;
            $parent_tab->add();
        }
        $tab = new Tab();
        foreach (Language::getLanguages() as $language)
        $tab->name[$language['id_lang']] = $this->l('Dor Price Filter');
        $tab->class_name = 'AdminDorPriceFilter';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminDorMenu'); 
        $tab->module = $this->name;
        $tab->add();
        $this->_clearCache('*');
        Configuration::updateValue($this->name . '_dor_filter_minPrice', -1);
        Configuration::updateValue($this->name . '_dor_filter_maxPrice', 0);
        Configuration::updateValue($this->name . '_dor_filter_param', "en=>Price");
		if (!parent::install() || !$this->registerHook('displayLeftColumn') || !$this->registerHook('displayRightColumn') || !$this->registerHook('dorPriceRange') || !$this->registerHook('dorFilter') || !$this->registerHook('dorFilter2') || !$this->registerHook('displayHeader'))
			return false;
		return true;
	}
	public function uninstall(){
		$this->_clearCache('*');
		$tab = new Tab((int) Tab::getIdFromClassName('AdminDorPriceFilter'));
        $tab->delete();
        Configuration::deleteByName($this->name . '_dor_filter_minPrice', -1);
        Configuration::deleteByName($this->name . '_dor_filter_maxPrice', 0);
        Configuration::deleteByName($this->name . '_dor_filter_param', "en=>Price");
		return parent::uninstall();
	}
	public function hookdisplayHeader($params){
		if (!isset($this->context->controller->php_self) || $this->context->controller->php_self != 'category')
            return;
        global $cookie;
        $isoLang = Context::getContext()->language->iso_code;
		$id_currency = $cookie->id_currency; 
		$id_shop = (int)Context::getContext()->shop->id;
		$minPrice = Configuration::get($this->name . '_dor_filter_minPrice');
		$maxPrice = Configuration::get($this->name . '_dor_filter_maxPrice');
		$paramFilter = Configuration::get($this->name . '_dor_filter_param');
		$paramPrice = array("en"=>"Price");
		if($paramFilter != ""){
            $paramFilter = explode(PHP_EOL, $paramFilter);
            if(count($paramFilter) > 0){
                foreach ($paramFilter as $key => $pars) {
                    if(!empty($pars)){
                        $pars = explode("=>", $pars);
                        $iso = (isset($pars[0]) && $pars[0] != "")?$pars[0]:"en";
                        $filter = (isset($pars[1]) && $pars[1] != "")?$pars[1]:"Price";
                        $paramPrice[$iso] = @trim($filter);
                    }
                }
                
            }
            
        }
		$priceRange = $this->getPrices($id_shop,$id_currency)[0];
		if(trim($minPrice) != "" && (int)$minPrice > -1){
			$priceRange["price_min"] = (int)$minPrice;
		}
		if(trim($maxPrice) != "" && (int)$maxPrice > 0){
			$priceRange["price_max"] = (int)$maxPrice;
		}
		Media::addJsDef(
            array(
                'DORRANGE' => array(
                    "price_min"=>($priceRange["price_min"] != "")?$priceRange["price_min"]:-1,
                    "price_max"=>($priceRange["price_max"] != "")?$priceRange["price_max"]:0,
                    "paramPrice"=>(isset($paramPrice[$isoLang]) && $paramPrice[$isoLang] != "")?$paramPrice[$isoLang]:"Price"
                )
            )
        );
		$this->context->controller->addCSS($this->_path . 'assets/css/dorFilter.css');
    	$this->context->controller->addJS($this->_path . 'assets/js/dorFilter.js');
	}
	public function hookDorPriceRange($params)
	{
		if (!isset($this->context->controller->php_self) || $this->context->controller->php_self != 'category')
            return;
		$this->context->smarty->assign(array(
			'price_total' => 1
		));
		return $this->display(__FILE__, '/dorPriceRange.tpl');
	}
	public function hookDisplayLeftColumn($params)
    {
        return $this->hookDorPriceRange($params);
    }
	private function _postProcess() {
        Configuration::updateValue($this->name . '_dor_filter_minPrice', Tools::getValue('dor_filter_minPrice'));
        Configuration::updateValue($this->name . '_dor_filter_maxPrice', Tools::getValue('dor_filter_maxPrice'));
        Configuration::updateValue($this->name . '_dor_filter_param', Tools::getValue('dor_filter_param'));
        $this->_html .= $this->displayConfirmation($this->l('Configuration updated'));
    }
	public function getContent()
	{
		$output = '';
		if (Tools::isSubmit('submitPriceFilterUpdate'))
		{
			if (!sizeof($this->_postErrors))
                $this->_postProcess();
            else {
                foreach ($this->_postErrors AS $err) {
                    $this->_html .= '<div class="alert error">' . $err . '</div>';
                }
            }
		}
		return $output . $this->_displayForm();
	}
	public function _displayForm()
	{
		$id_lang = (int)Context::getContext()->language->id;
		$languages = $this->context->controller->getLanguages();
		$default_language = (int)Configuration::get('PS_LANG_DEFAULT');
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings Filter'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => Context::getContext()->getTranslator()->trans('Min Price', array(), 'Modules.dor_filter'),
						'name' => 'dor_filter_minPrice',
						'class' => 'dor-data fixed-width-xs',
						'desc' => Context::getContext()->getTranslator()->trans('Input Min Price Search. With value <= -1 then will get value from database', array(), 'Modules.dor_filter'),
						'default' => 0
					),
					array(
						'type' => 'text',
						'label' => Context::getContext()->getTranslator()->trans('Max Price', array(), 'Modules.dor_filter'),
						'name' => 'dor_filter_maxPrice',
						'class' => 'dor-data fixed-width-xs',
						'desc' => Context::getContext()->getTranslator()->trans('Input Max Price Search. With value <= 0 then will get value from database', array(), 'Modules.dor_filter'),
						'default' => 0
					),
					array(
                        'type' => 'textarea',
                        'label' => 'Param Filter:',
                        'name' => 'dor_filter_param',
                        'class' => 'fixed-width-lg dor_filter_param',
                        'desc'=>'<div style="margin:0px;padding:0px;font-size:15px;font-style:italic;"><p style="margin:0px;padding:0px;color:#ff0000;">This is params filter price add to URL</p><p style="margin:0px;padding:0pxfont-size:13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>FORMAT:</strong> Iso Language=>Param <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>EX:</strong> en=>Price</p></div>'
                    ),
				),
				'submit' => array(
					'title' => Context::getContext()->getTranslator()->trans('Save'),
				)
			),
		);
		$helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPriceFilterUpdate';
        $helper->module = $this;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        $helper->override_folder = '/';
        return $helper->generateForm(array($fields_form));
	}
    public function getConfigFieldsValues()
    {
        return array(
            'dor_filter_minPrice' =>     Tools::getValue('dor_filter_minPrice', Configuration::get($this->name . '_dor_filter_minPrice')),
            'dor_filter_maxPrice' =>     Tools::getValue('dor_filter_maxPrice', Configuration::get($this->name . '_dor_filter_maxPrice')),
            'dor_filter_param' =>     Tools::getValue('dor_filter_param', Configuration::get($this->name . '_dor_filter_param')),
        );
    }
    public static function getPrices($id_shop = 1, $id_currency = 1)
	{
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT MIN(psi.price_min) price_min, MAX(psi.price_max) price_max
			FROM `'._DB_PREFIX_.'layered_price_index` psi
			WHERE 1=1 AND psi.`id_shop` = '.(int)$id_shop.' AND psi.`id_currency` = '.(int)$id_currency
		);
		return $result;
	}
}

