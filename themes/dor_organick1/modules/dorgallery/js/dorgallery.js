var DORGALLERY = {
	checkVersion:parseInt($("#dor-gallery-base").attr("data-style")),
	BaseUrl:$("#galleryMainLink").val(),
	galleryVersion:'gallery',
	init:function(){
		DORGALLERY.TabGallery();
		DORGALLERY.PageGallery();
		DORGALLERY.ShowPopGallery();
		DORGALLERY.PortfolioGallery('#portfoliolist0');
		DORGALLERY.galleryVersion = jQuery("#versionGallery").val();
	},
	TabGallery:function(){
		jQuery(".header-tab-gallery a").click(function(e){
			e.preventDefault();
			var _this = this;
			var idObjTxt = jQuery(this).attr('aria-controls');
			var idObj = parseInt(idObjTxt.replace("gallery-",""));
			var checkData = jQuery("#portfoliolist"+idObj).html();
			if(jQuery.trim(checkData) == ""){
				var params = {}
				params.cateid = idObj;
				params.ajaxtype = 'ajax';
				params.type = DORGALLERY.checkVersion;
				jQuery.ajax({
		            url: DORGALLERY.BaseUrl,
		            data:params,
		            type:"GET",
		            success:function(data){
		            	jQuery(".header-tab-gallery li a").removeClass("active");
						jQuery(_this).addClass("active");
						jQuery(_this).closest("li").addClass("active");
		            	var results = JSON.parse(data);
		            	
		            	jQuery(".data-gallery").addClass("hidden");
		            	jQuery("#portfoliolist"+idObj).closest(".data-gallery").removeClass("hidden");
	            		jQuery("#portfoliolist"+idObj).html(results);
	            		if(DORGALLERY.checkVersion == 1){
	            			DORGALLERY.PortfolioGallery("#portfoliolist"+idObj);
	            			DORPHOTOSWIPE.initPhotoSwipeFromDOM(".dor-gallery-show");
	            		}else{
	            			GRIDBASE.init("#portfoliolist"+idObj+' .grid');
	            			DORPHOTOSWIPE2.initPhotoSwipeFromDOM("#portfoliolist"+idObj+".dor-gallery-show");
	            			DORPHOTOSWIPE2.GalleryOpt = "#portfoliolist"+idObj+".dor-gallery-show";
	            		}
	            		
	            		DORGALLERY.PageGallery();
	            		
		                return false;
		            }
		        });
			}else{
				jQuery(".header-tab-gallery li a").removeClass("active");
				jQuery(".header-tab-gallery li").removeClass("active");
				jQuery(_this).addClass("active");
				jQuery(_this).closest("li").addClass("active");
				jQuery(".data-gallery").addClass("hidden");
		        jQuery("#portfoliolist"+idObj).closest(".data-gallery").removeClass("hidden");
		        if(DORGALLERY.checkVersion == 1){
		        	$("#portfoliolist"+idObj).mixItUp('destroy');
		        	DORGALLERY.PortfolioGallery("#portfoliolist"+idObj);
		    	}else{
		    		DORPHOTOSWIPE2.GalleryOpt = "#portfoliolist"+idObj+".dor-gallery-show";
		    	}
		        
			}
			
		});
	},
	PageGallery:function(){
		jQuery(".load-more-gallery a").click(function(e){
			e.preventDefault();
			var _this = this;
			var idObjTxt = jQuery(".header-tab-gallery li.active a").attr('aria-controls');
			var idObj = parseInt(idObjTxt.replace("gallery-",""));
			var page = parseInt(jQuery(this).find("span").text());
			var params = {}
			params.cateid = idObj;
			params.page = page;
			params.type = 'ajax';
			jQuery.ajax({
	            url: prestashop.urls.base_url + DORGALLERY.galleryVersion,
	            data:params,
	            type:"GET",
	            success:function(data){
	            	jQuery("#"+idObjTxt+" .page-show-gallery").hide();
	            	jQuery("#"+idObjTxt+" .dor-gallery-page a").removeClass("active");
					jQuery(_this).addClass("active");
	            	if(DORGALLERY.galleryVersion == 'gallery')
	            		var results = JSON.parse(data);
	            	else
	            		var results = data;
	            	jQuery("#"+idObjTxt+" .data-gallery").html(results);
	            	DORGALLERY.ShowPopGallery();
	                return false;
	            }
	        });
		});
	},
	PageGalleryV2:function(){
		jQuery(".dorgallery-base-2 .dor-gallery-page a").unbind("click");
		jQuery(".dorgallery-base-2 .dor-gallery-page a").click(function(e){
			e.preventDefault();
			jQuery(".gallery-content-detail").removeClass("grid");
			jQuery(".gallery-content-detail").removeClass("grid--loaded");
			var _this = this;
			var idObjTxt = jQuery(".header-tab-gallery li a.active").attr('aria-controls');
			var idObj = parseInt(idObjTxt.replace("gallery-",""));
			var page = parseInt(jQuery(this).attr("rel"));
			var params = {}
			params.cateid = idObj;
			params.page = page+1;
			params.type = 'ajax';
			jQuery.ajax({
	            url: prestashop.urls.base_url + DORGALLERY.galleryVersion,
	            data:params,
	            type:"GET",
	            success:function(data){
	            	if(DORGALLERY.galleryVersion == 'gallery')
	            		var results = JSON.parse(data);
	            	else{
	            		var results = data;
	            		var lengthVal = jQuery(results).find(".item-gallery").length;
		            	if(lengthVal >= 11){
		            		jQuery("#"+idObjTxt+" .dor-gallery-page a").attr("rel",page+1);
		            	}else{
		            		jQuery("#"+idObjTxt+" .dor-gallery-page").remove();
		            	}
	            	}
	            	jQuery("#"+idObjTxt+" .data-gallery").html(results);
            		DORGALLERY.ShowPopGallery();
	                return false;
	            }
	        });
		});
	},
	ShowPopGallery:function(){
		if(DORGALLERY.checkVersion == 1){
			DORPHOTOSWIPE.initPhotoSwipeFromDOM(".dor-gallery-show");
		}else{
			DORPHOTOSWIPE2.initPhotoSwipeFromDOM(".dor-gallery-show");
		}
		
	},
	PortfolioGallery:function(obj){
		var checkLoad = $(".header-tab-gallery .filter.active").attr("data-filter");
		jQuery(obj).mixItUp({
			selectors: {
				target: '.portfolio',
				filter: '.filter'	
			},
			load: {
				filter: checkLoad  
			}	     
		});
	}
}

jQuery(document).ready(function(){
	DORGALLERY.init();
});

