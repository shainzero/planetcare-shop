<?php
if (!defined('_PS_VERSION_'))
    exit;
define('_MODULE_DORGALLERY_DIR_',_PS_MODULE_DIR_. 'dogallery/images/' );
require_once (dirname(__FILE__) . '/classes/DorGalleryItem.php');
require_once (dirname(__FILE__) . '/classes/DorGalleryCategory.php');
class dorgallery extends Module {
    public function __construct(){
        $this->_html = '';
        $this->name = 'dorgallery';
        $this->tab = 'front_office_features';
        $this->version = '2.1.1';
        $this->author = 'Dorado Themes';
        $this->need_upgrade = true;
        $this->controllers = array('gallery', 'gallery2', 'gallery3', 'category', 'details');
        $this->secure_key = Tools::encrypt($this->name);
        $this->smart_shop_id = Context::getContext()->shop->id;
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Dor Gallery Show');
        $this->description = $this->l('The Most Powerfull Prestashop Gallery Module - by Dorado Themes');
        $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');
    }

    public function install(){
        Configuration::updateGlobalValue('drogallery_perpage', 30);
        Configuration::updateGlobalValue('drogallery_thumbwidthlists', 450);
        Configuration::updateGlobalValue('drogallery_thumbheightlists', 500);
        Configuration::updateGlobalValue('drogallery_thumbwidthdetail', 450);
        Configuration::updateGlobalValue('drogallery_thumbheightdetail', 500);
        Configuration::updateGlobalValue('drogallery_cate', 0);
        Configuration::updateGlobalValue('drogallery_style', 1);
        Configuration::updateGlobalValue('drogallery_metatitle', 'Dorado Gallery Smart'); 
        Configuration::updateGlobalValue('drogallery_metakeyword', 'Themes, Dorado Themes, Gallery Dorado, Gallery Themes'); 
        Configuration::updateGlobalValue('drogallery_metadesc', 'Prestashop powerfull gallery site developing module. It has hundrade of extra plugins. This module developed by doradothemes.com'); 
        $langs = Language::getLanguages();
        if (!parent::install() || !$this->registerHook('displayHeader') || !$this->registerHook('displayBackOfficeHeader'))
        return false;
        $sql = array();
        require_once(dirname(__FILE__) . '/sql/install.php');
        foreach ($sql as $sq) :
            if (!Db::getInstance()->Execute($sq))
                return false;
        endforeach;
        $this->CreateDorGalleryTabs();
        return true;
    }
    public function  hookdisplayBackOfficeHeader($params){
        $this->context->controller->addCSS($this->_path.'css/dorgallery.admin.css', 'all');
        $this->context->controller->addJS($this->_path.'js/admin.dorgallery.js');
        $this->smarty->assign( array('dorgallery_dir' => __PS_BASE_URI__));
        return $this->display(__FILE__, 'views/templates/admin/addjs.tpl');
    }
  
    public function uninstall() {
        if (!parent::uninstall() ||
            !Configuration::deleteByName('drogallery_style') ||
            !Configuration::deleteByName('drogallery_metatitle') ||
            !Configuration::deleteByName('drogallery_metakeyword') ||
            !Configuration::deleteByName('drogallery_metadesc') ||
            !Configuration::deleteByName('drogallery_cate') ||
            !Configuration::deleteByName('drogallery_perpage') ||
            !Configuration::deleteByName('drogallery_thumbwidthlists') ||
            !Configuration::deleteByName('drogallery_thumbheightlists') ||
            !Configuration::deleteByName('drogallery_thumbwidthdetail') ||
            !Configuration::deleteByName('drogallery_thumbheightdetail')
        )
        return false;
        require_once(dirname(__FILE__) . '/sql/uninstall_tab.php');
        foreach ($idtabs as $tabid):
            if ($tabid) {
                $tab = new Tab($tabid);
                $tab->delete();
            }
        endforeach;
        $dortab = new Tab((int) Tab::getIdFromClassName('AdminDorGallery'));
        $dortab->delete();
                $sql = array();
        require_once(dirname(__FILE__) . '/sql/uninstall.php');
        foreach ($sql as $s) :
            if (!Db::getInstance()->Execute($s))
                return false;
        endforeach;
        return true;
    }
    public function hookDisplayHeader($params) {
        $checkPage = isset($this->context->controller->phpself)?$this->context->controller->phpself:"";
        if($checkPage != "index" && $checkPage == "gallery"){
            $this->context->controller->addCSS($this->_path.'css/dorgallery.css');
            $this->context->controller->addJS($this->_path.'js/lib/jquery.mixitup.min.js');
            $this->context->controller->addJS($this->_path.'js/dorgallery.js');
            $type = isset($_GET['type'])?$_GET['type']:"";
            $style = (int) Configuration::get('drogallery_style');
            if($type == 2 || ($style == 2 && $type != 1)){
                $this->context->controller->addCSS($this->_path.'grid/css/style1.css');
                $this->context->controller->addJS($this->_path.'grid/js/modernizr-custom.js');
                $this->context->controller->addJS($this->_path.'grid/js/imagesloaded.pkgd.min.js');
                $this->context->controller->addJS($this->_path.'grid/js/masonry.pkgd.min.js');
                $this->context->controller->addJS($this->_path.'grid/js/classie.js');
                $this->context->controller->addJS($this->_path.'grid/js/main.js');
                $this->context->controller->addJS($this->_path.'grid/js/base.js');
            }
            
        }
    }

    private function CreateDorGalleryTabs() {
                $langs = Language::getLanguages();
                $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
                $dorgallerytab = new Tab();
                $dorgallerytab->class_name = "AdminGalleryShow";
                $dorgallerytab->module = "";
                $dorgallerytab->id_parent = 0;
                foreach($langs as $l){
                        $dorgallerytab->name[$l['id_lang']] = $this->l('Dor Gallery');
                }
                $dorgallerytab->save();
                $tabid_parent = $dorgallerytab->id;
                require_once(dirname(__FILE__) . '/sql/install_tab.php');
                foreach ($tabgalleryvalue as $tab){
                    $gallerytab = new Tab();
                    $gallerytab->class_name = $tab['class_name'];
                    $gallerytab->id_parent = $tabid_parent;
                    $gallerytab->module = $tab['module'];
                    foreach ($langs as $l) {
                        $gallerytab->name[$l['id_lang']] = $this->l($tab['name']);
                    }
                    $gallerytab->save();
                }
                return true;
            }

    public function getContent(){
                $html = '';
                if(Tools::isSubmit('savedorgallery'))
                {
                    Configuration::updateValue('drogallery_metatitle', Tools::getvalue('drogallery_metatitle'));
                    Configuration::updateValue('drogallery_style', Tools::getvalue('drogallery_style'));
                    Configuration::updateValue('drogallery_metakeyword', Tools::getvalue('drogallery_metakeyword'));
                    Configuration::updateValue('drogallery_metadesc', Tools::getvalue('drogallery_metadesc'));
                    Configuration::updateValue('drogallery_cate', implode(',', Tools::getValue('drogallery_cate')));
                    Configuration::updateValue('drogallery_perpage', Tools::getvalue('drogallery_perpage'));
                    Configuration::updateValue('drogallery_thumbwidthlists', Tools::getvalue('drogallery_thumbwidthlists'));
                    Configuration::updateValue('drogallery_thumbheightlists', Tools::getvalue('drogallery_thumbheightlists'));
                    Configuration::updateValue('drogallery_thumbwidthdetail', Tools::getvalue('drogallery_thumbwidthdetail'));
                    Configuration::updateValue('drogallery_thumbheightdetail', Tools::getvalue('drogallery_thumbheightdetail'));
                    $html = $this->displayConfirmation($this->l('The settings have been updated successfully.'));
                    $helper = $this->SettingForm();
                    $html .= $helper->generateForm($this->fields_form); 
                   $html .= ''; 
                    return $html;
                }
                else
                {
                   $helper = $this->SettingForm();
                   $html .= $helper->generateForm($this->fields_form); 
                   $html .= ''; 
                   return $html;
                }
            }

    public function SettingForm() {
        $blog_url = dorgallery::GetDorGalleryLink('dorgallery');
        $default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
        $styles = array(
            array( 'id'=>1,'mode'=>'Style 01'),
            array('id'=>2,'mode'=>'Style 02'),
            array('id'=>3,'mode'=>'Style 03'),
        );
        $this->fields_form[0]['form'] = array(
          'legend' => array(
          'title' => Context::getContext()->getTranslator()->trans('Dor Gallery Main Setting', array(), 'Modules.Dorgallery'),
            ),
            'input' => array(
                array(
                        'type' => 'select',
                        'label' => 'Style: ',
                        'name' => 'drogallery_style',
                        'options' => array(
                            'query' => $styles,
                            'id' => 'id',
                            'name'=>'mode',
                        ),
                    ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Meta Title', array(), 'Modules.Dorgallery'),
                    'name' => 'drogallery_metatitle',
                    'size' => 70,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Meta Keyword', array(), 'Modules.Dorgallery'),
                    'name' => 'drogallery_metakeyword',
                    'size' => 70,
                    'required' => true
                ),
                array(
                    'type' => 'textarea',
                    'label' => Context::getContext()->getTranslator()->trans('Meta Description', array(), 'Modules.Dorgallery'),
                    'name' => 'drogallery_metadesc',
                    'rows' => 7,
                    'cols' => 66,
                    'required' => true
                ),
                array(
                    'type' => 'selectlist',
                    'label' => 'Gallery Category',
                    'name' => 'drogallery_cate[]',
                    'multiple'=>true,
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Thumb Width Lists', array(), 'Modules.Dorgallery'),
                    'name' => 'drogallery_thumbwidthlists',
                    'size' => 5,
                    'default' => 875,
                    'required' => false
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Thumb Height Lists', array(), 'Modules.Dorgallery'),
                    'name' => 'drogallery_thumbheightlists',
                    'size' => 5,
                    'default' => 500,
                    'required' => false
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Thumb Width Detail', array(), 'Modules.Dorgallery'),
                    'name' => 'drogallery_thumbwidthdetail',
                    'size' => 5,
                    'default' => 740,
                    'required' => false
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Thumb Height Detail', array(), 'Modules.Dorgallery'),
                    'name' => 'drogallery_thumbheightdetail',
                    'size' => 5,
                    'default' => 366,
                    'required' => false
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Number per page', array(), 'Modules.Dorgallery'),
                    'name' => 'drogallery_perpage',
                    'size' => 15,
                    'required' => true
                ),
            ),
            'submit' => array(
                'title' => Context::getContext()->getTranslator()->trans('Save', array(), 'Modules.Dorgallery'),
                'class' => 'button'
            )
        );
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        foreach (Language::getLanguages(false) as $lang)
                            $helper->languages[] = array(
                                    'id_lang' => $lang['id_lang'],
                                    'iso_code' => $lang['iso_code'],
                                    'name' => $lang['name'],
                                    'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
                            );
        $helper->toolbar_btn = array(
            'save' =>
            array(
                'desc' => Context::getContext()->getTranslator()->trans('Save', array(), 'Modules.Dorgallery'),
                'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save'.$this->name.'token=' . Tools::getAdminTokenLite('AdminModules'),
            )
        );
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;    
        $helper->submit_action = 'save'.$this->name;
        $categories = DorGalleryCategory::getCategory();
        $dataCategories = $this->getCategoryOptions($categories);
        $helper->tpl_vars = array(
            'cate_data' => $dataCategories
        );
        $helper->fields_value['drogallery_perpage'] = Configuration::get('drogallery_perpage');
        $helper->fields_value['drogallery_thumbwidthlists'] = Configuration::get('drogallery_thumbwidthlists');
        $helper->fields_value['drogallery_thumbheightlists'] = Configuration::get('drogallery_thumbheightlists');
        $helper->fields_value['drogallery_thumbwidthdetail'] = Configuration::get('drogallery_thumbwidthdetail');
        $helper->fields_value['drogallery_thumbheightdetail'] = Configuration::get('drogallery_thumbheightdetail');
        $helper->fields_value['drogallery_metakeyword'] = Configuration::get('drogallery_metakeyword');
        $helper->fields_value['drogallery_metatitle'] = Configuration::get('drogallery_metatitle');
        $helper->fields_value['drogallery_style'] = Configuration::get('drogallery_style');
        $helper->fields_value['drogallery_metadesc'] = Configuration::get('drogallery_metadesc');
        $helper->fields_value['drogallery_cate'] = Configuration::get('drogallery_cate');
        return $helper;
      }

    public function getCategoryOptions($categories) {
        $cateCurrent = Configuration::get('drogallery_cate');
        $cateCurrent = explode(',', $cateCurrent);
        foreach ($categories as $key => $category) {
            if($category["id_dorgallery_category"] == 0){
                $category["name"] = "All Gallery";
            }
            if (in_array((int)$category["id_dorgallery_category"], $cateCurrent)) {
                $this->_html .= '<option value="'.(int)$category["id_dorgallery_category"].'" selected="selected">'.$category["name"].'</option>';
            }else{
                $this->_html .= '<option value="'.(int)$category["id_dorgallery_category"].'">'.$category["name"].'</option>';
            }


            
        }
         return $this->_html ;
    }



    public static function GetDorGalleryUrl() {
            $ssl_enable = Configuration::get('PS_SSL_ENABLED');
            $id_lang = (int)Context::getContext()->language->id;
            $id_shop = (int)Context::getContext()->shop->id;
            $rewrite_set = (int)Configuration::get('PS_REWRITING_SETTINGS');
            $ssl = null;
                static $force_ssl = null;
		if ($ssl === null)
		{
			if ($force_ssl === null)
				$force_ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));
			$ssl = $force_ssl;
		}
		if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') && $id_shop !== null)
			$shop = new Shop($id_shop);
		else
			$shop = Context::getContext()->shop;
		$base = (($ssl && $ssl_enable) ? 'https://'.$shop->domain_ssl : 'http://'.$shop->domain);   
                $langUrl = Language::getIsoById($id_lang).'/';
                if ((!$rewrite_set && in_array($id_shop, array((int)Context::getContext()->shop->id,  null))) || !Language::isMultiLanguageActivated($id_shop) || !(int)Configuration::get('PS_REWRITING_SETTINGS', null, null, $id_shop))
                    $langUrl = '';
		return $base.$shop->getBaseURI().$langUrl;
           }
           
    public static function GetDorGalleryLink($rewrite = 'dorgallery',$params = null, $id_shop = null,$id_lang = null){
                 $url = dorgallery::GetDorGalleryUrl();
                 $dispatcher = Dispatcher::getInstance();
                 if($params != null){
                     return $url.$dispatcher->createUrl($rewrite, $id_lang, $params);
                 }else{
                   return $url.$dispatcher->createUrl($rewrite);
                 }
              }

    
}