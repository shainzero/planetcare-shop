<?php

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'dorgallery_category` (
  `id_dorgallery_category` int(11) NOT NULL auto_increment,
  `id_parent` varchar(45) DEFAULT NULL,
  `position` varchar(45) DEFAULT NULL,
  `desc_limit` varchar(45) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id_dorgallery_category`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'dorgallery_category_lang` (
  `id_dorgallery_category` int(11) NOT NULL,
  `id_lang` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `meta_keyword` varchar(200) DEFAULT NULL,
  `meta_description` varchar(350) DEFAULT NULL,
  `content` varchar(500) DEFAULT NULL,
  `link_rewrite` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_dorgallery_category`,`id_lang`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'dorgallery_category_shop` (
  `id_dorgallery_category_shop`  int(11) NOT NULL auto_increment,
  `id_dorgallery_category` int(11) NOT NULL,
  `id_shop` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_dorgallery_category_shop`,`id_dorgallery_category`,`id_shop`)
)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'dorgallery` (
  `id_dorgallery` int(11) NOT NULL auto_increment,
  `id_dorgallery_category` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `viewed` int(11) DEFAULT NULL,
  `image` varchar(245) DEFAULT NULL,
  `image_width` int(5) DEFAULT 500,
  `image_height` int(5) DEFAULT 500,
  PRIMARY KEY (`id_dorgallery`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'dorgallery_lang` (
  `id_dorgallery` int(11) NOT NULL,
  `id_lang` varchar(45) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `meta_keyword` varchar(200) DEFAULT NULL,
  `meta_description` varchar(450) DEFAULT NULL,
  `content` text,
  `link_rewrite` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_dorgallery`,`id_lang`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'dorgallery_shop` (
  `id_dorgallery_shop` int(11) NOT NULL auto_increment,
  `id_dorgallery` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_dorgallery_shop`,`id_dorgallery`,`id_shop`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

?>