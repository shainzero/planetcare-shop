<?php
$tabgalleryvalue = array(
    array(
        'class_name' => 'AdminGallerySetup',
        'id_parent' => 16,
        'module' => 'dorgallery',
        'name' => 'Dor Gallery Configure',
    ),
    array(
        'class_name' => 'AdminGalleryCategory',
        'id_parent' => 16,
        'module' => 'dorgallery',
        'name' => 'Dor Gallery Category',
    ),
    array(
        'class_name' => 'AdminGalleryItem',
        'id_parent' => 16,
        'module' => 'dorgallery',
        'name' => 'Dor Gallery Item',
    )
);
?>