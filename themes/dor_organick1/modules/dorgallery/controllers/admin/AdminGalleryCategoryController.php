<?php
class AdminGalleryCategoryController extends AdminController {

    public $module;

    public function __construct() {
        $this->table = 'dorgallery_category';
        $this->className = 'DorGalleryCategory';
        $this->imageCate_dir = '../modules/dorgallery/images/category';
        $this->module = 'dorgallery';
        $this->lang = true;
        $this->bootstrap = true;
        $this->need_instance = 0;
        $this->context = Context::getContext();
        if (Shop::isFeatureActive())
            Shop::addTableAssociation($this->table, array('type' => 'shop'));
		parent::__construct();
        $this->fields_list = array(
            'id_dorgallery_category' => array(
                    'title' => Context::getContext()->getTranslator()->trans('Id', array(), 'Modules.Dorgallery'),
                    'width' => 100,
                    'type' => 'text',
            ),
            'name' => array(
                    'title' => Context::getContext()->getTranslator()->trans('Name', array(), 'Modules.Dorgallery'),
                    'width' => 440,
                    'type' => 'text',
                    'lang' => true
            ),
            'image' => array(
                'title' => Context::getContext()->getTranslator()->trans('Image', array(), 'Modules.Dorgallery'),
                'image' => $this->imageCate_dir,
                'orderby' => false,
                'search' => false,
                'width' => 200,
                'align' => 'center',
                'orderby' => false,
                'filter' => false,
                'search' => false
            ),
            'active' => array(
                'title' => Context::getContext()->getTranslator()->trans('Status', array(), 'Modules.Dorgallery'),
                'width' => '70',
                'align' => 'center',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false
            )
        );
        
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'dorgallery_category_shop gcl ON a.id_dorgallery_category=gcl.id_dorgallery_category && gcl.id_shop IN('.implode(',',Shop::getContextListShopID()).')';
 
        $this->_select = 'gcl.id_shop';
        $this->_defaultOrderBy = 'a.id_dorgallery_category';
        $this->_defaultOrderWay = 'DESC';
        
        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
        {
           $this->_group = 'GROUP BY a.id_dorgallery_category';
        }

        parent::__construct();
    }

    public function renderForm() 
     {
        $img_desc = '';
        $img_desc .= Context::getContext()->getTranslator()->trans('Upload a Avatar from your computer.<br/>N.B : Only jpg image is allowed', array(), 'Modules.Dorgallery');
        if(Tools::getvalue('id_dorgallery_category') != '' && Tools::getvalue('id_dorgallery_category') != NULL){
             $img_desc .= '<br/><img style="height:auto;width:300px;clear:both;border:1px solid black;" alt="" src="'.__PS_BASE_URI__.'modules/dorgallery/images/category/'.Tools::getvalue('id_dorgallery_category').'.jpg" /><br />';
        }
        $this->fields_form = array(
          'legend' => array(
          'title' => Context::getContext()->getTranslator()->trans('Dor Gallery Category', array(), 'Modules.Dorgallery'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Name', array(), 'Modules.Dorgallery'),
                    'class' => 'galleryCategoryName',
                    'name' => 'name',
                    'size' => 60,
                    'required' => true,
                    'desc' => Context::getContext()->getTranslator()->trans('Enter Your Category Name', array(), 'Modules.Dorgallery'),
                    'lang' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => Context::getContext()->getTranslator()->trans('Description', array(), 'Modules.Dorgallery'),
                    'name' => 'content',
                    'lang' => true,
                    'rows' => 10,
                    'cols' => 62,
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'required' => false,
                    'desc' => Context::getContext()->getTranslator()->trans('Enter Your Category Description', array(), 'Modules.Dorgallery')
                ),
                array(
                    'type' => 'file',
                    'label' => Context::getContext()->getTranslator()->trans('Category Image:', array(), 'Modules.Dorgallery'),
                    'name' => 'category_image',
                    'display_image' => false,
                    'desc' => $img_desc
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Meta Keyword', array(), 'Modules.Dorgallery'),
                    'name' => 'meta_keyword',
                    'lang' => true,
                    'size' => 60,
                    'required' => false,
                    'desc' => Context::getContext()->getTranslator()->trans('Enter Your Category Meta Keyword. Separated by comma(,)', array(), 'Modules.Dorgallery')
                ),
                array(
                    'type' => 'textarea',
                    'label' => Context::getContext()->getTranslator()->trans('Meta Description', array(), 'Modules.Dorgallery'),
                    'name' => 'meta_description',
                    'rows' => 10,
                    'cols' => 62,
                    'lang' => true,
                    'required' => false,
                    'desc' => Context::getContext()->getTranslator()->trans('Enter Your Category Meta Description', array(), 'Modules.Dorgallery')
                ),
                array(
                    'type' => 'text',
                    'label' => Context::getContext()->getTranslator()->trans('Link Rewrite', array(), 'Modules.Dorgallery'),
                    'name' => 'link_rewrite',
                    'class' => 'galleryLinkRewrite',
                    'size' => 60,
                    'lang' => true,
                    'required' => true,
                    'desc' => Context::getContext()->getTranslator()->trans('Enetr Your Category Slug. Use In SEO Friendly URL', array(), 'Modules.Dorgallery')
                ),
                /*array(
                    'type' => 'select',
                    'label' => Context::getContext()->getTranslator()->trans('Parent Category', array(), 'Modules.Dorgallery'),
                    'name' => 'id_parent',
                    'options' => array(
                        'query' =>DorGalleryCategory::getCategory(),
                        'id' => 'id_dorgallery_category',
                        'name' => 'name'
                    ),
                    'desc' => Context::getContext()->getTranslator()->trans('Select Your Parent Category', array(), 'Modules.Dorgallery')
                ),*/
         
                array(
                    'type' => 'radio',
                    'label' => Context::getContext()->getTranslator()->trans('Status', array(), 'Modules.Dorgallery'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                        'id' => 'active',
                        'value' => 1,
                        'label' => Context::getContext()->getTranslator()->trans('Enabled', array(), 'Modules.Dorgallery')
                        ),
                        array(
                        'id' => 'active',
                        'value' => 0,
                        'label' => Context::getContext()->getTranslator()->trans('Disabled', array(), 'Modules.Dorgallery')
                        )
                        )
                 )
            ),
            'submit' => array(
                'title' => Context::getContext()->getTranslator()->trans('Save', array(), 'Modules.Dorgallery'),
                'class' => 'button'
            )
        );

        if (Shop::isFeatureActive())
        {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => Context::getContext()->getTranslator()->trans('Shop association:', array(), 'Modules.Dorgallery'),
                'name' => 'checkBoxShopAsso',
            );
        }

        if (!($DorGalleryCategory = $this->loadObject(true)))
            return;

        $this->fields_form['submit'] = array(
            'title' => Context::getContext()->getTranslator()->trans('Save', array(), 'Modules.Dorgallery'),
            'class' => 'button'
        );
        return parent::renderForm();
    }

    public function renderList() {
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        return parent::renderList();
    }
    public function postProcess()
    {
        if (Tools::isSubmit('deletedorgallery_category') && Tools::getValue('id_dorgallery_category') != '')
        {
			$id_lang = (int)Context::getContext()->language->id;
			$catGallery = (int)DorGalleryItem::getToltalByCategory($id_lang,Tools::getValue('id_dorgallery_category'));
			if((int)$catGallery != 0)
            {
				$this->errors[] = Tools::displayError('You need to delete all posts associate with this category .');
			}
            else
            {
				$DorGalleryCategory = new DorGalleryCategory((int) Tools::getValue('id_dorgallery_category'));
				if (!$DorGalleryCategory->delete())
                {
					$this->errors[] = Tools::displayError('An error occurred while deleting the object.')
							. ' <b>' . $this->table . ' (' . Db::getInstance()->getMsgError() . ')</b>';
				}else
                {
					Hook::exec('actionsbdeletecat', array('DorGalleryCategory' => $DorGalleryCategory));
					Tools::redirectAdmin($this->context->link->getAdminLink('AdminGalleryCategory'));
				}
			} 
        }
        elseif (Tools::isSubmit('submitAdddorgallery_category'))
        {
            parent::validateRules();
            if (count($this->errors))
                return false;
            if (!$id_dorgallery_category = (int) Tools::getValue('id_dorgallery_category')) {
                $DorGalleryCategory = new DorGalleryCategory();
                $languages = Language::getLanguages(false);
                foreach ($languages as $language){
                    $title = str_replace('"','',htmlspecialchars_decode(html_entity_decode(Tools::getValue('name_'.$language['id_lang']))));
                    $DorGalleryCategory->name[$language['id_lang']] = $title;
                    $DorGalleryCategory->meta_keyword[$language['id_lang']] = Tools::getValue('meta_keyword_'.$language['id_lang']);
                    $DorGalleryCategory->meta_description[$language['id_lang']] = Tools::getValue('meta_description_'.$language['id_lang']);
                    $DorGalleryCategory->content[$language['id_lang']] = Tools::getValue('content'.$language['id_lang']);
                    if(Tools::getValue('link_rewrite_'.$language['id_lang'])=='' && Tools::getValue('link_rewrite_'.$language['id_lang']) == null){
                        $DorGalleryCategory->link_rewrite[$language['id_lang']] = str_replace(array(' ',':', '\\', '/', '#', '!','*','.','?'),'-',Tools::getValue('name_'.$language['id_lang']));
                    }else{
                        $DorGalleryCategory->link_rewrite[$language['id_lang']] = str_replace(array(' ',':', '\\', '/', '#', '!','*','.','?'),'-',Tools::getValue('link_rewrite_'.$language['id_lang']));
                    }
                }
                $DorGalleryCategory->id_parent = Tools::getValue('id_parent');   
                $DorGalleryCategory->position = Tools::getValue('position');   
                $DorGalleryCategory->desc_limit = Tools::getValue('desc_limit');
                $DorGalleryCategory->active = Tools::getValue('active');
                $DorGalleryCategory->created = Date('y-m-d H:i:s');

                if (!$DorGalleryCategory->save())
                    $this->errors[] = Tools::displayError('An error has occurred: Can\'t save the current object');
                else{
                    Hook::exec('actionsbnewcat', array('DorGalleryCategory' => $DorGalleryCategory));
                    $this->processImageCategory($_FILES,$DorGalleryCategory->id);
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminGalleryCategory'));
                }
            }elseif($id_dorgallery_category = Tools::getValue('id_dorgallery_category')){
                $DorGalleryCategory = new DorGalleryCategory($id_dorgallery_category);
                $languages = Language::getLanguages(false);
                foreach ($languages as $language){
                    $title = str_replace('"','',htmlspecialchars_decode(html_entity_decode(Tools::getValue('name_'.$language['id_lang']))));
                    $DorGalleryCategory->name[$language['id_lang']] = $title;
                    $DorGalleryCategory->meta_keyword[$language['id_lang']] = Tools::getValue('meta_keyword_'.$language['id_lang']);
                    $DorGalleryCategory->meta_description[$language['id_lang']] = Tools::getValue('meta_description_'.$language['id_lang']);
                    $DorGalleryCategory->content[$language['id_lang']] = Tools::getValue('content_'.$language['id_lang']);
                    $DorGalleryCategory->link_rewrite[$language['id_lang']] = str_replace(array(' ',':', '\\', '/', '#', '!','*','.','?'),'-',Tools::getValue('link_rewrite_'.$language['id_lang']));
                }
                            
                $DorGalleryCategory->id_parent = Tools::getValue('id_parent');   
                $DorGalleryCategory->position = Tools::getValue('position');   
                $DorGalleryCategory->desc_limit = Tools::getValue('desc_limit');
                $DorGalleryCategory->active = Tools::getValue('active');
                if (!$DorGalleryCategory->update())
                    $this->errors[] = Tools::displayError('An error occurred while updating an object.') . ' <b>' . $this->table . ' (' . Db::getInstance()->getMsgError() . ')</b>';
                else
                    Hook::exec('actionsbupdatecat', array('DorGalleryCategory' => $DorGalleryCategory));
                $this->processImageCategory($_FILES,$DorGalleryCategory->id_dorgallery_category);
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminGalleryCategory'));
            }
        }elseif (Tools::isSubmit('statusdorgallery_category') && Tools::getValue($this->identifier)) {

            if ($this->tabAccess['edit'] === '1'){
                if (Validate::isLoadedObject($object = $this->loadObject())){
                    if ($object->toggleStatus()) {
                        Hook::exec('actionsbtogglecat', array('SmartBlogCat' => $this->object));
                        $identifier = ((int) $object->id_parent ? '&id_dorgallery_category=' . (int) $object->id_parent : '');
                        Tools::redirectAdmin($this->context->link->getAdminLink('AdminGalleryCategory'));
                    } else
                        $this->errors[] = Tools::displayError('An error occurred while updating the status.');
                } else
                    $this->errors[] = Tools::displayError('An error occurred while updating the status for an object.')
                            . ' <b>' . $this->table . '</b> ' . Tools::displayError('(cannot load object)');
            } else
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
        }elseif(Tools::isSubmit('dorgallery_categoryOrderby')&& Tools::isSubmit('dorgallery_categoryOrderway'))
        {
            $this->_defaultOrderBy = Tools::getValue('dorgallery_categoryOrderby');
            $this->_defaultOrderWay = Tools::getValue('dorgallery_categoryOrderway');
        }
    }

    public function initToolbar() {
        parent::initToolbar();
    }

    public function processImageCategory($FILES,$id){
 
        if (isset($FILES['category_image']) && isset($FILES['category_image']['tmp_name']) && !empty($FILES['category_image']['tmp_name'])) {
            if ($error = ImageManager::validateUpload($FILES['category_image'], 4000000))
                return $this->displayError($this->l('Invalid image'));
            else {
                $filename = '../img/tmp/dorgallery_category_mini_'.$id.'_1.jpg';
                
                $ext = substr($FILES['category_image']['name'], strrpos($FILES['category_image']['name'], '.') + 1);
                $file_name = $id . '.' . $ext;
                $path = _PS_MODULE_DIR_ .'dorgallery/images/category/' . $file_name;
                if (!move_uploaded_file($FILES['category_image']['tmp_name'], $path))
                    return $this->displayError($this->l('An error occurred while attempting to upload the file.'));
                else{
                    unlink($filename);
                }
                
            }
        }
    }
}
