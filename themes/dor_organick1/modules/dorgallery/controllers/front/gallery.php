<?php

if (!class_exists( 'DorImageBase' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/DorImageBase.php');
}
class dorgalleryGalleryModuleFrontController extends ModuleFrontController
{   
    public $phpself = 'gallery';
    public $ssl = true;
    public $dorgalleryCategory;
    public function init(){
            parent::init();
    }
    public function initContent(){
        parent::initContent();
        $this->context->smarty->assign(array('page_name'=>"dor-gallery"));
        $limit = Configuration::get('drogallery_perpage');
        $style = Configuration::get('drogallery_style');
        $start = 0;
        $params = $_GET;

        $page = isset($params['page'])?$params['page']:1;
        $current = $page;
        $page = $page - 1;
        $start = $page*$limit;
        $cateId = isset($params['cateid'])?$params['cateid']:0;
        $type = isset($params['type'])?$params['type']:"";
        $ajaxtype = isset($_GET['ajaxtype'])?$_GET['ajaxtype']:"";
        $template_name  = 'gallery/gallery.tpl';
        if($type == 2 || ($style == 2 && $type != 1)){
            $template_name  = 'gallery/gallery2.tpl';
        }
        if($type == 1 || ($style == 1 && $type != 2)){
            $template_name  = 'gallery/gallery.tpl';
        }
        $listCategory = array();
        $total = 0;
        $pageSite = "";
        //if($type == ""){
            $listCategory = $this->getListCategory();
            $total = $this->getToltalGallery($cateId);
            $pageSite = $this->getPageSite($total,$limit);
        //}
        $listgallerys = $this->getItemGallery($cateId,$start,$limit);
        $galleries = array();
        if(count($listgallerys) > 0){
            $thumbWidth = Configuration::get('drogallery_thumbwidthlists');
            $thumbHeight = Configuration::get('drogallery_thumbheightlists');
            $quanlity_image = 90;
            $thumbWidth = $thumbWidth != ""?(int)$thumbWidth:450;
            $thumbHeight = $thumbHeight != ""?(int)$thumbHeight:450;
            $sizeThumb = $thumbWidth."x".$thumbHeight;
            foreach ($listgallerys as $key => $item) {
                $pathImg = "dorgallery/images/".$item['id_dorgallery'].".jpg";
                $linkRewrite = $item['id_dorgallery']."_".$item['link_rewrite'];
                if($type == 2 || ($style == 2 && $type != 1)){
                    $images = DorImageBase::renderThumbMasonry($pathImg,$thumbWidth,$thumbHeight,$quanlity_image);
                }else if($type == 1 || ($style == 1 && $type != 2)){
                   $images = DorImageBase::renderThumbDorGallery($pathImg,$linkRewrite,$thumbWidth,$thumbHeight,$quanlity_image); 
                }
                $imageSource = _PS_MODULE_DIR_.$pathImg;
                list($width, $height) = getimagesize($imageSource); 
                $arr = array('h' => $height, 'w' => $width );
                $item['imagesize'] = $width.'x'.$height;
                $item['image'] = __PS_BASE_URI__.'modules/'.$pathImg;
                $item['thumb_image'] = $images;
                $galleries[] = $item;
            }
        }
        $CateArr = array();
        foreach ($listCategory as $key => $cate) {
            $CateArr[$key] = ".cate".$cate['id_dorgallery_category'];
        }

        $this->context->smarty->assign(
            array(
                'page_name'=>"dor-gallery",
                'total'=>$total,
                'current'=>$current,
                'pageSite'=>$pageSite,
                'tabdefaultID'=>0,
                'categories'=>$listCategory,
                'cateStrList'=>implode($CateArr, ', '),
                'galleries'=>$galleries
            )
        );
        if($ajaxtype != "" && $ajaxtype == "ajax"){
            $RESULT = '';
            if($type == 2 || ($style == 2 && $type != 1)){
                $RESULT .= '<div class="grid">';
                foreach ($galleries as $key => $gallery) {
                    $RESULT .= '<div class="grid__item" data-size="'.$gallery["image_width"].'x'.$gallery["image_height"].'">
                        <a href="'.$gallery["image"].'" data-cat="cate'.$cateId.'" data-author="'.$gallery["cate_name"].'" data-size="'.$gallery["image_width"].'x'.$gallery["image_height"].'" data-med-size="'.$gallery["image_width"].'x'.$gallery["image_height"].'" class="img-wrap main-item-gallery"><img src="'.$gallery["thumb_image"].'" alt="'.$gallery["name"].'" />
                            <span class="item-gallery-info">
                                <span class="item-gallery-info-inner">
                                    <span class="item-gallery-info-wrapper">
                                        <figure>'.$gallery["name"].'</figure>
                                        <em>'.$gallery["cate_name"].'</em>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>';
                }
                $RESULT .= '</div>';
            }else if($type == 1 || ($style == 1 && $type != 2)){
                foreach ($galleries as $key => $gallery) {
                    $RESULT .= '<a
                                    href="'.$gallery["image"].'" 
                                    class="portfolio img-wrap main-item-gallery cate'.$cateId.'"
                                    data-med="'.$gallery["image"].'" 
                                    data-size="'.$gallery["image_width"].'x'.$gallery["image_height"].'"
                                    data-med-size="'.$gallery["image_width"].'x'.$gallery["image_height"].'"
                                    data-author="'.$gallery["cate_name"].'"
                                    data-cat="cate'.$cateId.'"
                                >
                                    <span class="gallery-item-media"><img src="'.$gallery["thumb_image"].'" alt=\"'.$gallery["name"].'\"></span>
                                    <span class="item-gallery-info">
                                        <span class="item-gallery-info-inner">
                                            <span class="item-gallery-info-wrapper">
                                                <figure>'.$gallery["name"].'</figure>
                                                <em>'.$gallery["cate_name"].'</em>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                                ';
                }
            }
            

            echo Tools::jsonEncode($RESULT);die;
        }else{
            $this->setTemplate($template_name); 
        }
    }


    public static function getPageSite($total,$limit=12,$page=0){
        $htmlPage = "";
        if($total > $limit){
            for($i=0;$i<ceil($total/$limit);$i++){
                if($page == $i){
                    $htmlPage .= "<a href='#' class='ipage active'><span>".($i+1)."</span></a>";
                }else{
                    $htmlPage .= "<a href='#' class='ipage'><span>".($i+1)."</span></a>";
                }
                
            }
        }
        return $htmlPage;
    }
    public function getListCategory($id_lang=null,$id_shop=null){
        if($id_lang == null){
            $id_lang = (int)Context::getContext()->language->id;
        }
        if($id_shop == null){
            $id_shop = (int)Context::getContext()->shop->id;
        }
        $sql = '
                SELECT * FROM `'._DB_PREFIX_.'dorgallery_category` dgc 
                    INNER JOIN `'._DB_PREFIX_.'dorgallery_category_lang` dgcl ON(dgcl.`id_dorgallery_category` = dgc.`id_dorgallery_category` AND dgcl.`id_lang` = '.(int)($id_lang).')
                    INNER JOIN `'._DB_PREFIX_.'dorgallery_category_shop` dgcs ON dgcs.id_dorgallery_category = dgc.id_dorgallery_category and dgcs.id_shop = '.(int) $id_shop.' WHERE dgc.`active`= 1';
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        
        return $result;
    }
    public function getItemGallery($cateID = 0,$limit_start,$limit,$id_lang=null,$id_shop=null){
        if($id_lang == null){
            $id_lang = (int)Context::getContext()->language->id;
        }
        if($id_shop == null){
            $id_shop = (int)Context::getContext()->shop->id;
        }
        if($limit_start == '')
            $limit_start = 0;
        if($limit == '')
            $limit = 12;
        $where = " WHERE dg.active = 1";
        $sql = 'SELECT dg.*, dgl.*, dgcl.`name` cate_name FROM '._DB_PREFIX_.'dorgallery dg';
        if($cateID != 0){
            $where .= ' AND dg.id_dorgallery_category = '.(int) $cateID;
        }
        $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_lang dgl ON dg.id_dorgallery = dgl.id_dorgallery AND dgl.id_lang = '.$id_lang;
        $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_shop dgs ON dgs.id_dorgallery = dg.id_dorgallery AND dgs.id_shop = '.(int)$id_shop;
        $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_category dgc ON dg.id_dorgallery_category = dgc.id_dorgallery_category';
        $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_category_lang dgcl ON dgc.id_dorgallery_category = dgcl.id_dorgallery_category AND dgcl.id_lang = '.$id_lang;
        $sql .= $where;
        $sql .= ' ORDER BY dg.id_dorgallery DESC LIMIT '.$limit_start.','.$limit;
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        return $result;
    }
    public static function getToltalGallery($cateID, $id_lang = null,$id_shop=null){
        if($id_lang == null){
            $id_lang = (int)Context::getContext()->language->id;
        }
        if($id_shop == null){
            $id_shop = (int)Context::getContext()->shop->id;
        }
        $where = " WHERE p.active = 1 AND pl.id_lang = ".$id_lang;
        $sql = 'SELECT * FROM '._DB_PREFIX_.'dorgallery p';
                $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_lang pl ON p.id_dorgallery=pl.id_dorgallery';
                $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_shop ps ON pl.id_dorgallery = ps.id_dorgallery AND ps.id_shop = '.(int) $id_shop;
                if($cateID != 0){
                    $where .= ' AND p.id_dorgallery_category = '.(int) $cateID;
                    $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_category dgc ON p.id_dorgallery_category = dgc.id_dorgallery_category';
                    $sql .= ' INNER JOIN '._DB_PREFIX_.'dorgallery_category_lang dgcl ON dgc.id_dorgallery_category = dgcl.id_dorgallery_category AND dgcl.id_lang = '.$id_lang;
                }
                $sql .= $where;
        if (!$gallery = Db::getInstance()->executeS($sql))
            return false;           
        return count($gallery);
    }

    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();
    
        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Gallery', [], 'Breadcrumb'),
            'url' => $this->context->link->getModuleLink('dorgallery', 'gallery')
         ];
         return $breadcrumb;
    }

 }