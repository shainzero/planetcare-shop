<div class="dor-wishlist">
	<a class="addToDorWishlist" href="#" onclick="WishlistCart('wishlist_block_list', 'add',jQuery('#product_page_product_id').val(), jQuery('#idCombination').val(), jQuery('#quantity_wanted').val(), 0); return false;" data-toggle="tooltip" title="" data-original-title="{l s='Add to Wishlist' mod='dorblockwishlist'}">
		<i class="pe-7s-like"></i>
		<span class="wishlist-txt">{l s="Add to Wishlist" mod='dorblockwishlist'}</span>
	</a>
</div>