<?php
if (!class_exists( 'DorImageBase' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/DorImageBase.php');
}
if (!class_exists( 'DorCaches' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/Caches/DorCaches.php');
}
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
use PrestaShop\PrestaShop\Adapter\Category\CategoryProductSearchProvider;
use PrestaShop\PrestaShop\Adapter\Image\ImageRetriever;
use PrestaShop\PrestaShop\Adapter\Product\PriceFormatter;
use PrestaShop\PrestaShop\Core\Product\ProductListingPresenter;
use PrestaShop\PrestaShop\Core\Product\ProductPresenter;
use PrestaShop\PrestaShop\Adapter\Product\ProductColorsRetriever;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchContext;
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
use PrestaShop\PrestaShop\Core\Product\ProductExtraContentFinder;
class dor_bizproduct extends Module {
	var $_postErrors  = array();
	public function __construct() {
		$this->name 		= 'dor_bizproduct';
		$this->tab 			= 'front_office_features';
		$this->version 		= '2.0.0';
        $this->bootstrap =true;
		$this->author 		= 'Dorado Themes';
		$this->displayName 	= $this->l('Dor Biz Product Group');
		$this->description 	= $this->l('Dor Biz Product Group');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		parent :: __construct();
       
	}
    
	public function install() {
        // Install Tabs
        if(!(int)Tab::getIdFromClassName('AdminDorMenu')) {
            $parent_tab = new Tab();
            // Need a foreach for the language
            $parent_tab->name[$this->context->language->id] = $this->l('Dor Extensions');
            $parent_tab->class_name = 'AdminDorMenu';
            $parent_tab->id_parent = 0; // Home tab
            $parent_tab->module = $this->name;
            $parent_tab->add();
        }
        $tab = new Tab();
        foreach (Language::getLanguages() as $language)
        $tab->name[$language['id_lang']] = $this->l('Dor Biz Product');
        $tab->class_name = 'AdminDorBizproduct';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminDorMenu'); 
        $tab->module = $this->name;
        $tab->add();
        $today = date("Y-m-d");
        Configuration::updateValue($this->name . '_show_new', 1);
	    Configuration::updateValue($this->name . '_enableDorCache', 0);
        Configuration::updateValue($this->name . '_show_sale', 1);
        Configuration::updateValue($this->name . '_show_feature', 1);
        Configuration::updateValue($this->name . '_show_mostview', 0);
        Configuration::updateValue($this->name . '_show_toprated', 0);
        Configuration::updateValue($this->name . '_show_best', 0);
        Configuration::updateValue($this->name . '_biz_thumb_quanity', 100);
        Configuration::updateValue($this->name . '_biz_thumb_width', 350);
        Configuration::updateValue($this->name . '_biz_thumb_height', 467);
        Configuration::updateValue($this->name . '_p_limit', 6);
        Configuration::updateValue($this->name . '_biz_perpage', 3);
        Configuration::updateValue($this->name . '_biz_from_date', "2012-01-01");
        Configuration::updateValue($this->name . '_biz_to_date', $today);
		Configuration::updateValue($this->name . '_default_tab', 'new_product');
		return parent :: install()
            && $this->registerHook('dorBizproduct')
			&& $this->registerHook('displayBackOfficeHeader')
			&& $this->registerHook('header');
	}

      public function uninstall() {
        $today = date("Y-m-d");
        $tab = new Tab((int) Tab::getIdFromClassName('AdminDorBizproduct'));
        $tab->delete();
        $this->_clearCache('bizproduct.tpl');
        return parent::uninstall();
    }

  
	public function psversion() {
		$version=_PS_VERSION_;
		$exp=$explode=explode(".",$version);
		return $exp[1];
	}
    
	public function hookdisplayHeader($params)
	{
		if (!isset($this->context->controller->php_self) || $this->context->controller->php_self != 'index')
            return;
		$this->context->controller->addCSS($this->_path.'css/biz_product.css');
        $this->context->controller->addJS($this->_path . 'js/dorbizproduct.js');
	}
    public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addJS($this->_path . 'js/dorbizproduct.admin.js');
    }
    public function hookDorBizproduct($params) {
            $nb = Configuration::get($this->name . '_p_limit');
            $languages = Language::getLanguages(true, $this->context->shop->id);

            $productTabslider = array();
            if(Configuration::get($this->name . '_show_toprated')) {
                $productTabslider[] = array('id'=>'toprated_product','link'=>'/','name' => $this->l('Top Rated'));
            } 
            if(Configuration::get($this->name . '_show_sale')) {
                $productTabslider[] = array('id'=> 'special_product','link'=>'prices-drop','name' => $this->l('Special'));
            }
            if(Configuration::get($this->name . '_show_best')) {
                $productTabslider[] = array('id'=>'besseller_product','link'=>'best-sales','name' => $this->l('Best Seller'));
            }
            if(Configuration::get($this->name . '_show_new')) {
                $productTabslider[] = array('id'=>'new_product','link'=>'new-products', 'name' => $this->l('New Arrivals'));
            }
            if(Configuration::get($this->name . '_show_feature')) {
                $productTabslider[] = array('id'=>'feature_product','link'=>'/','name' => $this->l('Featured'));
            }
            if(Configuration::get($this->name . '_show_mostview')) {
                $productTabslider[] = array('id'=>'mostview_product','link'=>'/','name' => $this->l('Most Viewed'));
            }

            $this->smarty->assign(array(
                'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
                'languages' => $languages,
    
            ));
            $this->context->smarty->assign('productTabslider', $productTabslider);
        return $this->display(__FILE__, 'bizproduct.tpl');
    }

	  public function getContent() {
        $output = '<h2>' . $this->displayName . '</h2>';
        if (Tools::isSubmit('submitUpdate')) {
            if (!sizeof($this->_postErrors))
                $this->_postProcess();
            else {
                foreach ($this->_postErrors AS $err) {
                    $this->_html .= '<div class="alert error">' . $err . '</div>';
                }
            }
        }
        return $output . $this->_displayForm();
    }

    public function getSelectOptionsHtml($options = NULL, $name = NULL, $selected = NULL) {
        $html = "";
        $html .='<select name =' . $name . ' style="width:130px">';
        if (count($options) > 0) {
            foreach ($options as $key => $val) {
                if (trim($key) == trim($selected)) {
                    $html .='<option value=' . $key . ' selected="selected">' . $val . '</option>';
                } else {
                    $html .='<option value=' . $key . '>' . $val . '</option>';
                }
            }
        }
        $html .= '</select>';
        return $html;
    }

    private function _postProcess() {
        $this->_html = isset($this->_html)?$this->_html:"";
        Configuration::updateValue($this->name . '_show_new', Tools::getValue('show_new'));
        Configuration::updateValue($this->name . '_enableDorCache', Tools::getValue('enableDorCache'));
        Configuration::updateValue($this->name . '_show_sale', Tools::getValue('show_sale'));
        Configuration::updateValue($this->name . '_show_feature', Tools::getValue('show_feature'));
        Configuration::updateValue($this->name . '_biz_from_date', Tools::getValue('biz_from_date'));
        Configuration::updateValue($this->name . '_biz_to_date', Tools::getValue('biz_to_date'));
        Configuration::updateValue($this->name . '_show_mostview', Tools::getValue('show_mostview'));
        Configuration::updateValue($this->name . '_show_toprated', Tools::getValue('show_toprated'));
        Configuration::updateValue($this->name . '_show_best', Tools::getValue('show_best'));
        Configuration::updateValue($this->name . '_biz_thumb_width', Tools::getValue('biz_thumb_width'));
        Configuration::updateValue($this->name . '_biz_thumb_quanity', Tools::getValue('biz_thumb_quanity'));
        Configuration::updateValue($this->name . '_biz_thumb_height', Tools::getValue('biz_thumb_height'));
        Configuration::updateValue($this->name . '_p_limit', Tools::getValue('p_limit'));
        Configuration::updateValue($this->name . '_biz_perpage', Tools::getValue('biz_perpage'));
        Configuration::updateValue($this->name . '_default_tab', Tools::getValue('default_tab'));
        $this->_html .=  $this->displayConfirmation($this->l('Configuration updated')) ;
    }
	
	private function _displayForm(){
        
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => Context::getContext()->getTranslator()->trans('Sectting', array(), 'Modules.dor_bizproduct'),
                    'icon' => 'icon-link'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => Context::getContext()->getTranslator()->trans('Show New Products:', array(), 'Modules.dor_bizproduct'),
                        'name' => 'show_new',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => Context::getContext()->getTranslator()->trans('Enabled', array(), 'Modules.dor_bizproduct')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => Context::getContext()->getTranslator()->trans('Disabled', array(), 'Modules.dor_bizproduct')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => Context::getContext()->getTranslator()->trans('Show special Products:', array(), 'Modules.dor_bizproduct'),
                        'name' => 'show_sale',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => Context::getContext()->getTranslator()->trans('Enabled', array(), 'Modules.dor_bizproduct')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => Context::getContext()->getTranslator()->trans('Disabled', array(), 'Modules.dor_bizproduct')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => Context::getContext()->getTranslator()->trans('Show Bestselling Products: ', array(), 'Modules.dor_bizproduct'),
                        'name' => 'show_best',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => Context::getContext()->getTranslator()->trans('Enabled', array(), 'Modules.dor_bizproduct')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => Context::getContext()->getTranslator()->trans('Disabled', array(), 'Modules.dor_bizproduct')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => Context::getContext()->getTranslator()->trans('Show Feature Products: ', array(), 'Modules.dor_bizproduct'),
                        'name' => 'show_feature',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => Context::getContext()->getTranslator()->trans('Enabled', array(), 'Modules.dor_bizproduct')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => Context::getContext()->getTranslator()->trans('Disabled', array(), 'Modules.dor_bizproduct')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => Context::getContext()->getTranslator()->trans('Show Top Rated Products: ', array(), 'Modules.dor_bizproduct'),
                        'name' => 'show_toprated',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => Context::getContext()->getTranslator()->trans('Enabled', array(), 'Modules.dor_bizproduct')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => Context::getContext()->getTranslator()->trans('Disabled', array(), 'Modules.dor_bizproduct')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => Context::getContext()->getTranslator()->trans('Show Most View Products: ', array(), 'Modules.dor_bizproduct'),
                        'name' => 'show_mostview',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => Context::getContext()->getTranslator()->trans('Enabled', array(), 'Modules.dor_bizproduct')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => Context::getContext()->getTranslator()->trans('Disabled', array(), 'Modules.dor_bizproduct')
                            )
                        ),
                    ),
                    array(
                       'type' => 'text',
                       'label' => Context::getContext()->getTranslator()->trans('Most View From Date:', array(), 'Modules.dor_bizproduct'),
                       'name' => 'biz_from_date',
                    ),
                    array(
                       'type' => 'text',
                       'label' => Context::getContext()->getTranslator()->trans('Most View To Date:', array(), 'Modules.dor_bizproduct'),
                       'name' => 'biz_to_date',
                    ),
                    array(
                       'type' => 'text',
                       'label' => Context::getContext()->getTranslator()->trans('Thumb Quanity:', array(), 'Modules.dor_bizproduct'),
                       'name' => 'biz_thumb_quanity',
                    ),
                    array(
                       'type' => 'text',
                       'label' => Context::getContext()->getTranslator()->trans('Thumb Image Width:', array(), 'Modules.dor_bizproduct'),
                       'name' => 'biz_thumb_width',
                    ),
                    array(
                       'type' => 'text',
                       'label' => Context::getContext()->getTranslator()->trans('Thumb Image Height:', array(), 'Modules.dor_bizproduct'),
                       'name' => 'biz_thumb_height',
                    ),
                   array(
                       'type' => 'text',
                       'label' => Context::getContext()->getTranslator()->trans('Products Limit:', array(), 'Modules.dor_bizproduct'),
                       'name' => 'p_limit',
                   ),
                   array(
                       'type' => 'text',
                       'label' => Context::getContext()->getTranslator()->trans('Products Per Page:', array(), 'Modules.dor_bizproduct'),
                       'name' => 'biz_perpage',
                   ),
                   array(
                        'type' => 'switch',
                        'label' => Context::getContext()->getTranslator()->trans('Use Dor Caches: ', array(), 'Modules.dor_bizproduct'),
                        'name' => 'enableDorCache',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => Context::getContext()->getTranslator()->trans('Enabled', array(), 'Modules.dor_bizproduct')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => Context::getContext()->getTranslator()->trans('Disabled', array(), 'Modules.dor_bizproduct')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => Context::getContext()->getTranslator()->trans('Save', array(), 'Modules.dor_bizproduct'),
                    'name' => 'submitUpdate',
                ),
            )
        );
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->submit_action = 'submitUpdate';
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $module = _PS_MODULE_DIR_ ;
        $helper->tpl_vars = array(
            'module' =>$module,
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );
        return $helper->generateForm(array($fields_form));

	}

    public function getConfigFieldsValues()
    {
        return array(

        'show_new' =>     Tools::getValue('show_new', Configuration::get($this->name . '_show_new')),
        'enableDorCache' =>     Tools::getValue('enableDorCache', Configuration::get($this->name . '_enableDorCache')),
        'show_sale' =>   Tools::getValue('show_sale', Configuration::get($this->name . '_show_sale')),
        'show_feature' =>     Tools::getValue('show_feature', Configuration::get($this->name . '_show_feature')),
        'biz_from_date' =>     Tools::getValue('biz_from_date', Configuration::get($this->name . '_biz_from_date')),
        'biz_to_date' =>     Tools::getValue('biz_to_date', Configuration::get($this->name . '_biz_to_date')),
        'show_mostview' =>     Tools::getValue('show_mostview', Configuration::get($this->name . '_show_mostview')),
		'show_toprated' =>     Tools::getValue('show_toprated', Configuration::get($this->name . '_show_toprated')),
        'show_best' =>    Tools::getValue('show_best', Configuration::get($this->name . '_show_best')),
        'biz_thumb_width' =>    Tools::getValue('biz_thumb_width', Configuration::get($this->name . '_biz_thumb_width')),
        'biz_thumb_quanity' =>    Tools::getValue('biz_thumb_quanity', Configuration::get($this->name . '_biz_thumb_quanity')),
        'biz_thumb_height' =>    Tools::getValue('biz_thumb_height', Configuration::get($this->name . '_biz_thumb_height')),
        'p_limit' =>    Tools::getValue('p_limit', Configuration::get($this->name . '_p_limit')),
		'biz_perpage' =>    Tools::getValue('biz_perpage', Configuration::get($this->name . '_biz_perpage')),
        'min_item' =>     Tools::getValue('min_item', Configuration::get($this->name . '_min_item')),
		'default_tab' =>     Tools::getValue('default_tab', Configuration::get($this->name . '_default_tab')),
        );
    }
	private function _installHookCustomer(){
		$hookspos = array(
				'tabsProducts',
			); 
		foreach( $hookspos as $hook ){
			if( Hook::getIdByName($hook) ){
				
			} else {
				$new_hook = new Hook();
				$new_hook->name = pSQL($hook);
				$new_hook->title = pSQL($hook);
				$new_hook->add();
				$id_hook = $new_hook->id;
			}
		}
		return true;
	}

    public function hookAjaxCall($params)
    {
        global $smarty, $cookie;
        $varsData = "";
        $page = isset($page)?$page:0;
        $id_shop = (int) Context::getContext()->shop->id;
        $id_lang = (int)Context::getContext()->language->id;
        $dorShowQuantity = Configuration::get('dor_themeoptions_dorShowQuantity');
        if(isset($_POST['tab']) && $_POST['tab'] != ""){
            $nb = Configuration::get($this->name . '_p_limit');
            $nPerPage = Configuration::get($this->name . '_biz_perpage');
            $thumbwidth = Configuration::get($this->name . '_biz_thumb_width');
            $thumbheight = Configuration::get($this->name . '_biz_thumb_height');
            $thumbquanity = Configuration::get($this->name . '_biz_thumb_quanity');
            $fromDate = Configuration::get($this->name . '_biz_from_date');
            $toDate = Configuration::get($this->name . '_biz_to_date');
            $tab = $_POST['tab'];
            $dorTimeCache  = Configuration::get('dor_themeoptions_dorTimeCache',Configuration::get('dorTimeCache'));
            $dorTimeCache = $dorTimeCache?$dorTimeCache:86400;
            $dorCaches  = Configuration::get($this->name . '_enableDorCache');
            $objCache = new DorCaches(array('name'=>'default','path'=>_PS_ROOT_DIR_.'/override/Dor/Caches/smartcaches/bizproduct/','extension'=>'.cache'));
            $fileCache = 'BizproductCaches-Shop-'.$tab.$id_shop."-Lang-".$id_lang;
            $objCache->setCache($fileCache);
            $cacheData = $objCache->renderData($fileCache);
            if($cacheData && $dorCaches){
                $data = $cacheData['lists'];
            }else{
                if($tab == 'new_product'){
                    $productData = Product::getNewProducts((int) Context::getContext()->language->id, 0, ($nb ? $nb : 9));
                }elseif($tab == 'special_product'){
                    $productData = Product::getPricesDrop((int) Context::getContext()->language->id, 0, ($nb ? $nb : 9));
                }elseif($tab == 'besseller_product'){
                    ProductSale::fillProductSales();
                    $productData =  $this->getBestSales ((int) Context::getContext()->language->id, 0, ($nb ? $nb : 9), null,  null);
                }elseif($tab == 'feature_product'){
                    $category = new Category(Context::getContext()->shop->getCategory(), (int) Context::getContext()->language->id);
                    $productData = $category->getProducts((int) Context::getContext()->language->id, 0, ($nb ? $nb : 9));
                }else if($tab == "toprated_product"){
                    $productData =  $this->getTopRatedProduct ((int) Context::getContext()->language->id, $page, ($nb ? $nb : 9), null,  null);
                    if(count($productData) == 0){
                        $productData = Product::getNewProducts((int) Context::getContext()->language->id, 0, ($nb ? $nb : 9));
                    }
                }else if($tab=="mostview_product"){
                    $productData = $this->getTableMostViewed($fromDate,$toDate,($nb ? $nb : 5));
                    $productData = $productData['products'];
                }
                 $data = array();
                if($productData){
                    $id_lang = (int)Context::getContext()->language->id;
                    $assembler = new ProductAssembler($this->context);
                    $presenterFactory = new ProductPresenterFactory($this->context);
                    $presentationSettings = $presenterFactory->getPresentationSettings();
                    $presenter = new ProductPresenter(
                        new ImageRetriever(
                            $this->context->link
                        ),
                        $this->context->link,
                        new PriceFormatter(),
                        new ProductColorsRetriever(),
                        $this->context->getTranslator()
                    );

                    $products_for_template = [];

                    foreach ($productData as $rawProduct) {
                        $products_for_template[] = $presenter->present(
                            $presentationSettings,
                            $assembler->assembleProduct($rawProduct),
                            $this->context->language
                        ); 
                    }
                    /*foreach ($products_for_template as $key => $item) {
                        $id_image = Product::getCover($item['id_product']);
                        $images = "";
                        if (sizeof($id_image) > 0){
                            $image = new Image($id_image['id_image']);
                            $image_url = "/p/".$image->getExistingImgPath().".jpg";
                            $width=$thumbwidth;$height=$thumbheight;
                            $linkRewrite = $item['id_product']."_".$id_image['id_image']."_".$item['link_rewrite'];
                            $images = DorImageBase::renderThumbProduct($image_url,$linkRewrite,$width,$height,$type = '', $title = '', $isThumb = true, $thumbquanity);
                            $item['imageThumb'] = $images;
                        }
                        
                        $item['imageData'] = $images;
                        $data[] = $item;
                    }*/
                    $data = $products_for_template;
                    if($dorCaches){
                        $datas['lists'] = $products_for_template;
                        $objCache->store($fileCache, $datas, $expiration = $dorTimeCache);
                    }
                }
            }
            $per = $nPerPage;
            $products = array_chunk($data, $per);
            $cartUrl = $this->context->link->getPageLink('cart', true);
            $this->context->smarty->assign('static_token', Tools::getToken(false));
            $this->context->smarty->assign('cartUrl', $cartUrl);
            $this->context->smarty->assign('carturl', $cartUrl);
            $this->context->smarty->assign('productlists', $products);
            $this->context->smarty->assign('tabId', $tab);
            $this->context->smarty->assign('dorShowQuantity', $dorShowQuantity);
            $varsData = $this->display(__FILE__, 'product-lists.tpl');
        }
        $dataResult = Tools::jsonEncode($varsData);
        return $dataResult;
    }
    public function RotatorImg($idproduct) {
            $id_shop = (int)Context::getContext()->shop->id;
            $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'image` img'; 
            $sql .= ' LEFT JOIN `'. _DB_PREFIX_ . 'image_shop` imgs';
            $sql .= ' ON img.id_image = imgs.id_image';
            $sql .= ' where imgs.`id_shop` ='.$id_shop ;
            $sql .= ' AND img.`id_product` ='.$idproduct ;
            $sql .= ' AND imgs.`rotator` =1' ;
            $imageNew = Db::getInstance()->ExecuteS($sql);
            if(!$imageNew) {
                  $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'image` img'; 
                  $sql .= ' where img.`rotator` =1';
                  $sql .= ' AND img.`id_product` ='.$idproduct ;
                  $imageNew = Db::getInstance()->ExecuteS($sql);
            }

            $images = array(
                'rotator_img'=>$imageNew,
                'idproduct'=>$idproduct
            );

        return $images;
    }
	public static function getBestSales($id_lang, $page_number = 0, $nb_products = 10, $order_by = null, $order_way = null)
	{
		if ($page_number < 0) $page_number = 0;
		if ($nb_products < 1) $nb_products = 10;
		$final_order_by = $order_by;
		$order_table = ''; 		
		if (is_null($order_by) || $order_by == 'position' || $order_by == 'price') $order_by = 'sales';
		if ($order_by == 'date_add' || $order_by == 'date_upd')
			$order_table = 'product_shop'; 				
		if (is_null($order_way) || $order_by == 'sales') $order_way = 'DESC';
		$groups = FrontController::getCurrentCustomerGroups();
		$sql_groups = (count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1');
		$interval = Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20;
		
		$prefix = '';
		if ($order_by == 'date_add')
			$prefix = 'p.';
		
		$sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity,
					pl.`description`, pl.`description_short`, pl.`link_rewrite`, pl.`meta_description`,
					pl.`meta_keywords`, pl.`meta_title`, pl.`name`,
					m.`name` AS manufacturer_name, p.`id_manufacturer` as id_manufacturer,
					MAX(image_shop.`id_image`) id_image, il.`legend`,
					ps.`quantity` AS sales, t.`rate`, pl.`meta_keywords`, pl.`meta_title`, pl.`meta_description`,
					DATEDIFF(p.`date_add`, DATE_SUB(NOW(),
					INTERVAL '.$interval.' DAY)) > 0 AS new
				FROM `'._DB_PREFIX_.'product_sale` ps
				LEFT JOIN `'._DB_PREFIX_.'product` p ON ps.`id_product` = p.`id_product`
				'.Shop::addSqlAssociation('product', 'p', false).'
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
				LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
				Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
				LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (product_shop.`id_tax_rules_group` = tr.`id_tax_rules_group`)
					AND tr.`id_country` = '.(int)Context::getContext()->country->id.'
					AND tr.`id_state` = 0
				LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
				'.Product::sqlStock('p').'
				WHERE product_shop.`active` = 1
					AND product_shop.`visibility` != \'none\'
					AND p.`id_product` IN (
						SELECT cp.`id_product`
						FROM `'._DB_PREFIX_.'category_group` cg
						LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_category` = cg.`id_category`)
						WHERE cg.`id_group` '.$sql_groups.'
					)
				GROUP BY product_shop.id_product
				ORDER BY '.(!empty($order_table) ? '`'.pSQL($order_table).'`.' : '').'`'.pSQL($order_by).'` '.pSQL($order_way).'
				LIMIT '.(int)($page_number * $nb_products).', '.(int)$nb_products;

		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		if ($final_order_by == 'price')
			Tools::orderbyPrice($result, $order_way);
		if (!$result)
			return false;
		return Product::getProductsProperties($id_lang, $result);
	}
    public function getTopRatedProduct($id_lang, $page_number = 0, $nb_products = 10, $order_by = null, $order_way = null)
    {
        if ($page_number < 0) $page_number = 0;
        if ($nb_products < 1) $nb_products = 10;
        $final_order_by = $order_by;
        $final_order_by = 'price';
        $order_table = 'product_shop';    
        if (is_null($order_by) || $order_by == 'position' || $order_by == 'price') $order_by = 'p.id_product';
        if ($order_by == 'date_add' || $order_by == 'date_upd')
            $order_table = 'product_shop';              
        if (is_null($order_way) || $order_by == 'p.id_product') $order_way = 'DESC';
        $groups = FrontController::getCurrentCustomerGroups();
        $sql_groups = (count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1');
        $interval = Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20;
        $prefix = '';
        if ($order_by == 'date_add')
            $prefix = 'p.';
        
        $listProductIDs = $this->getIdProductTopRated($nb_products);
        if(count($listProductIDs) > 0){
            $idproducts = array();
            foreach ($listProductIDs as $i => $productid) {
                array_push($idproducts,$productid['id_product']);
            }
            $idproducts = implode($idproducts, ",");
            $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, pl.`link_rewrite`, pl.`name`,
                    MAX(image_shop.`id_image`) id_image, il.`legend`, t.`rate`,
                    DATEDIFF(p.`date_add`, DATE_SUB(NOW(),
                    INTERVAL '.$interval.' DAY)) > 0 AS new
                FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p', false).'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
                    ON p.`id_product` = pl.`id_product`
                    AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
                LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
                Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
                LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
                LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (product_shop.`id_tax_rules_group` = tr.`id_tax_rules_group`)
                    AND tr.`id_country` = '.(int)Context::getContext()->country->id.'
                    AND tr.`id_state` = 0
                LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
                '.Product::sqlStock('p').'
                WHERE product_shop.`active` = 1
                    AND product_shop.`visibility` != \'none\'
                    AND product_shop.id_product IN ('.$idproducts.')
                GROUP BY product_shop.id_product
                LIMIT '.(int)($page_number * $nb_products).', '.(int)$nb_products;
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
            /*if ($final_order_by == 'price')
                Tools::orderbyPrice($result, $order_way);*/
            if (!$result)
                return false;
            return Product::getProductsProperties($id_lang, $result);
        }else{
            return false;
        }
    }
    public static function getIdProductTopRated($limit){
        $sql = 'SELECT `id_product`, (SUM(`grade`) / COUNT(`grade`)) AS grade FROM `'._DB_PREFIX_.'dorproduct_comment` WHERE 1 GROUP BY `id_product` ORDER BY grade DESC LIMIT '.(int)$limit;
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        return $result;
    }
    public function getTableMostViewed($date_from, $date_to, $limit=2)
    {
        if (Configuration::get('PS_STATSDATA_PAGESVIEWS'))
            $data = $this->getTotalViewed($date_from, $date_to, $limit);
        else
            $data = '<div class="alert alert-info">'.$this->l('You must enable the "Save global page views" option from the "Data mining for statistics" module in order to display the most viewed products, or use the Google Analytics module.').'</div>';
        return array('products' => $data);
    }

    public function getTotalViewed($date_from, $date_to, $limit = 10)
    {
        $id_lang = (int)Context::getContext()->language->id;
        $sql = '
            SELECT p.*,pa.id_object, pv.counter, pl.link_rewrite, pl.`name`, product_shop.*
            FROM `'._DB_PREFIX_.'page_viewed` pv
            LEFT JOIN `'._DB_PREFIX_.'date_range` dr ON pv.`id_date_range` = dr.`id_date_range`
            LEFT JOIN `'._DB_PREFIX_.'page` pa ON pv.`id_page` = pa.`id_page`
            LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = pa.`id_object`
            '.Shop::addSqlAssociation('product', 'p', false).'
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
                    ON p.`id_product` = pl.`id_product`
                    AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
            LEFT JOIN `'._DB_PREFIX_.'page_type` pt ON pt.`id_page_type` = pa.`id_page_type`
            LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (product_shop.`id_tax_rules_group` = tr.`id_tax_rules_group`)
            WHERE pt.`name` = \'product\'
                    AND product_shop.`active` = 1
                    AND product_shop.`visibility` != \'none\'
            '.Shop::addSqlRestriction(false, 'pv').'
            AND dr.`time_start` BETWEEN "'.pSQL($date_from).'" AND "'.pSQL($date_to).'"
            AND dr.`time_end` BETWEEN "'.pSQL($date_from).'" AND "'.pSQL($date_to).'"
            GROUP BY product_shop.id_product
            ORDER BY pv.counter DESC
            LIMIT '.(int)$limit;
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        return Product::getProductsProperties($id_lang, $result);
    }
}