<?php

// Security
if (!defined('_PS_VERSION_'))
    exit;

// Checking compatibility with older PrestaShop and fixing it
if (!defined('_MYSQL_ENGINE_'))
    define('_MYSQL_ENGINE_', 'MyISAM');
use PrestaShop\PrestaShop\Core\Module\WidgetInterface;
// Loading Models
require_once(_PS_MODULE_DIR_ .'dor_managerblockfooter/models/Managerblockfooter.php');

class Dor_managerblockfooter extends Module implements WidgetInterface {
    public  $hookAssign   = array();
    public $_staticModel =  "";
    public function __construct() {
        $this->name = 'dor_managerblockfooter';
        $this->tab = 'front_office_features';
        $this->version = '2.0';
        $this->author = 'Dorado Themes';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.7');
        $this->hookAssign = array('footer');
        $this->_staticModel = new Managerblockfooter();
        parent::__construct();

        $this->displayName = $this->getTranslator()->trans('Dor Manage Footer Blocks');
        $this->description = $this->getTranslator()->trans('Dor Manage Footer Blocks');

        $this->confirmUninstall = $this->getTranslator()->trans('Are you sure you want to uninstall?');
        $this->admin_tpl_path = _PS_MODULE_DIR_ . $this->name . '/views/templates/admin/';
    }
    public function install() {
		$res = $this->installDb();
          // Install Tabs
		if(!(int)Tab::getIdFromClassName('AdminDorMenu')) {
			$parent_tab = new Tab();
			$parent_tab->name[$this->context->language->id] = $this->getTranslator()->trans('Dor Extensions');
			$parent_tab->class_name = 'AdminDorMenu';
			$parent_tab->id_parent = 0; // Home tab
			$parent_tab->module = $this->name;
			$parent_tab->add();
		}

        $tab = new Tab();
        // Need a foreach for the language
		foreach (Language::getLanguages() as $language)
        $tab->name[$language['id_lang']] = $this->getTranslator()->trans('Dor Manage Footer Blocks');
        $tab->class_name = 'AdminDorManagerFooter';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminDorMenu'); 
        $tab->module = $this->name;
        $tab->add();
        // Set some defaults
        return parent::install() &&
                $this->registerHook('footer') &&
        		$this->_installHookCustomer()&&
                $this->registerHook('blockDoradoFooter')&&
                $this->registerHook('doradoFooterTop')&&
                $this->registerHook('doradoFooterAdv')&&
        		$this->registerHook('doradoFooter1')&&
        		$this->registerHook('doradoFooter2')&&
        		$this->registerHook('doradoFooter3')&&
                $this->registerHook('doradoFooter4')&&
                $this->registerHook('doradoFooter5')&&
                $this->registerHook('doradoFooter6')&&
                $this->registerHook('doradoFooter7')&&
                $this->registerHook('doradoFooter8')&&
                $this->registerHook('doradoFooter9')&&
        		$this->registerHook('doradoFooter10')&&
                $this->registerHook('displayBackOfficeHeader');
		return (bool)$res;
    }
    public function uninstall() {
        Configuration::deleteByName('dor_managerblockfooter');
		$res = $this->uninstallDb();
        $tab = new Tab((int) Tab::getIdFromClassName('Admindormanagerfooter'));
        $tab->delete();
        // Uninstall Module
        if (!parent::uninstall())
            return false;
        return true;
		return (bool)$res;
    }
/* database */
public function installDb(){
        $res = Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'dor_blockfooter` (
			  `id_dor_blockfooter` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `identify` varchar(128) NOT NULL,
			  `hook_position` varchar(128) NOT NULL,
			  `name_module` varchar(128) NOT NULL,
			  `hook_module` varchar(128) NOT NULL,
			  `order` int(10) unsigned NOT NULL,
			  `insert_module` int(10) unsigned NOT NULL,
			  `active` int(10) unsigned NOT NULL,
			  `is_default` int(10) unsigned NOT NULL DEFAULT "0",
			  `showhook` int(10) unsigned NOT NULL,
			  PRIMARY KEY (`id_dor_blockfooter`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15'
        );
        if ($res)
            $res &= Db::getInstance()->execute(
                'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'dor_blockfooter_lang` (
				  `id_dor_blockfooter` int(11) unsigned NOT NULL,
				  `id_lang` int(11) unsigned NOT NULL,
				  `title` varchar(128) NOT NULL,
				  `description` longtext,
				  PRIMARY KEY (`id_dor_blockfooter`,`id_lang`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        if ($res)
            $res &= Db::getInstance()->execute(
                'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'dor_blockfooter_shop` (
                  `id_dor_blockfooter` int(11) unsigned NOT NULL,
                  `id_shop` int(11) unsigned NOT NULL,
                  PRIMARY KEY (`id_dor_blockfooter`,`id_shop`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $id_shop = (int)Context::getContext()->shop->id;
        $sql =  "
            INSERT INTO `"._DB_PREFIX_."dor_blockfooter` (`id_dor_blockfooter`, `identify`, `hook_position`, `name_module`, `hook_module`, `order`, `insert_module`, `active`, `is_default`, `showhook`) VALUES
                (1, 'dor-our-store-footer', 'doradoFooter1', 'Chose Module', 'displayFooter', 0, 0, 0, 0, 1),
                (2, 'dor-quick-shop-footer', 'doradoFooter1', 'Chose Module', 'displayFooter', 1, 0, 0, 0, 0),
                (3, 'dor-information-footer', 'doradoFooter1', 'Chose Module', 'displayFooter', 2, 0, 0, 0, 1),
                (4, 'dor-our-policy', 'doradoFooter1', 'Chose Module', 'displayFooter', 3, 0, 0, 0, 1),
                (5, 'dor-footer-copyright-and-info', 'doradoFooterAdv', 'Chose Module', 'displayFooter', 0, 0, 0, 0, 1);
            ";
        if ($res)
        $res &=  Db::getInstance()->Execute($sql);


        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
        $urlFile = Tools::htmlentitiesutf8($protocol.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
        foreach (Language::getLanguages() as $language){
            $id_lang = $language['id_lang'];
            
        $sql2 =  "
            INSERT INTO `"._DB_PREFIX_."dor_blockfooter_lang` (`id_dor_blockfooter`, `id_lang`, `title`, `description`) VALUES
            (1, ".$id_lang.", 'Our Store', '<section class=\"footer-group-link footer-block col-xs-12 col-sm-4\" id=\"block_contact_footer\">\r\n<h4 class=\"hidden-sm-down hidden\">Our Store</h4>\r\n<div class=\"title clearfix hidden-md-up\" data-target=\"#footer_contact_block\" data-toggle=\"collapse\"><span class=\"h3\">Our Store</span> <span class=\"pull-xs-right\"> <span class=\"navbar-toggler collapse-icons\"> <i class=\"material-icons add\"></i> <i class=\"material-icons remove\"></i> </span> </span></div>\r\n<ul class=\"toggle-footer collapse\" id=\"footer_contact_block\">\r\n<li class=\"item logo-footer\"><a href=\"/\" title=\"Organick Store\"><img src=\"".$urlFile."img/cms/dorado/icon/logo-footer.png\" alt=\"Organick\" width=\"471\" height=\"221\" /></a></li>\r\n<li class=\"item\">\r\n<div class=\"footer-our-store\"><span class=\"pe-7s-headphones\"><span class=\"hidden\">icon</span></span>\r\n<div class=\"our-store-info\">\r\n<h5>Get Question ? Contact Us 24/7</h5>\r\n<p>(+60) 6789 6789 / 6666 8888</p>\r\n<p>support@organick.com</p>\r\n</div>\r\n</div>\r\n</li>\r\n<li class=\"item contact-us-info-footer\">\r\n<h5>Contact Info</h5>\r\n<p>Organick - Oxford Street 06 United Kingdom</p>\r\n</li>\r\n</ul>\r\n</section>'),
            (2, ".$id_lang.", 'Quick Shop', '<section class=\"footer-group-link footer-block col-xs-12 col-sm-2 footer-block-wap\" id=\"block_quickshop_links_footer\">\r\n<h4 class=\"hidden-sm-down\">Quick Shop</h4>\r\n<div class=\"title clearfix hidden-md-up\" data-target=\"#footer_quickshop_block\" data-toggle=\"collapse\"><span class=\"h3\">Quick Shop</span> <span class=\"pull-xs-right\"> <span class=\"navbar-toggler collapse-icons\"> <i class=\"material-icons add\"></i> <i class=\"material-icons remove\"></i> </span> </span></div>\r\n<ul class=\"toggle-footer collapse\" id=\"footer_quickshop_block\">\r\n<li class=\"item\"><a href=\"#\" title=\"Clothing\">Clothing</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Jewellery\">Jewellery</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Shoes\">Shoes</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Accessories\">Accessories</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Collections\">Collections</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Hot Sales\">Hot Sales</a></li>\r\n</ul>\r\n</section>'),
            (3, ".$id_lang.", 'Information', '<section class=\"footer-group-link footer-block col-xs-12 col-sm-3 footer-block-wap\" id=\"block_infomation_links_footer\">\r\n<h4 class=\"hidden-sm-down\">Information</h4>\r\n<div class=\"title clearfix hidden-md-up\" data-target=\"#footer_infomation\" data-toggle=\"collapse\"><span class=\"h3\">Information</span> <span class=\"pull-xs-right\"> <span class=\"navbar-toggler collapse-icons\"> <i class=\"material-icons add\"></i> <i class=\"material-icons remove\"></i> </span> </span></div>\r\n<ul class=\"toggle-footer collapse\" id=\"footer_infomation\">\r\n<li class=\"item\"><a href=\"#\" title=\"My account\">My account</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Order history\">Order history</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Wishlist\">Wishlist</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Returns\">Returns</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Privacy policy\">Privacy policy</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Site map\">Site map</a></li>\r\n</ul>\r\n</section>'),
            (4, ".$id_lang.", 'Our Policy', '<section class=\"footer-group-link footer-block col-xs-12 col-sm-3 footer-block-wap\" id=\"block_ourpolicy_links_footer\">\r\n<h4 class=\"hidden-sm-down\">Our Policy</h4>\r\n<div class=\"title clearfix hidden-md-up\" data-target=\"#footer_ourpolicy\" data-toggle=\"collapse\"><span class=\"h3\">Our Policy</span> <span class=\"pull-xs-right\"> <span class=\"navbar-toggler collapse-icons\"> <i class=\"material-icons add\"></i> <i class=\"material-icons remove\"></i> </span> </span></div>\r\n<ul class=\"toggle-footer collapse\" id=\"footer_ourpolicy\">\r\n<li class=\"item\"><a href=\"#\" title=\"Help & Contact\">Help & Contact</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Shipping & taxes\">Shipping & taxes</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Return policy\">Return policy</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"About Us\">About Us</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Affiliates\">Affiliates</a></li>\r\n<li class=\"item\"><a href=\"#\" title=\"Legal Notice\">Legal Notice</a></li>\r\n</ul>\r\n</section>'),
            (5, ".$id_lang.", 'Dor Footer Copyright And Info', '<div class=\"footer-copyright-payment clearfix\">\r\n<div class=\"footer-bottom-info-wapper clearfix\">\r\n<div class=\"col-lg-6 col-sm-6 col-sx-12\">Copyright 2017 <a href=\"http://doradothemes.com\" title=\"Dorado Themes\">Organick</a> - All Rights Reserved</div>\r\n<div class=\"col-lg-6 col-sm-6 col-sx-12 pull-right  footer-payment-line\">\r\n<div class=\"payment\">\r\n<ul>\r\n<li><a href=\"#\"><img src=\"".$urlFile."img/cms/dorado/payment/payment-01.png\" alt=\"Organick\" width=\"46\" height=\"28\" /></a></li>\r\n<li><a href=\"#\"><img src=\"".$urlFile."img/cms/dorado/payment/payment-02.png\" alt=\"Organick\" width=\"46\" height=\"28\" /></a></li>\r\n<li><a href=\"#\"><img src=\"".$urlFile."img/cms/dorado/payment/payment-03.png\" alt=\"Organick\" width=\"46\" height=\"28\" /></a></li>\r\n<li><a href=\"#\"><img src=\"".$urlFile."img/cms/dorado/payment/payment-04.png\" alt=\"Organick\" width=\"46\" height=\"28\" /></a></li>\r\n<li><a href=\"#\"><img src=\"".$urlFile."img/cms/dorado/payment/payment-05.png\" alt=\"Organick\" width=\"46\" height=\"28\" /></a></li>\r\n<li><a href=\"#\"><img src=\"".$urlFile."img/cms/dorado/payment/payment-06.png\" alt=\"Organick\" width=\"46\" height=\"28\" /></a></li>\r\n<li><a href=\"#\"><img src=\"".$urlFile."img/cms/dorado/payment/payment-07.png\" alt=\"Organick\" width=\"46\" height=\"28\" /></a></li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>');
            ";
            if ($res)
            $res &=  Db::getInstance()->Execute($sql2);
        }

        $sql3 = "
            INSERT INTO `"._DB_PREFIX_."dor_blockfooter_shop` (`id_dor_blockfooter`, `id_shop`) VALUES
            (1, ".$id_shop."),
            (2, ".$id_shop."),
            (3, ".$id_shop."),
            (4, ".$id_shop."),
            (5, ".$id_shop.")
            ";
        if ($res)
        $res &=  Db::getInstance()->Execute($sql3);
        return (bool)$res;
    }
	
private function uninstallDb() {
    Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'dor_blockfooter`');
    Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'dor_blockfooter_lang`');
    Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'dor_blockfooter_shop`');
    return true;
}

/*  */
      
    public function hookFooter($param) {
        $id_shop = (int)Context::getContext()->shop->id;
        $staticBlocks = $this->_staticModel->getDorBlockFooterLists($id_shop,'displayFooter');
        if(count($staticBlocks)<1) return null;
        $this->smarty->assign(array(
            'staticblocks' => $staticBlocks,
        ));
       return $this->display(__FILE__, 'block_footer.tpl');
    }
    
    
     public function hookDisplayBackOfficeHeader($params) {
	if (method_exists($this->context->controller, 'addJquery'))
	 {        
	  $this->context->controller->addJquery();
	  $this->context->controller->addJS(($this->_path).'js/dorblockfooter.js');
	 }
    }   
    /* define some hook customer */
    public function hookBlockDoradoFooter($param) {
        $id_shop = (int)Context::getContext()->shop->id;
        $staticBlocks = $this->_staticModel->getDorBlockFooterLists($id_shop,'blockDoradoFooter');
        if(count($staticBlocks)<1) return null;
        $this->smarty->assign(array(
            'staticblocks' => $staticBlocks,
        ));
       return $this->display(__FILE__, 'block_footer.tpl');
    } 
    /* define some hook customer */
    public function hookDoradoFooterTop($param) {
        $id_shop = (int)Context::getContext()->shop->id;
        $staticBlocks = $this->_staticModel->getDorBlockFooterLists($id_shop,'doradoFooterTop');
        if(count($staticBlocks)<1) return null;
        $this->smarty->assign(array(
            'staticblocks' => $staticBlocks,
        ));
       return $this->display(__FILE__, 'block_footer.tpl');
    }   
    /* define some hook customer */
    public function hookDoradoFooterAdv($param) {
        $id_shop = (int)Context::getContext()->shop->id;
        $staticBlocks = $this->_staticModel->getDorBlockFooterLists($id_shop,'doradoFooterAdv');
        if(count($staticBlocks)<1) return null;
        $this->smarty->assign(array(
            'staticblocks' => $staticBlocks,
        ));
       return $this->display(__FILE__, 'block_footer.tpl');
    }	
    /* define some hook customer */
	public function hookDoradoFooter1($param) {
        $id_shop = (int)Context::getContext()->shop->id;
        $staticBlocks = $this->_staticModel->getDorBlockFooterLists($id_shop,'doradoFooter1');
        if(count($staticBlocks)<1) return null;
        $this->smarty->assign(array(
            'staticblocks' => $staticBlocks,
        ));
       return $this->display(__FILE__, 'block_footer.tpl');
    }
    
	public function hookDoradoFooter2($param) {
        $id_shop = (int)Context::getContext()->shop->id;
        $staticBlocks = $this->_staticModel->getDorBlockFooterLists($id_shop,'doradoFooter2');
        if(count($staticBlocks)<1) return null;
        $this->smarty->assign(array(
            'staticblocks' => $staticBlocks,
        ));
       return $this->display(__FILE__, 'block_footer.tpl');
    }
    
	public function hookDoradoFooter3($param) {
        $id_shop = (int)Context::getContext()->shop->id;
        $staticBlocks = $this->_staticModel->getDorBlockFooterLists($id_shop,'doradoFooter3');
        if(count($staticBlocks)<1) return null;
        $this->smarty->assign(array(
            'staticblocks' => $staticBlocks,
        ));
       return $this->display(__FILE__, 'block_footer.tpl');
    }
    
    public function hookDoradoFooter4($param) {
        $id_shop = (int)Context::getContext()->shop->id;
        $staticBlocks = $this->_staticModel->getDorBlockFooterLists($id_shop,'doradoFooter4');
        if(count($staticBlocks)<1) return null;
        $this->smarty->assign(array(
            'staticblocks' => $staticBlocks,
        ));
       return $this->display(__FILE__, 'block_footer.tpl');
    }
    public function hookDoradoFooter10($param) {
        $id_shop = (int)Context::getContext()->shop->id;
        $staticBlocks = $this->_staticModel->getDorBlockFooterLists($id_shop,'doradoFooter10');
        if(count($staticBlocks)<1) return null;
        $this->smarty->assign(array(
            'staticblocks' => $staticBlocks,
        ));
       return $this->display(__FILE__, 'block_footer.tpl');
    }
    
    public function DorCacheManagerFooterBlock($hookname){
        $dorCaches  = Tools::getValue('enableDorCache',Configuration::get('enableDorCache'));
        $id_shop = (int)Context::getContext()->shop->id;
        $objCache = new DorCaches(array('name'=>'default','path'=>_PS_ROOT_DIR_.'/override/Dor/Caches/smartcaches/managerfooterblock/','extension'=>'.cache'));
        $fileCache = 'ManagerFooterBlock-Shop'.$id_shop.'-'.$hookname;
        $objCache->setCache($fileCache);
        $cacheData = $objCache->renderData($fileCache);
        $staticBlocks = array();
        if($cacheData && $dorCaches){
            $staticBlocks = $cacheData['lists'];
        }else{
            $staticBlocks = $this->_staticModel->getStaticblockLists($id_shop,$hookname);
            if(count($staticBlocks)<1) return null;
            if($dorCaches){
                $data['lists'] = $staticBlocks;
                $objCache->store($fileCache, $data, $expiration = TIME_CACHE_HOME);
            }
        }
        return $staticBlocks;
    }

    public function getModulById($id_module) {
        return Db::getInstance()->getRow('
            SELECT m.*
            FROM `' . _DB_PREFIX_ . 'module` m
            JOIN `' . _DB_PREFIX_ . 'module_shop` ms ON (m.`id_module` = ms.`id_module` AND ms.`id_shop` = ' . (int) ($this->context->shop->id) . ')
            WHERE m.`id_module` = ' . $id_module);
    }

    public function getHooksByModuleId($id_module) {
        $module = self::getModulById($id_module);
        $moduleInstance = Module::getInstanceByName($module['name']);
        $hooks = array();
        if ($this->hookAssign){
            foreach ($this->hookAssign as $hook) {
                if (_PS_VERSION_ < "1.5") {
                    if (is_callable(array($moduleInstance, 'hook' . $hook))) {
                        $hooks[] = $hook;
                    }
                } else {
                    $retro_hook_name = Hook::getRetroHookName($hook);
                    if (is_callable(array($moduleInstance, 'hook' . $hook)) || is_callable(array($moduleInstance, 'hook' . $retro_hook_name))) {
                        $hooks[] = $retro_hook_name;
                    }
                }
            }
        }
        $results = self::getHookByArrName($hooks);
        return $results;
    }

    public static function getHookByArrName($arrName) {
        $result = Db::getInstance()->ExecuteS('
		SELECT `id_hook`, `name`
		FROM `' . _DB_PREFIX_ . 'hook` 
		WHERE `name` IN (\'' . implode("','", $arrName) . '\')');
        return $result;
    }
    public function getListModuleInstalled() {
        $mod = new Dor_managerblockfooter();
        $modules = $mod->getModulesInstalled(0);
        $arrayModule = array();
        foreach($modules as $key => $module) {
            if($module['active']==1) {
                $arrayModule[0] = array('id_module'=>0, 'name'=>'Chose Module');
                $arrayModule[$key] = $module;
            }
        }
        if ($arrayModule)
            return $arrayModule;
        return array();
    }
	
	private function _installHookCustomer(){
		$hookspos = array(
                'hookBlockDoradoFooter',
                'doradoFooterTop',
                'doradoFooterAdv',
				'doradoFooter1',
				'doradoFooter2',
				'doradoFooter3',
                'doradoFooter4',
                'doradoFooter5',
                'doradoFooter6',
                'doradoFooter7',
                'doradoFooter8',
                'doradoFooter9',
				'doradoFooter10',
			); 
		foreach( $hookspos as $hook ){
			if( Hook::getIdByName($hook) ){
				
			} else {
				$new_hook = new Hook();
				$new_hook->name = pSQL($hook);
				$new_hook->title = pSQL($hook);
				$new_hook->add();
				$id_hook = $new_hook->id;
			}
		}
		return true;
	}
    public function renderWidget($hookName, array $params)
    {
        return $this->display(__FILE__, 'block_footer.tpl');
    }

    public function getWidgetVariables($hookName, array $params)
    {
        
    }

}