<?php
if (!defined('_PS_VERSION_'))
	exit;

class Dorpages extends Module
{
	public function __construct()
	{
		$this->name = 'dorpages';
		$this->tab = 'front_office_features';
		$this->version = '2.0.0';
		$this->author = 'Dorado Themes';
		$this->controllers = array('aboutus','aboutus2','dorcategory','dorpage1','dorpage2','dorpage3','dorpage4','dorpage5');
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Dor Page Custom HTML');
		$this->description = $this->l('Dor Page Custom HTML');
		$this->confirmUninstall = $this->l('Are you sure about removing these details?');
	}

	public function install()
	{
		if (!parent::install() ||
			!$this->registerHook('backOfficeHeader'))
			return false;
		return true;
	}
	public function uninstall()
	{
		if (!$this->unregisterHook('backOfficeHeader') ||
			!parent::uninstall())
			return false;
		return true;
	}
	public function hookBackOfficeHeader()
	{
		if(!Tools::getIsset('configure') || Tools::getValue('configure') != $this->name)
			return;

		$this->context->controller->addCSS(array());
		$this->context->controller->addJquery();
		$this->context->controller->addJS(array());
	}
	public function getContent()
	{
		$this->context->smarty->assign(array());
		return $this->display(__FILE__, 'views/templates/admin/configure.tpl');
	}
}
