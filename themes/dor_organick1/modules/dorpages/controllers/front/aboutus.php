<?php
class dorpagesAboutusModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
	}
	
	public function initContent()
	{
		parent::initContent();
		$this->context->smarty->assign(array());
		$this->context->smarty->assign(array('page_name'=>"dor-about-us"));
		$this->setTemplate('dorpage/aboutus.tpl');
	}
	public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();
     
        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('About us', [], 'Breadcrumb'),
            'url' => ""
         ];
         return $breadcrumb;
     }
	public function setMedia()
    {
        parent::setMedia();
        $this->addjQuery();
    }
}