<?php
class dorpagesDorpage3ModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
	}
	
	public function initContent()
	{
		parent::initContent();
		$this->context->smarty->assign(array());
		$this->context->smarty->assign(array('page_name'=>"dor-page3"));
		$this->setTemplate('dorpages/dorpage3.tpl');
	}
	public function setMedia()
    {
        parent::setMedia();
        $this->addjQuery();
    }
}
