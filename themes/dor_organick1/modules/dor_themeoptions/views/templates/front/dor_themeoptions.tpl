<style type="text/css">
	/****Top Nav***/
	{if $dorTopbarColorText !=''}
        #dor-topbar01 .topbar-infomation span, #dor-topbar01 .line-selected,
        #dor-topbar01 .topbar-infomation-right ul li span,
        #dor-topbar01 .topbar-infomation i, #dor-topbar01 .line-selected i, 
        #dor-topbar01 .topbar-infomation-right i,
        #dor-topbar01 .topbar-infomation li span::after, 
        #dor-topbar01 .topbar-infomation-right ul li span::after {
            color:{$dorTopbarColorText} !important;
        }
        #dor-topbar01 .topbar-infomation li span::after, 
        #dor-topbar01 .topbar-infomation-right ul li span::after{
            background-color: {$dorTopbarColorText} !important;
        }
    {/if}
    {if $dorTopbarBgOutside !=''}
    #dor-topbar, .dor-topbar-wrapper, .dor-topbar-inner{
    	background:{$dorTopbarBgOutside} !important;
    }
    {/if}
    {if $dorTopbarBgColor !=''}
    .dor-topbar-inner .container{
    	background:{$dorTopbarBgColor};
    }
    {/if}
    {if $dorTopbarColorLink !=''}
    #dor-topbar a{
    	color:{$dorTopbarColorLink} !important;
    }
    {/if}
    {if $dorTopbarColorLinkHover !=''}
    #dor-topbar a:hover{
    	color:{$dorTopbarColorLinkHover} !important;
    }
    {/if}
    /***Header****/
	{if $dorHeaderBgOutside !=''}
        body header#header,
        header#header.fixed.fixed-tran{
            background-color:{$dorHeaderBgOutside} !important;
        }
    {/if}
	{if $dorHeaderBgColor !=''}
        body header#header .container,
        header#header.fixed.fixed-tran .container{
            background-color:{$dorHeaderBgColor} !important;
        }
    {/if}

    {if $dorHeaderColorText !=''}
        body header#header .menu-group-show div, 
        body header#header .menu-group-show div span, 
        body header#header .menu-group-show div p{
            color:{$dorHeaderColorText} !important;
        }
    {/if}
	{if $dorHeaderColorLink !=''}
        body header#header a{
            color:{$dorHeaderColorLink} !important;
        }
    {/if}
    {if $dorHeaderColorLinkHover !=''}
        body header#header a:hover,
        body header#header a:hover span,
        body header#header a:hover p{
            color:{$dorHeaderColorLinkHover} !important;
        }
    {/if}
    {if $dorHeaderColorIcon !=''}
        header#header i{
            color:{$dorHeaderColorIcon} !important;
        }
    {/if}
	{if $dorHeaderColorIconHover !=''}
        body header#header.header-absolute button[type="button"]:hover i,
        body header#header.header-absolute i:hover{
            color:{$dorHeaderColorIconHover} !important;
        }
    {/if}
	
	/****Footer****/
	{if $dorFooterBgOutside !=''}
    body #footer,
    body #footer .doradoFooterAdv,
	body #footer .footer-container,
    body #footer .footer-copyright-payment{
		background:{$dorFooterBgOutside} !important;
	}
	{/if}
	{if $dorFooterBgColor !=''}
	body #footer .footer-container .container{
		background:{$dorFooterBgColor} !important;
	}
	{/if}
	{if $dorFooterColorText !=''}
	body #footer .footer-container .container,
	body #footer .footer-container .container div,
	body #footer .footer-container .container span,
    body #footer .footer-container .container section,
    body #footer .footer-container .container h4,
    body #footer .footer-container .container h3,
    body #footer .footer-container .container h3.h3,
    body #footer .footer-container .container h5,
    body #footer .footer-container .container strong,
    body #footer .footer-container .container h3 > a.text-uppercase,
	body #footer .footer-container .container i{
		color:{$dorFooterColorText} !important;
	}
	{/if}
	{if $dorFooterColorLink !=''}
	body #footer .footer-container .container a,
    body #footer .footer-container .container a span,
    body #footer .footer-container .container a > i,
    body #footer .footer-container .container a > em,
	body #footer .footer-container .container div a{
		color:{$dorFooterColorLink} !important;
	}
	{/if}
	{if $dorFooterColorLinkHover !=''}
	body #footer .footer-container .container a:hover,
	body #footer .footer-container .container div a:hover,
    body #footer .footer-container .container a:hover span,
    body #footer .footer-container .container div a:hover span{
		color:{$dorFooterColorLinkHover} !important;
	}
	{/if}


    /*****Mega Menu*****/
    {if $dorMegamenuBgOutside !=''}
        body #header .header-top .dor-header-menu,
        body #header .menu-group-show > .header-megamenu{
            background:{$dorMegamenuBgOutside} !important;
        }
    {/if}
    {if $dorMegamenuBgColor !=''}
        body #header .header-top .dor-megamenu .navbar-nav,
        body #header .dor-megamenu .navbar-nav,
        body header#header.fixed.fixed-tran .menu-group-show .container,
        body header#header.fixed.fixed-tran .menu-group-show{
            background:{$dorMegamenuBgColor} !important;
        }
    {/if}
    {if $dorMegamenuColorLink !=''}
        body #header .dor-megamenu ul.navbar-nav > li > a span.menu-title{
            color:{$dorMegamenuColorLink} !important;
        }
    {/if}
    {if $dorMegamenuColorLinkHover !=''}
        body #header .dor-megamenu ul.navbar-nav > li.active > a span.menu-title,
        body #header .dor-megamenu ul.navbar-nav > li > a:hover span.menu-title{
            color:{$dorMegamenuColorLinkHover} !important;
        }
    {/if}
    {if $dorMegamenuColorSubText !=''}
        body .dor-megamenu #dor-top-menu .dropdown-menu,
        body .dor-megamenu #dor-top-menu .dropdown-menu div,
        body .dor-megamenu #dor-top-menu .dropdown-menu ul li{
            color:{$dorMegamenuColorSubText} !important;
        }
    {/if}
    {if $dorMegamenuColorSubLink !=''}
        body .dor-megamenu #dor-top-menu .dropdown-menu a,
        body .dor-megamenu #dor-top-menu .dropdown-menu a span{
            color:{$dorMegamenuColorSubLink} !important;
        }
    {/if}
    {if $dorMegamenuColorSubLinkHover !=''}
        body .dor-megamenu #dor-top-menu .dropdown-menu a:hover,
        body .dor-megamenu #dor-top-menu .dropdown-menu a:hover span{
            color:{$dorMegamenuColorSubLinkHover} !important;
        }
        body .dor-megamenu #dor-top-menu .dropdown-menu li > a:hover{
            border-bottom-color: {$dorMegamenuColorSubLinkHover} !important;
        }
    {/if}

    {if $dorVermenuBgOutside !=''}
        body #dor-verticalmenu .dor-vertical-title{
            background-color:{$dorVermenuBgOutside} !important;
        }
    {/if}

    {if $dorVermenuBgColor !=''}
        body #dor-verticalmenu .dor-verticalmenu{
            background-color:{$dorVermenuBgColor} !important;
        }
    {/if}

    {if $dorVermenuColorText !=''}
        body #dor-verticalmenu .dor-vertical-title h4,
        body #dor-verticalmenu .dor-vertical-title h4 span,
        body header#header #dor-verticalmenu .dor-vertical-title h4,
        body header#header #dor-verticalmenu .dor-vertical-title h4 span,
        body .fa-icon-menu > i{
            color:{$dorVermenuColorText} !important;
        }
    {/if}

    {if $dorVermenuColorLink !=''}
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu > li > a > span.menu-title,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu > li > a > span.menu-icon i{
            color:{$dorVermenuColorLink} !important;
        }
    {/if}
    {if $dorVermenuColorLinkHover !=''}
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu > li > a:hover > span.menu-title,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu > li > a:hover > span.menu-icon i{
            color:{$dorVermenuColorLinkHover} !important;
        }
    {/if}
    {if $dorVermenuColorSubText !=''}
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu div,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu .widget-content,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu .widget-content div{
            color:{$dorVermenuColorSubText} !important;
        }
    {/if}
    {if $dorVermenuColorSubLink !=''}
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu > li > a,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu ul li a,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu a > span,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu .widget-content .widget-heading a{
            color:{$dorVermenuColorSubLink} !important;
        }
    {/if}
    {if $dorVermenuColorSubLinkHover !=''}
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu > li > a:hover,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu ul li a:hover,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu a:hover > span,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu .widget-content .widget-heading a:hover,
        body #dor-verticalmenu .dor-verticalmenu .nav.navbar-nav.verticalmenu .dropdown-menu .widget-content .widget-heading a:hover span{
            color:{$dorVermenuColorSubLinkHover} !important;
        }
    {/if}
    /*****End Mega Menu*****/


    {if isset($dorPriceColor) && $dorPriceColor != ""}
        #products-viewed .product-price-and-shipping span.price, .products .product-price-and-shipping span.price, .product_list .product-price-and-shipping span.price,
        #dor-tab-product-category2 .product-price-and-shipping > span.price {
            color: {$dorPriceColor} !important;
        }
    {/if}

    {if isset($dorPricePrimaryColor) && $dorPricePrimaryColor != ""}
        #products-viewed .product-price-and-shipping > span:nth-child(3).price, .product_list .product-price-and-shipping > span:nth-child(3).price, .products .product-price-and-shipping > span:nth-child(3).price, #dor-tab-product-category2 .product-price-and-shipping > span:nth-child(3).price {
            color: {$dorPricePrimaryColor} !important;
        }
    {/if}

    {if isset($dorOldPriceColor) && $dorOldPriceColor != ""}
        .regular-price {
            color: {$dorOldPriceColor} !important;
        }
    {/if}
    {if isset($dorFlagSaleBg) && $dorFlagSaleBg != ""}
        .quickview .product-flags > li.product-flag.on-sale::before, .sale-box.box-status::before, #content .product-flags > li.product-flag.on-sale::before {
            border-color: transparent {$dorFlagSaleBg} transparent transparent;
        }
        .quickview .product-flags > li.product-flag.on-sale, .sale-box.box-status, #content .product-flags > li.product-flag.on-sale {
            background-color: {$dorFlagSaleBg};
        }
    {/if}
    {if isset($dorFlagSaleColor) && $dorFlagSaleColor != ""}
        
        .quickview .product-flags > li.product-flag.on-sale, .sale-box.box-status, #content .product-flags > li.product-flag.on-sale {
            color: {$dorFlagSaleColor} !important;
        }
    {/if}
    {if isset($dorFlagNewBg) && $dorFlagNewBg != ""}
        .box-status::before, .product-tabs-content a::before, #content .product-flags > li.product-flag::before, .quickview .product-flags > li.product-flag::before{
            border-color: transparent {$dorFlagNewBg} transparent transparent;
        }
        .quickview .product-flags > li.product-flag, .box-status, .product-tabs-content a, #content .product-flags > li.product-flag{
            background-color: {$dorFlagNewBg};
        }
        #product #content-wrapper #content .product-flags > li.product-flag.new::before {
            border-color: transparent {$dorFlagNewBg} transparent transparent !important;
        }
        #product #content .product-flags > li.product-flag.new{
            background-color: {$dorFlagNewBg} !important;
        }
    {/if}
    {if isset($dorFlagNewColor) && $dorFlagNewColor != ""}
    .quickview .product-flags > li.product-flag, .box-status, .product-tabs-content a, #content .product-flags > li.product-flag{
            color: {$dorFlagNewColor} !important;
        }
    {/if}
</style>