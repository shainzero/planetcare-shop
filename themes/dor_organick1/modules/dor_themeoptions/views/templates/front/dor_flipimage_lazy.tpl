<a href="{$product.url}" class="thumbnail product-thumbnail product_img_link">
  <img
    class = "img-responsive thumbnail-image-1 dorlazy"
    data-original="{$product.cover.bySize.home_default.url}"
    src = ""
    alt = "{$product.cover.legend}"
    data-full-size-image-url = "{$product.cover.large.url}"
  >
  {if isset($product.flip) && $product.flip}
  <img
    class = "img-responsive thumbnail-image-2 dorlazy"
    data-original="{$product.flip.bySize.home_default.url}"
    src = ""
    alt = "{$product.flip.legend}"
    data-full-size-image-url = "{$product.flip.large.url}"
  >
  {/if}
</a>