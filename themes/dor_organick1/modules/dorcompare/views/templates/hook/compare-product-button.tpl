<div class="compare compare-product-button">
	<a class="add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id="product-button" data-productid="{$product.id_product}" data-toggle="tooltip" title="" data-original-title="{l s='Add compare'}"><i class="pe-7s-shuffle"></i><span class="compare-button-txt hidden">{l s='Add to compare'}</span></a>
</div>