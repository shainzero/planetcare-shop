var DORTESTIMONIAL = {
	init:function(){
		DORTESTIMONIAL.TestimonialCarousel();
	},
	TestimonialCarousel:function(){
		$('.testimonials-slide').owlCarousel({
	        items: 1,
	        loop: true,
	        nav: true,
	        autoplay: false,
	        margin:0,
	        responsive: {
	            0: {items: 1},
	            1200: {items: 1},
	            992: {items: 1},
	            650: {items: 1},
	            370: {items: 1},
	            300: {items: 1}
	        },
	        navText: ["<i class='fa fa-long-arrow-left'></i>", "<i class='fa fa-long-arrow-right'></i>"]
	    });
	}
}

jQuery(document).ready(function(){
	DORTESTIMONIAL.init();
});