<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

//require(dirname(__FILE__).'/menutoplinks.class.php');
if (!defined('_PS_VERSION_'))
    exit;
if (!class_exists( 'DorCaches' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/Caches/DorCaches.php');
}
define('_DORMEGAMENU_MCRYPT_KEY_', md5('key_dormegamenu'));
define('_DORMEGAMENU_MCRYPT_IV_', md5('iv_dormegamenu'));

include_once(_PS_MODULE_DIR_.'dor_megamenu/classes/DorMegamenuMcrypt.php');
include_once(_PS_MODULE_DIR_.'dor_megamenu/classes/DorMegamenuHelper.php');
include_once(_PS_MODULE_DIR_.'dor_megamenu/classes/DorMegamenu.php');
include_once(_PS_MODULE_DIR_.'dor_megamenu/classes/DorMegamenuWidget.php');
include_once(_PS_MODULE_DIR_.'dor_megamenu/classes/DorMegamenuWidgetBase.php');
class Dor_MegaMenu extends Module
{
    const MENU_JSON_CACHE_KEY = 'MOD_BLOCKTOPMENU_MENU_JSON';

    protected $_menu = '';
    protected $_html = '';
    protected $user_groups;

    /*
     * Pattern for matching config values
     */
    protected $pattern = '/^([A-Z_]*)[0-9]+/';

    /*
     * Name of the controller
     * Used to set item selected or not in top menu
     */
    protected $page_name = '';

    /*
     * Spaces per depth in BO
     */
    protected $spacer_size = '5';

    public function __construct()
    {
        $this->name = 'dor_megamenu';
        $this->tab = 'front_office_features';
        $this->version = '2.0.0';
        $this->author = 'Dorado Themes';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->secure_key = Tools::encrypt($this->name);
        $this->prefix = 'dormm_';
        parent::__construct();
        $this->displayName = $this->l('Dor Megamenu');
        $this->description = $this->l('Manager and display megamenu use bootstrap framework');
        $this->templateFile = 'module:dor_megamenu/views/templates/hook/megamenu.tpl';
    }

    public function install()
    {
        // Install Tabs
        if(!(int)Tab::getIdFromClassName('AdminDorMenu')) {
            $parent_tab = new Tab();
            // Need a foreach for the language
            $parent_tab->name[$this->context->language->id] = $this->l('Dor Extensions');
            $parent_tab->class_name = 'AdminDorMenu';
            $parent_tab->id_parent = 0; // Home tab
            $parent_tab->module = $this->name;
            $parent_tab->add();
        }
        $tab = new Tab();
        foreach (Language::getLanguages() as $language)
        $tab->name[$language['id_lang']] = $this->l('Dor Megamenu');
        $tab->class_name = 'AdminDorMegamenu';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminDorMenu'); 
        $tab->module = $this->name;
        $tab->add();

        if (!parent::install() || !$this->registerHook('actionAdminControllerSetMedia')
            || !$this->registerHook('displayTop') || !$this->registerHook('dormegamenu') || !$this->registerHook('displayHeader') ||

            !$this->registerHook('actionObjectCategoryUpdateAfter') ||
            !$this->registerHook('actionObjectCategoryDeleteAfter') ||
            !$this->registerHook('actionObjectCategoryAddAfter') ||

            !$this->registerHook('actionObjectCmsUpdateAfter') ||
            !$this->registerHook('actionObjectCmsDeleteAfter') ||
            !$this->registerHook('actionObjectCmsAddAfter') ||

            !$this->registerHook('actionObjectSupplierUpdateAfter') ||
            !$this->registerHook('actionObjectSupplierDeleteAfter') ||
            !$this->registerHook('actionObjectSupplierAddAfter') ||

            !$this->registerHook('actionObjectManufacturerUpdateAfter') ||
            !$this->registerHook('actionObjectManufacturerDeleteAfter') ||
            !$this->registerHook('actionObjectManufacturerAddAfter') ||

            !$this->registerHook('actionObjectProductUpdateAfter') ||
            !$this->registerHook('actionObjectProductDeleteAfter') ||
            !$this->registerHook('actionObjectProductAddAfter') ||

            !$this->registerHook('categoryUpdate') ||
            !$this->registerHook('actionShopDataDuplication') ||
            !$this->registerHook('actionObjectLanguageAddAfter')
            ) {
            return false;
        }
        include_once (_PS_MODULE_DIR_.'dor_megamenu/install/install.php');
        $id_shop = (int)Context::getContext()->shop->id;
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'https://' : 'http://';
        $urlFile = Tools::htmlentitiesutf8($protocol.$_SERVER['HTTP_HOST'].__PS_BASE_URI__);
        $res = "";
        $sql1 =  "
            INSERT INTO `"._DB_PREFIX_."dormegamenu` (`id_dormegamenu`, `active`, `type`, `value`, `id_parent`, `position`, `level_depth`, `date_add`, `date_upd`, `params`) VALUES
            (1, 1, 'category', '1', NULL, NULL, NULL, NULL, NULL, NULL),
            (2, 1, 'custom-link', '', 1, 0, 1, '2017-11-30 23:34:58', '2017-11-30 23:34:58', ''),
            (3, 1, 'custom-link', '', 1, 1, 1, '2017-11-30 23:35:05', '2017-12-12 01:03:03', 'a:13:{s:14:\"id_dormegamenu\";s:1:\"3\";s:12:\"column_width\";s:2:\"12\";s:11:\"widget_list\";s:10:\"1512903036\";s:6:\"target\";s:5:\"_self\";s:12:\"sticky_lable\";s:0:\"\";s:10:\"icon_class\";s:0:\"\";s:14:\"addition_class\";s:0:\"\";s:15:\"menu_background\";s:0:\"\";s:13:\"submenu_align\";s:4:\"left\";s:13:\"submenu_width\";s:3:\"240\";s:6:\"action\";s:13:\"updateSubMenu\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}'),
            (4, 1, 'custom-link', '', 1, 3, 1, '2017-11-30 23:35:10', '2017-12-13 06:13:13', 'a:13:{s:14:\"id_dormegamenu\";s:1:\"4\";s:12:\"column_width\";s:2:\"12\";s:11:\"widget_list\";s:10:\"1512903036\";s:6:\"target\";s:5:\"_self\";s:12:\"sticky_lable\";s:0:\"\";s:10:\"icon_class\";s:0:\"\";s:14:\"addition_class\";s:0:\"\";s:15:\"menu_background\";s:0:\"\";s:13:\"submenu_align\";s:4:\"left\";s:13:\"submenu_width\";s:3:\"240\";s:6:\"action\";s:13:\"updateSubMenu\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}'),
            (5, 1, 'category', '3', 1, 2, 1, '2017-11-30 23:35:37', '2017-12-11 05:52:06', 'a:14:{s:14:\"id_dormegamenu\";s:1:\"5\";s:12:\"column_width\";s:1:\"3\";s:11:\"widget_list\";s:10:\"1512989516\";s:6:\"target\";s:5:\"_self\";s:12:\"sticky_lable\";s:0:\"\";s:10:\"icon_class\";s:0:\"\";s:14:\"addition_class\";s:0:\"\";s:15:\"menu_background\";s:0:\"\";s:13:\"submenu_align\";s:9:\"fullwidth\";s:13:\"submenu_width\";s:3:\"300\";s:6:\"params\";s:191:\"{\"rows\":[{\"cols\":[{\"col_width\":\"3\",\"widget_key\":1512975212},{\"col_width\":\"3\",\"widget_key\":1512975467},{\"col_width\":\"3\",\"widget_key\":1512989051},{\"col_width\":\"3\",\"widget_key\":\"1512989516\"}]}]}\";s:6:\"action\";s:13:\"updateSubMenu\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}'),
            (6, 1, 'custom-link', '', 1, 4, 1, '2017-11-30 23:36:27', '2017-12-10 05:50:53', 'a:14:{s:14:\"id_dormegamenu\";s:1:\"6\";s:12:\"column_width\";s:2:\"12\";s:11:\"widget_list\";s:10:\"1512903036\";s:6:\"target\";s:5:\"_self\";s:12:\"sticky_lable\";s:0:\"\";s:10:\"icon_class\";s:0:\"\";s:14:\"addition_class\";s:0:\"\";s:15:\"menu_background\";s:0:\"\";s:13:\"submenu_align\";s:4:\"left\";s:13:\"submenu_width\";s:3:\"240\";s:6:\"params\";s:66:\"{\"rows\":[{\"cols\":[{\"col_width\":\"12\",\"widget_key\":\"1512903036\"}]}]}\";s:6:\"action\";s:13:\"updateSubMenu\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}'),
            (7, 1, 'custom-link', '', 1, 5, 1, '2017-11-30 23:36:36', '2017-12-10 09:34:20', 'a:14:{s:14:\"id_dormegamenu\";s:1:\"7\";s:12:\"column_width\";s:2:\"12\";s:11:\"widget_list\";s:10:\"1512916446\";s:6:\"target\";s:5:\"_self\";s:12:\"sticky_lable\";s:0:\"\";s:10:\"icon_class\";s:0:\"\";s:14:\"addition_class\";s:0:\"\";s:15:\"menu_background\";s:0:\"\";s:13:\"submenu_align\";s:4:\"left\";s:13:\"submenu_width\";s:3:\"240\";s:6:\"params\";s:66:\"{\"rows\":[{\"cols\":[{\"col_width\":\"12\",\"widget_key\":\"1512916446\"}]}]}\";s:6:\"action\";s:13:\"updateSubMenu\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}'),
            (8, 1, 'custom-link', '', 3, 0, 1, '2017-12-12 01:00:17', '2017-12-12 01:00:17', ''),
            (9, 1, 'custom-link', '', 3, 1, 2, '2017-12-12 01:00:43', '2017-12-12 01:02:34', 'a:11:{s:14:\"id_dormegamenu\";s:1:\"9\";s:6:\"target\";s:5:\"_self\";s:12:\"sticky_lable\";s:0:\"\";s:10:\"icon_class\";s:0:\"\";s:14:\"addition_class\";s:0:\"\";s:15:\"menu_background\";s:0:\"\";s:13:\"submenu_align\";s:4:\"left\";s:13:\"submenu_width\";s:3:\"240\";s:6:\"action\";s:13:\"updateSubMenu\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}'),
            (10, 1, 'custom-link', '', 9, 0, 1, '2017-12-12 01:00:55', '2017-12-12 01:00:55', ''),
            (11, 1, 'custom-link', '', 9, 1, 3, '2017-12-12 01:01:11', '2017-12-12 01:04:23', ''),
            (12, 1, 'custom-link', '', 9, 2, 3, '2017-12-12 01:01:30', '2017-12-12 01:04:03', ''),
            (13, 1, 'custom-link', '', 9, 3, 1, '2017-12-12 01:02:02', '2017-12-12 01:02:02', ''),
            (14, 1, 'custom-link', '', 3, 2, 1, '2017-12-12 01:03:29', '2017-12-12 01:03:29', ''),
            (15, 1, 'custom-link', '', 3, 3, 1, '2017-12-12 01:05:10', '2017-12-12 01:05:10', ''),
            (16, 1, 'custom-link', '', 3, 4, 1, '2017-12-12 01:05:37', '2017-12-12 01:05:37', ''),
            (17, 1, 'custom-link', '', 4, 0, 1, '2017-12-13 06:12:31', '2017-12-13 06:12:31', ''),
            (18, 1, 'custom-link', '', 4, 1, 1, '2017-12-13 06:12:47', '2017-12-13 06:12:47', '');
        ";
        
        Db::getInstance()->Execute($sql1);


        foreach (Language::getLanguages() as $language){
            $id_lang = $language['id_lang'];
            $sql2="
                INSERT INTO `"._DB_PREFIX_."dormegamenu_lang` (`id_dormegamenu`, `id_lang`, `name`, `description`, `url`) VALUES
                (1, ".$id_lang.", 'Root', NULL, NULL),
                (2, ".$id_lang.", 'Home', '', '#/home'),
                (3, ".$id_lang.", 'Pages', '', '#'),
                (4, ".$id_lang.", 'Galleries', '', '#/gallery'),
                (5, ".$id_lang.", 'Shop', '', ''),
                (6, ".$id_lang.", 'Blog', '', '#/blogs.html'),
                (7, ".$id_lang.", 'Contact', '', '#/contact-us'),
                (8, ".$id_lang.", 'Gallery', '', '#/gallery'),
                (9, ".$id_lang.", 'About Us', '', '#/about-us'),
                (10, ".$id_lang.", 'About Us V1', '', '#/about-us'),
                (11, ".$id_lang.", 'About Us V2', '', '#/content/4-about-us-v2'),
                (12, ".$id_lang.", 'About Us V3', '', '#/content/6-about-us-v3'),
                (13, ".$id_lang.", 'About Us V4', '', '#/content/7-about-us-v4'),
                (14, ".$id_lang.", 'Not Page', '', '#/404.html'),
                (15, ".$id_lang.", 'Under Construction', '', '#/under-const.html'),
                (16, ".$id_lang.", 'Track Your Order', '', '#/order-tracking'),
                (17, ".$id_lang.", 'Gallery V1', '', '#/gallery?type=1'),
                (18, ".$id_lang.", 'Gallery V2', '', '#/gallery?type=2');
            ";
            Db::getInstance()->Execute($sql2);
        }
        $sql3 = "
            INSERT INTO `"._DB_PREFIX_."dormegamenu_shop` (`id_dormegamenu`, `id_shop`) VALUES
            (1, ".$id_shop."),
            (2, ".$id_shop."),
            (3, ".$id_shop."),
            (4, ".$id_shop."),
            (5, ".$id_shop."),
            (6, ".$id_shop."),
            (7, ".$id_shop."),
            (8, ".$id_shop."),
            (9, ".$id_shop."),
            (10, ".$id_shop."),
            (11, ".$id_shop."),
            (12, ".$id_shop."),
            (13, ".$id_shop."),
            (14, ".$id_shop."),
            (15, ".$id_shop."),
            (16, ".$id_shop."),
            (17, ".$id_shop."),
            (18, ".$id_shop.");
        ";
        Db::getInstance()->Execute($sql3);

        $sql4 = "
            INSERT INTO `"._DB_PREFIX_."dormegamenu_widget` (`id_dormegamenu_widget`, `name`, `type`, `params`, `id_shop`, `wkey`) VALUES
                (1, 'Blogs Lists', 'links', 'a:83:{s:15:\"savedormegamenu\";s:1:\"1\";s:21:\"id_dormegamenu_widget\";s:1:\"1\";s:4:\"type\";s:5:\"links\";s:4:\"name\";s:11:\"Blogs Lists\";s:14:\"widget_title_1\";s:0:\"\";s:14:\"widget_title_3\";s:0:\"\";s:14:\"widget_title_6\";s:0:\"\";s:14:\"widget_title_7\";s:0:\"\";s:14:\"widget_title_8\";s:0:\"\";s:13:\"text_link_1_1\";s:13:\"Blog Style V1\";s:13:\"text_link_1_3\";s:13:\"Blog Style V1\";s:13:\"text_link_1_6\";s:13:\"Blog Style V1\";s:13:\"text_link_1_7\";s:13:\"Blog Style V1\";s:13:\"text_link_1_8\";s:13:\"Blog Style V1\";s:6:\"link_1\";s:80:\"".$urlFile."blogs.html?type=1\";s:11:\"icon_link_1\";s:0:\"\";s:13:\"text_link_2_1\";s:13:\"Blog Style V2\";s:13:\"text_link_2_3\";s:13:\"Blog Style V2\";s:13:\"text_link_2_6\";s:13:\"Blog Style V2\";s:13:\"text_link_2_7\";s:13:\"Blog Style V2\";s:13:\"text_link_2_8\";s:13:\"Blog Style V2\";s:6:\"link_2\";s:80:\"".$urlFile."blogs.html?type=2\";s:11:\"icon_link_2\";s:0:\"\";s:13:\"text_link_3_1\";s:13:\"Blog Style V3\";s:13:\"text_link_3_3\";s:13:\"Blog Style V3\";s:13:\"text_link_3_6\";s:13:\"Blog Style V3\";s:13:\"text_link_3_7\";s:13:\"Blog Style V3\";s:13:\"text_link_3_8\";s:13:\"Blog Style V3\";s:6:\"link_3\";s:80:\"".$urlFile."blogs.html?type=3\";s:11:\"icon_link_3\";s:0:\"\";s:13:\"text_link_4_1\";s:17:\"Blog Masonry Grid\";s:13:\"text_link_4_3\";s:17:\"Blog Masonry Grid\";s:13:\"text_link_4_6\";s:17:\"Blog Masonry Grid\";s:13:\"text_link_4_7\";s:17:\"Blog Masonry Grid\";s:13:\"text_link_4_8\";s:17:\"Blog Masonry Grid\";s:6:\"link_4\";s:80:\"".$urlFile."blogs.html?type=4\";s:11:\"icon_link_4\";s:0:\"\";s:13:\"text_link_5_1\";s:0:\"\";s:13:\"text_link_5_3\";s:6:\"link 5\";s:13:\"text_link_5_6\";s:6:\"link 5\";s:13:\"text_link_5_7\";s:6:\"link 5\";s:13:\"text_link_5_8\";s:6:\"link 5\";s:6:\"link_5\";s:0:\"\";s:11:\"icon_link_5\";s:0:\"\";s:13:\"text_link_6_1\";s:0:\"\";s:13:\"text_link_6_3\";s:6:\"link 6\";s:13:\"text_link_6_6\";s:6:\"link 6\";s:13:\"text_link_6_7\";s:6:\"link 6\";s:13:\"text_link_6_8\";s:6:\"link 6\";s:6:\"link_6\";s:0:\"\";s:11:\"icon_link_6\";s:0:\"\";s:13:\"text_link_7_1\";s:0:\"\";s:13:\"text_link_7_3\";s:6:\"link 7\";s:13:\"text_link_7_6\";s:6:\"link 7\";s:13:\"text_link_7_7\";s:6:\"link 7\";s:13:\"text_link_7_8\";s:6:\"link 7\";s:6:\"link_7\";s:0:\"\";s:11:\"icon_link_7\";s:0:\"\";s:13:\"text_link_8_1\";s:0:\"\";s:13:\"text_link_8_3\";s:6:\"link 8\";s:13:\"text_link_8_6\";s:6:\"link 8\";s:13:\"text_link_8_7\";s:6:\"link 8\";s:13:\"text_link_8_8\";s:6:\"link 8\";s:6:\"link_8\";s:0:\"\";s:11:\"icon_link_8\";s:0:\"\";s:13:\"text_link_9_1\";s:0:\"\";s:13:\"text_link_9_3\";s:6:\"link 9\";s:13:\"text_link_9_6\";s:6:\"link 9\";s:13:\"text_link_9_7\";s:6:\"link 9\";s:13:\"text_link_9_8\";s:6:\"link 9\";s:6:\"link_9\";s:0:\"\";s:11:\"icon_link_9\";s:0:\"\";s:14:\"text_link_10_1\";s:0:\"\";s:14:\"text_link_10_3\";s:7:\"link 10\";s:14:\"text_link_10_6\";s:7:\"link 10\";s:14:\"text_link_10_7\";s:7:\"link 10\";s:14:\"text_link_10_8\";s:7:\"link 10\";s:7:\"link_10\";s:0:\"\";s:12:\"icon_link_10\";s:0:\"\";s:12:\"additionclss\";s:0:\"\";s:6:\"action\";s:9:\"addWidget\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}', ".$id_shop.", '1512903036'),
                (2, 'Contact Lists', 'links', 'a:83:{s:15:\"savedormegamenu\";s:1:\"1\";s:21:\"id_dormegamenu_widget\";s:1:\"0\";s:4:\"type\";s:5:\"links\";s:4:\"name\";s:13:\"Contact Lists\";s:14:\"widget_title_1\";s:0:\"\";s:14:\"widget_title_3\";s:0:\"\";s:14:\"widget_title_6\";s:0:\"\";s:14:\"widget_title_7\";s:0:\"\";s:14:\"widget_title_8\";s:0:\"\";s:13:\"text_link_1_1\";s:10:\"Contact V1\";s:13:\"text_link_1_3\";s:10:\"Contact V1\";s:13:\"text_link_1_6\";s:10:\"Contact V1\";s:13:\"text_link_1_7\";s:10:\"Contact V1\";s:13:\"text_link_1_8\";s:10:\"Contact V1\";s:6:\"link_1\";s:77:\"".$urlFile."contact-us?v=1\";s:11:\"icon_link_1\";s:0:\"\";s:13:\"text_link_2_1\";s:10:\"Contact V2\";s:13:\"text_link_2_3\";s:10:\"Contact V2\";s:13:\"text_link_2_6\";s:10:\"Contact V2\";s:13:\"text_link_2_7\";s:10:\"Contact V2\";s:13:\"text_link_2_8\";s:10:\"Contact V2\";s:6:\"link_2\";s:77:\"".$urlFile."contact-us?v=2\";s:11:\"icon_link_2\";s:0:\"\";s:13:\"text_link_3_1\";s:10:\"Contact V3\";s:13:\"text_link_3_3\";s:10:\"Contact V3\";s:13:\"text_link_3_6\";s:10:\"Contact V3\";s:13:\"text_link_3_7\";s:10:\"Contact V3\";s:13:\"text_link_3_8\";s:10:\"Contact V3\";s:6:\"link_3\";s:77:\"".$urlFile."contact-us?v=3\";s:11:\"icon_link_3\";s:0:\"\";s:13:\"text_link_4_1\";s:10:\"Contact V4\";s:13:\"text_link_4_3\";s:10:\"Contact V4\";s:13:\"text_link_4_6\";s:10:\"Contact V4\";s:13:\"text_link_4_7\";s:10:\"Contact V4\";s:13:\"text_link_4_8\";s:10:\"Contact V4\";s:6:\"link_4\";s:77:\"".$urlFile."contact-us?v=4\";s:11:\"icon_link_4\";s:0:\"\";s:13:\"text_link_5_1\";s:10:\"Contact V5\";s:13:\"text_link_5_3\";s:10:\"Contact V5\";s:13:\"text_link_5_6\";s:10:\"Contact V5\";s:13:\"text_link_5_7\";s:10:\"Contact V5\";s:13:\"text_link_5_8\";s:10:\"Contact V5\";s:6:\"link_5\";s:77:\"".$urlFile."contact-us?v=5\";s:11:\"icon_link_5\";s:0:\"\";s:13:\"text_link_6_1\";s:0:\"\";s:13:\"text_link_6_3\";s:6:\"link 6\";s:13:\"text_link_6_6\";s:6:\"link 6\";s:13:\"text_link_6_7\";s:6:\"link 6\";s:13:\"text_link_6_8\";s:6:\"link 6\";s:6:\"link_6\";s:0:\"\";s:11:\"icon_link_6\";s:0:\"\";s:13:\"text_link_7_1\";s:0:\"\";s:13:\"text_link_7_3\";s:6:\"link 7\";s:13:\"text_link_7_6\";s:6:\"link 7\";s:13:\"text_link_7_7\";s:6:\"link 7\";s:13:\"text_link_7_8\";s:6:\"link 7\";s:6:\"link_7\";s:0:\"\";s:11:\"icon_link_7\";s:0:\"\";s:13:\"text_link_8_1\";s:0:\"\";s:13:\"text_link_8_3\";s:6:\"link 8\";s:13:\"text_link_8_6\";s:6:\"link 8\";s:13:\"text_link_8_7\";s:6:\"link 8\";s:13:\"text_link_8_8\";s:6:\"link 8\";s:6:\"link_8\";s:0:\"\";s:11:\"icon_link_8\";s:0:\"\";s:13:\"text_link_9_1\";s:0:\"\";s:13:\"text_link_9_3\";s:6:\"link 9\";s:13:\"text_link_9_6\";s:6:\"link 9\";s:13:\"text_link_9_7\";s:6:\"link 9\";s:13:\"text_link_9_8\";s:6:\"link 9\";s:6:\"link_9\";s:0:\"\";s:11:\"icon_link_9\";s:0:\"\";s:14:\"text_link_10_1\";s:0:\"\";s:14:\"text_link_10_3\";s:7:\"link 10\";s:14:\"text_link_10_6\";s:7:\"link 10\";s:14:\"text_link_10_7\";s:7:\"link 10\";s:14:\"text_link_10_8\";s:7:\"link 10\";s:7:\"link_10\";s:0:\"\";s:12:\"icon_link_10\";s:0:\"\";s:12:\"additionclss\";s:0:\"\";s:6:\"action\";s:9:\"addWidget\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}', ".$id_shop.", '1512916446'),
                (3, 'Shop Pages', 'links', 'a:83:{s:15:\"savedormegamenu\";s:1:\"1\";s:21:\"id_dormegamenu_widget\";s:1:\"0\";s:4:\"type\";s:5:\"links\";s:4:\"name\";s:10:\"Shop Pages\";s:14:\"widget_title_1\";s:10:\"Shop Pages\";s:14:\"widget_title_3\";s:10:\"Shop Pages\";s:14:\"widget_title_6\";s:10:\"Shop Pages\";s:14:\"widget_title_7\";s:10:\"Shop Pages\";s:14:\"widget_title_8\";s:10:\"Shop Pages\";s:13:\"text_link_1_1\";s:8:\"1 Column\";s:13:\"text_link_1_3\";s:8:\"1 Column\";s:13:\"text_link_1_6\";s:8:\"1 Column\";s:13:\"text_link_1_7\";s:8:\"1 Column\";s:13:\"text_link_1_8\";s:8:\"1 Column\";s:6:\"link_1\";s:78:\"".$urlFile."3-shop?cate_type=1\";s:11:\"icon_link_1\";s:0:\"\";s:13:\"text_link_2_1\";s:14:\"2 Columns Left\";s:13:\"text_link_2_3\";s:14:\"2 Columns Left\";s:13:\"text_link_2_6\";s:14:\"2 Columns Left\";s:13:\"text_link_2_7\";s:14:\"2 Columns Left\";s:13:\"text_link_2_8\";s:14:\"2 Columns Left\";s:6:\"link_2\";s:78:\"".$urlFile."3-shop?cate_type=2\";s:11:\"icon_link_2\";s:0:\"\";s:13:\"text_link_3_1\";s:15:\"2 Columns Right\";s:13:\"text_link_3_3\";s:15:\"2 Columns Right\";s:13:\"text_link_3_6\";s:15:\"2 Columns Right\";s:13:\"text_link_3_7\";s:15:\"2 Columns Right\";s:13:\"text_link_3_8\";s:15:\"2 Columns Right\";s:6:\"link_3\";s:78:\"".$urlFile."3-shop?cate_type=3\";s:11:\"icon_link_3\";s:0:\"\";s:13:\"text_link_4_1\";s:18:\"4 Products Per Row\";s:13:\"text_link_4_3\";s:18:\"4 Products Per Row\";s:13:\"text_link_4_6\";s:18:\"4 Products Per Row\";s:13:\"text_link_4_7\";s:18:\"4 Products Per Row\";s:13:\"text_link_4_8\";s:18:\"4 Products Per Row\";s:6:\"link_4\";s:89:\"".$urlFile."3-shop?cate_type=1&cate_row=4\";s:11:\"icon_link_4\";s:0:\"\";s:13:\"text_link_5_1\";s:18:\"5 Products Per Row\";s:13:\"text_link_5_3\";s:18:\"5 Products Per Row\";s:13:\"text_link_5_6\";s:18:\"5 Products Per Row\";s:13:\"text_link_5_7\";s:18:\"5 Products Per Row\";s:13:\"text_link_5_8\";s:18:\"5 Products Per Row\";s:6:\"link_5\";s:89:\"".$urlFile."3-shop?cate_type=1&cate_row=5\";s:11:\"icon_link_5\";s:0:\"\";s:13:\"text_link_6_1\";s:18:\"6 Products Per Row\";s:13:\"text_link_6_3\";s:18:\"6 Products Per Row\";s:13:\"text_link_6_6\";s:18:\"6 Products Per Row\";s:13:\"text_link_6_7\";s:18:\"6 Products Per Row\";s:13:\"text_link_6_8\";s:18:\"6 Products Per Row\";s:6:\"link_6\";s:89:\"".$urlFile."3-shop?cate_type=1&cate_row=6\";s:11:\"icon_link_6\";s:0:\"\";s:13:\"text_link_7_1\";s:0:\"\";s:13:\"text_link_7_3\";s:6:\"link 7\";s:13:\"text_link_7_6\";s:6:\"link 7\";s:13:\"text_link_7_7\";s:6:\"link 7\";s:13:\"text_link_7_8\";s:6:\"link 7\";s:6:\"link_7\";s:0:\"\";s:11:\"icon_link_7\";s:0:\"\";s:13:\"text_link_8_1\";s:0:\"\";s:13:\"text_link_8_3\";s:6:\"link 8\";s:13:\"text_link_8_6\";s:6:\"link 8\";s:13:\"text_link_8_7\";s:6:\"link 8\";s:13:\"text_link_8_8\";s:6:\"link 8\";s:6:\"link_8\";s:0:\"\";s:11:\"icon_link_8\";s:0:\"\";s:13:\"text_link_9_1\";s:0:\"\";s:13:\"text_link_9_3\";s:6:\"link 9\";s:13:\"text_link_9_6\";s:6:\"link 9\";s:13:\"text_link_9_7\";s:6:\"link 9\";s:13:\"text_link_9_8\";s:6:\"link 9\";s:6:\"link_9\";s:0:\"\";s:11:\"icon_link_9\";s:0:\"\";s:14:\"text_link_10_1\";s:0:\"\";s:14:\"text_link_10_3\";s:7:\"link 10\";s:14:\"text_link_10_6\";s:7:\"link 10\";s:14:\"text_link_10_7\";s:7:\"link 10\";s:14:\"text_link_10_8\";s:7:\"link 10\";s:7:\"link_10\";s:0:\"\";s:12:\"icon_link_10\";s:0:\"\";s:12:\"additionclss\";s:0:\"\";s:6:\"action\";s:9:\"addWidget\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}', ".$id_shop.", '1512975212'),
                (4, 'Product Pages', 'links', 'a:83:{s:15:\"savedormegamenu\";s:1:\"1\";s:21:\"id_dormegamenu_widget\";s:1:\"0\";s:4:\"type\";s:5:\"links\";s:4:\"name\";s:13:\"Product Pages\";s:14:\"widget_title_1\";s:13:\"Product Pages\";s:14:\"widget_title_3\";s:13:\"Product Pages\";s:14:\"widget_title_6\";s:13:\"Product Pages\";s:14:\"widget_title_7\";s:13:\"Product Pages\";s:14:\"widget_title_8\";s:13:\"Product Pages\";s:13:\"text_link_1_1\";s:16:\"Standard Product\";s:13:\"text_link_1_3\";s:16:\"Standard Product\";s:13:\"text_link_1_6\";s:16:\"Standard Product\";s:13:\"text_link_1_7\";s:16:\"Standard Product\";s:13:\"text_link_1_8\";s:16:\"Standard Product\";s:6:\"link_1\";s:106:\"".$urlFile."shop/1-1-japanese-strawberry.html?detail_col=1\";s:11:\"icon_link_1\";s:0:\"\";s:13:\"text_link_2_1\";s:21:\"Product Right Sidebar\";s:13:\"text_link_2_3\";s:21:\"Product Right Sidebar\";s:13:\"text_link_2_6\";s:21:\"Product Right Sidebar\";s:13:\"text_link_2_7\";s:21:\"Product Right Sidebar\";s:13:\"text_link_2_8\";s:21:\"Product Right Sidebar\";s:6:\"link_2\";s:106:\"".$urlFile."shop/1-1-japanese-strawberry.html?detail_col=2\";s:11:\"icon_link_2\";s:0:\"\";s:13:\"text_link_3_1\";s:20:\"Product Left Sidebar\";s:13:\"text_link_3_3\";s:20:\"Product Left Sidebar\";s:13:\"text_link_3_6\";s:20:\"Product Left Sidebar\";s:13:\"text_link_3_7\";s:20:\"Product Left Sidebar\";s:13:\"text_link_3_8\";s:20:\"Product Left Sidebar\";s:6:\"link_3\";s:106:\"".$urlFile."shop/1-1-japanese-strawberry.html?detail_col=3\";s:11:\"icon_link_3\";s:0:\"\";s:13:\"text_link_4_1\";s:25:\"Product Gallery Thumbnail\";s:13:\"text_link_4_3\";s:25:\"Product Gallery Thumbnail\";s:13:\"text_link_4_6\";s:25:\"Product Gallery Thumbnail\";s:13:\"text_link_4_7\";s:25:\"Product Gallery Thumbnail\";s:13:\"text_link_4_8\";s:25:\"Product Gallery Thumbnail\";s:6:\"link_4\";s:106:\"".$urlFile."shop/1-1-japanese-strawberry.html?detail_col=4\";s:11:\"icon_link_4\";s:0:\"\";s:13:\"text_link_5_1\";s:19:\"Product With Sticky\";s:13:\"text_link_5_3\";s:19:\"Product With Sticky\";s:13:\"text_link_5_6\";s:19:\"Product With Sticky\";s:13:\"text_link_5_7\";s:19:\"Product With Sticky\";s:13:\"text_link_5_8\";s:19:\"Product With Sticky\";s:6:\"link_5\";s:106:\"".$urlFile."shop/1-1-japanese-strawberry.html?detail_col=5\";s:11:\"icon_link_5\";s:0:\"\";s:13:\"text_link_6_1\";s:28:\"Product Sticky Right Sidebar\";s:13:\"text_link_6_3\";s:28:\"Product Sticky Right Sidebar\";s:13:\"text_link_6_6\";s:28:\"Product Sticky Right Sidebar\";s:13:\"text_link_6_7\";s:28:\"Product Sticky Right Sidebar\";s:13:\"text_link_6_8\";s:28:\"Product Sticky Right Sidebar\";s:6:\"link_6\";s:106:\"".$urlFile."shop/1-1-japanese-strawberry.html?detail_col=6\";s:11:\"icon_link_6\";s:0:\"\";s:13:\"text_link_7_1\";s:27:\"Product Sticky Left Sidebar\";s:13:\"text_link_7_3\";s:27:\"Product Sticky Left Sidebar\";s:13:\"text_link_7_6\";s:27:\"Product Sticky Left Sidebar\";s:13:\"text_link_7_7\";s:27:\"Product Sticky Left Sidebar\";s:13:\"text_link_7_8\";s:27:\"Product Sticky Left Sidebar\";s:6:\"link_7\";s:106:\"".$urlFile."shop/1-1-japanese-strawberry.html?detail_col=7\";s:11:\"icon_link_7\";s:0:\"\";s:13:\"text_link_8_1\";s:0:\"\";s:13:\"text_link_8_3\";s:6:\"link 8\";s:13:\"text_link_8_6\";s:6:\"link 8\";s:13:\"text_link_8_7\";s:6:\"link 8\";s:13:\"text_link_8_8\";s:6:\"link 8\";s:6:\"link_8\";s:0:\"\";s:11:\"icon_link_8\";s:0:\"\";s:13:\"text_link_9_1\";s:0:\"\";s:13:\"text_link_9_3\";s:6:\"link 9\";s:13:\"text_link_9_6\";s:6:\"link 9\";s:13:\"text_link_9_7\";s:6:\"link 9\";s:13:\"text_link_9_8\";s:6:\"link 9\";s:6:\"link_9\";s:0:\"\";s:11:\"icon_link_9\";s:0:\"\";s:14:\"text_link_10_1\";s:0:\"\";s:14:\"text_link_10_3\";s:7:\"link 10\";s:14:\"text_link_10_6\";s:7:\"link 10\";s:14:\"text_link_10_7\";s:7:\"link 10\";s:14:\"text_link_10_8\";s:7:\"link 10\";s:7:\"link_10\";s:0:\"\";s:12:\"icon_link_10\";s:0:\"\";s:12:\"additionclss\";s:0:\"\";s:6:\"action\";s:9:\"addWidget\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}', ".$id_shop.", '1512975467'),
                (5, 'Products Special', 'productlist', 'a:15:{s:15:\"savedormegamenu\";s:1:\"1\";s:21:\"id_dormegamenu_widget\";s:1:\"5\";s:4:\"type\";s:11:\"productlist\";s:4:\"name\";s:16:\"Products Special\";s:14:\"widget_title_1\";s:16:\"Products Special\";s:14:\"widget_title_3\";s:16:\"Products Special\";s:14:\"widget_title_6\";s:16:\"Products Special\";s:14:\"widget_title_7\";s:16:\"Products Special\";s:14:\"widget_title_8\";s:16:\"Products Special\";s:5:\"limit\";s:1:\"4\";s:9:\"list_type\";s:7:\"special\";s:12:\"additionclss\";s:0:\"\";s:6:\"action\";s:9:\"addWidget\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}', ".$id_shop.", '1512989051'),
                (6, 'Menu Image Right', 'html', 'a:18:{s:15:\"savedormegamenu\";s:1:\"1\";s:21:\"id_dormegamenu_widget\";s:1:\"6\";s:4:\"type\";s:4:\"html\";s:4:\"name\";s:16:\"Menu Image Right\";s:14:\"widget_title_1\";s:0:\"\";s:14:\"widget_title_3\";s:0:\"\";s:14:\"widget_title_6\";s:0:\"\";s:14:\"widget_title_7\";s:0:\"\";s:14:\"widget_title_8\";s:0:\"\";s:13:\"htmlcontent_1\";s:199:\"<div class=\"menu-image-style right-style\"><a href=\"#\"><img src=\"".$urlFile."img/cms/dorado/menu/about-pic-01.png\" width=\"410\" height=\"410\" /></a></div>\";s:13:\"htmlcontent_3\";s:0:\"\";s:13:\"htmlcontent_6\";s:0:\"\";s:13:\"htmlcontent_7\";s:0:\"\";s:13:\"htmlcontent_8\";s:0:\"\";s:12:\"additionclss\";s:0:\"\";s:6:\"action\";s:9:\"addWidget\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}', ".$id_shop.", '1512989516'),
                (7, 'Pages Lists', 'links', 'a:83:{s:15:\"savedormegamenu\";s:1:\"1\";s:21:\"id_dormegamenu_widget\";s:1:\"7\";s:4:\"type\";s:5:\"links\";s:4:\"name\";s:11:\"Pages Lists\";s:14:\"widget_title_1\";s:0:\"\";s:14:\"widget_title_3\";s:0:\"\";s:14:\"widget_title_6\";s:0:\"\";s:14:\"widget_title_7\";s:0:\"\";s:14:\"widget_title_8\";s:0:\"\";s:13:\"text_link_1_1\";s:7:\"Gallery\";s:13:\"text_link_1_3\";s:7:\"Gallery\";s:13:\"text_link_1_6\";s:7:\"Gallery\";s:13:\"text_link_1_7\";s:7:\"Gallery\";s:13:\"text_link_1_8\";s:7:\"Gallery\";s:6:\"link_1\";s:67:\"".$urlFile."gallery\";s:11:\"icon_link_1\";s:0:\"\";s:13:\"text_link_2_1\";s:8:\"About Us\";s:13:\"text_link_2_3\";s:8:\"About Us\";s:13:\"text_link_2_6\";s:8:\"About Us\";s:13:\"text_link_2_7\";s:8:\"About Us\";s:13:\"text_link_2_8\";s:8:\"About Us\";s:6:\"link_2\";s:68:\"".$urlFile."about-us\";s:11:\"icon_link_2\";s:0:\"\";s:13:\"text_link_3_1\";s:9:\"Not Found\";s:13:\"text_link_3_3\";s:9:\"Not Found\";s:13:\"text_link_3_6\";s:9:\"Not Found\";s:13:\"text_link_3_7\";s:9:\"Not Found\";s:13:\"text_link_3_8\";s:9:\"Not Found\";s:6:\"link_3\";s:68:\"".$urlFile."404.html\";s:11:\"icon_link_3\";s:0:\"\";s:13:\"text_link_4_1\";s:18:\"Under Construction\";s:13:\"text_link_4_3\";s:18:\"Under Construction\";s:13:\"text_link_4_6\";s:18:\"Under Construction\";s:13:\"text_link_4_7\";s:18:\"Under Construction\";s:13:\"text_link_4_8\";s:18:\"Under Construction\";s:6:\"link_4\";s:76:\"".$urlFile."under-const.html\";s:11:\"icon_link_4\";s:0:\"\";s:13:\"text_link_5_1\";s:16:\"Track Your Order\";s:13:\"text_link_5_3\";s:16:\"Track Your Order\";s:13:\"text_link_5_6\";s:16:\"Track Your Order\";s:13:\"text_link_5_7\";s:16:\"Track Your Order\";s:13:\"text_link_5_8\";s:16:\"Track Your Order\";s:6:\"link_5\";s:74:\"".$urlFile."order-tracking\";s:11:\"icon_link_5\";s:0:\"\";s:13:\"text_link_6_1\";s:0:\"\";s:13:\"text_link_6_3\";s:6:\"link 6\";s:13:\"text_link_6_6\";s:6:\"link 6\";s:13:\"text_link_6_7\";s:6:\"link 6\";s:13:\"text_link_6_8\";s:6:\"link 6\";s:6:\"link_6\";s:0:\"\";s:11:\"icon_link_6\";s:0:\"\";s:13:\"text_link_7_1\";s:0:\"\";s:13:\"text_link_7_3\";s:6:\"link 7\";s:13:\"text_link_7_6\";s:6:\"link 7\";s:13:\"text_link_7_7\";s:6:\"link 7\";s:13:\"text_link_7_8\";s:6:\"link 7\";s:6:\"link_7\";s:0:\"\";s:11:\"icon_link_7\";s:0:\"\";s:13:\"text_link_8_1\";s:0:\"\";s:13:\"text_link_8_3\";s:6:\"link 8\";s:13:\"text_link_8_6\";s:6:\"link 8\";s:13:\"text_link_8_7\";s:6:\"link 8\";s:13:\"text_link_8_8\";s:6:\"link 8\";s:6:\"link_8\";s:0:\"\";s:11:\"icon_link_8\";s:0:\"\";s:13:\"text_link_9_1\";s:0:\"\";s:13:\"text_link_9_3\";s:6:\"link 9\";s:13:\"text_link_9_6\";s:6:\"link 9\";s:13:\"text_link_9_7\";s:6:\"link 9\";s:13:\"text_link_9_8\";s:6:\"link 9\";s:6:\"link_9\";s:0:\"\";s:11:\"icon_link_9\";s:0:\"\";s:14:\"text_link_10_1\";s:0:\"\";s:14:\"text_link_10_3\";s:7:\"link 10\";s:14:\"text_link_10_6\";s:7:\"link 10\";s:14:\"text_link_10_7\";s:7:\"link 10\";s:14:\"text_link_10_8\";s:7:\"link 10\";s:7:\"link_10\";s:0:\"\";s:12:\"icon_link_10\";s:0:\"\";s:12:\"additionclss\";s:0:\"\";s:6:\"action\";s:9:\"addWidget\";s:10:\"secure_key\";s:32:\"f415f75100726443f99eac7bdb5578e9\";s:7:\"id_shop\";s:1:\"1\";}', ".$id_shop.", '1512990402');
        ";
        Db::getInstance()->Execute($sql4);
        return true;
    }

    public function uninstall()
    {
        $tab = new Tab((int) Tab::getIdFromClassName('AdminDorMegamenu'));
        $tab->delete();
        Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'dormegamenu`');
        Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'dormegamenu_lang`');
        Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'dormegamenu_shop`');
        Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'dormegamenu_widget`');
        if (parent::uninstall())
            return true;
        return false;
    }

    public function getConfigName($name) {
        return Tools::strtoupper($this->prefix . $name);
    }

    public function reset()
    {
        if (!$this->uninstall(false)) {
            return false;
        }
        if (!$this->install(false)) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
        if (Tools::getValue('action') && Tools::getValue('secure_key') == $this->secure_key) {
            $fnc = (string)Tools::getValue('action');
            $this->{$fnc}();
        }
        
        $html = '<h2>'.$this->displayName.'</h2>';

        return $this->adminMainPage();
    }

    public function adminMainPage() {
        $base_config_url = 'index.php?controller=AdminModules&configure='.$this->name.'&token='.Tools::getValue('token');

        $this->context->smarty->assign(array(
            'html_choices_select' => $this->renderChoicesSelect(),
            'ajaxurl' => _MODULE_DIR_.$this->name.'/ajax.php',
            'adminajaxurl' => $base_config_url,
            'id_shop' => (int)Context::getContext()->shop->id,
            'secure_key' => $this->secure_key,
            'list_menu' => $this->listMenu(),
            'list_widgets' => DorMegamenuWidget::renderAdminListWidgets()
        ));

        return $this->display(__FILE__, 'admin_main_page.tpl');
    }

    public function widgetForm() {
        $obj = new DorMegamenuWidget();
        $obj->loadEngines();

        $id = (int)Tools::getValue('id');
        $type = trim(Tools::strtolower(Tools::getValue('widget_type')));
        $data = array();
        if ($id) {
            $data = $obj->getWidetById($id);
        }

        if (isset($data['type'])) {
            $type = $data['type'];
        }

        $data['params']['type'] = $type;
        echo $obj->getForm($type, $data);
        die();
    }

    public function getListWidgets() {
        $obj = new DorMegamenuWidget();
        $obj->loadEngines();
        return $obj->getListWidgets();
    }
    public function listMenu() {
        $obj = new ObjectDorMegamenu(1);
        $list_menu = $obj->recurseGetAdminMenuTree();

        return $list_menu;
    }

    public function getSubmenuSettingForm($id_dormegamenu) {
        $obj = new ObjectDorMegamenu($id_dormegamenu);
        $show_menumenu = false;
        if ($obj->id_parent == 1) {
            $show_menumenu = true;
        }
        $data = $obj->parserParams($obj->params);
        //d(json_decode($data['params']));
        $this->context->smarty->assign(array(
            'ajaxurl' => _MODULE_DIR_.$this->name.'/ajax.php',
            'secure_key' => $this->secure_key,
            'listWidgets' => DorMegamenuWidget::getWidgets(),
            'id_dormegamenu' => $id_dormegamenu,
            'data' => $data,
            'show_menumenu' => $show_menumenu
        ));
        return $this->display(__FILE__, 'admin_submenu_setting_form.tpl');
    }

    public function editMenu() {
        echo $this->getMenuForm( (int)Tools::getValue('id_menu') );
        die();
    }

    public function getMenuForm($id_dormegamenu) {
        $obj = new ObjectDorMegamenu( $id_dormegamenu );
        $inputs = array();
        $inputs[] = array(
            'type' => 'hidden',
            'label' => $this->l('Id Megamenu'),
            'name' => 'id_dormegamenu',
            'default' => ''
        );
        $inputs[] = array(
            'type' => 'text',
            'label' => $this->l('Name'),
            'name' => 'name',
            'default' => '',
            'lang' => true
        );
        $inputs[] = array(
            'type' => 'text',
            'label' => $this->l('Description'),
            'name' => 'description',
            'default' => '',
            'lang' => true
        );
        if ($obj->type == 'custom-link') {
            $inputs[] = array(
                'type' => 'text',
                'label' => $this->l('Custom Link'),
                'name' => 'url',
                'default' => '',
                'lang' => true
            );
        }
        $inputs[] = array(
            'type' => 'switch',
            'label' => $this->l('Active'),
            'name' => 'active',
            'values' => array(
                array(
                    'id' => 'active_on',
                    'value' => 1,
                    'label' => $this->l('Enabled')
                ),
                array(
                    'id' => 'active_off',
                    'value' => 0,
                    'label' => $this->l('Disabled')
                )
            )
        );
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $obj->type . ' : '. $obj->name[$this->context->language->id],
            ),
            'input' => $inputs
        );
        $default_lang = $this->context->language->id;
        $helper = new HelperForm();
        $helper->module = new $this->name;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang)
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name.'&widgets=1&rand='.rand().'&wtype='.Tools::getValue('wtype');
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->name;
        $helper->submit_action = 'save'.$this->name;
        $helper->tpl_vars = array(
            'fields_value' => DorMegamenuHelper::getConfigFieldsValues($fields_form, $obj),
            'languages' => Language::getLanguages(false),
            'id_language' => $default_lang
        );
        $form = $helper->generateForm($fields_form);

        $this->context->smarty->assign(array(
            'ajaxurl' => _MODULE_DIR_.$this->name.'/ajax.php',
            'secure_key' => $this->secure_key,
            'form' => $form
        ));
        return $this->display(__FILE__, 'admin_menu_form.tpl');
    }

    public function renderChoicesSelect()
    {
        $html = '<ul id="list-to-choose">';

        // BEGIN Categories
        $html .= '<li id="add-categories" class="control-section accordion-section add-category">';

        $html .= '<h3 class="accordion-section-title hndle" tabindex="0">';
        $html .= $this->l('Categories');
        $html .= '</h3>';
        $html .= '<div class="accordion-section-content">';
            $html .= '<div class="inside">';

            $shops_to_get = Shop::getContextListShopID();
            foreach ($shops_to_get as $shop_id) {
                $html .= $this->generateCategoriesOption(DorMegamenuHelper::customGetNestedCategories($shop_id, null, (int)$this->context->language->id, false));
            }
            $html .= '</div>';
            $html .= '<p class="button-controls">';
            $html .= '<a class="select-all" data-type="category" href="#">'.$this->l('Select All').'</a>';
            $html .= '<button class="btn btn-default add-to-menu btn-xs pull-right" type="button" data-type="category">'.$this->l('Add To Menu').'</button>';
            $html .= '</p>';
        $html .= '</div>';

        $html .= '</li>';

        // CMS Category
        $html .= '<li id="add-cms-category" class="control-section accordion-section add-cms-category">';

        $html .= '<h3 class="accordion-section-title hndle" tabindex="0">';
        $html .= $this->l('Cms Category');
        $html .= '</h3>';
        $html .= '<div class="accordion-section-content">';
            $html .= '<div class="inside">';
            $html .= $this->getCMSCategoryOptions(0, 1, $this->context->language->id);
            
            $html .= '</div>';
            $html .= '<p class="button-controls">';
            $html .= '<a class="select-all" data-type="cms-category" href="#">'.$this->l('Select All').'</a>';
            $html .= '<button class="btn btn-default add-to-menu btn-xs pull-right" type="button" data-type="cms-category">'.$this->l('Add To Menu').'</button>';
            $html .= '</p>';
        $html .= '</div>';

        $html .= '</li>';

        // BEGIN CMS
        $cmss = CMS::listCms();
        if (!empty($cmss)) {
            $html .= '<li id="add-cms" class="control-section accordion-section add-cms">';
            $html .= '<h3 class="accordion-section-title hndle" tabindex="0">';
            $html .= $this->l('Cms');
            $html .= '</h3>';
            $html .= '<div class="accordion-section-content">';
                $html .= '<div class="inside">';
                $html .= '<ul>';
                foreach ($cmss as $cms) {
                    $html .= '<li>';
                        $html .= '<label class="checkbox-inline">';
                        $html .= '<input type="checkbox" name="id_cms['.$cms['id_cms'].']" value="'.$cms['id_cms'].'"> ';
                        $html .= $cms['meta_title'];
                        $html .= '</label>';

                    $html .= '</li>';
                }
                $html .= '</ul>';
                $html .= '</div>';
                $html .= '<p class="button-controls">';
                $html .= '<a class="select-all" data-type="cms" href="#">'.$this->l('Select All').'</a>';
                $html .= '<button class="btn btn-default add-to-menu btn-xs pull-right" type="button" data-type="cms">'.$this->l('Add To Menu').'</button>';
                $html .= '</p>';
            $html .= '</div>';

            $html .= '</li>';
        }

        // BEGIN SUPPLIER
        $suppliers = Supplier::getSuppliers(false, $this->context->language->id);
        if (!empty($suppliers)) {
            $html .= '<li id="add-supplier" class="control-section accordion-section add-supplier">';
            $html .= '<h3 class="accordion-section-title hndle" tabindex="0">';
            $html .= $this->l('Supplier');
            $html .= '</h3>';
            $html .= '<div class="accordion-section-content">';
                $html .= '<div class="inside">';
                $html .= '<ul>';
                foreach ($suppliers as $supplier) {
                    $html .= '<li>';
                        $html .= '<label class="checkbox-inline">';
                        $html .= '<input type="checkbox" name="id_supplier['.$supplier['id_supplier'].']" value="'.$supplier['id_supplier'].'"> ';
                        $html .= $supplier['name'];
                        $html .= '</label>';

                    $html .= '</li>';
                }
                $html .= '</ul>';
                $html .= '</div>';
                $html .= '<p class="button-controls">';
                $html .= '<a class="select-all" data-type="supplier" href="#">'.$this->l('Select All').'</a>';
                $html .= '<button class="btn btn-default add-to-menu btn-xs pull-right" type="button" data-type="supplier">'.$this->l('Add To Menu').'</button>';
                $html .= '</p>';
            $html .= '</div>';

            $html .= '</li>';
        }

        // BEGIN Manufacturer
        $manufacturers = Manufacturer::getManufacturers(false, $this->context->language->id);
        if (!empty($manufacturers)) {
            $html .= '<li id="add-manufacturer" class="control-section accordion-section add-manufacturer">';
            $html .= '<h3 class="accordion-section-title hndle" tabindex="0">';
            $html .= $this->l('Manufacturer');
            $html .= '</h3>';
            $html .= '<div class="accordion-section-content">';
                $html .= '<div class="inside">';
                $html .= '<ul>';
                foreach ($manufacturers as $manufacturer) {
                    $html .= '<li>';
                        $html .= '<label class="checkbox-inline">';
                        $html .= '<input type="checkbox" name="id_manufacturer['.$manufacturer['id_manufacturer'].']" value="'.$manufacturer['id_manufacturer'].'"> ';
                        $html .= $manufacturer['name'];
                        $html .= '</label>';
                    $html .= '</li>';
                }
                $html .= '</ul>';
                $html .= '</div>';
                $html .= '<p class="button-controls">';
                $html .= '<a class="select-all" data-type="manufacturer" href="#">'.$this->l('Select All').'</a>';
                $html .= '<button class="btn btn-default add-to-menu btn-xs pull-right" type="button" data-type="manufacturer">'.$this->l('Add To Menu').'</button>';
                $html .= '</p>';
            $html .= '</div>';

            $html .= '</li>';
        }

        // BEGIN Shops
        if (Shop::isFeatureActive()) {

            $html .= '<li id="add-shops" class="control-section accordion-section add-shop">';

            $html .= '<h3 class="accordion-section-title hndle" tabindex="0">';
            $html .= $this->l('Shops');
            $html .= '</h3>';
            $html .= '<div class="accordion-section-content">';
                $html .= '<div class="inside">';

                $shops = Shop::getShopsCollection();
                $html .= '<ul>';
                foreach ($shops as $shop) {
                    if (!$shop->setUrl() && !$shop->getBaseURL()) {
                        continue;
                    }
                    $html .= '<li>';
                    $html .= '<label class="checkbox-inline">';
                    $html .= '<input type="checkbox" name="id_shop['.$shop->id.']" value="'.$shop->id.'"> ';
                    $html .= $shop->name;
                    $html .= '<label>';
                    $html .= '</li>';
                }
                $html .= '</ul>';
                $html .= '</div>';
                $html .= '<p class="button-controls">';
                $html .= '<a class="select-all" data-type="shop" href="#">'.$this->l('Select All').'</a>';
                $html .= '<button class="btn btn-default add-to-menu btn-xs pull-right" type="button" data-type="shop">'.$this->l('Add To Menu').'</button>';
                $html .= '</p>';
            $html .= '</div>';

            $html .= '</li>';

        }

        // BEGIN Products
        $html .= '<li id="add-products" class="control-section accordion-section add-product">';

        $html .= '<h3 class="accordion-section-title hndle" tabindex="0">';
        $html .= $this->l('Products');
        $html .= '</h3>';
        $html .= '<div class="accordion-section-content">';
            $html .= '<div class="inside">';

                $html .= '<ul>';
                    $html .= '<li>';
                    $html .= '<div class="inner"><span>'.$this->l('Product ID').'</span>';
                    $html .= '<input type="text" name="id_product" class="id_product" placeholder="'.$this->l('Enter your product ID').'"> ';
                    $html .= '</div>';
                    $html .= '</li>';
                $html .= '</ul>';
            $html .= '</div>';
            $html .= '<p class="button-controls">';
            $html .= '<button class="btn btn-default add-to-menu btn-xs pull-right" type="button" data-type="product">'.$this->l('Add To Menu').'</button>';
            $html .= '</p>';
        $html .= '</div>';

        $html .= '</li>';

        // BEGIN Custom Url
        $html .= '<li id="add-custom-link" class="control-section accordion-section add-custom-link">';

        $html .= '<h3 class="accordion-section-title hndle" tabindex="0">';
        $html .= $this->l('Custom Link');
        $html .= '</h3>';
        $html .= '<div class="accordion-section-content">';
            $html .= '<div class="inside">';

                $html .= '<ul>';
                    $html .= '<li>';
                    $html .= '<div class="inner"><span>'.$this->l('Menu Name').'</span>';
                    $html .= '<input type="text" name="custom_name" class="custom_name" placeholder="'.$this->l('Enter Your Menu Name').'"> ';
                    $html .= '</div>';
                    $html .= '</li>';

                    $html .= '<li>';
                    $html .= '<div class="inner"><span>'.$this->l('Menu Url').'</span>';
                    $html .= '<input type="text" name="custom_link" class="custom_link" placeholder="'.$this->l('Enter Your Menu Url').'"> ';
                    $html .= '</div>';
                    $html .= '</li>';
                $html .= '</ul>';
            $html .= '</div>';
            $html .= '<p class="button-controls">';
            $html .= '<button class="btn btn-default add-to-menu btn-xs pull-right" type="button" data-type="custom-link">'.$this->l('Add To Menu').'</button>';
            $html .= '</p>';
        $html .= '</div>';

        $html .= '</li>';

        $html .= '</ul>';
        return $html;
    }
    protected function getCMSCategoryOptions($parent = 0, $depth = 1, $id_lang = false, $id_shop = false)
    {
        $html = '';
        $id_lang = $id_lang ? (int)$id_lang : (int)Context::getContext()->language->id;
        $id_shop = ($id_shop !== false) ? $id_shop : Context::getContext()->shop->id;
        $categories = DorMegamenuHelper::getCMSCategories(false, (int)$parent, (int)$id_lang, (int)$id_shop);

        if (!empty($categories)) {
            $html .= '<ul>';
            foreach ($categories as $category) {
                $html .= '<li>';
                    $html .= '<label class="checkbox-inline">';
                    $html .= '<input type="checkbox" name="id_cms_category['.$category['id_cms_category'].']" value="'.$category['id_cms_category'].'"> ';
                    $html .= $category['name'];
                    $html .= '</label>';
                    $html .= $this->getCMSCategoryOptions($category['id_cms_category'], (int)$depth + 1, (int)$id_lang);
                $html .= '</li>';
            }
            $html .= '</ul>';
        }

        return $html;
    }

    protected function generateCategoriesOption($categories)
    {
        $html = '';
        $html .= '<ul>';
        foreach ($categories as $key => $category) {
            $html .= '<li>';
                $html .= '<label class="checkbox-inline">';
                $html .= '<input type="checkbox" name="id_category['.$category['id_category'].']" value="'.$category['id_category'].'"> ';
                $html .= $category['name'];
                $html .= '</label>';
            
                if (isset($category['children']) && !empty($category['children'])) {
                    $html .= $this->generateCategoriesOption($category['children']);
                }

            $html .= '</li>';
        }
        $html .= '</ul>';
        return $html;
    }

    public function renderWidgetContent($type, $data)
    {
        $output = '';
        if ($type) {
            $data['id_lang'] = $this->context->language->id;

            $this->smarty->assign($data);
            $output .= '<div class="widget-content">';
            $output .= $this->display(__FILE__, 'widgets/'.$type.'.tpl');
            $output .= '</div>';
        }
        return $output;
    }
    public function hookActionAdminControllerSetMedia() {
        if (get_class($this->context->controller) == 'AdminModulesController' && Tools::getValue('configure') == 'dor_megamenu')
        {
            if (method_exists($this->context->controller, 'addJquery'))
                $this->context->controller->addJquery();
            $this->context->controller->addJqueryUI(array(
                'ui.accordion',
                'ui.sortable'
            ));
            $this->context->controller->addCSS($this->_path.'views/css/admin/form.css');

            // admin tree js
            $admin_webpath = str_ireplace(_PS_CORE_DIR_, '', _PS_ADMIN_DIR_);
            $admin_webpath = preg_replace('/^'.preg_quote(DIRECTORY_SEPARATOR, '/').'/', '', $admin_webpath);
            $bo_theme = ((Validate::isLoadedObject(Context::getContext()->employee)
                && Context::getContext()->employee->bo_theme) ? Context::getContext()->employee->bo_theme : 'default');

            if (!file_exists(_PS_BO_ALL_THEMES_DIR_.$bo_theme.DIRECTORY_SEPARATOR.'template')) {
                $bo_theme = 'default';
            }
            $js_tree = __PS_BASE_URI__.$admin_webpath.'/themes/'.$bo_theme.'/js/tree.js';

            // add jss
            $this->context->controller->addJs(
                array(
                    $this->_path.'views/js/admin/jquery.nestable.js',
                    $this->_path.'views/js/admin/form.js',
                    _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
                    _PS_JS_DIR_.'admin/tinymce.inc.js',
                    $js_tree
                )
            );
        }
    }
    public function hookDisplayHeader() {
        $this->context->controller->addCSS($this->_path.'views/css/style.css');
        $this->context->controller->addJS($this->_path.'views/js/script.js');
    }

    public function hookDisplayTop($params) {
        
        $tpl = 'megamenu.tpl';

        $id_shop = (int)Context::getContext()->shop->id;
        $id_lang = (int)Context::getContext()->language->id;
        $cache_id = $this->getCacheId($id_shop."-".$id_lang);

        if (!$this->isCached($this->templateFile, $cache_id))
        {
            $fileCache = "DorMegamenu-Shop".$id_shop.'-'.$id_lang;
            $dorTimeCache  = Configuration::get('dor_themeoptions_dorTimeCache',Configuration::get('dorTimeCache'));
            $dorTimeCache = $dorTimeCache?$dorTimeCache:86400;
            $dorCaches  = Configuration::get('dor_themeoptions_enableDorCache',Configuration::get('enableDorCache'));
            $cacheData = array();
            if($dorCaches){
                $objCache = new DorCaches(array('name'=>'default','path'=>_PS_ROOT_DIR_.'/override/Dor/Caches/smartcaches/megamenu/','extension'=>'.cache'));
                $objCache->setCache($fileCache);
                $cacheData = $objCache->renderData($fileCache);
            }
            if($cacheData && $dorCaches){
                $output = $cacheData;
            }else{
                $obj = new ObjectDorMegamenu(1);
                $output = $obj->renderMegaMenu($this);
                if($dorCaches){
                    $objCache->store($fileCache, $output, $expiration = $dorTimeCache);
                }
            }
            $this->smarty->assign([
                'output' => $output
            ]);
        }
        return $this->display(__FILE__, 'megamenu.tpl',$cache_id);
    }
    public function hookDormegamenu($params){
        return $this->hookDisplayTop($params);
    }


    public function hookActionObjectCategoryAddAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectCategoryUpdateAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectCategoryDeleteAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectCmsUpdateAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectCmsDeleteAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectCmsAddAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectSupplierUpdateAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectSupplierDeleteAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectSupplierAddAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectManufacturerUpdateAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectManufacturerDeleteAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectManufacturerAddAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectProductUpdateAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectProductDeleteAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookActionObjectProductAddAfter($params)
    {
        $this->clearMenuCache();
    }

    public function hookCategoryUpdate($params)
    {
        $this->clearMenuCache();
    }

    protected function getCacheDirectory()
    {
        return _PS_CACHE_DIR_ . DIRECTORY_SEPARATOR . 'dor_megamenu';
    }

    protected function clearMenuCache()
    {
        $dir = $this->getCacheDirectory();

        if (!is_dir($dir)) {
            return;
        }

        foreach (scandir($dir) as $entry) {
            if (preg_match('/\.json$/', $entry)) {
                unlink($dir . DIRECTORY_SEPARATOR . $entry);
            }
        }
    }

}
