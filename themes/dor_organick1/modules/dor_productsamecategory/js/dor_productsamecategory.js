var DORRELATED = {
	init:function(){
		DORRELATED.RelatedCarousel();
		window.addEventListener('load', function (){
		    DORRELATED.SameCategoryLoadDefault();
		    DORRELATED.SameBrandLoadDefault();
		});
	},
	RelatedCarousel:function(){
		var perpage = 4;
		/*var checkSidebar = $("#dor-left-column").html();
		if(typeof checkSidebar != "undefined"){
			perpage = 3;
		}*/
		$('#productscategory_same .product_list_related').owlCarousel({
            items: perpage,
	        loop: true,
	        navigation: true,
	        nav: true,
	        autoplay: false,
	        margin:20,
	        lazyLoad: true,
	        responsive: {
	            0: {items: 1},
	            1200: {items: 4},
	            992: {items: 4},
	            650: {items: 3, margin:20},
	            370: {items: 2, margin:20},
	            300: {items: 1, margin:20}
	        },
	        onInitialize: function (event) {
		        if ($('#productscategory_same .product_list_related').find('article').length <= 1) {
		           this.settings.loop = false;
		        }
		    },
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
        });
        $('#productssamebrand .product_list_related').owlCarousel({
            items: perpage,
	        loop: true,
	        navigation: true,
	        nav: true,
	        autoplay: false,
	        margin:20,
	        lazyLoad: true,
	        responsive: {
	            0: {items: 1},
	            1200: {items: 4},
	            992: {items: 4},
	            650: {items: 3, margin:20},
	            370: {items: 2, margin:20},
	            300: {items: 1, margin:20}
	        },
	        onInitialize: function (event) {
		        if ($('#productssamebrand .product_list_related').find('article').length <= 1) {
		           this.settings.loop = false;
		        }
		    },
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
        });
	},
	ReloadProcess:function(){
		var html = "";
		html += '<div class="graph__preloader">';
	        html += '<div class="js-preloader preloader">';
	          html += '<b class="preloader__bar -bar1"></b>';
	          html += '<b class="preloader__bar -bar2"></b>';
	          html += '<b class="preloader__bar -bar3"></b>';
	          html += '<b class="preloader__bar -bar4"></b>';
	          html += '<b class="preloader__bar -bar5"></b>';
	        html += '</div>';
	    html += '</div>';
	    return html;
	},

	SameCategoryLoadDefault:function(){
		var urlAjax = jQuery("#productscategory_list_data").attr("data-ajaxurl");
		var processHtml = DORRELATED.ReloadProcess();
		if(typeof urlAjax == "undefined") return false;
		var type = 0;
		var id_product = jQuery("#productscategory_list_data").attr("data-product-id");
		var id_category = jQuery("#productscategory_list_data").attr("data-category-id");
		jQuery("#productscategory_list_data .productSameCategory-inner").css({
			"min-height":420,
			"max-height":420,
			"overflow":"hidden",
			"opacity":0,
			"visibility":"hidden"
		});
		jQuery("#productscategory_list_data .loaddingAjaxTab").remove();
		jQuery("#productscategory_list_data .graph__preloader").remove();
		jQuery("#productscategory_list_data").append(processHtml);
		var params = {}
		params.type = type;
		params.id_product = id_product;
		params.id_category = id_category;
		jQuery.ajax({
            url: urlAjax,
            data:params,
            type:"POST",
            success:function(data){
            	var results = JSON.parse(data);
            	jQuery("#productscategory_list_data .productSameCategory-wrapper").html(results);
            	setTimeout(function(){
            		DORRELATED.RelatedCarousel();
            		jQuery("#productscategory_list_data .productSameCategory-inner").css({
            			"min-height":"auto",
            			"overflow":"inherit",
						"max-height":"none",
						"opacity":1,
						"visibility":"visible"
            		});
            		jQuery("#productscategory_list_data .graph__preloader").remove();
            	},200);
            	
				if(typeof DORTHEME != "undefined" && typeof DORTHEME.ToolTipBootstrap == "function"){
					DORTHEME.ToolTipBootstrap();
				}
				if(typeof DORTHEME != "undefined" && typeof DORTHEME.DorQuantity != "undefined" && typeof DORTHEME.DorQuantity == "function"){
            		//DORTHEME.DorQuantity();
            	}
                return false;
            }
        });
	},

	SameBrandLoadDefault:function(){
		var urlAjax = jQuery("#productssamebrand_list_data").attr("data-ajaxurl");
		var processHtml = DORRELATED.ReloadProcess();
		if(typeof urlAjax == "undefined") return false;
		var type = 1;
		var id_product = jQuery("#productssamebrand_list_data").attr("data-product-id");
		var id_brand = jQuery("#productssamebrand_list_data").attr("data-brand-id");
		jQuery("#productssamebrand_list_data .productSameCategory-inner").css({
			"min-height":420,
			"max-height":420,
			"overflow":"hidden",
			"opacity":0,
			"visibility":"hidden"
		});
		jQuery("#productssamebrand_list_data .loaddingAjaxTab").remove();
		jQuery("#productssamebrand_list_data .graph__preloader").remove();
		jQuery("#productssamebrand_list_data").append(processHtml);
		var params = {}
		params.type = type;
		params.id_product = id_product;
		params.id_brand = id_brand;
		jQuery.ajax({
            url: urlAjax,
            data:params,
            type:"POST",
            success:function(data){
            	var results = JSON.parse(data);
            	jQuery("#productssamebrand_list_data .productSameCategory-wrapper").html(results);
            	setTimeout(function(){
            		DORRELATED.RelatedCarousel();
            		jQuery("#productssamebrand_list_data .productSameCategory-inner").css({
            			"min-height":"auto",
            			"overflow":"inherit",
						"max-height":"none",
						"opacity":1,
						"visibility":"visible"
            		});
            		jQuery("#productssamebrand_list_data .graph__preloader").remove();
            	},200);
            	
				if(typeof DORTHEME != "undefined" && typeof DORTHEME.ToolTipBootstrap == "function"){
					DORTHEME.ToolTipBootstrap();
				}
				if(typeof DORTHEME != "undefined" && typeof DORTHEME.DorQuantity != "undefined" && typeof DORTHEME.DorQuantity == "function"){
            		//DORTHEME.DorQuantity();
            	}
                return false;
            }
        });
	},
}
jQuery(document).ready(function(){
	DORRELATED.init();
});