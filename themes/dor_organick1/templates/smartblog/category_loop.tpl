{assign var="options" value=null}
{$options.id_post = $post.id_post}
{$options.slug = $post.link_rewrite}
<div class="single-blog">
    <div class="blog-info">
          {if $smartshowauthor ==1}
          <span class="author-name">
            <i class="fa fa-user-circle-o hidden" aria-hidden="true"></i>{l s="by"} 
              <a rel="author" href="#">
                  {if $smartshowauthorstyle != 0}
                    {$post.firstname} {$post.lastname}
                  {else}
                    {$post.lastname} {$post.firstname}
                  {/if}
              </a>
          </span>
          {/if}
          <span class="blog-date">
            <i class="fa fa-clock-o hidden" aria-hidden="true"></i>
            <span class="month-date"><small>{$post.created|date_format:"%B %d, %Y"}</small></span>
         </span>
         <span class="comments-number hidden">
              <i class="fa fa-comment"></i>{$post.totalcomment} {l s="Comment"}
          </span>
          
          {assign var="optionsCate" value=null}
          {$optionsCate.id_category = $post.id_category}
          {$optionsCate.slug = $post.cat_link_rewrite}
            <span class="post-cate">
             {l s="in"} <a href="{smartblog::GetSmartBlogLink('smartblog_category',$optionsCate)}">{$post.cat_name}</a>
            </span>
      </div>
    <div class="blog-image">
        <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}" title="{$post.meta_title}">
            <img src="{$post.thumb_image}" alt="Blog">
        </a>
    </div>
    <div class="blog-content">

        <div class="title-desc">
            <a title="{$post.meta_title}" href='{smartblog::GetSmartBlogLink('smartblog_post',$options)}'><h4>{$post.meta_title}</h4></a>
            <p>{$post.short_description|strip_tags:'UTF-8'|truncate:550:'...'}</p>
        </div>
        <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}" class="readmore">{l s="Read More"}</a>
        <div class="line-post-public">
          <span class="like-post"><i class="fa fa-heart-o" aria-hidden="true"></i><em>9</em></span>
          <span class="viewed-post"><i class="fa fa-eye" aria-hidden="true"></i><em>{$post.viewed}</em></span>
          <span class="facebook-post"><i class="fa fa-facebook" aria-hidden="true"></i><em>160</em></span>
          <span class="share-post"><i class="fa fa-share-alt" aria-hidden="true"></i></span>
        </div>
    </div>
</div>