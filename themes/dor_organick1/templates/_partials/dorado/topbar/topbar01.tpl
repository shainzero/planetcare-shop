{block name='topbar'}
	<div id="dor-topbar01" class="dor-topbar-wrapper">
		<div class="dor-topbar-inner">
			<div class="container-fluid">
				<div class="row">
					{hook h='topbarDorado1'}
					<a href="#" rel="nofollow" class="select-setting hidden pull-right"><i class="material-icons">&#xE8B8;</i></a>
				</div>
			</div>
		</div>
	</div>
{/block}